const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res)=>{
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.port || 4400;

app.listen(port, (req, res)=>{
    console.log(`node server running on ${port}`);
});
var https = require('https');

var fs = require('fs');

var https_options = {
  key: fs.readFileSync("./ssl/participant.net.key"),

  cert: fs.readFileSync("./ssl/participant.net2.crt"),
};
// https.createServer(https_options,app, function (req, res) {
//     console.log("Server running on port this");   
//    }).listen(443);