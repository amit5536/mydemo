import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';
export interface S3UploaderConfig {
    apiVersion?: string;
    region: string;
    bucket: string;
    credentials: {
        accessKeyId?: string;
        secretAccessKey?: string;
        identityPoolId?: string;
        logins?: any;
        roleArn?: string;
        roleName?: string;
        providerId?: string;
        token?: string;
    };
}
export declare const CONFIG: InjectionToken<S3UploaderConfig>;
export declare class S3UploaderService {
    private s3UploaderConfig;
    private client;
    constructor(s3UploaderConfig: S3UploaderConfig);
    authenticate(providerName: string, token: string): void;
    upload(file: File, acl?: string, bucket?: string): Observable<any>;
}
