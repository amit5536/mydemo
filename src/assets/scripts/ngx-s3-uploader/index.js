import { Component, EventEmitter, HostListener, Inject, Injectable, InjectionToken, NgModule, Output, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { CognitoIdentityCredentials, S3, WebIdentityCredentials, config } from "aws-sdk"

var CONFIG = new InjectionToken('config');
var S3UploaderService = (function () {
    /**
     * @param {?} s3UploaderConfig
     */
    function S3UploaderService(s3UploaderConfig) {
        this.s3UploaderConfig = s3UploaderConfig;
        var credentials = {
            accessKeyId: s3UploaderConfig.credentials.accessKeyId,
            secretAccessKey: s3UploaderConfig.credentials.secretAccessKey,
        };
        // if the identity pool id  is populated I use the Cognito credentials to authenticate
        if (s3UploaderConfig.credentials.identityPoolId) {
            credentials = new CognitoIdentityCredentials({
                IdentityPoolId: s3UploaderConfig.credentials.identityPoolId,
                Logins: this.s3UploaderConfig.credentials.logins || {},
            });
        }
        // if role is populated I use the Web Federated Identity to authenticate
        if (s3UploaderConfig.credentials.roleArn) {
            credentials = new WebIdentityCredentials({
                RoleArn: s3UploaderConfig.credentials.roleArn,
                RoleSessionName: s3UploaderConfig.credentials.roleName,
                ProviderId: this.s3UploaderConfig.credentials.providerId,
                WebIdentityToken: this.s3UploaderConfig.credentials.token
            });
        }
        config.region = s3UploaderConfig.region;
        config.credentials = credentials;
        this.client = new S3();
    }
    /**
     * @param {?} providerName
     * @param {?} token
     * @return {?}
     */
    S3UploaderService.prototype.authenticate = function (providerName, token) {
        if (config.credentials instanceof CognitoIdentityCredentials) {
            config.credentials['params']['Logins'] = {};
            config.credentials['params']['Logins'][providerName] = token;
            // Expire credentials to refresh them on the next request.
            config.credentials['expired'] = true;
        }
    };
    /**
     * @param {?} file
     * @param {?=} acl
     * @param {?=} bucket
     * @return {?}
     */
    S3UploaderService.prototype.upload = function (file, acl, bucket) {
        var _this = this;
        if (acl === void 0) { acl = 'public-read'; }
        return Observable.create(function (observer) {
            _this.client.upload({ Key: acl, Body: file, Bucket: bucket || _this.s3UploaderConfig.bucket }, function (error, data) {
                if (error) {
                    return observer.error(error);
                }
                observer.next(data);
                observer.complete();
            });
        });
    };
    return S3UploaderService;
}());
S3UploaderService.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
S3UploaderService.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] },] },
]; };

var S3UploaderComponent = (function () {
    /**
     * @param {?} s3UploaderService
     */
    function S3UploaderComponent(s3UploaderService) {
        this.s3UploaderService = s3UploaderService;
        this.success = new EventEmitter();
        this.error = new EventEmitter();
    }
    /**
     * @return {?}
     */
    S3UploaderComponent.prototype.onClick = function () {
        var /** @type {?} */ event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    /**
     * @return {?}
     */
    S3UploaderComponent.prototype.onChange = function () {
        var _this = this;
        var /** @type {?} */ file = this.fileInput.nativeElement.files[0];
        this.s3UploaderService.upload(file)
            .subscribe(function (data) {
            _this.success.next(data);
        }, function (error) {
            _this.error.next(error);
        });
    };
    return S3UploaderComponent;
}());
S3UploaderComponent.decorators = [
    { type: Component, args: [{
                selector: 's3-uploader',
                template: "\n        <input #fileInput style='display: none;' type='file' (change)='onChange()'>\n        <ng-content></ng-content>\n    ",
            },] },
];
/**
 * @nocollapse
 */
S3UploaderComponent.ctorParameters = function () { return [
    { type: S3UploaderService, },
]; };
S3UploaderComponent.propDecorators = {
    'success': [{ type: Output },],
    'error': [{ type: Output },],
    'fileInput': [{ type: ViewChild, args: ['fileInput',] },],
    'onClick': [{ type: HostListener, args: ['click',] },],
};

var S3UploaderModule = (function () {
    function S3UploaderModule() {
    }
    /**
     * @param {?} config
     * @return {?}
     */
    S3UploaderModule.forRoot = function (config$$1) {
        return {
            ngModule: S3UploaderModule,
            providers: [
                { provide: CONFIG, useValue: config$$1 },
                S3UploaderService
            ]
        };
    };
    return S3UploaderModule;
}());
S3UploaderModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    S3UploaderComponent,
                ],
                exports: [
                    S3UploaderComponent,
                ]
            },] },
];
/**
 * @nocollapse
 */
S3UploaderModule.ctorParameters = function () { return []; };

export { S3UploaderModule, S3UploaderComponent, CONFIG, S3UploaderService };
