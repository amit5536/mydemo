import { ModuleWithProviders } from '@angular/core';
import { S3UploaderConfig } from './s3-uploader.service';
export * from './s3-uploader.component';
export * from './s3-uploader.service';
export declare class S3UploaderModule {
    static forRoot(config: S3UploaderConfig): ModuleWithProviders;
}
