import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {
  private subject = new Subject<any>();

  constructor() { }

  update(key: string, value: any) {
    // if (typeof value != 'string') {
    //   value = JSON.stringify(value);
    // }
  
    localStorage.setItem(key, value);
    this.subject.next();
  }


  // getMessage is method to take message from component
  onUpdate(): Observable<any> {
    return this.subject.asObservable();
  }
}
