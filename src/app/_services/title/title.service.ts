import { Injectable } from '@angular/core';
import { getDOM } from '@angular/platform-browser/src/dom/dom_adapter';
import { Title } from '@angular/platform-browser';

@Injectable()
export class TitleService {
    constructor(private titleService: Title) { }

    getTitle(): string {
        return this.titleService.getTitle();
    }

    setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }
}
