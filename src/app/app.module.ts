import { AuthGuard } from "./auth/gaurd/auth.guard";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { DashboardSectionComponent } from "./dashboard-section/dashboard-section.component";

import { DataGridComponent } from "./data-grid/data-grid.component";
import { MemoComponent } from "./memo/memo.component";
import { SearchBarComponent } from "./search-bar/search-bar.component";
import { ProjectMemoFormComponent } from "./project-memo-form/project-memo-form.component";
import { TitleService } from "./_services/title/title.service";
import { UserListComponent } from "./user-list/user-list.component";
import { GridComponent } from "./_shared/grid/grid.component";
import { SharedModule } from "./_shared/shared.module";
import { AlertService } from "src/app/_shared/alert/alert.service";
import { S3UploaderModule } from "src/assets/scripts/ngx-s3-uploader";
import { LoaderService } from "./_shared/main-loader/loader.service";
import { ConfirmationDialogueService } from "./_shared/confirmation-dialogue/confirmation-dialogue.service";
import { SharedServiceService } from './_services/shared-service.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardSectionComponent,
    SearchBarComponent,
    ProjectMemoFormComponent,
    DataGridComponent,
    MemoComponent,
    UserListComponent,
    GridComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    S3UploaderModule.forRoot({
      region: "US-West-Oregon",
      bucket: "participant-media-files",
      credentials: {
        accessKeyId: "AKIAJTDLYI34ZN5SNNTQ",
        secretAccessKey: "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"
      }
    })
  ],
  providers: [
    TitleService,
    SharedServiceService,
    AlertService,
    LoaderService,
    AuthGuard,
    ConfirmationDialogueService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
