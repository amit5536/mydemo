import { Component, HostListener } from "@angular/core";
import { TitleService } from "./_services/title/title.service";
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
import { AlertService } from "./_shared/alert/alert.service";
import { HttpClient } from '@angular/common/http';
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor(
    private http: HttpClient,
    private titleService: TitleService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const currentUrl = event.url;
        const title = this.getTitle(currentUrl);
        titleService.setTitle(title);
      }
    });
  }

  pageTitle: any = {
    "/login": ""
  };
  @HostListener('window:scroll', [])
  onWindowScroll() {
   const aa = document.body.offsetHeight;
  }
  // collect that title data properties from all child routes
  // there might be a better way but this worked for me
  getTitle(currentUrl: string) {
    if (currentUrl.includes("/login")) {
      return "Login - Participant Media";
    } else if (currentUrl.includes("/forgot")) {
      return "Request Password Reset - Participant Media";
    } else if (currentUrl.includes("/reset")) {
      return "Password Reset - Participant Media";
    } else if (currentUrl.includes("dashboard")) {
      return "Dashboard - Participant Media";
    } else if (currentUrl.includes("users")) {
      return "Users - Participant Media";
    }  else if (currentUrl.includes("404")) {
      return "404 - Participant Media";
    } else if (currentUrl.includes("projects/")) {
      const urlParts = currentUrl.split("/");
      if (urlParts.length > 2) {
        return urlParts[2] + " - Participant Media";
      }
    } else if (currentUrl.includes("addendum/")) {
      const urlParts = currentUrl.split("/");
      if (urlParts.length > 2) {
        return urlParts[2] + " - Participant Media";
      }
    }

    return "Participant Media";
  }

  alert() {
    this.alertService.error("Some Message");
  }
}
