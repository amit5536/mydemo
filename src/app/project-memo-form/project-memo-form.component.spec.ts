import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMemoFormComponent } from './project-memo-form.component';

describe('ProjectMemoFormComponent', () => {
  let component: ProjectMemoFormComponent;
  let fixture: ComponentFixture<ProjectMemoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMemoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMemoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
