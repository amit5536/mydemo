import { EventEmitter, Input } from "@angular/core";
import { Component, OnInit, Output } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { UserAuthHelper } from "../_utility/user_auth.helper";
import { LoaderService } from "../_shared/main-loader/loader.service";
import { AlertService } from "../_shared/alert/alert.service";
import { ProjectService } from "../dashboard/_service/project.service";

const userHeading = `No matches found.`;
const userMessage = `Your filters and/or search parameters didn’t turn up
  any matches. Make some adjustments or reset your filters to try again.`;
const suDefaultHeading = `Welcome!`;
const suDefaultMessage = `Get started by creating a Project Memo`;
const suFilterHeading = `No matches found.`;
// tslint:disable-next-line:max-line-length
const suFilterMessage = `Your filters and/or search parameters didn’t turn up any matches. Make some adjustments or reset your filters to try again.`;
const baDefaultHeading = `Welcome!`;
// tslint:disable-next-line:max-line-length
const baDefaultMessage = `There are no unassigned documents needing your attention and none assigned to you pending submission to Rightsline.`;
const baFilterHeading = `No matches found.`;
const baFilterMessage = `Your filters and/or search parameters
didn’t turn up any matches. Make some adjustments or reset your filters to try again.`;

@Component({
  selector: "app-welcome-page",
  templateUrl: "./welcome-page.component.html",
  styleUrls: ["./welcome-page.component.css"]
})
export class WelcomePageComponent implements OnInit {
  private userRole = "";
  private currentUrl = "";
  @Output() clear = new EventEmitter();
  @Input() hasFilters: any;
  message = `Your filters and/or search parameters didn’t turn up any matches.
                      Make some adjustments or reset your filters to try again`;
  heading = "Welcome !";
  showAddButton = false;
  constructor(
    private router: Router,
    private loader: LoaderService,
    private alert: AlertService,
    private service: ProjectService
  ) {}

  ngOnInit(): void {
    const user = UserAuthHelper.GetProfile();
    this.userRole = user ? user.type : "";
    this.currentUrl = document.location.href;
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
        this.setMessage();
      }
    });
    this.setMessage();
  }

  resetFilters() {
    this.clear.emit();
  }

  setMessage() {
    if (this.currentUrl.includes("user")) {
      this.heading = userHeading;
      this.message = userMessage;
    } else if (this.currentUrl.includes("dashboard")) {
      if (this.userRole.includes("Standard")) {
        if (this.hasFilters) {
          this.heading = suFilterHeading;
          this.message = suFilterMessage;
        } else {
          this.heading = suDefaultHeading;
          this.message = suDefaultMessage;
          this.showAddButton = true;
        }
      } else {
        if (this.hasFilters) {
          this.heading = baFilterHeading;
          this.message = baFilterMessage;
        } else {
          this.heading = baDefaultHeading;
          this.message = baDefaultMessage;
        }
      }
    }
  }

  addProjects() {
    this.loader.show();
    this.service.generateProjectId().subscribe(
      (res: any) => {
        if (res.data) {
          const projectId = res.data;
          const url = "/projects/" + projectId;
          this.loader.hide();
          this.router.navigate([url]);
        }
      },
      (error: any) => {
        this.loader.hide();
        this.alert.error(error.error.details);
      }
    );
  }
}
