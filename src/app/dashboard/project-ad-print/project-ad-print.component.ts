import { AddendumFormModel } from "./../../_models/projects.model";
import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProjectService } from "../_service/project.service";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { LoaderService } from "../../_shared/main-loader/loader.service";
import { timer } from "rxjs/observable/timer";
import { fromEvent } from "rxjs";
import { map, debounceTime } from "rxjs/operators";

@Component({
  selector: "app-project-ad-print",
  templateUrl: "./project-ad-print.component.html",
  styleUrls: ["./project-ad-print.component.css"]
})
export class ProjectAdPrintComponent implements OnInit, OnDestroy {
  @ViewChild("table") table: any;
  addendumId: any;
  addendum = new AddendumFormModel();
  compLoaded: boolean;
  userRole: any;
  private timer = timer(1000); // wait one second before calling the method
  private subscription: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: ProjectService,
    private loader: LoaderService
  ) {}

  ngOnInit() {
    this.loader.show();
    this.userRole = UserAuthHelper.GetProfile().type;
    this.activatedRoute.params.subscribe(res => {
      this.addendumId = res.id;
      this.loadDetails();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  loadDetails() {
    this.compLoaded = true;
    this.service.loadAddendumDetails(this.addendumId).subscribe(
      (res: any) => {
        this.addendum = res.data;
        setTimeout(() => {
          window.print();
          this.loader.hide();
        }, 1000);
      },
      (error: any) => {
        this.loader.hide();
        window.close();
      }
    );
  }
  alert(e) {}
}
