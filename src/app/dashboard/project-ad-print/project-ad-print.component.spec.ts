import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectAdPrintComponent } from './project-ad-print.component';

describe('ProjectAdPrintComponent', () => {
  let component: ProjectAdPrintComponent;
  let fixture: ComponentFixture<ProjectAdPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectAdPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectAdPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
