import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ProjectService } from "../_service/project.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../_shared/main-loader/loader.service";

declare var $: any;

@Component({
  selector: "app-addendum-modal",
  templateUrl: "./addendum-modal.component.html",
  styleUrls: ["./addendum-modal.component.css"]
})
export class AddendumModalComponent implements OnInit {
  projects: ProjectListModel[] = [];
  selectedProject: any = null;
  name: any;
  lastSearch: string;
  subscription: any;
  projectsList: ProjectListModel[] = [];
  notfirst: any;
  constructor(
    private service: ProjectService,
    private router: Router,
    private loader: LoaderService
  ) {}

  ngOnInit() {
    this.projects = [];
    this.SearchProjects("");
  }

  FilterProject(value: string) {
    if (this.lastSearch !== value) {
      this.lastSearch = value;
      const lastvalue = value.toLocaleLowerCase();
      this.projects = this.projectsList.filter(
        x =>
          x.catalog.toLocaleLowerCase().includes(lastvalue) ||
          x.contractorCompanyName.toLocaleLowerCase().includes(lastvalue) ||
          x.contractorContactName.toLocaleLowerCase().includes(lastvalue) ||
          x.createdAt
            .toString()
            .toLocaleLowerCase()
            .includes(lastvalue) ||
          x.pmID.toLocaleLowerCase().includes(lastvalue) ||
          x.projectName.toLocaleLowerCase().includes(lastvalue)
      );
    }
  }

  SearchProjects(value: string) {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.service
      .seachProject(value)
      .subscribe((res: any) => {
        this.projectsList = res.data;
        this.FilterProject("");
      });
  }

  projectSelected(option: any) {
    this.selectedProject = this.projects.find(x => x.pmID === option.value);
    if (this.selectedProject) {
      this.name =
        this.selectedProject.contractorCompanyName +
        " on " +
        this.selectedProject.projectName;
    }
  }

  countinue() {
    this.loader.show();
    this.service
      .getAddendumId(
        this.selectedProject.pmID,
        this.selectedProject.projectName
      )
      .subscribe(
        (res: any) => {
          this.clear();
          const url = "/addendum/" + res.data;
          this.loader.hide();
          this.router.navigate([url]);
          $("#addendumModal").modal("hide");
        },
        error => {
          this.loader.hide();
        }
      );
  }

  viewProject() {
    const webpage = location.origin + "/projects/" + this.selectedProject.pmID;
    const mydiv = document.getElementById("myDiv1");
    const aTag = document.createElement("a");
    aTag.setAttribute("href", webpage);
    aTag.setAttribute("target", "_blank");
    aTag.setAttribute("id", "dumLink");
    aTag.setAttribute("rel", "noopener");
    aTag.innerHTML = "link text";
    mydiv.appendChild(aTag);
    document.getElementById("dumLink").click();
  }

  clear() {
    if (this.name) {
      this.selectedProject = null;
      this.name = "";
    }
  }
  displayFn(val) {
    return  val;
  }

  validateEntries() {
    if (this.name) {
      this.name = this.name.trim();
      if (
        this.name &&
        this.projects.filter(
          x =>
            x.contractorCompanyName ===
            this.selectedProject.contractorCompanyName +
              " on " +
              this.selectedProject.projectName
        ).length !== 1
      ) {
        this.name = "";
        this.selectedProject = null;
      }
    }
  }
  mouseEntered(event: any) {
    if (!this.notfirst) {
      this.notfirst = true;
      event.target.blur();
      setTimeout(() => {
        event.target.focus();
      }, 10);
    }
  }
}

export class ProjectListModel {
  catalog: string;
  contractorCompanyName: string;
  contractorContactName: string;
  createdAt: string;
  pmID: string;
  projectName: string;
}
