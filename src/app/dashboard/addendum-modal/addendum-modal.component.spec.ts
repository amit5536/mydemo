import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddendumModalComponent } from './addendum-modal.component';

describe('AddendumModalComponent', () => {
  let component: AddendumModalComponent;
  let fixture: ComponentFixture<AddendumModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddendumModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendumModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
