import { UserModel } from "src/app/_models/user-list.model";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { URLS } from "../../_models/_config";
import { UsersFilterModel } from "../../_models/users-filter.model";
import { StringHelper } from 'src/app/_utility/string.helper';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) {}

  updatePassword({ password, email, rightline_id }: { password: string; email: string; rightline_id: string; }): any {
    const url = URLS.updatePassword;
    const httpOptions = {
      headers: new HttpHeaders({
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.put(url, { password, email, rightline_id }, httpOptions);
  }

  updateUser(user: UserModel) {
    let body: any = Object.assign({}, user);
    body = StringHelper.RemoveRedundantSpacesFromObject(body);
    const url = URLS.updateUser;
    const httpOptions = {
      headers: new HttpHeaders({
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.put(url, body, httpOptions);
  }

  addUser(user: UserModel) {
    let body: any = Object.assign({}, user);
    body = StringHelper.RemoveRedundantSpacesFromObject(body);
    body.host = document.location.origin + "/reset/";
    const url = URLS.addUser;
    const httpOptions = {
      headers: new HttpHeaders({
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.post(url, body, httpOptions);
  }

  getUsers(requestModel: UsersFilterModel) {
    const url = URLS.getUsers;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.post(url, requestModel, httpOptions);
  }

  deleteUser(user: UserModel): any {
    const url = URLS.deleteUser + user.id;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.delete(url, httpOptions);
  }

  myProfile() {
    const url = URLS.getuserprofile;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  async getCountryCodeList() {
    const response = await this.http.get(URLS.getCountryCodeList).toPromise();
    return this.augmentPhoneCodeList(response);
}

augmentPhoneCodeList(countryList): Array<any> {
  const countryCodeOptions = [];
  let countryCodeObj;
  for (const countryObj of countryList) {
      countryCodeObj = {
          label: `(+${countryObj.callingCodes[0]}) ${countryObj.name}`,
          value: `+${countryObj.callingCodes[0]} `,
          flag: countryObj.flag,
      };
      countryCodeOptions.push(countryCodeObj);
  }
  return countryCodeOptions;
}
}
