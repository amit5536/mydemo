import { Injectable } from "@angular/core";
import {
  ProjectFilterModel,
  ProjectFormModel,
  AddendumFormModel
} from "src/app/_models/projects.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { URLS } from "src/app/_models/_config";
import { StringHelper } from "src/app/_utility/string.helper";

@Injectable({
  providedIn: "root"
})
export class ProjectService {
  constructor(private http: HttpClient) {}

  getProjects(filters: ProjectFilterModel) {
    const url = URLS.getProjects;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.post(url, filters, httpOptions);
  }

  generateProjectId(): any {
    const url = URLS.generateProjectId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  getProjectsDetails(projectId: string): any {
    const url = URLS.getProjectDetails + projectId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  SaveProgress(projectId: string, project: ProjectFormModel): any {
    project.contractorContactPhone = project.contractorContactPhone.replace(/[&\/\\#,+()$~%.'":*?<>{}" "-]/g, '');
    const url = URLS.updateProject + projectId;
    let body: any = Object.assign({}, project);
    body = StringHelper.RemoveRedundantSpacesFromObject(body);
    if (!body.attachment) {
      delete body.attachment;
    }
    delete body.mailList;
    delete body.from;
    body.cc = project.mailList.map(x => x.email).join(",");
    body.pmURL = location.href;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.put(url, body, httpOptions);
  }

  loadUsers(): any {
    const url = URLS.getUsersList;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  getCatalogList(): any {
    const url = URLS.getCatalogList;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  getPeoplesEmail(cc: string): any {
    const url = URLS.searchEmail + cc;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  GetContractorsList(): any {
    const url = URLS.getContactList;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  GetRightLineUserList(): any {
    const url = URLS.getRightLineUserList;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  SubmitProjectStandardUser(projectId: string): any {
    const url = URLS.submitProjectSU + projectId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.put(url, {}, httpOptions);
  }

  deleteProjectMemo(projectId: string): any {
    const url = URLS.deleteProjectMemo + projectId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.delete(url, httpOptions);
  }

  deleteAddendum(projectId: string): any {
    const url = URLS.deleteAddendum + projectId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.delete(url, httpOptions);
  }

  seachProject(value: string) {
    const url = URLS.searchProject + value;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  getAddendumId(pmID: any, parentProjectMemo: any) {
    const url = URLS.createUpdateAddendum;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    const body = { parentProjectMemo: pmID };
    return this.http.put(url, body, httpOptions);
  }

  loadAddendumDetails(addendumId: string) {
    const url = URLS.getAddendumDetails + addendumId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.get(url, httpOptions);
  }

  updateAddendum(addendum: AddendumFormModel) {
    const url = URLS.createUpdateAddendum;
    let body: any = Object.assign({}, addendum);
    delete body.mailList;
    body = StringHelper.RemoveRedundantSpacesFromObject(body);
    body.cc = addendum.mailList.map(x => x.email).join(",");
    body.adURL = location.href;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.put(url, body, httpOptions);
  }

  SubmitAddendumSU(addendumId: string) {
    const url = URLS.submitAddendumSU + addendumId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    const body = {};
    return this.http.put(url, body, httpOptions);
  }

  SubmitAddendumBA(addendumId: string) {
    const url = URLS.submitAddendumBA + addendumId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    const body = {};
    return this.http.put(url, body, httpOptions);
  }

  SubmitProjectBA(projectId: string) {
    const url = URLS.submitProjectBA + projectId;
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        auth_token: localStorage.getItem("token")
      })
    };
    return this.http.put(url, {}, httpOptions);
  }
}
