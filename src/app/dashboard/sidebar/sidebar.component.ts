import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { ProjectService } from "../_service/project.service";
import { AlertService } from "../../_shared/alert/alert.service";
import { LoaderService } from "../../_shared/main-loader/loader.service";
import { HardReload } from "src/app/_utility/hardreload.helper";
declare var $: any;

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  open = true;
  showUsers = false;
  constructor(
    private router: Router,
    private service: ProjectService,
    private alert: AlertService,
    private loader: LoaderService
  ) {}

  ngOnInit() {
    const role = UserAuthHelper.GetProfile().type;
    this.showUsers = role === "Business Affairs" || role === "Super Admin";

    // $(window).on("scroll", () => {
    //   const scrollVal = $(window).scrollTop();
    //   if (scrollVal <= 50) {
    //     $(".sidebar-menu").css("padding-top", 50 - scrollVal + "px");
    //   } else {
    //     $(".sidebar-menu").css("padding-top", "0");
    //   }
    // });
  }

  addendum() {
    $("#addendumModal").modal("show");
  }

  generateNewProject() {
    this.loader.show();
    this.service.generateProjectId().subscribe(
      (res: any) => {
        if (res.data) {
          const projectId = res.data;
          const url = "/projects/" + projectId;
          this.loader.hide();
          HardReload.redirectTo(url, this.router);
        }
      },
      (error: any) => {
        this.loader.hide();
        this.alert.error(error.error.details);
      }
    );
  }

  newproject() {
    this.generateNewProject();
  }

  toggel() {
    // this.open = !this.open;
    // document.getElementById('mydashboard').classList.toggle("dashboard-close");
  }

  isActive(instruction: any[]): boolean {
    // Set the second parameter to true if you want to require an exact match.
    return location.href.includes(instruction[0]);
  }

  dashboard() {
    if (location.href.includes("/dashboard")) {
      HardReload.redirectTo("/", this.router);
    } else {
      this.router.navigate(["/dashboard"]);
    }
  }

  usersList() {
    if (location.href.includes("/users")) {
      HardReload.redirectTo("/users", this.router);
    } else {
      this.router.navigate(["/users"]);
    }
  }
}
