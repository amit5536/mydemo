import { Component, OnInit, ViewChild } from "@angular/core";
import {
  ProjectFilterModel,
  ProjectListModel
} from "src/app/_models/projects.model";
import { ProjectService } from "../_service/project.service";
import $ from "jquery";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { Router, ActivatedRoute } from "@angular/router";
import { LoaderService } from "../../_shared/main-loader/loader.service";
import { AlertService } from "../../_shared/alert/alert.service";
import { Subscription } from "rxjs";
import { Location, JsonPipe } from "@angular/common";

declare var $: any;

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.css"]
})
export class ProjectsComponent implements OnInit {
  @ViewChild("firstSelect") firstSelect: any;
  @ViewChild("secondSelect") secondSelect: any;
  @ViewChild("thirdSelect") thirdSelect: any;
  // @ViewChildren(MatCheckbox) matCheckbox: QueryList<MatCheckbox>;

  filters: ProjectFilterModel = new ProjectFilterModel();
  List: ProjectListModel[] = [];
  totalUsers: number;
  displayForm = false;
  isProfile = false;
  userTypes = [];
  usersStatus = [];
  assignmentTypes = [];
  // user: UserModel = new UserModel();
  pageIndex = 0;
  userRole = "";
  loading = true;
  statusLength = 4;
  subscription: Subscription;
  loadingFilters: boolean;
  isTestSearchActive = false;
  queryParam: any;
  first: boolean;
  constructor(
    private service: ProjectService,
    private router: Router,
    private loader: LoaderService,
    private alert: AlertService,
  ) {}

  ngOnInit() {
    // alert('This screen is still pending please ignore');
    this.userRole = UserAuthHelper.GetProfile().type;
    this.first = true;
    this.setFilters();
  }

  setFilters() {
    this.userTypes = [
      { selected: true, value: "PM", display: "PM" },
      { selected: true, value: "AD", display: "AD" }
    ];
    this.assignmentTypes = [
      { selected: true, value: "Assigned to Me", display: "Assigned to Me" },
      {
        selected: true,
        value: "Assigned to Others",
        display: "Assigned to Others"
      },
      { selected: true, value: "Unassigned", display: "Unassigned" }
    ];
    this.filters = new ProjectFilterModel();
    if (this.userRole === "Standard") {
      this.usersStatus = [
        { selected: true, value: "Draft", display: "Draft" },
        { selected: true, value: "Submitted", display: "Submitted" },
        { selected: true, value: "Assigned", display: "In Review" },
        { selected: false, value: "Accepted", display: "Accepted" }
      ];
      this.statusLength = 3;
      this.filters.sort = "createdAt";
    } else {
      this.usersStatus = [
        { selected: false, value: "Draft", display: "Draft" },
        { selected: true, value: "Submitted", display: "Needs Pickup" },
        { selected: true, value: "Assigned", display: "Assigned" },
        { selected: false, value: "Accepted", display: "Accepted" }
      ];
      this.filters.sort = "submittedAt";
      this.statusLength = 2;
    }
    this.setFilter();
  }

  loadProjects() {
    if (
      !(
        this.filters.type.length === 0 ||
        this.filters.status.length === 0 ||
        this.filters.assignment.length === 0
      )
    ) {
      this.loading = true;
      this.List = [];
      this.totalUsers = 0;
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      const url = "/dashboard";
      localStorage.setItem('queryParam', JSON.stringify(this.filters));
      this.subscription = this.service.getProjects(this.filters).subscribe(
        (res: any) => {
          this.totalUsers = res.data.totalCount;
          this.List = res.data.result;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
    }
  }

  sort(key: string) {
    if (this.filters.sort === key) {
      this.filters.order = this.filters.order === "desc" ? "asc" : "desc";
    } else {
      this.filters.order = "desc";
    }
    this.filters.sort = key;
    this.loadProjects();
  }

  pageChanged(event: any) {
    this.filters.limit = event.pageSize;
    this.filters.offset = event.pageIndex * event.pageSize;
    this.pageIndex = event.pageIndex;
    this.loadProjects();
  }

  search() {
    // this.filters.limit = 10;
    this.filters.offset = 0;
    this.pageIndex = 0;
    if (this.filters.searchText) {
      this.isTestSearchActive = true;
    } else {
      this.isTestSearchActive = false;
    }
    this.loadProjects();
  }

  setFilter() {
      // this.queryParam = Object.assign(
      //   this.activatedRouter.snapshot.queryParams
      // );
      // this.queryParam = JSON.parse(JSON.stringify(this.queryParam));
      this.loadingFilters = true;
      if (this.first && localStorage.getItem('queryParam')) {
        this.queryParam = localStorage.getItem('queryParam');
        this.filters = JSON.parse(this.queryParam);
        this.assignmentTypes.forEach(ele => {
          if (this.filters.assignment.includes(ele.value)) {
            ele.selected = true;
          } else {
            ele.selected = false;
          }
        });
        this.usersStatus.forEach(ele => {
          if (this.filters.status.includes(ele.value)) {
            ele.selected = true;
          } else {
            ele.selected = false;
          }
        });
        this.userTypes.forEach(ele => {
          if (this.filters.type.includes(ele.value)) {
            ele.selected = true;
          } else {
            ele.selected = false;
          }
        });
        if (this.filters.searchText) {
          this.isTestSearchActive = true;
        }
        const pageIndex =  (this.filters.offset / this.filters.limit);
        this.pageIndex = pageIndex;
        this.loadingFilters = false;
        this.loadProjects();
        this.first = false;
      } else {
      this.loadingFilters = true;
      this.first = false;
      setTimeout(() => {
        this.filters.type = this.userTypes
          .filter(x => x.selected)
          .map(x => x.value);
        this.filters.assignment = this.assignmentTypes
          .filter(x => x.selected)
          .map(x => x.value);
        this.filters.status = this.usersStatus
          .filter(x => x.selected)
          .map(x => x.value);
        this.loadingFilters = false;
        this.search();
      }, 200);
    }
  }

  reset() {
    this.setFilters();
    this.loadProjects();
  }

  goto(user: any) {
    let url = "";
    if (user.type === "Project Memo") {
      url = "/projects/" + user.projectId;
    } else if (user.type === "Addendum") {
      url = "addendum/" + user.projectId;
    }
    this.router.navigate([url]);
  }

  openModel() {
    setTimeout(() => {
      $("#userModal").modal("show");
    }, 300);
  }

  ModelClosed(val: boolean) {
    $("#userModal").modal("hide");
    setTimeout(() => {
      this.displayForm = false;
    }, 200);
    if (val) {
      this.search();
    }
  }

  addAddendum() {
    $("#addendumModal").modal("show");
  }

  generateNewProject() {
    this.loader.show();
    this.service.generateProjectId().subscribe(
      (res: any) => {
        if (res.data) {
          const projectId = res.data;
          const url = "/projects/" + projectId;
          this.loader.hide();
          this.router.navigate([url]);
        }
      },
      (error: any) => {
        this.loader.hide();
        this.alert.error(error.error.details);
      }
    );
  }

  newproject() {
    this.generateNewProject();
  }

  closeDD() {
    this.firstSelect.close();
    this.secondSelect.close();
    if (this.thirdSelect) {
      this.thirdSelect.close();
    }
  }


}
