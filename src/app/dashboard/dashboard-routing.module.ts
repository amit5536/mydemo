import { HomeComponent } from "./home/home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UsersDashboardComponent } from "./users/users-dashboard/users-dashboard.component";
import { DashboardComponent } from "./component/dashboard.component";
import { ProjectsComponent } from "./projects/projects.component";
// import { PageNotFoundComponent } from "../_shared/page-not-found/page-not-found.component";
import { AddendumFormComponent } from "./addendum-form/addendum-form.component";
import { ConfirmDeactivateGuard } from './_gaurd/confirm.deactivate.gaurd';
import { ProjectFormComponent } from './project-form/project-form.component';
import { ProjectMemoPrintComponent } from './project-memo-print/project-memo-print.component';
import { ProjectAdPrintComponent } from './project-ad-print/project-ad-print.component';

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    children: [
      { path: "", redirectTo: "dashboard", pathMatch: "full" },
      { path: "dashboard", component: ProjectsComponent },
      { path: "users", component: UsersDashboardComponent },
      { path: "users/:token", component: UsersDashboardComponent },
      { path: "projects/:projectId", component: ProjectFormComponent, canDeactivate: [ConfirmDeactivateGuard] },
      { path: "addendum/:id", component: AddendumFormComponent, canDeactivate: [ConfirmDeactivateGuard] },
      { path: "home", component: HomeComponent }
      // { path: "**", component: PageNotFoundComponent }
    ]
  },
  { path: "projects/:projectId/print", component: ProjectMemoPrintComponent },
  { path: "addendum/:id/print", component: ProjectAdPrintComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardRoutingModule { }

