import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ProjectFormModel } from "src/app/_models/projects.model";
import { UserAuthHelper } from 'src/app/_utility/user_auth.helper';

@Component({
  selector: "app-print-memo",
  templateUrl: "./print-memo.component.html",
  styleUrls: ["./print-memo.component.css"]
})
export class PrintMemoComponent implements OnInit {
  @Input() project: ProjectFormModel;
  @Input() projectId: number;
  @Input() codeSelected;
  @Output() downLoadFiles = new EventEmitter<string>();
  userRole: any;
  today = new Date();

  constructor() {}

  ngOnInit() {
  }
  print() {
    UserAuthHelper.PrintPage();
  }

  download() {
    this.downLoadFiles.emit("nae");
  }
}
