import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { LoaderService } from "../../_shared/main-loader/loader.service";
import { ProjectService } from "../_service/project.service";
import { AlertService } from "../../_shared/alert/alert.service";
import { ProjectFormModel } from "src/app/_models/projects.model";

@Component({
  selector: "app-project-memo-print",
  templateUrl: "./project-memo-print.component.html",
  styleUrls: ["./project-memo-print.component.css"]
})
export class ProjectMemoPrintComponent implements OnInit, AfterViewInit {
  projectId: any;
  project: ProjectFormModel = new ProjectFormModel();
  compLoaded: boolean;
  today = new Date();
  i = 0;
  showHeading = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private loader: LoaderService,
    private service: ProjectService,
    private alert: AlertService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      if (res && res.projectId) {
        this.projectId = res.projectId;
        this.loadDetails();
      } else {
        window.close();
      }
    });
  }

  ngAfterViewInit(): void {}

  loadDetails() {
    this.loader.show();
    if (this.projectId) {
      this.service.getProjectsDetails(this.projectId).subscribe(
        (res: any) => {
          this.project = res.data;
          this.compLoaded = true;
          this.loader.hide();
          if (
            this.project.contractorCompanyName &&
            this.project.catalog &&
            this.project.from
          ) {
            this.showHeading = true;
          }
          setTimeout(() => {
            window.print();
          }, 500);
        },
        (error: any) => {
          this.loader.hide();
          this.compLoaded = true;
          this.alert.error(error.error.details, true);
          setTimeout(() => {
            window.close();
          }, 500);
        }
      );
    }
  }

  value() {
    this.i++;
    return this.i;
  }
}
