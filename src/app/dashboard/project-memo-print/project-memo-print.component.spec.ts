import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMemoPrintComponent } from './project-memo-print.component';

describe('ProjectMemoPrintComponent', () => {
  let component: ProjectMemoPrintComponent;
  let fixture: ComponentFixture<ProjectMemoPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMemoPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMemoPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
