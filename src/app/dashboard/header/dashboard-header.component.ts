import { UsersService } from "src/app/dashboard/_service/users.service";
import { Component, OnInit } from "@angular/core";
import { UserModel } from "src/app/_models/user-list.model";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { DashboardHeaderService } from "./dashboard-header.service";
import { HardReload } from '../../_utility/hardreload.helper';

declare var $: any;

@Component({
  selector: "app-dashboard-header",
  templateUrl: "./dashboard-header.component.html",
  styleUrls: ["./dashboard-header.component.css"]
})
export class DashboardHeaderComponent implements OnInit {
  username = "";
  displayForm: boolean;
  user = new UserModel();
  header: any;
  userRole: any;
  constructor(
    private userService: UsersService,
    private router: Router,
    private headerService: DashboardHeaderService
  ) {}

  ngOnInit() {
    this.header = this.getTitle(location.href);
    const profile = UserAuthHelper.GetProfile();
    this.username = profile.fullName;
    this.userRole = profile.type;
    this.header = this.getTitle(location.href);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const currentUrl = event.url;
        this.header = this.getTitle(currentUrl);
      }
    });
    this.headerService.getTitle().subscribe(res => {
      if (res) {
        this.header = res;
      }
    });
  }
  getTitle(currentUrl: string): any {
    if (currentUrl.includes("user")) {
      return "Users";
    } else if (currentUrl.includes("projects/")) {
      const urlParts = currentUrl.split("/");
      if (urlParts.length > 4) {
        return "Project Memo " + urlParts[4];
      }
    } else if (currentUrl.includes("addendum/")) {
      const urlParts = currentUrl.split("/");
      if (urlParts.length > 4) {
        return "Addendum " + urlParts[4];
      }
    } else {
      return "Dashboard";
    }
  }

  myProfile() {
    this.userService.myProfile().subscribe(
      (res: any) => {
        this.user = res.data;
        this.user.password = "";
        this.displayForm = true;
        this.openModel();
      },
      error => {}
    );
  }

  openModel() {
    setTimeout(() => {
      $("#userProfile").modal("show");
    }, 100);
  }

  logout() {
    localStorage.clear();
    this.router.navigate(["login"]);
  }

  ModelClosed(val: boolean) {
    $("#userProfile").modal("hide");
    setTimeout(() => {
      this.displayForm = false;
    }, 200);
  }

  gotoDashboard() {
    HardReload.redirectTo("/", this.router);
  }

}
