import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { Router } from "@angular/router";
import { HardReload } from "../../_utility/hardreload.helper";

@Injectable()
export class DashboardHeaderService {
  constructor(private router: Router) {}
  private subject = new Subject<any>();

  setTitle(title: string) {
    this.subject.next(title);
  }

  getTitle(): Observable<boolean> {
    return this.subject.asObservable();
  }

}
