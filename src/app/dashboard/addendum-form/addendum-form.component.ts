import { AlertService } from "./../../_shared/alert/alert.service";
import {
  AddendumFormModel,
  AssignToModel,
  CCListModel,
  AttachmentListExtModel
} from "./../../_models/projects.model";
import { ActivatedRoute, Router } from "@angular/router";
import { ProjectService } from "./../_service/project.service";
import { Component, OnInit, AfterViewInit, OnDestroy } from "@angular/core";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { FileUploadService } from "../_service/file-upload.service";
import { DwonloadHelper } from "src/app/_utility/filedownload.helper";
import { DashboardHeaderService } from "../header/dashboard-header.service";
import { LoaderService } from "../../_shared/main-loader/loader.service";

declare var $: any;

@Component({
  selector: "app-addendum-form",
  templateUrl: "./addendum-form.component.html",
  styleUrls: ["./addendum-form.component.css"]
})
export class AddendumFormComponent implements OnInit, OnDestroy {
  id: number;
  addendumId: string;
  addendum: AddendumFormModel;
  userRole: string;
  Users: AssignToModel[] = [];
  CCList: CCListModel[] = [];
  CCSuggestions: CCListModel[] = [];
  files: FileList;
  attachments: AttachmentListExtModel[] = [];
  showPrintModal: boolean;
  ccIsInvalid: boolean;
  showFileSizeError: boolean;
  savingProgress: boolean;
  formDisabled = true;
  confirmationTitle: string;
  confirmationMessage: string;
  confirmationAction: string;
  chroneJob: any;
  isFormDirty = false;
  startDetectingChanges = false;
  needReload = false;
  projecturl: string;
  searchedEmail: string;
  isUploadIncomplete: boolean;
  UsersList: AssignToModel[] = [];
  undefiend = undefined;

  constructor(
    private service: ProjectService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fileService: FileUploadService,
    private alert: AlertService,
    private headerService: DashboardHeaderService,
    private loader: LoaderService
  ) {}

  ngOnInit() {
    this.userRole = UserAuthHelper.GetProfile().type;
    this.addendum = new AddendumFormModel();
    this.attachments = [];
    this.loadUsers();
    this.activatedRoute.params.subscribe(res => {
      this.addendumId = res.id;
      this.loadDetails();
    });
  }

  loadUsers(): any {
    this.service.loadUsers().subscribe((res: any) => {
      this.UsersList = res.data;
      this.Users = Object.assign([], this.UsersList);
    });
  }

  loadDetails() {
    this.service.loadAddendumDetails(this.addendumId).subscribe(
      (res: any) => {
        this.addendum = res.data;
        this.setHeaderTitle();
        this.attachments = Object.assign([], this.addendum.attachment);
        if (this.addendum.cc) {
          this.CCList = Object.assign([], this.addendum.mailList);
        }
        this.addendum.cc = "";
        this.formDisabled =
          (this.userRole === "Standard" &&
            this.addendum.adStatus !== "Draft") ||
          this.addendum.adStatus === "Accepted";
        setTimeout(() => {
          this.startDetectingChanges = true;
          this.createChroneJob();
        }, 500);
      },
      (error: any) => {
        this.alert.error(error.error.details, true);
        this.isFormDirty = false;
        this.router.navigate(["/dashboard"]);
      }
    );
  }

  setHeaderTitle() {
    if (
      this.addendum.contractorCompanyName &&
      this.addendum.catalog &&
      this.addendum.from
    ) {
      const title =
        "Addendum for " +
        this.addendum.contractorCompanyName +
        " on " +
        this.addendum.catalog +
        " from " +
        this.addendum.from;
      this.headerService.setTitle(title);
    } else {
      this.headerService.setTitle("Addendum - " + this.addendumId);
    }
  }

  blur() {
    this.id = 0;
  }

  focus(id: number, ele: any) {
    this.id = id;
    if (ele) {
      setTimeout(() => {
        const it: any = document.getElementById("ccinput");
        it.focus();
      }, 550);
    }
    return true;
  }

  viewProject() {
    const webpage =
      location.origin + "/projects/" + this.addendum.parentProjectMemo;
    const mydiv = document.getElementById("myDiv");
    const aTag = document.createElement("a");
    aTag.setAttribute("href", webpage);
    aTag.setAttribute("target", "_blank");
    aTag.setAttribute("id", "dumLink");
    aTag.setAttribute("rel", "noopener");
    aTag.innerHTML = "link text";
    mydiv.appendChild(aTag);
    document.getElementById("dumLink").click();
  }

  SaveProgress(reload: boolean = false) {
    if (this.startDetectingChanges) {
      if (reload) {
        this.needReload = true;
      }
      this.isFormDirty = true;
    }
  }

  saveNewChanges() {
    this.savingProgress = true;
    if (this.needReload) {
      this.loader.show();
    }
    setTimeout(() => {
      this.addendum.mailList = this.CCList;
      this.addendum.attachment = this.attachments.filter(x => !x.NotLoaded);
      this.service.updateAddendum(this.addendum).subscribe(
        (res: any) => {
          if (this.needReload) {
            this.loadDetails();
          }
          this.savingProgress = false;
          this.isFormDirty = false;
          this.needReload = false;
          this.loader.hide();
        },
        (error: any) => {
          this.savingProgress = false;
          this.isFormDirty = false;
          this.needReload = false;
          this.loader.hide();
          this.alert.error(error.error.details);
        }
      );
    }, 0);
  }

  searchPeopleEmail() {
    this.CheckIfAllEmailsAreValid();
    if (this.addendum.cc && this.addendum.cc !== this.searchedEmail) {
      this.searchedEmail = this.addendum.cc;
      this.service.getPeoplesEmail(this.addendum.cc).subscribe((res: any) => {
        this.CCSuggestions = res.data;
      });
    }
  }

  CheckIfAllEmailsAreValid(): any {
    this.ccIsInvalid = this.CCList.some(x =>
      !x.name ? !this.validateEmail(x.email) : false
    );
  }

  validateEmail(email) {
    // tslint:disable-next-line:max-line-length
    // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const re = /^\S+@\S+$/;
    return re.test(String(email).toLowerCase());
  }

  addEmailToList() {
    if (this.CCSuggestions.length > 0) {
      this.CCList.push(this.CCSuggestions[0]);
    } else {
      this.addendum.cc = this.addendum.cc.replace(",", "");
      this.CCList.push({ name: null, email: this.addendum.cc });
    }
    setTimeout(() => {
      this.addendum.cc = "";
      this.CheckIfAllEmailsAreValid();
    }, 0);
    this.SaveProgress();
  }

  removeFromCCList(p: any) {
    if (!this.formDisabled) {
      this.CCList.splice(p, 1);
      this.CheckIfAllEmailsAreValid();
    }
    this.SaveProgress();
  }

  uploadAttachment(event: any) {
    this.isFormDirty = true;
    this.isUploadIncomplete = true;
    const length = event.target.files.length;
    this.files = event.target.files;
    this.showFileSizeError = false;
    const mxFileSize = 25 * 1024 * 1024;
    for (let i = 0; i < length; i++) {
      if (event.target.files[i].size < mxFileSize) {
        this.attachments.push({
          file: event.target.files[i].name,
          url: "",
          NotLoaded: true,
          id: this.addendumId
        });
      } else {
        this.showFileSizeError = true;
      }
    }
    this.UploadFiles();
  }

  removeAttachments(f: any) {
    if (!this.formDisabled) {
      this.attachments = this.attachments.filter(x => x !== f);
      this.addendum.attachment = this.addendum.attachment.filter(x => x !== f);
      $("#fileInput").replaceWith($("#fileInput").val('').clone(true));
      this.SaveProgress();
    }
  }

  UploadFiles() {
    const length = this.files.length;
    const mxFileSize = 25 * 1024 * 1024;
    for (let i = 0; i < length; i++) {
      if (this.files[i].size < mxFileSize) {
        this.fileService
          .uploadFile(this.files[i], this.addendumId + "-" + this.files[i].name)
          .subscribe(
            data => {
              // data.Bucket, data.ETag, data.Key, data.Location, data.key
              const item = this.attachments.find(
                x => x.id + "-" + x.file === data.key
              );
              item.NotLoaded = false;
              item.url = data.Location;
              this.addendum.attachment = this.attachments
                .filter(x => !x.NotLoaded)
                .map(x => ({
                  file: item.file,
                  url: data.Location
                }));
              if (this.attachments.every(x => !x.NotLoaded)) {
                this.isUploadIncomplete = false;
                this.SaveProgress();
              }
            },
            error => {}
          );
      }
    }
  }

  getPosts(option: any) {
    const p = this.CCSuggestions.find(x => x.email === option.value);
    this.addendum.cc = "";
    this.CCList.push(p);
    this.CCSuggestions = [];
  }

  print() {
    this.showPrintModal = true;
    setTimeout(() => {
      $("#printAddendumModal").modal("show");
    }, 500);
  }

  downLoadFiles() {
    this.addendum.attachment.forEach(ele => {
      DwonloadHelper.downloadFile(ele.url, ele.file);
    });
  }

  downloadSingleFile(f: any) {
    DwonloadHelper.downloadFile(f.url, f.file);
  }

  deleteAddendum() {
    this.loader.show();
    this.service.deleteAddendum(this.addendumId).subscribe(
      res => {
        this.isFormDirty = false;
        this.loader.hide();
        this.router.navigate(["/dashboard"]);
      },
      error => {
        this.loader.hide();
        this.alert.error(error.error.details);
      }
    );
  }

  openDeleteModal() {
    this.confirmationTitle = "Delete Addendum";
    this.confirmationMessage =
      "Are you sure you want to delete this Addendum? Once deleted it cannot be recovered.";
    this.confirmationAction = "DELETE";
    $("#confirmationModal").modal("show");
    $("#confirmation-modal-cancle").focus();
  }

  confirm() {
    if (this.confirmationAction === "DELETE") {
      this.deleteAddendum();
      $("#confirmationModal").modal("hide");
    } else if (this.confirmationAction === "SUBMIT") {
      if (this.userRole.includes("Standard")) {
        this.confirmSubmitStandardUser();
      } else {
        this.confirmSubmitBA();
      }
    }
    $("#confirmationModal").modal("hide");
  }

  confirmSubmitBA() {
    this.loader.show();
    if (this.isFormDirty) {
      this.service.updateAddendum(this.addendum).subscribe(
        (res: any) => {
          this.savingProgress = false;
          this.isFormDirty = false;
          this.needReload = false;
          this.BAFinalSubmit();
        },
        (error: any) => {
          this.savingProgress = false;
          this.isFormDirty = false;
          this.needReload = false;
          this.loader.hide();
          this.alert.error(error.error.details);
        }
      );
    } else {
      this.BAFinalSubmit();
    }
  }

  BAFinalSubmit() {
    this.service.SubmitAddendumBA(this.addendumId).subscribe(
      (res: any) => {
        this.loader.hide();
        this.alert.success("This Addendum has been marked as Accepted.", true);
        this.loadDetails();
      },
      (error: any) => {
        this.alert.error(error.error.details);
        this.loader.hide();
      }
    );
  }

  FilterAssignTo() {
    this.Users = this.UsersList.filter(x =>
      (x.firstName + " " + x.lastName).toLocaleLowerCase().includes(this.addendum.assignedUser.toLocaleLowerCase()));
  }

  ValidateAssignTo() {
    if (this.addendum.assignedTo) {
      if (!this.UsersList.some(x => x.firstName + " " + x.lastName === this.addendum.assignedTo.trim())) {
       this.addendum.assignedUser = "";
       this.addendum.assignedTo = "";
      }
    }
  }

  selectionChanged(option: any) {
    const user = this.UsersList.find(x => x.uniqueid === option.value);
    if (user) {
      this.addendum.assignedUser =  user.firstName + " " + user.lastName;
      this.addendum.assignedTo = option.value;
    }
    this.needReload = true;
    this.saveNewChanges();
  }

  submit() {
    setTimeout(() => {
      if (this.submitFormValid()) {
        if (this.userRole === "Standard") {
          this.openConfirmSubmitStandardUserModal();
        } else {
          this.openConfirmSubmitBAUserModal();
        }
      }
    }, 0);
  }

  openConfirmSubmitBAUserModal() {
    this.confirmationTitle = "Submit Addendum";
    this.confirmationMessage =
      "Are you sure you want to mark this as completed? Keep in mind that you are required to manually update data in Rightsline.";
    this.confirmationAction = "SUBMIT";
    $("#confirmationModal").modal("show");
    $("#confirmation-modal-cancle").focus();
  }

  submitFormValid() {
    const errorEle = document.getElementsByClassName("form-group error");
    if (errorEle.length === 0) {
      return true;
    } else {
      const ele: any = errorEle[0];
      const errorTop = ele.offsetTop;
      $("html, body").animate(
        {
          scrollTop: errorTop - 128
        },
        500
      );
    }
  }

  openConfirmSubmitStandardUserModal() {
    this.confirmationTitle = "Submit Addendum";
    this.confirmationMessage =
      "Are you sure this is ready to be sent to Business Affairs?";
    this.confirmationAction = "SUBMIT";
    $("#confirmationModal").modal("show");
    $("#confirmation-modal-cancle").focus();
  }

  createChroneJob() {
    this.chroneJob = null;
    this.chroneJob = setInterval(() => {
      if (this.isFormDirty && !this.isUploadIncomplete) {
        this.saveNewChanges();
      }
    }, 5000);
  }

  confirmSubmitStandardUser() {
    this.loader.show();
    if (this.isFormDirty) {
      this.service.updateAddendum(this.addendum).subscribe(
        (res: any) => {
          this.savingProgress = false;
          this.isFormDirty = false;
          this.needReload = false;
          this.UserFinalSubmit();
        },
        (error: any) => {
          this.savingProgress = false;
          this.isFormDirty = false;
          this.needReload = false;
          this.loader.hide();
          this.alert.error(error.error.details);
        }
      );
    } else {
      this.UserFinalSubmit();
    }
  }

  UserFinalSubmit() {
    this.service.SubmitAddendumSU(this.addendumId).subscribe(
      (res: any) => {
        this.loader.hide();
        this.alert.success("Your Addendum has been submitted.", true);
        this.isFormDirty = false;
        this.router.navigate(["dashboard"]);
      },
      (error: any) => {
        this.alert.error(error.error.details);
        this.loader.hide();
      }
    );
  }

  sendCopiesBlur() {
    this.addendum.cc = this.addendum.cc.trim();
  }

  ngOnDestroy(): void {
    this.chroneJob = null;
    if (this.isFormDirty) {
      this.saveNewChanges();
    }
  }
  displayFn(val) {
    return  val;
  }
}
