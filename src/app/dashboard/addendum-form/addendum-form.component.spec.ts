import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddendumFormComponent } from './addendum-form.component';

describe('AddendumFormComponent', () => {
  let component: AddendumFormComponent;
  let fixture: ComponentFixture<AddendumFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddendumFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendumFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
