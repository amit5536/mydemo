import { AddendumFormModel } from "./../../_models/projects.model";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ProjectFormModel } from "src/app/_models/projects.model";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";

@Component({
  selector: "app-print-addendum",
  templateUrl: "./print-addendum.component.html",
  styleUrls: ["./print-addendum.component.css"]
})
export class PrintAddendumComponent implements OnInit {
  @Input() addendum: AddendumFormModel;
  @Output() downLoadFiles = new EventEmitter<string>();
  today = new Date();
  addendumId: any;
  public userRole: any;
  constructor() {}

  ngOnInit() {
    // this.downLoadFiles = new EventEmitter<string>();
    this.userRole = UserAuthHelper.GetProfile().type;

  }

  print() {
    UserAuthHelper.PrintPage();
  }

  download() {
    this.downLoadFiles.emit("nae");
  }
}

// const webpage =
// location.origin + "/addendum/" + this.addendum.adID + "/print";
// const mydiv = document.getElementById("myprintDiv");
// const aTag = document.createElement("a");
// aTag.setAttribute("href", webpage);
// aTag.setAttribute("target", "_blank");
// aTag.setAttribute("id", "dumprintLink");
// aTag.setAttribute("rel", "noopener");
// aTag.innerHTML = "link text";
// mydiv.appendChild(aTag);
// document.getElementById("dumprintLink").click();
// UserAuthHelper.Addendum = this.addendum;
