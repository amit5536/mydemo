import { UsersService } from "../_service/users.service";
import { Component, OnInit } from "@angular/core";
import { UsersFilterModel } from "../../_models/users-filter.model";
import { UserListModel } from "../../_models/user-list.model";
import { Router } from '@angular/router';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    // if (location.href.includes('refresh')) {
    //   this.router.navigate(['/dashboard']);
    // }
  }
}
