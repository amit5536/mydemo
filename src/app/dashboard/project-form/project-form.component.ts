import { DwonloadHelper } from "./../../_utility/filedownload.helper";
import {
  AssignToModel,
  CatalogListModel,
  CCListModel,
  AttachmentListExtModel
} from "./../../_models/projects.model";
import { Location } from "@angular/common";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { ProjectService } from "../_service/project.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertService } from "../../_shared/alert/alert.service";
import { ProjectFormModel } from "src/app/_models/projects.model";
import { FileUploadService } from "../_service/file-upload.service";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { DashboardHeaderService } from "../header/dashboard-header.service";
import { AfterViewInit } from "@angular/core";
import { LoaderService } from "../../_shared/main-loader/loader.service";
import { UsersService } from '../_service/users.service';
import { SharedServiceService } from 'src/app/_services/shared-service.service';

declare var $: any;

@Component({
  selector: "app-project-form",
  templateUrl: "./project-form.component.html",
  styleUrls: ["./project-form.component.css"]
})
export class ProjectFormComponent implements OnInit, OnDestroy, AfterViewInit {
   code = "+91";
  selectedFiles: any[] = [];
  projectId: string;
  project: ProjectFormModel;
  id: number;
  Users: AssignToModel[] = [];
  CatalogList: CatalogListModel[] = [];
  CCList: CCListModel[] = [];
  CCSuggestions: CCListModel[] = [];
  files: FileList;
  attachments: AttachmentListExtModel[] = [];
  showPrintModal: boolean;
  ContractorsList: any[] = [];
  ContractorsListOptions: any[];
  savingProgress: boolean;
  formDisabled = true;
  userRole: any;
  ccIsInvalid = false;
  showFileSizeError = false;
  confirmationMessage: string;
  confirmationAction: "DELETE" | "SUBMIT";
  confirmationTitle: string;
  btnTitle: string;
  needReload: any;
  isFormDirty: boolean;
  startDetectingChanges: boolean;
  chroneJob: any;
  @ViewChild("cc") ccEle: ElementRef;
  searchedEmail: string;
  isUploadIncomplete: boolean;
  isPhoneValid: any;
  CatalogListItems: CatalogListModel[] = [];
  UsersList: AssignToModel[] = [];
  codeSelected: any = {
    flag: '',
    code: ''
  };
  countryCodeOptions: any;
  undefiend = undefined;
  constructor(
    private service: ProjectService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alert: AlertService,
    private fileService: FileUploadService,
    private location: Location,
    private headerService: DashboardHeaderService,
    private loader: LoaderService,
    private usersService: UsersService,
    private sharedServiceService: SharedServiceService
  ) {
    this.sharedServiceService.onUpdate().subscribe(response => {
      setTimeout(() => {
        this.isPhoneValid = localStorage.getItem('isPhoneNumberValid');

      }, 500);

    });
  }

  ngOnInit() {
    this.project = new ProjectFormModel();
    this.ContractorsList = [];
    this.userRole = UserAuthHelper.GetProfile().type;
    this.activatedRoute.params.subscribe(res => {
      if (res && res.projectId) {
        this.projectId = res.projectId;
        this.loadDetails();
      } else {
        this.isFormDirty = false;
        this.router.navigate(["/dashboard"]);
      }
    });
  }
  async getCountryCodeList() {
    const data = await this.usersService.getCountryCodeList();
    const a = [{
    label: "(+1) United States of America",
    value: "+1 ",
    flag: "https://restcountries.eu/data/usa.svg"
  },
  {
    label: "(+44) United Kingdom of Great Britain and Northern Ireland",
    value: "+44 ",
    flag: "https://restcountries.eu/data/gbr.svg"

  }];
    this.countryCodeOptions = [...a, ...data];
    if (this.countryCodeOptions.length > 0) {
      this.loadProjectDetails();
    }
  }
  onCountryChange(ev) {
  const data = this.countryCodeOptions.find(ele => ele.label === ev.label);
  this.codeSelected = {
    flag: data.flag,
    code: data.value
  };
  this.project.contractorCountryCode = data.label;
  this.SaveProgress();
  }
  ngAfterViewInit(): void {
    $(document).on("keydown", ".modal-confirm", event => {
      if (event.keyCode === 9) {
        event.preventDefault();
        $(".modal-cancel").focus();
      }
    });
  }
  setCatalog(event: any): boolean {
    let id = null;
    if (event.option.value) {
      id = this.CatalogList.find(x => x.catalogName === event.option.value).id;
    }
    this.project.catalogId = id;
    return true;
  }
  generateNewProject() {
    this.service.generateProjectId().subscribe(
      (res: any) => {
        if (res.data) {
          this.projectId = res.data;
          const url = "/projects/" + this.projectId;
          this.location.go(url);
          this.loadDetails();
        }
      },
      (error: any) => {
        this.alert.error(error.error.details);
      }
    );
  }

  FilterAssignTo() {
    this.Users = this.UsersList.filter(x => x.firstName.toLocaleLowerCase().includes(this.project.assignedUser.toLocaleLowerCase()));
  }

  ValidateAssignTo() {
    if (this.project.assignedTo) {
      if (!this.UsersList.some(x => x.firstName + " " + x.lastName === this.project.assignedTo.trim())) {
       this.project.assignedUser = "";
       this.project.assignedTo = "";
      }
    }
  }

  loadDetails(): any {
    this.getCountryCodeList();
    this.loadUsers();
    this.loadCatalogList();
    this.loadContacts();
  }

  loadContacts(): any {
    this.service.GetContractorsList().subscribe(res => {
      this.ContractorsList = res.data.contact;
      this.ContractorsListOptions = this.ContractorsList.filter(x => true);
      // tslint:disable-next-line:max-line-length
      if (this.project.pmStatus !== "Accepted") {
        const result = this.ContractorsListOptions.find(x => x.contactName === this.project.contractorCompanyName);
        if (!result) {
          this.project.contractorCompanyName = '';
        }
      }
    });
  }

  FilterCompany(value: string) {
    value = value.trim();
    this.ContractorsListOptions = this.ContractorsList.filter(x =>
      x.contactName.toLocaleLowerCase().includes(value.toLocaleLowerCase())
    );
  }

  loadCatalogList(): any {
    this.service.getCatalogList().subscribe((res: any) => {
      this.CatalogListItems = res.data.catalog;
      this.CatalogList = this.CatalogListItems.filter(x => true);
      if (this.project.pmStatus !== "Accepted") {
      const result  = this.CatalogListItems.find(x => x.catalogName === this.project.catalog);
      if (!result) {
        this.project.catalog = '';
      }
    }
    });
  }

  loadUsers(): any {
    if (this.projectId) {
      this.service.loadUsers().subscribe((res: any) => {
        this.UsersList = res.data;
        this.Users = Object.assign([], this.UsersList);
      });
    }
  }

  loadProjectDetails() {
    this.loader.show();
    if (this.projectId) {
      this.service.getProjectsDetails(this.projectId).subscribe(
        (res: any) => {
          this.project = res.data;
          if (this.project.contractorCountryCode && this.countryCodeOptions) {

            const data = this.countryCodeOptions.find(ele => ele.label === this.project.contractorCountryCode);
            if (data) {
              this.codeSelected = {
                flag: data.flag,
                code: data.value
              };
              this.project.contractorCountryCode = data.label;
            } else {
              this.codeSelected = {
                flag: this.countryCodeOptions[241].flag,
                code: this.countryCodeOptions[241].value
              };
            }

          } else {
            this.codeSelected = {
              flag: this.countryCodeOptions[241].flag,
              code: this.countryCodeOptions[241].value
            };
            this.project.contractorCountryCode = this.countryCodeOptions[241].label;
          }

          this.setHeaderTitle();
          this.attachments = Object.assign([], this.project.attachment);
          if (this.project && this.project.cc) {
            this.CCList = Object.assign([], this.project.mailList);
            this.CheckIfAllEmailsAreValid();
          }
          this.project.cc = "";
          this.formDisabled =
            this.project.pmStatus === "Accepted" ||
            ((this.project.pmStatus === "Assigned" ||
              this.project.pmStatus === "Submitted") &&
              this.userRole === "Standard");
          this.loader.hide();
          setTimeout(() => {
            this.startDetectingChanges = true;
            this.createChroneJob();
          }, 500);
        },
        (error: any) => {
          this.loader.hide();
          this.alert.error(error.error.details, true);
          this.isFormDirty = false;
          this.router.navigate(["/dashboard"]);
        }
      );
    }
  }

  validateCompanyName() {
    this.project.contractorCompanyName = this.project.contractorCompanyName.trim();
    if (
      this.project.contractorCompanyName &&
      this.ContractorsListOptions.filter(
        x => x.contactName === this.project.contractorCompanyName
      ).length !== 1
    ) {
      this.project.contractorCompanyName = "";
    }
  }

  setHeaderTitle() {
    if (
      this.project.contractorCompanyName &&
      this.project.catalog &&
      this.project.from
    ) {
      const title =
        "Project Memo for " +
        this.project.contractorCompanyName +
        " on " +
        this.project.catalog +
        " from " +
        this.project.from;
      this.headerService.setTitle(title);
    } else {
      this.headerService.setTitle("Project Memo - " + this.projectId);
    }
  }

  blur() {
    this.id = 0;
  }

  focus(id: number, ele: any) {
    this.id = id;
    if (ele) {
      setTimeout(() => {
        const it: any = document.getElementById("ccinput");
        it.focus();
      }, 550);
    }
    return true;
  }

  SaveProgress(reload: boolean = false) {
    if (this.startDetectingChanges) {
      if (reload) {
        this.needReload = true;
      }
      this.isFormDirty = true;
    }
  }

  createChroneJob() {
    this.chroneJob = null;
    this.chroneJob = setInterval(() => {
      if (this.isFormDirty && !this.isUploadIncomplete) {
        this.saveNewChanges();
      }
    }, 5000);
  }

  saveNewChanges() {
    if (this.needReload) {
      this.loader.show();
    }
    this.savingProgress = true;
    setTimeout(() => {
      this.project.mailList = Object.assign([], this.CCList);
      this.project.attachment = this.attachments
        .filter(x => !x.NotLoaded)
        .map(x => ({
          file: x.file,
          url: x.url
        }));
      this.service.SaveProgress(this.projectId, this.project).subscribe(
        (res: any) => {
          this.loader.hide();
          if (this.needReload) {
            this.loadProjectDetails();
          }
          this.needReload = false;
          this.isFormDirty = false;
          this.savingProgress = false;
        },
        (error: any) => {
          this.needReload = false;
          this.isFormDirty = false;
          this.savingProgress = false;
          this.loader.hide();
          this.alert.error("Hmmm… changes aren’t saving contact support.");
        }
      );
    }, 0);
  }

  searchPeopleEmail() {
    this.CheckIfAllEmailsAreValid();
    if (this.project.cc && this.project.cc !== this.searchedEmail) {
      this.searchedEmail = this.project.cc;
      this.service.getPeoplesEmail(this.project.cc).subscribe((res: any) => {
        this.CCSuggestions = res.data;
      });
    }
  }

  CheckIfAllEmailsAreValid(): any {
    this.ccIsInvalid = this.CCList.some(x =>
      !x.name ? !this.validateEmail(x.email) : false
    );
  }

  removeFromCCList(p: any) {
    if (!this.formDisabled) {
      this.CCList.splice(p, 1);
      this.CheckIfAllEmailsAreValid();
    }
    this.SaveProgress();
  }

  uploadAttachment(event: any) {
    this.isFormDirty = true;
    this.isUploadIncomplete = true;
    const length = event.target.files.length;
    this.files = event.target.files;
    this.showFileSizeError = false;
    const mxFileSize = 25 * 1024 * 1024;
    for (let i = 0; i < length; i++) {
      if (event.target.files[i].size < mxFileSize) {
        const atch = {
          file: event.target.files[i].name,
          url: "",
          NotLoaded: true,
          id: this.projectId
        };
        this.attachments.push(atch);
      } else {
        this.showFileSizeError = true;
      }
    }
    this.UploadFiles();
  }

  removeAttachments(f: any) {
    if (!this.formDisabled) {
      this.attachments = this.attachments.filter(x => x !== f);
      this.project.attachment = this.project.attachment.filter(x => x !== f);
      $("#fileInput").val(null);
    }
    this.SaveProgress();
  }

  UploadFiles() {
    const length = this.files.length;
    const mxFileSize = 25 * 1024 * 1024;
    for (let i = 0; i < length; i++) {
      if (this.files[i].size < mxFileSize) {
        this.fileService
          .uploadFile(this.files[i], this.projectId + "-" + this.files[i].name)
          .subscribe(
            data => {
              const item = this.attachments.find(
                x => x.id + "-" + x.file === data.key
              );
              item.NotLoaded = false;
              item.url = data.Location;
              if (this.attachments.every(x => !x.NotLoaded)) {
                this.isUploadIncomplete = false;
                this.SaveProgress();
              }
            },
            error => {}
          );
      }
    }
  }

  getPosts(option: any) {
    const p = this.CCSuggestions.find(x => x.email === this.project.cc);
    this.CCList.push(p);
    setTimeout(() => {
      this.project.cc = "";
    }, 0);
    this.CheckIfAllEmailsAreValid();
  }

  print() {
    this.showPrintModal = true;
    setTimeout(() => {
      $("#printProjectModal").modal("show");
    }, 500);
  }

  addAddendum() {
    this.loader.show();
    this.service
      .getAddendumId(this.projectId, this.project.projectName)
      .subscribe(
        (res: any) => {
          const url = "/addendum/" + res.data;
          this.loader.hide();
          this.isFormDirty = false;
          this.router.navigate([url]);
        },
        error => {
          this.loader.hide();
        }
      );
  }

  compantSelected(option: any) {
    this.project.contractorCompanyName = option.value;
    let id = null;
    if (option.value) {
      id = this.ContractorsListOptions.find(x => x.contactName === option.value).id;
    }
    this.project.contactId = id;
    this.SaveProgress();
  }

  addEmailToList() {
    if (this.CCSuggestions.length > 0) {
      this.CCList.push(this.CCSuggestions[0]);
    } else {
      this.project.cc = this.project.cc.replace(",", "");
      this.CCList.push({ name: null, email: this.project.cc });
    }
    setTimeout(() => {
      this.project.cc = "";
      this.CheckIfAllEmailsAreValid();
    }, 0);
    this.SaveProgress();
  }

  addDate(onDate: string, num: number) {
    if (
      typeof this.project.expirationDate === "string" ||
      typeof this.project.effectiveDate === "string"
    ) {
      if (this.project.effectiveDate) {
        this.project.effectiveDate = new Date(this.project.effectiveDate);
      }
      if (this.project.expirationDate) {
        this.project.expirationDate = new Date(this.project.expirationDate);
      }
    }
    if (onDate === "effectiveDate") {
      if (!this.project.effectiveDate) {
        if (!this.project.expirationDate) {
          this.project.effectiveDate = new Date();
        } else {
          this.project.effectiveDate = this.project.expirationDate;
        }
      }
      if (
        num < 0 ||
        !this.project.expirationDate ||
        (num > 0 && this.project.effectiveDate < this.project.expirationDate)
      ) {
        this.project.effectiveDate = this.addDays(
          new Date(this.project.effectiveDate),
          num
        );
      }
      this.project.effectiveDate.setHours(0, 0, 1, 0);
    } else if (onDate === "expirationDate") {
      if (!this.project.expirationDate) {
        if (!this.project.effectiveDate) {
          this.project.expirationDate = new Date();
        } else {
          this.project.expirationDate = this.project.effectiveDate;
        }
      }
      if (
        num > 0 ||
        !this.project.effectiveDate ||
        (num < 0 && this.project.effectiveDate < this.project.expirationDate)
      ) {
        this.project.expirationDate = this.addDays(
          new Date(this.project.expirationDate),
          num
        );
      }
      this.project.expirationDate.setHours(0, 0, 0, 0);
    }
    return true;
  }

  addDays(date: Date, days: number) {
    if (!date || isNaN(date.getTime())) {
      date = new Date();
      date.setHours(0, 0, 0, 0);
    }
    date.setDate(date.getDate() + days);
    return date;
  }

  validateEmail(email) {
    // tslint:disable-next-line:max-line-length
    // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const re = /^\S+@\S+$/;
    return re.test(String(email).toLowerCase());
  }

  downLoadFiles() {
    this.project.attachment.forEach(ele => {
      DwonloadHelper.downloadFile(ele.url, ele.file);
    });
  }

  downloadSingleFile(f: any) {
    DwonloadHelper.downloadFile(f.url, f.file);
  }

  deleteProjectMemo() {
    this.service.deleteProjectMemo(this.projectId).subscribe(
      res => {
        this.isFormDirty = false;
        this.router.navigate(["/dashboard"]);
      },
      error => {
        this.alert.error(error.details);
      }
    );
  }

  openDeleteModal() {
    this.confirmationTitle = "Delete Project Memo";
    this.confirmationMessage =
      "Are you sure you want to delete this Project Memo? Once deleted it cannot be recovered.";
    this.confirmationAction = "DELETE";
    $("#confirmationModal").modal("show");
  }

  confirm() {
    if (this.confirmationAction === "DELETE") {
      this.delete();
      $("#confirmationModal").modal("hide");
    } else if (this.confirmationAction === "SUBMIT") {
      if (this.userRole.includes("Standard")) {
        this.confirmSubmitStandardUser();
      } else {
        this.confirmSubmitBA();
      }
    }
    $("#confirmationModal").modal("hide");
  }

  confirmSubmitBA() {
    this.loader.show();
    if (this.isFormDirty) {
      this.service.SaveProgress(this.projectId, this.project).subscribe(
        (res: any) => {
          this.needReload = false;
          this.isFormDirty = false;
          this.savingProgress = false;
          this.BAFinalSubmit();
        },
        (error: any) => {
          this.needReload = false;
          this.isFormDirty = false;
          this.savingProgress = false;
          this.loader.hide();
          this.alert.error("Hmmm… changes aren’t saving contact support.");
        }
      );
    } else {
      this.BAFinalSubmit();
    }
  }

  BAFinalSubmit() {
    this.service.SubmitProjectBA(this.projectId).subscribe(
      (res: any) => {
        this.alert.success("Your Project Memo has been submitted.", true);
        this.loader.hide();
        this.isFormDirty = false;
        this.router.navigate(["dashboard"]);
      },
      (error: any) => {
        this.alert.error(error.error.details);
        this.loader.hide();
      }
    );
  }

  selectionChanged(option: any) {
    const user = this.UsersList.find(x => x.uniqueid === option.value);
    if (user) {
      this.project.assignedUser =  user.firstName + " " + user.lastName;
      this.project.assignedTo = option.value;
    }
    this.needReload = true;
    this.saveNewChanges();
  }

  delete() {
    this.service.deleteProjectMemo(this.projectId).subscribe(
      (res: any) => {
        this.alert.error("That Project Memo has been deleted.", true);
        this.isFormDirty = false;
        this.router.navigate(["/dashboard"]);
      },
      error => {
        this.alert.error(
          error.details ? error.details : "Something went wrong on server!"
        );
      }
    );
  }

  submit() {
    this.btnTitle = null;
    setTimeout(() => {
      if (this.submitFormValid()) {
        if (this.userRole === "Standard") {
          this.openConfirmSubmitStandardUserModal();
        } else {
          this.openConfirmSubmitBAUserModal();
          // if (this.checkContactExist()) {
          //   this.openConfirmSubmitBAUserModal();
          // } else {
          //   this.alert.success(`Owner of this project is missing from rightsline. Please update ${this.userRole.email}
          //                       to any existing user of rightline.`, true);
          // }
        }
      }
    }, 0);
  }
  checkContactExist() {
    const contact = this.ContractorsList.find(x => x.contactName === this.project.from);
    if (contact) {
       return true;
    } else {
        return false;
    }
  }
  openConfirmSubmitBAUserModal() {
    this.confirmationTitle = "Submit Project Memo";
    this.confirmationMessage =
      "Are you sure this is ready to be sent to Rightsline?";
    this.confirmationAction = "SUBMIT";
    this.btnTitle = "Submit";
    $("#confirmationModal").modal("show");
    $("#confirmation-modal-cancle").focus();
  }

  submitFormValid() {

    const errorPhone = document.getElementsByClassName("form-group phone-valid");

    const errorEle = document.getElementsByClassName("form-group error");
    if (errorEle.length === 0) {
      if (this.isPhoneValid === 'false') {
          const a: any = errorPhone[0];
          const erTop = a.offsetTop;
          $("html, body").animate(
            {
              scrollTop: erTop - 128
            },
            500
          );
      } else {
        return true;
      }

    } else {

      const ele: any = errorEle[0];
      const errorTop = ele.offsetTop;
      $("html, body").animate(
        {
          scrollTop: errorTop - 128
        },
        500
      );
    }
    return false;
  }

  openConfirmSubmitStandardUserModal() {
    this.confirmationTitle = "Submit Project Memo";
    this.confirmationMessage =
      "Are you sure this is ready to be sent to Business Affairs?";
    this.confirmationAction = "SUBMIT";
    $("#confirmationModal").modal("show");
    $("#confirmation-modal-cancle").focus();
  }

  confirmSubmitStandardUser() {
    this.loader.show();
    if (this.isFormDirty) {
      this.service.SaveProgress(this.projectId, this.project).subscribe(
        (res: any) => {
          this.needReload = false;
          this.isFormDirty = false;
          this.savingProgress = false;
          this.UserFinalSubmit();
        },
        (error: any) => {
          this.needReload = false;
          this.isFormDirty = false;
          this.savingProgress = false;
          this.loader.hide();
          this.alert.error("Hmmm… changes aren’t saving contact support.");
        }
      );
    } else {
      this.UserFinalSubmit();
    }
  }
  UserFinalSubmit() {
    this.service.SubmitProjectStandardUser(this.projectId).subscribe(
      (res: any) => {
        this.alert.success("Your Project Memo has been submitted.", true);
        this.loader.hide();
        this.isFormDirty = false;
        this.router.navigate(["dashboard"]);
      },
      error => {
        this.loader.hide();
        this.alert.error(error.error.details);
      }
    );
  }

  ngOnDestroy(): void {
    this.chroneJob = null;
    if (this.isFormDirty) {
      this.saveNewChanges();
    }
  }

  sendCopiesBlur() {
    this.project.cc = this.project.cc.trim();
  }

  FilterCatalogList(value: string) {
    this.CatalogList = this.CatalogListItems.filter(x =>
      x.catalogName.toLocaleLowerCase().includes(value.toLocaleLowerCase())
    );
  }
  ValidateCatalog(value: string) {
    if (!this.CatalogListItems.find(x => x.catalogName.trim() === value.trim())) {
      this.project.catalog = "";
    }
  }





  addhyphen(event) {
    if (event && event.target && event.target.value) {
        event.target.value = event.target.value.split('-').join('');    // Remove dash (-) if mistakenly entered.

        const finalVal = event.target.value.match(/\d{3}(?=\d{2,3})|\d+/g).join('-');
        event.target.value = finalVal;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 43) {
      return true;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
}
displayFn(val) {
  return  val;
}
}
