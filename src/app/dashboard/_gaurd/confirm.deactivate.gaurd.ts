import { AddendumFormComponent } from './../addendum-form/addendum-form.component';
import { Injectable, Inject } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ConfirmationDialogueService } from 'src/app/_shared/confirmation-dialogue/confirmation-dialogue.service';
import { ProjectFormComponent } from '../project-form/project-form.component';


@Injectable()
export class ConfirmDeactivateGuard implements CanDeactivate<ProjectFormComponent> {

    constructor(private confiramtion: ConfirmationDialogueService) { }

    canDeactivate(target: ProjectFormComponent | AddendumFormComponent) {
        if (target.isUploadIncomplete) {
            return this.confiramtion.confirm('Unsaved Changes',
            // tslint:disable-next-line:max-line-length
            'You have an upload in progress, leaving now will cause this upload to be abandoned. Are you sure you want to leave?',
            'Leave', true, 'Stay'
        );
        } else if (target.isFormDirty) {
            return this.confiramtion.confirm('Unsaved Changes',
                // tslint:disable-next-line:max-line-length
                'Some of the changes you have made are not yet saved. If you leave now those changes will be lost. Are you sure you want to leave?',
                'SAVE & CONTINUE'
            );
        }
        return true;
    }
}
