import { DashboardComponent } from "./component/dashboard.component";
import { NgModule } from "@angular/core";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { UsersDashboardComponent } from "./users/users-dashboard/users-dashboard.component";
import { UserAccountComponent } from "./users/user-account/user-account.component";
import { UsersService } from "./_service/users.service";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { SharedModule } from "../_shared/shared.module";
import { ProjectsComponent } from "./projects/projects.component";
import { HomeComponent } from "./home/home.component";
import { DashboardHeaderComponent } from "./header/dashboard-header.component";
import { ProjectService } from "./_service/project.service";
import { FileUploadService } from "./_service/file-upload.service";
import { AddendumFormComponent } from "./addendum-form/addendum-form.component";
import { PrintMemoComponent } from "./print-memo/print-memo.component";
import { AddendumModalComponent } from "./addendum-modal/addendum-modal.component";
import { PrintAddendumComponent } from "./print-addendum/print-addendum.component";
import { DashboardHeaderService } from "./header/dashboard-header.service";
import { ConfirmDeactivateGuard } from './_gaurd/confirm.deactivate.gaurd';
import { ProjectFormComponent } from './project-form/project-form.component';
import { ProjectMemoPrintComponent } from './project-memo-print/project-memo-print.component';
import { ProjectAdPrintComponent } from './project-ad-print/project-ad-print.component';

@NgModule({
  declarations: [
    DashboardComponent,
    UsersDashboardComponent,
    DashboardHeaderComponent,
    UserAccountComponent,
    SidebarComponent,
    ProjectsComponent,
    HomeComponent,
    ProjectFormComponent,
    AddendumFormComponent,
    PrintMemoComponent,
    AddendumModalComponent,
    PrintAddendumComponent,
    ProjectMemoPrintComponent,
    ProjectAdPrintComponent
  ],
  imports: [SharedModule, DashboardRoutingModule],
  providers: [
    UsersService,
    ProjectService,
    FileUploadService,
    DashboardHeaderService,
    ConfirmDeactivateGuard
  ],
  bootstrap: [DashboardComponent]
})
export class DashboardModule {}
