import {
  Component,
  OnInit,
  ViewChild,
  QueryList,
  ViewChildren
} from "@angular/core";
import { UsersFilterModel } from "src/app/_models/users-filter.model";
import { UserListModel, UserModel } from "src/app/_models/user-list.model";
import { UsersService } from "src/app/dashboard/_service/users.service";
import { MatCheckbox } from "@angular/material";
import { Subscription } from "rxjs";
import JWTDecoder from "jwt-decode";
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../_service/project.service';
declare var $: any;

@Component({
  selector: "app-users-dashboard",
  templateUrl: "./users-dashboard.component.html",
  styleUrls: ["./users-dashboard.component.css"]
})
export class UsersDashboardComponent implements OnInit {
  @ViewChild("firstSelect") firstSelect;
  @ViewChild("secondSelect") secondSelect;
  // @ViewChildren(MatCheckbox) matCheckbox: QueryList<MatCheckbox>;

  filters: UsersFilterModel;
  usersList: UserListModel[] = [];
  totalUsers: number;
  displayForm = false;
  isProfile = false;
  userTypes: any[];
  usersStatus: any[];
  user: UserModel = new UserModel();
  pageIndex = 0;
  loading = true;
  subscription: Subscription;
  loadingFilters: boolean;
  isTestSearchActive = false;
  rightlineUserList: any[] = [];
  rightlineListOptions: any[];
  constructor(private service: UsersService,  private activeRoute: ActivatedRoute, private prodservice: ProjectService) {
    this.loadContacts();
  }

  ngOnInit() {
    this.setFilters();
    this.activeRoute.params.subscribe(params => {
      if (params && params.token) {
        this.user =  JWTDecoder(params.token);
        if (this.user.email) {
          this.user.password = "";
          this.displayForm = true;
          this.openModel();
        }
      }
    });
  }

  loadContacts(): any {
    this.prodservice.GetRightLineUserList().subscribe(res => {
          this.rightlineUserList = res.data;
          this.rightlineListOptions = res.data.filter(x => true);
    });
  }

  setFilters() {
    this.filters = new UsersFilterModel();
    this.userTypes = [
      { selected: true, value: "Standard", display: "Standard" },
      { selected: true, value: "Business Affairs", display: "Business Affairs" }
    ];
    this.usersStatus = [
      { selected: true, value: "Active", display: "Active" },
      { selected: true, value: "Inactive", display: "Inactive" }
    ];
    this.filters.sort = "firstName";
    this.setFiltersValue();
  }

  loadUsers() {
    if (
      this.filters.status &&
      this.filters.type &&
      this.filters.status.length !== 0 &&
      this.filters.type.length !== 0
    ) {
      this.usersList = [];
      this.totalUsers = 0;
      this.loading = true;
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.subscription = this.service.getUsers(this.filters).subscribe(
        (res: any) => {
          this.totalUsers = res.data.totalUsers;
          this.usersList = res.data.user;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
    }
  }

  sort(key: string) {
    if (this.filters.sort === key) {
      this.filters.order = this.filters.order === "desc" ? "asc" : "desc";
    } else {
      this.filters.order = "desc";
    }
    this.filters.sort = key;
    this.loadUsers();
  }

  pageChanged(event: any) {
    this.filters.limit = event.pageSize;
    this.filters.offset = event.pageIndex * event.pageSize;
    this.pageIndex = event.pageIndex;
    this.loadUsers();
  }

  search() {
    // this.filters.limit = 10;
    this.filters.offset = 0;
    this.pageIndex = 0;
    if (this.filters.searchText) {
      this.isTestSearchActive = true;
    } else {
      this.isTestSearchActive = false;
    }
    this.loadUsers();
  }

  setFiltersValue() {
    this.loadingFilters = true;
    setTimeout(() => {
      this.filters.status = this.usersStatus
        .filter(x => x.selected)
        .map(x => x.value);
      this.filters.type = this.userTypes
        .filter(x => x.selected)
        .map(x => x.value);
      this.loadingFilters = false;
      this.filter();
    }, 300);
  }

  reset() {
    this.isTestSearchActive = false;
    this.setFilters();
  }

  filter() {
    this.search();
  }

  update(user: any) {
    this.user = Object.assign({}, user);
    this.user.password = "";
    this.displayForm = true;
    this.openModel();
  }

  openModel() {
    setTimeout(() => {
      $("#userModal").modal("show");
    }, 500);
  }

  ModelClosed(val: boolean) {
    // $("#userModal").modal("hide");
    this.displayForm = false;
    if (val) {
      this.search();
    }
  }

  addNewUser() {
    this.user = new UserModel();
    this.user.password = "";
    this.displayForm = true;
    this.openModel();
  }

  closeDD() {
    this.firstSelect.close();
    this.secondSelect.close();
  }

  addNewUserupdate(event) {
    this.loadContacts();
  }
}
