import { UserListModel, UserModel } from "src/app/_models/user-list.model";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  AfterViewInit
} from "@angular/core";
import { UserAuthHelper } from "src/app/_utility/user_auth.helper";
import { UsersService } from "src/app/dashboard/_service/users.service";
import { AlertService } from "../../../_shared/alert/alert.service";
import { ProjectService } from '../../_service/project.service';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

declare const $: any;

@Component({
  selector: "app-user-account",
  templateUrl: "./user-account.component.html",
  styleUrls: ["./user-account.component.css"]
})
export class UserAccountComponent implements AfterViewInit {
  @Input() user: UserModel = new UserModel();
  @Output() closes = new EventEmitter<any>();
  @Output() addNewUser = new EventEmitter<any>();
  @Input() isProfile = false;
  @Input() rightlineUserList;
  @Input() rightlineListOptions;
  currentUserRole = UserAuthHelper.GetProfile().type;
  numberMissing = true;
  lowerCaseMissing = true;
  symbolMissing = true;
  upperCaseMissing = true;
  id: number;
  userRole = "";
  userEmail = "";
  confirmationHeader: string;
  confirmationMessage: string;
  confirmationButtonText: string;
  isDirty: boolean;

  constructor(private userService: UsersService, private alert: AlertService, private service: ProjectService, private router: Router) {
  }


  ngAfterViewInit(): void {
    const that = this;
    if ( this.isProfile) { this.loadContacts(); }
    $(document).on("click", ".uniqueModal", e => {
      if ($(e.target).hasClass("uniqueModal")) {
        if (that.isDirty && !that.isProfile) {
          $(".uniqueModal").modal("show");
          $("#confirmationUnsaved").modal("show");
        } else {
          $(".uniqueModal").modal("hide");
          this.isDirty = false;
          setTimeout(() => {
            that.closes.emit(false);
          }, 100);
        }
      }
    });
    setTimeout(() => {
      this.isDirty = false;
    }, 400);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    this.userRole = UserAuthHelper.GetProfile().type;
    this.userEmail = UserAuthHelper.GetProfile().email;

  }

  closeConfirmationUnsaved() {
    const that = this;
    $("#confirmationUnsaved").modal("hide");
    setTimeout(() => {
      $(".uniqueModal").modal("hide");
      this.isDirty = false;
      setTimeout(() => {
        that.closes.emit(false);
      }, 100);
    }, 200);
  }

  dirty() {
    this.isDirty = true;
  }

  cancleConfirmationUnsaved() {
    $("#confirmationUnsaved").modal("hide");
  }


  passwordChanged() {
    const password = this.user.password;
    if (/\d/.test(password)) {
      this.numberMissing = false;
    } else {
      this.numberMissing = true;
    }
    if (/[a-z]/.test(password)) {
      this.lowerCaseMissing = false;
    } else {
      this.lowerCaseMissing = true;
    }
    if (/[A-Z]/.test(password)) {
      this.upperCaseMissing = false;
    } else {
      this.upperCaseMissing = true;
    }
    if (/[^\w\s]/gi.test(password)) {
      this.symbolMissing = false;
    } else {
      this.symbolMissing = true;
    }
  }

  closeModel() {
    $(".uniqueModal").modal("hide");
    this.isDirty = false;
    setTimeout(() => {
      this.closes.emit(false);
    }, 100);
  }

  save() {
    if (this.isPasswordValid()) {
      if (this.isProfile && this.user.password) {
        this.updatePassword();
      } else if (this.formValid()) {
        if (this.user.id) {
          this.updateUser();
        } else {
          this.addUser();
        }
      }
    }
  }

  updatePassword(): any {
    // tslint:disable-next-line:max-line-length
    this.userService.updatePassword({ password: this.user.password, email: this.user.email, rightline_id: this.user.rightline_id }).subscribe(
      (res: any) => {
        this.alert.success(res.message, true);
        $(".uniqueModal").modal("hide");
        this.checkUserEmail(this.user.email);
        setTimeout(() => {
          this.closes.emit(true);
        }, 500);
      },
      error => {
        this.alert.error(error.error.details);
      }
    );
  }
  checkUserEmail(email): any {
      if (this.isProfile && this.userEmail !== email) {
        localStorage.clear();
        this.router.navigate(["login"]);
      } else {
        return true;
      }
  }

  updateUser(): any {
    this.userService.updateUser(this.user).subscribe(
      (res: any) => {
        this.addNewUser.emit();
        this.alert.success(res.message, true);
        $(".uniqueModal").modal("hide");
        this.checkUserEmail(this.user.email);
        setTimeout(() => {
          this.closes.emit(true);
        }, 500);
      },
      error => {
        $(".uniqueModal").modal("hide");
        setTimeout(() => {
          this.closes.emit(true);
        }, 500);
        this.alert.success(error.error.details);

      }
    );
  }

  addUser(): any {
    this.userService.addUser(this.user).subscribe(
      (res: any) => {
        this.alert.success(res.message, true);
        this.addNewUser.emit();
        $(".uniqueModal").modal("hide");
        setTimeout(() => {
          this.closes.emit(true);
        }, 500);
      },
      error => {
        this.alert.success(error.error.details, true);
      }
    );
  }

  setType(type: string) {
    this.user.type = type;
  }

  isPasswordValid() {
    return (
      !this.user.password ||
      (this.user.password &&
        !this.symbolMissing &&
        !this.upperCaseMissing &&
        !this.lowerCaseMissing &&
        !this.numberMissing)
    );
  }

  formValid() {
    return (
      this.submitFormValid() &&
      this.user.type &&
      this.user.status &&
      this.user.firstName &&
      this.user.email
    );
  }

  submitFormValid() {
    const errorEle = $("#userModal").find(".form-group.error");
    if (errorEle.length === 0) {
      return true;
    } else {
      const ele: any = errorEle[0];
      const errorTop = ele.offsetTop;
      $("#userModal").animate(
        {
          scrollTop: errorTop
        },
        500
      );
    }
    return false;
  }

  openDeleteModal() {
    this.confirmationHeader = "Delete User";
    this.confirmationMessage = `Are you sure you want to delete this user?
                                It is important to note that the users project memos and
                                addendums will remain in the system but, the user will no longer be visible.`;
    this.confirmationButtonText = "DELETE";
    $("#confirmationModal").modal("show");
    $("#confirmation-modal-cancle").focus();
  }

  unsavedConfirm() {}

  delete() {
    $(".uniqueModal").modal("hide");
    $("#confirmationModal").modal("hide");
    this.userService.deleteUser(this.user).subscribe(
      (res: any) => {
        this.alert.error("The user has been deleted.", true);
        this.isDirty = false;
        setTimeout(() => {
          this.closes.emit(true);
        }, 100);
      },
      (error: any) => {
        this.alert.error(error.error.details, true);
      }
    );
  }

  blur() {
    this.id = 0;
  }

  focus(id: number) {
    this.id = id;
    return true;
  }


  validateContact() {
    this.user.email = this.user.email.trim();
    if (this.user.email && this.rightlineListOptions  &&
       this.rightlineListOptions.filter(x => x.contactemail === this.user.email).length !== 1) {
         this.user.email = "";
    }
  }
  contactSelected(option: any) {
    this.user.email = option.value;
    let user = null;
    if (option.value) {
      user = this.rightlineListOptions.find(x => x.contactemail === option.value);
      this.user.firstName = option.value;
      this.user.is_deleted = user.is_deleted;
      this.user.rightline_id = user.rightline_id;
      this.user.firstName = user.contact_name;
    }

  }
  FilterContact(value) {
    if (value) {
    value = value.trim();
    this.rightlineListOptions = this.rightlineUserList.filter(x =>
      x.contact_name.toLocaleLowerCase().includes(value.toLocaleLowerCase()) ||
      x.contactemail.toLocaleLowerCase().includes(value.toLocaleLowerCase()));
    }
  }
  loadContacts(): any {
    this.service.GetRightLineUserList().subscribe(res => {
          this.rightlineUserList = res.data;
          this.rightlineListOptions = res.data.filter(x => true);
    });
  }
}
