import { Router } from '@angular/router';

export class HardReload {
    public static redirectTo(uri: string, router: Router) {
        router.navigateByUrl('/DummyComponent', {skipLocationChange: true}).then(() =>
        router.navigate([uri]));
    }
}
