export class StringHelper {
  public static ValueIsString(myVar: any) {
    if (typeof myVar === "string" || myVar instanceof String) {
      return true;
    } else {
      return false;
    }
  }

  public static RemoveRedundantSpacesFromObject(obj: any) {
    const body = Object.assign({}, obj);
    try {
      Object.keys(body).forEach(key => {
        if (this.ValueIsString(body[key])) {
          body[key] = body[key].trim();
        }
      });
    } catch (ex) {
      // tslint:disable-next-line:no-
    }
    return body;
  }
}
