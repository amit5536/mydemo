import { NgModule } from "@angular/core";

import { ResetComponent } from "./reset/reset.component";
import { LoginComponent } from "./login/login.component";
import { PasswordResetComponent } from "./password-reset/password-reset.component";

import { AuthRoutingModule } from "./auth-routing.module";
import { AuthHeaderComponent } from "./auth-header/auth-header.component";
import { SharedModule } from "../_shared/shared.module";

@NgModule({
  declarations: [
    PasswordResetComponent,
    LoginComponent,
    ResetComponent,
    AuthHeaderComponent
  ],
  imports: [SharedModule, AuthRoutingModule]
})
export class AuthModule { }
