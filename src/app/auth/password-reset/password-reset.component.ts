import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "../_service/auth.service";
import { AlertService } from "src/app/_shared/alert/alert.service";
import { ConfirmationDialogueService } from "src/app/_shared/confirmation-dialogue/confirmation-dialogue.service";

@Component({
  selector: "app-password-reset",
  templateUrl: "./password-reset.component.html",
  styleUrls: ["./password-reset.component.css"]
})
export class PasswordResetComponent implements OnInit {
  password = "";
  token: any;
  symbolMissing = true;
  numberMissing = true;
  upperCaseMissing = true;
  lowerCaseMissing = true;
  isPage = false;
  message = "";

  constructor(
    private auth: AuthService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private confirm: ConfirmationDialogueService,
    private alert: AlertService
  ) {
    this.password = "";
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.token = params.token;
      this.auth.checkResetToken(this.token).subscribe(
        res => {
            this.isPage = true;
        },
        error => {
          this.message = error.error.details;
         //  this.alert.success("Your password has been updated.", true, "center");
          this.alert.error(error.error.details, true);
          this.router.navigate(["/login"]);
        }
      );
    });
  }

  resetPassword() {
    this.message = "";
    if (
      !this.symbolMissing &&
      !this.upperCaseMissing &&
      !this.lowerCaseMissing &&
      !this.numberMissing
    ) {
      this.auth.changePassword(this.password, this.token).subscribe(
        res => {
          this.alert.success("Your password has been updated.", true, "center");
          this.router.navigate(["/login"]);
          // this.confirm
          //   .confirm("Success", "Your password has been updated.", "OK", false)
          //   .subscribe(() => {
          //     this.router.navigate(["/login"]);
          //   });
        },
        error => {
          this.message = error.error.details;
          // this.alert.error(error.error.details, true);
        }
      );
    }
  }

  passwordChanged() {
    const password = this.password;
    if (/\d/.test(password)) {
      this.numberMissing = false;
    } else {
      this.numberMissing = true;
    }
    if (/[a-z]/.test(password)) {
      this.lowerCaseMissing = false;
    } else {
      this.lowerCaseMissing = true;
    }
    if (/[A-Z]/.test(password)) {
      this.upperCaseMissing = false;
    } else {
      this.upperCaseMissing = true;
    }
    if (/[^\w\s]/gi.test(password)) {
      this.symbolMissing = false;
    } else {
      this.symbolMissing = true;
    }
  }
}
