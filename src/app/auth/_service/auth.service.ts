import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { URLS } from "src/app/_models/_config";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    const url = URLS.login;
    const body = {
      email,
      password
    };
    return this.http.post(url, body);
  }

  forgotPassword(email: string) {
    const host = document.location.origin + '/reset/';
    const url = URLS.forgotPassword;
    const body = {
      email,
      host
    };
    return this.http.post(url, body);
  }

  changePassword(password: string, resettoken: string) {
    const url = URLS.changePassword;
    const body = {
      password,
      resettoken
    };
    return this.http.post(url, body);
  }
  checkResetToken(token: any) {
    const url = `${URLS.checkTokenExist}/${token}`;
    return this.http.get(url);
  }

}
