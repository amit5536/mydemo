import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanActivate,
  Router
} from "@angular/router";
import { Observable } from "rxjs";

class Permissions {
  canActivate() {
    return localStorage.getItem("token") ? true : false;
  }
}

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  private permissions: Permissions = new Permissions();
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.permissions.canActivate()) {
      return true;
    } else {
      this.router.navigate(["/login"], { queryParams: { returnUrl: state.url }});
    }
  }
}
