import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardReload } from '../../_utility/hardreload.helper';

@Component({
  selector: 'app-auth-header',
  templateUrl: './auth-header.component.html',
  styleUrls: ['./auth-header.component.css']
})
export class AuthHeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  gotoDashboard() {
    HardReload.redirectTo('/', this.router);
  }

}
