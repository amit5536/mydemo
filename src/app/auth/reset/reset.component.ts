import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../_service/auth.service";
import { AlertService } from "src/app/_shared/alert/alert.service";
import { ConfirmationDialogueService } from "src/app/_shared/confirmation-dialogue/confirmation-dialogue.service";

@Component({
  selector: "app-reset",
  templateUrl: "./reset.component.html",
  styleUrls: ["./reset.component.css"]
})
export class ResetComponent implements OnInit {
  email: string;
  showAlert: boolean;
  message: string;
  constructor(
    private auth: AuthService,
    private router: Router,
    private alert: AlertService,
    private confirm: ConfirmationDialogueService
  ) {
    this.email = "";
  }

  ngOnInit() {}

  reset() {
    this.message = "";
    this.auth.forgotPassword(this.email).subscribe(
      (res: any) => {
        this.message =
          "Check your email for instructions on resetting your password.";
        this.alert.success(this.message, true, 'center');
        // this.confirm
        //   .confirm(
        //     "Success",
        //     "Check your email for instructions on resetting your password.",
        //     "OK",
        //     false
        //   )
        //   .subscribe(() => {
        //     this.router.navigate(["/login"]);
        //   });
        this.router.navigate(["/login"]);
      }, error => {
        this.message = error.error.details ? error.error.details : "Something went wrong on server";
      }
    );
  }
}
