import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { ResetComponent } from "./reset/reset.component";
import { PasswordResetComponent } from "./password-reset/password-reset.component";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "forgot", component: ResetComponent },
  { path: "reset/:token", component: PasswordResetComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
