import { ConfirmationDialogueService } from 'src/app/_shared/confirmation-dialogue/confirmation-dialogue.service';
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";

import JWTDecoder from "jwt-decode";
import { AuthService } from "../_service/auth.service";
import { AlertService } from "src/app/_shared/alert/alert.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  showAlert: boolean;
  message: string;
  returnUrl: any;
  constructor(
    private auth: AuthService,
    private router: Router,
    private titleService: Title,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.titleService.setTitle("Login - Participant Media");
    this.email = "";
    this.password = "";
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(param => {
      this.returnUrl = param.returnUrl;
    });
  }

  login() {
    this.message = "";
    this.auth.login(this.email, this.password).subscribe(
      res => {
        localStorage.setItem("token", res.token);
        const profile = JWTDecoder(localStorage.getItem("token"));
        localStorage.setItem("profile", JSON.stringify(profile));
        this.router.navigate([this.returnUrl ? this.returnUrl : "dashboard"]);
      },
      error => {
        this.message = error.error.details;
      //  this.alertService.error(error.error.details, false, "");
      }
    );
  }
}
