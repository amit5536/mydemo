import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./_shared/page-not-found/page-not-found.component";
import { DataGridComponent } from "./data-grid/data-grid.component";
import { AuthGuard } from "./auth/gaurd/auth.guard";

const routes: Routes = [
  {
    path: "",
    loadChildren: "./dashboard/dashboard.module#DashboardModule",
    canActivate: [AuthGuard]
  },
  { path: "", loadChildren: "./auth/auth.module#AuthModule" },
  {
    path: "html",
    component: DataGridComponent
  },
  { path: "404", component: PageNotFoundComponent },
  { path: "**", redirectTo: "404", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
