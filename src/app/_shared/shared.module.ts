import { SpinnerComponent } from "./../spinner/spinner.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { WelcomePageComponent } from "./../welcome-page/welcome-page.component";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import {ScrollDispatchModule} from '@angular/cdk/scrolling';

const MY_DATE_FORMATS = {
  parse: {
    dateInput: { month: "short", year: "numeric", day: "numeric" }
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: "input",
    monthYearLabel: { year: "numeric", month: "short" },
    dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
    monthYearA11yLabel: { year: "numeric", month: "long" }
  }
};

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatCheckboxModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  DateAdapter,
  MAT_DATE_FORMATS,
  MatPaginatorIntl,
} from "@angular/material";
import { FormsModule } from "@angular/forms";
import { LoaderComponent } from "./loader/loader.component";
import { AlertComponent } from "./alert/alert.component";
import { MyDateAdapter } from "./addapters/date.addapter";
import { CustomMatPaginatorIntl } from "./addapters/custom-mat-paginator";
import { MainLoaderComponent } from "./main-loader/main-loader.component";
import { ConfirmationDialogueComponent } from "./confirmation-dialogue/confirmation-dialogue.component";
import { AuthInterceptor } from "../_utility/httpintercepter.service";
import { SafeUrlPipe } from './pipe/safeurl.pipe';
import { TelPipe } from './pipe/tel.pipe';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatAutocompleteModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    FormsModule,
    HttpClientModule,
  ScrollDispatchModule,

  ],
  exports: [
    PageNotFoundComponent,
    MatAutocompleteModule,
    WelcomePageComponent,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    LoaderComponent,
    AlertComponent,
    SpinnerComponent,
    MainLoaderComponent,
    ConfirmationDialogueComponent,
    SafeUrlPipe,
    TelPipe,
  ScrollDispatchModule,

  ],
  declarations: [
    PageNotFoundComponent,
    WelcomePageComponent,
    LoaderComponent,
    AlertComponent,
    SpinnerComponent,
    MainLoaderComponent,
    ConfirmationDialogueComponent,
    SafeUrlPipe,
    TelPipe
  ],
  providers: [
    { provide: DateAdapter, useClass: MyDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl
    }
  ]
})
export class SharedModule {}
