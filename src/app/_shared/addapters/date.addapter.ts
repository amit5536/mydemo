import { NativeDateAdapter } from "@angular/material";

export class MyDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: any = "input"): string {
    if (displayFormat === "input") {
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      const d = this._to2digit(month) + "/" +  this._to2digit(day)  + "/" + this._to2digit(year);
      return d;
    } else {
        return date.toDateString();
    }
  }

  private _to2digit(n: number) {
    return ("00" + n).slice(-2);
  }
}
