import { Pipe, PipeTransform } from '@angular/core';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import { SharedServiceService } from 'src/app/_services/shared-service.service';

export interface IPhoneMask {
  phoneNumber: string;
  countryCode: string;
}

@Pipe({
  name: 'tel'
})
export class TelPipe implements PipeTransform {
  constructor(private SharedService: SharedServiceService) {
  }
  transform(value: any, countryCode?: string, args?: any): any {
    if (value && args) {
      const completePhoneNumber = args + value;
      const sss = completePhoneNumber.replace(" ", "");
      const phoneNumber = parsePhoneNumberFromString(sss);
      if (phoneNumber) {
        const a = phoneNumber.isValid();
        this.SharedService.update("isPhoneNumberValid", a);
        const result = phoneNumber.formatNational();
        return result;
      } else {
        return value;
      }
    } else {
      return value;
    }
  }
}


