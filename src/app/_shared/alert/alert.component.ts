import { Component, OnInit } from "@angular/core";
import { AlertService } from "./alert.service";
import $ from "jquery";

@Component({
  selector: "app-alert",
  templateUrl: "./alert.component.html",
  styleUrls: ["./alert.component.css"]
})
export class AlertComponent implements OnInit {
  message: any = "";
  Success: any;
  showerror = false;
  showsuccess = false;

  constructor(private alertService: AlertService) {}
  ngOnInit() {
    this.alertService.getMessage().subscribe(message => {
        if (message) {
          this.message = message;
          if (this.message.type === "success") {
            this.showsuccess = true;
            this.setHide();
          }
          if (this.message.type === "error") {
            this.setHide();
            this.showerror = true;
          }
        }
    });
  }

  setHide() {
    setTimeout(() => {
      this.showerror = false;
      this.showsuccess = false;
    }, 10000);
  }

  closeModal() {
    this.showerror = false;
    this.showsuccess = false;
  }
  closeAlert() {
    this.showerror = false;
    this.showsuccess = false;
  }
}
