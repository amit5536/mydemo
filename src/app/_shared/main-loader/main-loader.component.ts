import { Component, OnInit } from "@angular/core";
import { LoaderService } from "./loader.service";

@Component({
  selector: "app-main-loader",
  templateUrl: "./main-loader.component.html",
  styleUrls: ["./main-loader.component.css"]
})
export class MainLoaderComponent implements OnInit {
  showLoader = true;
  constructor(private loader: LoaderService) {}

  ngOnInit() {
    this.loader.getMessage().subscribe(res => {
     this.showLoader = res;
    });
  }
}
