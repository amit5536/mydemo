import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { PaginationModel, PropertyMetaModel, GridColumnType } from 'src/app/_models/pagination-model';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})

export class GridComponent implements OnChanges {
  @Input() public gridData: any[];
  @Input() public pageInfo: PaginationModel;
  @Input() public displayColumns: PropertyMetaModel[];
  @Output() pageInfoChange = new EventEmitter<PaginationModel>();
  @Output() action = new EventEmitter<any>();


  public cols: PropertyMetaModel[];
  public totalPage: any[];
  public indexer = 0;
  // showPagination: boolean;
  LastPage: number;

  ngOnChanges(changes: SimpleChanges): void {
    this.setColumns();
    this.setPagination();
  }

  setPagination(): any {
    if (this.pageInfo && this.pageInfo.TotalRecord > this.pageInfo.PageSize) {
      const totalPages = Math.ceil(
        this.pageInfo.TotalRecord / this.pageInfo.PageSize
      );
      this.LastPage = totalPages;
      this.totalPage = this.getVisiblePageIndex(
        totalPages,
        this.pageInfo.CurrentPage
      );
      // this.showPagination = true;
    } else {
     // this.showPagination = false;
    }
  }

  constructor() { }

  setColumns() {
    if (this.displayColumns) {
      this.cols = Object.assign([], this.displayColumns);
    } else {
      if (this.gridData && this.gridData.length > 0) {
        const props = Object.keys(this.gridData[0]).filter(
          x => !x.toLowerCase().endsWith("id")
        );
        this.cols = props.map(x => {
          return {
            ColumnName: x.toUpperCase(),
            PropertyName: x,
            PropertyType: GridColumnType.Text
          };
        });
      } else {
        this.cols = [];
      }
    }
  }

  SetPage(index: number) {
    this.pageInfo.CurrentPage = Number(index);
    this.emitPageInfoChange();
  }

  changePageSize(val) {
    this.pageInfo.PageSize = Number(val);
    this.emitPageInfoChange();
  }

  emitPageInfoChange() {
    this.pageInfoChange.emit(this.pageInfo);
  }

  getVisiblePageIndex(totalPages: number, CurrentPage: number) {
    const array = [];
    if (totalPages > 7) {
      let j = 1;
      for (let i = 0; i < 7 || array.length < 7; i++) {
        if (CurrentPage - (3 - i) >= 1 || CurrentPage - (3 - i) <= totalPages) {
          if (CurrentPage - (3 - i) > 0) {
            if (CurrentPage - (3 - i) <= totalPages) {
              array.push(CurrentPage - (3 - i));
            } else {
              array.unshift(CurrentPage - (3 + j));
              j++;
            }
          }
        }
      }
      return array;
    } else {
      for (let i = 1; i <= totalPages; i++) {
        array.push(i);
      }
      return array;
    }
  }

  ActionButtoClick(data: any, action: string, event: any) {
    if (event) {
      event.preventDefault();
    }
    this.action.emit({ action, value: data });
  }
}
