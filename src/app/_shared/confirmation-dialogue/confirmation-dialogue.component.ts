import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ConfirmationDialogueService } from "./confirmation-dialogue.service";
import { Subject } from "rxjs";

declare var $: any;

@Component({
  selector: "app-confirmation-dialogue",
  templateUrl: "./confirmation-dialogue.component.html",
  styleUrls: ["./confirmation-dialogue.component.css"]
})
export class ConfirmationDialogueComponent implements OnInit, AfterViewInit {
  confirmationTitle: string;
  confirmationMessage: string;
  btnTitle: string;
  private subject: Subject<boolean>;
  showCancleBtn = true;

  constructor(private confirmation: ConfirmationDialogueService) {}

  ngOnInit() {
    this.confirmation.ConfirmationSubscription().subscribe(res => {
      // tslint:disable-next-line:no-
      this.subject = res.subject;
      this.showConfirmationBox(
        res.title,
        res.message,
        res.btnOkText,
        res.showCancleBtn
      );
    });
  }
  ngAfterViewInit(): void {
    $(document).on("keydown", ".modal-confirm", event => {
      if (event.keyCode === 9) {
        event.preventDefault();
        $(".modal-cancel").focus();
      }
    });
  }

  showConfirmationBox(
    title: string,
    message: string,
    btnOkText: string,
    showCancleBtn: boolean = true
  ) {
    this.confirmationMessage = message;
    this.confirmationTitle = title;
    this.btnTitle = btnOkText;
    this.showCancleBtn = showCancleBtn;
    $("#app-confirmation").modal("show");
    // $("#app-confirmation-cancle").focus();
  }

  confirm() {
    this.subject.next(true);
    $("#app-confirmation").modal("hide");
  }

  cancle() {
    $("#app-confirmation").modal("hide");
    // this.subject.next(false);
  }
}
