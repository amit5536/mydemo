import { Injectable, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class ConfirmationDialogueService implements AfterViewInit {
  private confirmation: Subject<any> = new Subject();

  constructor() { }

  ngAfterViewInit() {

  }

  public confirm(
    title: string,
    message: string,
    btnOkText: string = 'OK',
    showCancleBtn: boolean = true,
    btnCancelText: string = 'CANCLE',
    dialogSize: 'sm' | 'lg' = 'sm') {
    const subject: Subject<boolean> = new Subject();
    this.confirmation.next({ subject, title, message, btnOkText, showCancleBtn, btnCancelText, dialogSize });
    return subject.asObservable();
  }

  ConfirmationSubscription() {
    return this.confirmation.asObservable();
  }
}
