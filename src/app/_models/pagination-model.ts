export class PaginationModel {
    public CurrentPage: number;
    public TotalRecord: number;
    public PageSize: number;
}

export class PropertyMetaModel {
    constructor() {
        this.PropertyType = GridColumnType.Text;
        this.ColumnName = "";
        this.DisplayText = "";
        this.PropertyName = "";
    }
    public ColumnName: string;
    public PropertyName: string;
    public PropertyType?: GridColumnType = GridColumnType.Text;
    public DisplayText?: string | string[];
}

export enum GridColumnType {
    Text,
    Link,
    Action,
    DisplayLink,
    Boolean,
    EditableText,
    Switch,
    Checkbox
}
