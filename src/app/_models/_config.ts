import { environment } from "src/environments/environment";

export const apiBaseUrl: string = environment.baseUrl;



export const URLS = {
  login: apiBaseUrl + "user/login",
  changePassword: apiBaseUrl + "changepassword",
  forgotPassword: apiBaseUrl + "resetpassword",
  getUsers: apiBaseUrl + "user/getusers",
  addUser: apiBaseUrl + "user/add-user",
  updateUser: apiBaseUrl + "user/update",
  updatePassword: apiBaseUrl + "updatepassword",
  deleteUser: apiBaseUrl + "deleteuser/",
  acticateUser: apiBaseUrl + "activateuser/",
  getuserprofile: apiBaseUrl + "user/getuserprofile",
  getProjects: apiBaseUrl + "project/getproject",
  generateProjectId: apiBaseUrl + "project/genrateprojectid",
  updateProject: apiBaseUrl + "project/updateproject/",
  getProjectDetails: apiBaseUrl + "project/getproject/",
  getUsersList: apiBaseUrl + "user/list",
  getCatalogList: apiBaseUrl + "rightline/cataloglist",
  searchEmail: apiBaseUrl + "user/searchemail/",
  getContactList: apiBaseUrl + "rightline/contactlist",
  submitProjectSU: apiBaseUrl + "project/submitprojectmemo/",
  submitProjectBA: apiBaseUrl + "rightline/submitprojectmemo/",
  deleteProjectMemo: apiBaseUrl + "project/removeproject/",
  searchProject: apiBaseUrl + "project/searchproject/",
  createUpdateAddendum: apiBaseUrl + "addendum/updateproject",
  getAddendumDetails: apiBaseUrl + "addendum/getproject/",
  submitAddendumSU: apiBaseUrl + 'addendum/submitaddendum/',
  submitAddendumBA: apiBaseUrl + "addendum/accept/",
  deleteAddendum: apiBaseUrl + "addendum/removeproject/",
  checkTokenExist: apiBaseUrl + "checkresettoken",
  getCountryCodeList: apiBaseUrl + "getcountrylist",
  getRightLineUserList: `${apiBaseUrl}rightline/userlist`
};
