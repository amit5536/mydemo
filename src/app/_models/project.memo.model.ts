export class ProjectMemoModel {
    constructor() {
        this.Attachments = [];
    }
    AssignedTo: string;
    From: string;
    CC: string;
    Created: Date;
    LastViewed: Date;
    LastViewedBy: string;
    Submitted: Date;
    Assigned: Date;
    Accepted: Date;
    ProjectName: string;
    Catalog: string;
    ContractorCompanyName: string;
    ContractorContactName: string;
    ContractorContactEmail: string;
    ContractorContactPhone: string;
    ContractorWorkOnsite: string;
    ContractorInsuranceGeneral: string;
    ContractorInsuranceEnO: string;
    ContractorInsuranceWorkersComp: string;
    ContractorInsuranceProduction: string;
    EffectiveDate: Date;
    ExpirationDate: Date;
    ContractorServices: string;
    ApproxHours: number;
    DeliverablesDescription: string;
    DeliverySchedule: string;
    TotalFees: number;
    PaymentSchedule: string;
    OwnershipofDeliverables: string;
    RightsLicensedbyContractor: string;
    IntendedUsebyContractor: string;
    IntendedUsebyParticipant: string;
    AssetsProvidedbyParticipant: string;
    AssetsProvidedbyContractor: string;
    ThirdPartyAssets: string;
    Attachments: any[];
    URL: string;
    ID: string;
    Status: string;
    RightslineID: string;
}
