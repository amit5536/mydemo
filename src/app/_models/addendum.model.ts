export class AddendumModel {
  From: string;
  AssignedTo: string;
  CC: string;
  ParentProjectMemo: string;
  Counterparties: string;
  ResponsibleParty: string;
  Changes: string;
  Reasons: string;
  ID: string;
  Status: string;
  Created: Date;
  LastViewed: Date;
  LastViewedBy: string;
  Submitted: Date;
  Assigned: Date;
  Accepted: Date;
}
