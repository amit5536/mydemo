export class PageDetailsModel {
  constructor() {
    this.limit = 10;
    this.offset = 0;
    this.sort = "";
    this.order = "desc";
  }
  limit: number;
  offset: number;
  sort: string;
  order: "desc" | "asc";
}

export class UsersFilterModel extends PageDetailsModel {
  constructor() {
    super();
    this.type = [''];
    this.status = [''];
    this.searchText = "";
  }
  searchText: string;
  type: string[];
  status: string[];
}
