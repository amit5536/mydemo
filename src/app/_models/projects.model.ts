import { PageDetailsModel } from "./users-filter.model";

export class ProjectFilterModel extends PageDetailsModel {
  constructor() {
    super();
    this.status = [''];
    this.type = [''];
    this.assignment = [''];
    this.startDate = new Date(
      new Date().setFullYear(new Date().getFullYear() - 1)
    );
    this.endDate = new Date();
  }
  searchText: string;
  type: string[];
  status: string[];
  assignment: string[];
  startDate: Date | string;
  endDate: Date | string;
}

export class ProjectListModel {
  id: number;
  submittedAt: Date;
  startAt: Date;
  assignedTo: string;
  projectId: string;
  vendor: string;
  project: string;
  type: string;
  status: string;
}

export class ProjectFormModel {
  assignedUser: string;
  catalogId: any;
  contactId: any;
  constructor() {
    // tslint:disable-next-line:no-string-literal
    this.assignedTo = "";
    this.cc = "";
    this.projectName = "";
    this.catalog = "";
    this.contractorCompanyName = "";
    this.contractorContactEmail = "";
    this.contractorContactPhone = "";
    this.contractorServices = "";
    this.approxHours = "";
    this.deliverablesDescription = "";
    this.deliverySchedule = "";
    this.totalFees = "";
    this.paymentSchedule = "";
    this.ownershipOfDeliverables = "";
    this.rightsLicensedByContractor = "";
    this.intendedUseByContractor = "";
    this.intendedUseByParticipant = "";
    this.assetsProvidedByParticipant = "";
    this.assetsProvidedByContractor = "";
    this.thirdPartyAssets = "";
    this.pmURL = "";
    this.attachment = [];
    this.pmStatus = "";
    this.contractorContactName = "";
    this.contractorWorkOnsite = null;
    this.contractorInsuranceGeneral = false;
    this.contractorInsuranceEnO = false;
    this.contractorInsuranceWorkersComp = false;
    this.contractorInsuranceProduction = false;
    this.effectiveDate = null; // new Date();
    this.expirationDate = null; // new Date();
    this.mailList = [];
    this.from = "";
  }

  from: string;
  mailList: any[];
  // tslint:disable-next-line:variable-name
  assignedTo: string;
  cc: string;
  projectName: string;
  catalog: string;
  effectiveDate: Date;
  expirationDate: Date;
  // tslint:disable-next-line:variable-name
  totalFees: string;
  contractorCompanyName: string;
  contractorContactEmail: string;
  contractorContactName: string;
  contractorContactPhone: string;
  contractorCountryCode: any;
  contractorWorkOnsite: boolean;
  contractorInsuranceGeneral: boolean;
  contractorInsuranceEnO: boolean;
  contractorInsuranceWorkersComp: boolean;
  contractorInsuranceProduction: boolean;
  contractorServices: string;
  approxHours: string;
  deliverablesDescription: string;
  deliverySchedule: string;
  paymentSchedule: string;
  ownershipOfDeliverables: string;
  rightsLicensedByContractor: string;
  intendedUseByContractor: string;
  intendedUseByParticipant: string;
  assetsProvidedByParticipant: string;
  assetsProvidedByContractor: string;
  thirdPartyAssets: string;
  pmURL: string;
  attachment: AttachmentListModel[];
  pmStatus: string;
}

export class AssignToModel {
  email: string;
  firstName: string;
  lastName: string;
  uniqueid: string;
}

export class CatalogListModel {
  id: number;
  catalogName: string;
}


export class CCListModel {
  name: string;
  email: string;
}

export class AttachmentListModel {
  file: string;
  url: string;
}

export class AttachmentListExtModel extends AttachmentListModel {
  constructor() {
    super();
    this.NotLoaded = false;
    this.id = "";
  }
  NotLoaded: boolean;
  id: string;
}

export class AddendumFormModel {
  from: string;
  assignedUser: string;
  constructor() {
    this.attachment = [];
    this.mailList = [];
    this.parentProjectMemo = "";
    this.reasons = "";
    this.changes = "";
    this.cc = "";
    this.counterparties = "";
    this.adID = "";
    this.assignedTo = "";
    this.responsibleParty = "";
    this.adStatus = "";
    this.adURL = "";
    this.projectName = "";
    this.catalog = "";
    this.contractorCompanyName = "";
    this.projectCreatedAt = "";
  }
  parentProjectMemo: string;
  cc: string;
  counterparties: string;
  responsibleParty: string;
  changes: string;
  reasons: string;
  adID: string;
  adStatus: string;
  attachment: AttachmentListModel[];
  adURL: string;
  assignedTo: string;
  projectName: string;
  catalog: string;
  contractorCompanyName: string;
  projectCreatedAt: Date | string;
  mailList: any[];
}
