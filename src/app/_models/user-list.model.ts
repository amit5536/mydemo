export class UserListModel {
  id: number;
  email: string;
  // tslint:disable-next-line:variable-name
  firstName: string;
  // tslint:disable-next-line:variable-name
  last_login: string;
  // tslint:disable-next-line:variable-name
  lastName: string;
  status: string;
  type: string;
  // tslint:disable-next-line:variable-name
  rightline_id: any;
  // tslint:disable-next-line:variable-name
  is_deleted: boolean;
}

export class UserModel extends UserListModel {
  constructor() {
    super();
    this.type = "";
    this.password = "";
  }
  password: string;
}
