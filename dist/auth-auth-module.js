(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["auth-auth-module"],{

/***/ "./src/app/auth/_service/auth.service.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/_service/auth.service.ts ***!
  \***********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_models_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_models/_config */ "./src/app/_models/_config.ts");




var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
    }
    AuthService.prototype.login = function (email, password) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].login;
        var body = {
            email: email,
            password: password
        };
        return this.http.post(url, body);
    };
    AuthService.prototype.forgotPassword = function (email) {
        var host = document.location.origin + '/reset/';
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].forgotPassword;
        var body = {
            email: email,
            host: host
        };
        return this.http.post(url, body);
    };
    AuthService.prototype.changePassword = function (password, resettoken) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].changePassword;
        var body = {
            password: password,
            resettoken: resettoken
        };
        return this.http.post(url, body);
    };
    AuthService.prototype.checkResetToken = function (token) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].checkTokenExist + "/" + token;
        return this.http.get(url);
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/auth/auth-header/auth-header.component.css":
/*!************************************************************!*\
  !*** ./src/app/auth/auth-header/auth-header.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvYXV0aC1oZWFkZXIvYXV0aC1oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auth/auth-header/auth-header.component.html":
/*!*************************************************************!*\
  !*** ./src/app/auth/auth-header/auth-header.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"d-flex navbar-expand-md bg-nav justify-content-center\">\n  <a class=\"navbar-brand\" (click)=\"gotoDashboard()\"><img src=\"../assets/images/logo.png\" class=\"img-fluid img-size\"></a>\n</nav>"

/***/ }),

/***/ "./src/app/auth/auth-header/auth-header.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/auth/auth-header/auth-header.component.ts ***!
  \***********************************************************/
/*! exports provided: AuthHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthHeaderComponent", function() { return AuthHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_utility/hardreload.helper */ "./src/app/_utility/hardreload.helper.ts");




var AuthHeaderComponent = /** @class */ (function () {
    function AuthHeaderComponent(router) {
        this.router = router;
    }
    AuthHeaderComponent.prototype.ngOnInit = function () {
    };
    AuthHeaderComponent.prototype.gotoDashboard = function () {
        _utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_3__["HardReload"].redirectTo('/', this.router);
    };
    AuthHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-auth-header',
            template: __webpack_require__(/*! ./auth-header.component.html */ "./src/app/auth/auth-header/auth-header.component.html"),
            styles: [__webpack_require__(/*! ./auth-header.component.css */ "./src/app/auth/auth-header/auth-header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthHeaderComponent);
    return AuthHeaderComponent;
}());



/***/ }),

/***/ "./src/app/auth/auth-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/auth/auth-routing.module.ts ***!
  \*********************************************/
/*! exports provided: AuthRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function() { return AuthRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _reset_reset_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./reset/reset.component */ "./src/app/auth/reset/reset.component.ts");
/* harmony import */ var _password_reset_password_reset_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./password-reset/password-reset.component */ "./src/app/auth/password-reset/password-reset.component.ts");






var routes = [
    { path: "login", component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: "forgot", component: _reset_reset_component__WEBPACK_IMPORTED_MODULE_4__["ResetComponent"] },
    { path: "reset/:token", component: _password_reset_password_reset_component__WEBPACK_IMPORTED_MODULE_5__["PasswordResetComponent"] }
];
var AuthRoutingModule = /** @class */ (function () {
    function AuthRoutingModule() {
    }
    AuthRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AuthRoutingModule);
    return AuthRoutingModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _reset_reset_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reset/reset.component */ "./src/app/auth/reset/reset.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _password_reset_password_reset_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./password-reset/password-reset.component */ "./src/app/auth/password-reset/password-reset.component.ts");
/* harmony import */ var _auth_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth-routing.module */ "./src/app/auth/auth-routing.module.ts");
/* harmony import */ var _auth_header_auth_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth-header/auth-header.component */ "./src/app/auth/auth-header/auth-header.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/_shared/shared.module.ts");








var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _password_reset_password_reset_component__WEBPACK_IMPORTED_MODULE_4__["PasswordResetComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"],
                _reset_reset_component__WEBPACK_IMPORTED_MODULE_2__["ResetComponent"],
                _auth_header_auth_header_component__WEBPACK_IMPORTED_MODULE_6__["AuthHeaderComponent"]
            ],
            imports: [_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_5__["AuthRoutingModule"]]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/auth/login/login.component.css":
/*!************************************************!*\
  !*** ./src/app/auth/login/login.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auth/login/login.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-auth-header></app-auth-header>\n<section class=\"pt-5\">\n  <div class=\"container-fluid\">\n    <form class=\"form-own\" #f=\"ngForm\" (submit)=\"f.touched && f.valid && login()\" autocomplete=\"off\" novalidate>\n      <div class=\"sign-in-screen\">\n        <div class=\"sign-in-screen-section\">\n          <div class=\"pb-4 text-center form-heading\">SIGN IN</div>\n          <div class=\"form-group\"\n            [ngClass]=\"{'error' : (f.submitted || mail.touched) && (mail.hasError('email')  || mail.hasError('required')) }\">\n            <mat-form-field class=\"example-full-width sign-input-2 mat-form-field  error\" floatLabel=\"always\">\n              <mat-label>Email</mat-label>\n              <input class=\"mat-input-element mat-form-field-autofill-control\" name=\"username\" type=\"text\" email matInput placeholder=\"\" #mail=\"ngModel\" [(ngModel)]=\"email\"\n                maxlength=\"240\" minlength=\"5\" required autocomplete=\"off\">\n              <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n              <mat-error *ngIf=\"mail.hasError('minlength') && !mail.hasError('email') && !mail.hasError('required')\">\n                Email should be atleast 5 character long\n              </mat-error>\n              <mat-error *ngIf=\"mail.hasError('email') && !mail.hasError('required')\">\n                Please enter a valid email address\n              </mat-error>\n              <mat-error *ngIf=\"mail.hasError('required')\">\n                Please enter your email address to login\n              </mat-error>\n            </mat-form-field>\n          </div>\n          <div class=\"form-group\"\n            [ngClass]=\"{'error' : (f.submitted || pass.touched) &&(pass.hasError('minlength')  || pass.hasError('required')) }\">\n            <mat-form-field class=\"example-full-width sign-input-2 mat-form-field \" floatLabel=\"always\">\n              <mat-label>Password</mat-label>\n              <input class=\"mat-input-element mat-form-field-autofill-control\" name=\"pass\" matInput placeholder=\"\" #pass=\"ngModel\" [(ngModel)]=\"password\" type=\"password\"\n                autocomplete=\"off\" minlength=\"8\" maxlength=\"40\" required>\n              <mat-error *ngIf=\"pass.hasError('minlength') && !pass.hasError('required')\">\n                Password should be atleast 8 character long\n              </mat-error>\n              <mat-error *ngIf=\"pass.hasError('required')\">\n                Please enter your password to login\n              </mat-error>\n            </mat-form-field>\n            <span *ngIf=\"message\" class=\"mat-error\" style=\"font-size:12px;\">\n              {{message}}\n            </span>\n          </div>\n          <div class=\"form-group d-flex flex-column\">\n            <button mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SIGN IN</button>\n            <div class=\"pt-xl-3 pt-lg-3 pt-md-2 pt-sm-2 text-center\">\n              <a routerLink=\"/forgot\" class=\"text-center forgot-color font-weight-500\">FORGOT PASSWORD </a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </form>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_service/auth.service */ "./src/app/auth/_service/auth.service.ts");
/* harmony import */ var src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");







var LoginComponent = /** @class */ (function () {
    function LoginComponent(auth, router, titleService, alertService, activatedRoute) {
        this.auth = auth;
        this.router = router;
        this.titleService = titleService;
        this.alertService = alertService;
        this.activatedRoute = activatedRoute;
        this.titleService.setTitle("Login - Participant Media");
        this.email = "";
        this.password = "";
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.queryParams.subscribe(function (param) {
            _this.returnUrl = param.returnUrl;
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.message = "";
        this.auth.login(this.email, this.password).subscribe(function (res) {
            localStorage.setItem("token", res.token);
            var profile = jwt_decode__WEBPACK_IMPORTED_MODULE_4___default()(localStorage.getItem("token"));
            localStorage.setItem("profile", JSON.stringify(profile));
            _this.router.navigate([_this.returnUrl ? _this.returnUrl : "dashboard"]);
        }, function (error) {
            _this.message = error.error.details;
            //  this.alertService.error(error.error.details, false, "");
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-login",
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/auth/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
            src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/password-reset/password-reset.component.css":
/*!******************************************************************!*\
  !*** ./src/app/auth/password-reset/password-reset.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvcGFzc3dvcmQtcmVzZXQvcGFzc3dvcmQtcmVzZXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auth/password-reset/password-reset.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/auth/password-reset/password-reset.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-auth-header></app-auth-header>\n<section class=\"pt-5\" *ngIf=\"isPage\">\n  <div class=\"container\">\n    <div class=\"\">\n      <form class=\"example-form form-own\" #f=\"ngForm\" (submit)=\"f.touched && f.valid && resetPassword()\"\n        autocomplete=\"off\" novalidate>\n        <div class=\"row justify-content-center mx-0\">\n          <div class=\"sign-in-screen-section\">\n            <div class=\"form-group pb-0\"\n              [ngClass]=\"{'error' :(f.submitted || pass.touched) &&(pass.hasError('minlength')  || pass.hasError('required')) }\">\n              <div class=\"pb-4 text-center form-heading\">PASSWORD RESET</div>\n              <mat-form-field class=\"example-full-width sign-input-2 mat-form-field\"\n                [ngClass]=\"{ 'error': f.submitted && !f.valid }\" floatLabel=\"always\">\n                <mat-label>New Password</mat-label>\n                <input class=\"mat-input-element mat-form-field-autofill-control\" name=\"pass\" matInput placeholder=\"\" (keyup)=\"passwordChanged()\" #pass=\"ngModel\"\n                  [(ngModel)]=\"password\" type=\"password\" autocomplete=\"off\" minlength=\"6\" maxlength=\"40\" required>\n              </mat-form-field>\n            </div>\n            <div class=\"light-black font-14\">Please make sure your password has:</div>\n            <div>\n              <ul class=\"list-unstyled password-text-list font-12 pt-2\">\n                <li [ngClass]=\"{'error' : f.submitted  && password.length < 8, 'active' : password.length > 7 }\">\n                  At least 8 characters\n                </li>\n                <li [ngClass]=\"{'error' : f.submitted  && symbolMissing, 'active' : !symbolMissing  }\">\n                  With a Symbol\n                </li>\n                <li [ngClass]=\"{'error' : f.submitted && numberMissing, 'active' : !numberMissing  }\">\n                  A Number\n                </li>\n                <li [ngClass]=\"{'error' : f.submitted  && lowerCaseMissing, 'active' : !lowerCaseMissing  }\">\n                  And a lowercase letter .</li>\n                <li [ngClass]=\"{'error' : f.submitted  && upperCaseMissing, 'active' : !upperCaseMissing }\">\n                  And a capital letter</li>\n              </ul>\n              <span *ngIf=\"message\" class=\"mat-error\" style=\"font-size:12px;\">\n                {{message}}\n              </span>\n            </div>\n            <button mat-raised-button color=\"primary\"\n              class=\"bg-color mt-3 font-weight-500 w-100 sign-btn-size-12 sign-btn\">SAVE & SIGN IN</button>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/auth/password-reset/password-reset.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/auth/password-reset/password-reset.component.ts ***!
  \*****************************************************************/
/*! exports provided: PasswordResetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordResetComponent", function() { return PasswordResetComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_service/auth.service */ "./src/app/auth/_service/auth.service.ts");
/* harmony import */ var src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var src_app_shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/_shared/confirmation-dialogue/confirmation-dialogue.service */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts");






var PasswordResetComponent = /** @class */ (function () {
    function PasswordResetComponent(auth, router, activeRoute, confirm, alert) {
        this.auth = auth;
        this.router = router;
        this.activeRoute = activeRoute;
        this.confirm = confirm;
        this.alert = alert;
        this.password = "";
        this.symbolMissing = true;
        this.numberMissing = true;
        this.upperCaseMissing = true;
        this.lowerCaseMissing = true;
        this.isPage = false;
        this.message = "";
        this.password = "";
    }
    PasswordResetComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activeRoute.params.subscribe(function (params) {
            _this.token = params.token;
            _this.auth.checkResetToken(_this.token).subscribe(function (res) {
                _this.isPage = true;
            }, function (error) {
                _this.message = error.error.details;
                //  this.alert.success("Your password has been updated.", true, "center");
                _this.alert.error(error.error.details, true);
                _this.router.navigate(["/login"]);
            });
        });
    };
    PasswordResetComponent.prototype.resetPassword = function () {
        var _this = this;
        this.message = "";
        if (!this.symbolMissing &&
            !this.upperCaseMissing &&
            !this.lowerCaseMissing &&
            !this.numberMissing) {
            this.auth.changePassword(this.password, this.token).subscribe(function (res) {
                _this.alert.success("Your password has been updated.", true, "center");
                _this.router.navigate(["/login"]);
                // this.confirm
                //   .confirm("Success", "Your password has been updated.", "OK", false)
                //   .subscribe(() => {
                //     this.router.navigate(["/login"]);
                //   });
            }, function (error) {
                _this.message = error.error.details;
                // this.alert.error(error.error.details, true);
            });
        }
    };
    PasswordResetComponent.prototype.passwordChanged = function () {
        var password = this.password;
        if (/\d/.test(password)) {
            this.numberMissing = false;
        }
        else {
            this.numberMissing = true;
        }
        if (/[a-z]/.test(password)) {
            this.lowerCaseMissing = false;
        }
        else {
            this.lowerCaseMissing = true;
        }
        if (/[A-Z]/.test(password)) {
            this.upperCaseMissing = false;
        }
        else {
            this.upperCaseMissing = true;
        }
        if (/[^\w\s]/gi.test(password)) {
            this.symbolMissing = false;
        }
        else {
            this.symbolMissing = true;
        }
    };
    PasswordResetComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-password-reset",
            template: __webpack_require__(/*! ./password-reset.component.html */ "./src/app/auth/password-reset/password-reset.component.html"),
            styles: [__webpack_require__(/*! ./password-reset.component.css */ "./src/app/auth/password-reset/password-reset.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmationDialogueService"],
            src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], PasswordResetComponent);
    return PasswordResetComponent;
}());



/***/ }),

/***/ "./src/app/auth/reset/reset.component.css":
/*!************************************************!*\
  !*** ./src/app/auth/reset/reset.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-color\n{\n    width: 100%;\n    background-color: #13b5ea;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9yZXNldC9yZXNldC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLFdBQVc7SUFDWCx5QkFBeUI7QUFDN0IiLCJmaWxlIjoic3JjL2FwcC9hdXRoL3Jlc2V0L3Jlc2V0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctY29sb3JcbntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTNiNWVhO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/auth/reset/reset.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/reset/reset.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-auth-header></app-auth-header>\n<!-- <div *ngIf=\"showAlert\" _ngcontent-c1=\"\" class=\"py-2 d-flex justify-content-between bg-nav text-color px-3 mx-3 my-3 \">\n  <span _ngcontent-c1=\"\"> {{message}}</span><span _ngcontent-c1=\"\" (click)=\"closeAlert()\"> CLOSE </span></div> -->\n<section class=\"pt-5\">\n  <div class=\"container\">\n    <div class=\"\">\n      <form class=\"example-form  form-own\" #f=\"ngForm\" (submit)=\"f.touched && f.valid && reset()\" autocomplete=\"off\"\n        novalidate>\n        <div class=\"row justify-content-center mx-0\">\n          <div class=\"sign-in-screen-section\">\n            <div class=\"form-group\"\n              [ngClass]=\"{'error' : (f.submitted || mail.touched) && (mail.hasError('email')  || mail.hasError('required')) }\">\n              <div class=\"pb-4 text-center form-heading\">REQUEST PASSWORD RESET</div>\n              <mat-form-field class=\"example-full-width sign-input-2 mat-form-field\" floatLabel=\"always\">\n                <mat-label>Email</mat-label>\n                <input class=\"mat-input-element mat-form-field-autofill-control\" name=\"username\" type=\"text\" email matInput placeholder=\"\" #mail=\"ngModel\" [(ngModel)]=\"email\"\n                  maxlength=\"240\" minlength=\"5\" required autocomplete=\"off\">\n                <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                <mat-error *ngIf=\"mail.hasError('minlength') && !mail.hasError('email') && !mail.hasError('required')\">\n                  Email should be atleast 5 character long\n                </mat-error>\n                <mat-error *ngIf=\"mail.hasError('email') && !mail.hasError('required')\">\n                  Please enter a valid email address\n                </mat-error>\n                <mat-error *ngIf=\"mail.hasError('required')\">\n                  Please enter your email address to reset password\n                </mat-error>\n              </mat-form-field>\n              <span *ngIf=\"message\" class=\"mat-error\" style=\"font-size:12px;\">\n                {{message}}\n              </span>\n            </div>\n            <button mat-raised-button color=\"primary\" class=\"bg-color mt-3 mb-4 sign-btn-size-12 sign-btn\">RESET\n              PASSWORD</button>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/auth/reset/reset.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/reset/reset.component.ts ***!
  \***********************************************/
/*! exports provided: ResetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetComponent", function() { return ResetComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_service/auth.service */ "./src/app/auth/_service/auth.service.ts");
/* harmony import */ var src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var src_app_shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/_shared/confirmation-dialogue/confirmation-dialogue.service */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts");






var ResetComponent = /** @class */ (function () {
    function ResetComponent(auth, router, alert, confirm) {
        this.auth = auth;
        this.router = router;
        this.alert = alert;
        this.confirm = confirm;
        this.email = "";
    }
    ResetComponent.prototype.ngOnInit = function () { };
    ResetComponent.prototype.reset = function () {
        var _this = this;
        this.message = "";
        this.auth.forgotPassword(this.email).subscribe(function (res) {
            _this.message =
                "Check your email for instructions on resetting your password.";
            _this.alert.success(_this.message, true, 'center');
            // this.confirm
            //   .confirm(
            //     "Success",
            //     "Check your email for instructions on resetting your password.",
            //     "OK",
            //     false
            //   )
            //   .subscribe(() => {
            //     this.router.navigate(["/login"]);
            //   });
            _this.router.navigate(["/login"]);
        }, function (error) {
            _this.message = error.error.details ? error.error.details : "Something went wrong on server";
        });
    };
    ResetComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-reset",
            template: __webpack_require__(/*! ./reset.component.html */ "./src/app/auth/reset/reset.component.html"),
            styles: [__webpack_require__(/*! ./reset.component.css */ "./src/app/auth/reset/reset.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"],
            src_app_shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmationDialogueService"]])
    ], ResetComponent);
    return ResetComponent;
}());



/***/ })

}]);
//# sourceMappingURL=auth-auth-module.js.map