(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./auth/auth.module": [
		"./src/app/auth/auth.module.ts",
		"common",
		"auth-auth-module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/dashboard/dashboard.module.ts",
		"common",
		"dashboard-dashboard-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/_models/_config.ts":
/*!************************************!*\
  !*** ./src/app/_models/_config.ts ***!
  \************************************/
/*! exports provided: apiBaseUrl, URLS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiBaseUrl", function() { return apiBaseUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URLS", function() { return URLS; });
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");

var apiBaseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].baseUrl;
var URLS = {
    login: apiBaseUrl + "user/login",
    changePassword: apiBaseUrl + "changepassword",
    forgotPassword: apiBaseUrl + "resetpassword",
    getUsers: apiBaseUrl + "user/getusers",
    addUser: apiBaseUrl + "user/add-user",
    updateUser: apiBaseUrl + "user/update",
    updatePassword: apiBaseUrl + "updatepassword",
    deleteUser: apiBaseUrl + "deleteuser/",
    acticateUser: apiBaseUrl + "activateuser/",
    getuserprofile: apiBaseUrl + "user/getuserprofile",
    getProjects: apiBaseUrl + "project/getproject",
    generateProjectId: apiBaseUrl + "project/genrateprojectid",
    updateProject: apiBaseUrl + "project/updateproject/",
    getProjectDetails: apiBaseUrl + "project/getproject/",
    getUsersList: apiBaseUrl + "user/list",
    getCatalogList: apiBaseUrl + "rightline/cataloglist",
    searchEmail: apiBaseUrl + "user/searchemail/",
    getContactList: apiBaseUrl + "rightline/contactlist",
    submitProjectSU: apiBaseUrl + "project/submitprojectmemo/",
    submitProjectBA: apiBaseUrl + "rightline/submitprojectmemo/",
    deleteProjectMemo: apiBaseUrl + "project/removeproject/",
    searchProject: apiBaseUrl + "project/searchproject/",
    createUpdateAddendum: apiBaseUrl + "addendum/updateproject",
    getAddendumDetails: apiBaseUrl + "addendum/getproject/",
    submitAddendumSU: apiBaseUrl + 'addendum/submitaddendum/',
    submitAddendumBA: apiBaseUrl + "addendum/accept/",
    deleteAddendum: apiBaseUrl + "addendum/removeproject/",
    checkTokenExist: apiBaseUrl + "checkresettoken",
    getCountryCodeList: apiBaseUrl + "getcountrylist",
    getRightLineUserList: apiBaseUrl + "rightline/userlist"
};


/***/ }),

/***/ "./src/app/_models/pagination-model.ts":
/*!*********************************************!*\
  !*** ./src/app/_models/pagination-model.ts ***!
  \*********************************************/
/*! exports provided: PaginationModel, PropertyMetaModel, GridColumnType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationModel", function() { return PaginationModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropertyMetaModel", function() { return PropertyMetaModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridColumnType", function() { return GridColumnType; });
var PaginationModel = /** @class */ (function () {
    function PaginationModel() {
    }
    return PaginationModel;
}());

var PropertyMetaModel = /** @class */ (function () {
    function PropertyMetaModel() {
        this.PropertyType = GridColumnType.Text;
        this.PropertyType = GridColumnType.Text;
        this.ColumnName = "";
        this.DisplayText = "";
        this.PropertyName = "";
    }
    return PropertyMetaModel;
}());

var GridColumnType;
(function (GridColumnType) {
    GridColumnType[GridColumnType["Text"] = 0] = "Text";
    GridColumnType[GridColumnType["Link"] = 1] = "Link";
    GridColumnType[GridColumnType["Action"] = 2] = "Action";
    GridColumnType[GridColumnType["DisplayLink"] = 3] = "DisplayLink";
    GridColumnType[GridColumnType["Boolean"] = 4] = "Boolean";
    GridColumnType[GridColumnType["EditableText"] = 5] = "EditableText";
    GridColumnType[GridColumnType["Switch"] = 6] = "Switch";
    GridColumnType[GridColumnType["Checkbox"] = 7] = "Checkbox";
})(GridColumnType || (GridColumnType = {}));


/***/ }),

/***/ "./src/app/_services/shared-service.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_services/shared-service.service.ts ***!
  \*****************************************************/
/*! exports provided: SharedServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedServiceService", function() { return SharedServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");



var SharedServiceService = /** @class */ (function () {
    function SharedServiceService() {
        this.subject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    SharedServiceService.prototype.update = function (key, value) {
        // if (typeof value != 'string') {
        //   value = JSON.stringify(value);
        // }
        localStorage.setItem(key, value);
        this.subject.next();
    };
    // getMessage is method to take message from component
    SharedServiceService.prototype.onUpdate = function () {
        return this.subject.asObservable();
    };
    SharedServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SharedServiceService);
    return SharedServiceService;
}());



/***/ }),

/***/ "./src/app/_services/title/title.service.ts":
/*!**************************************************!*\
  !*** ./src/app/_services/title/title.service.ts ***!
  \**************************************************/
/*! exports provided: TitleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleService", function() { return TitleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var TitleService = /** @class */ (function () {
    function TitleService(titleService) {
        this.titleService = titleService;
    }
    TitleService.prototype.getTitle = function () {
        return this.titleService.getTitle();
    };
    TitleService.prototype.setTitle = function (newTitle) {
        this.titleService.setTitle(newTitle);
    };
    TitleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]])
    ], TitleService);
    return TitleService;
}());



/***/ }),

/***/ "./src/app/_shared/addapters/custom-mat-paginator.ts":
/*!***********************************************************!*\
  !*** ./src/app/_shared/addapters/custom-mat-paginator.ts ***!
  \***********************************************************/
/*! exports provided: CustomMatPaginatorIntl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomMatPaginatorIntl", function() { return CustomMatPaginatorIntl; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var CustomMatPaginatorIntl = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CustomMatPaginatorIntl, _super);
    function CustomMatPaginatorIntl() {
        var _this = _super.call(this) || this;
        _this.getRangeLabel = function (page, pageSize, length) {
            if (length === 0 || pageSize === 0) {
                return "0 / " + length;
            }
            length = Math.max(length, 0);
            var le = "   " + length;
            var startIndex = page * pageSize;
            var endIndex = startIndex < length
                ? Math.min(startIndex + pageSize, length)
                : startIndex + pageSize;
            return startIndex + 1 + " - " + endIndex + "  " + le;
        };
        _this.getAndInitTranslations();
        return _this;
    }
    CustomMatPaginatorIntl.prototype.getAndInitTranslations = function () {
        this.itemsPerPageLabel = "Rows per page : ";
        this.nextPageLabel = "next";
        this.previousPageLabel = "prev";
        this.changes.next();
    };
    CustomMatPaginatorIntl = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CustomMatPaginatorIntl);
    return CustomMatPaginatorIntl;
}(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginatorIntl"]));



/***/ }),

/***/ "./src/app/_shared/addapters/date.addapter.ts":
/*!****************************************************!*\
  !*** ./src/app/_shared/addapters/date.addapter.ts ***!
  \****************************************************/
/*! exports provided: MyDateAdapter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyDateAdapter", function() { return MyDateAdapter; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");


var MyDateAdapter = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](MyDateAdapter, _super);
    function MyDateAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MyDateAdapter.prototype.format = function (date, displayFormat) {
        if (displayFormat === void 0) { displayFormat = "input"; }
        if (displayFormat === "input") {
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            var d = this._to2digit(month) + "/" + this._to2digit(day) + "/" + this._to2digit(year);
            return d;
        }
        else {
            return date.toDateString();
        }
    };
    MyDateAdapter.prototype._to2digit = function (n) {
        return ("00" + n).slice(-2);
    };
    return MyDateAdapter;
}(_angular_material__WEBPACK_IMPORTED_MODULE_1__["NativeDateAdapter"]));



/***/ }),

/***/ "./src/app/_shared/alert/alert.component.css":
/*!***************************************************!*\
  !*** ./src/app/_shared/alert/alert.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".alert-msg {\n    padding: 15px 20px;\n    position: absolute;\n    top: 110px;\n    right: 0;\n    left: 0;\n    z-index: 999;\n    -webkit-animation: fadein 0.5s, fadeout 0.5s 1.5s;\n    animation: fadein 0.5s, fadeout 0.5s 1.5s;\n}\n\n.dashboard-alert.center-alert {\n    margin: 0;\n}\n\n.center-alert .alert-msg {\n    top: 48px;\n    left: 50%;\n    -webkit-transform: translateX(-50%);\n            transform: translateX(-50%);\n    -webkit-animation: fadein2 0.5s, fadeout2 0.5s 1.5s;\n    animation: fadein2 0.5s, fadeout2 0.5s 1.5s;\n}\n\n@-webkit-keyframes fadein {\n    from {\n        top: 0;\n        opacity: 0;\n    }\n    to {\n        top: 120px;\n        opacity: 1;\n    }\n}\n\n@keyframes fadein {\n    from {\n        top: 0;\n        opacity: 0;\n    }\n    to {\n        top: 120px;\n        opacity: 1;\n    }\n}\n\n@-webkit-keyframes fadein2 {\n    from {\n        top: 0;\n        opacity: 0;\n    }\n    to {\n        top: 120px;\n        opacity: 1;\n    }\n}\n\n@keyframes fadein2 {\n    from {\n        top: 0;\n        opacity: 0;\n    }\n    to {\n        top: 45px;\n        opacity: 1;\n    }\n}\n\n/* @-webkit-keyframes fadeout {\n    from {top: 120px; opacity: 1;}\n    to {top: 0; opacity: 0;}\n  }\n\n  @keyframes fadeout {\n    from {top: 120px; opacity: 1;}\n    to {top: 0; opacity: 0;}\n  } */\n\n.alert-msg.left {\n    right: auto;\n    left: 0;\n}\n\n.alert-msg.right {\n    left: auto;\n    right: 0;\n}\n\n.alert-msg.center {\n    -webkit-transform: translateX(-50%);\n            transform: translateX(-50%);\n    left: 50%;\n}\n\n.alert-msg>li {\n    position: relative;\n    margin-bottom: 10px;\n    pointer-events: all;\n}\n\n.alert-msg .alert-box-wrapper {\n    background-color: rgba(0, 0, 0, 0.87);\n    box-shadow: 0 0 5px #ddd;\n    padding: 6px 12px;\n    border-radius: 4px;\n    font-size: 15.8px;\n    letter-spacing: 0.25px;\n    color: #fff;\n    font-weight: 400;\n    padding-right: 70px;\n    box-shadow: 0 3px 4px rgba(0, 0, 0, 0.14), 0 3px 3px -2px rgba(0, 0, 0, 0.12), 0 1px 8px rgba(0, 0, 0, 0.2);\n    -webkit-border-radius: 4px;\n    -moz-border-radius: 4px;\n    -ms-border-radius: 4px;\n    -o-border-radius: 4px;\n}\n\n.close-alert {\n    position: absolute;\n    right: 15px;\n    top: 6.5px;\n    cursor: pointer;\n    letter-spacing: 1.25px;\n}\n\n.alert-type-txt {\n    margin-right: 15px;\n    border-right: 1px solid #ddd;\n    padding-right: 15px;\n}\n\n.alert-msg li p, .alert-msg-error li p {\n    margin-bottom: 0;\n}\n\n.alert-msg li .fa {\n    font-size: 35px;\n}\n\n.alert-heading {\n    font-size: 15px;\n    font-weight: 500;\n}\n\n.alert-msg-error li img.img-of-error {\n    max-width: 35px;\n    position: relative;\n    top: -8px;\n}\n\n.alert-msg-error {\n    background: rgba(177, 49, 40, 0.88);\n    color: #fff;\n    padding: 15px 15px;\n    border-radius: 4px;\n    box-shadow: 0 0 3px #a7a7a7;\n    position: fixed;\n    top: 30px;\n    z-index: 99;\n}\n\n.dashboard-alert {\n    position: relative;\n    margin-left: 175px;\n    pointer-events: none;\n}\n\n@media(max-width:400px) {\n    .alert-msg, .alert-msg-error {\n        width: 94%;\n        right: 3%;\n        left: 3%;\n    }\n}\n\n@media(max-width:767px) {\n    .dashboard-alert {\n        margin-left: 0;\n    }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvX3NoYXJlZC9hbGVydC9hbGVydC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsUUFBUTtJQUNSLE9BQU87SUFDUCxZQUFZO0lBQ1osaURBQWlEO0lBQ2pELHlDQUF5QztBQUM3Qzs7QUFFQTtJQUNJLFNBQVM7QUFDYjs7QUFFQTtJQUNJLFNBQVM7SUFDVCxTQUFTO0lBQ1QsbUNBQTJCO1lBQTNCLDJCQUEyQjtJQUMzQixtREFBbUQ7SUFDbkQsMkNBQTJDO0FBQy9DOztBQUVBO0lBQ0k7UUFDSSxNQUFNO1FBQ04sVUFBVTtJQUNkO0lBQ0E7UUFDSSxVQUFVO1FBQ1YsVUFBVTtJQUNkO0FBQ0o7O0FBRUE7SUFDSTtRQUNJLE1BQU07UUFDTixVQUFVO0lBQ2Q7SUFDQTtRQUNJLFVBQVU7UUFDVixVQUFVO0lBQ2Q7QUFDSjs7QUFFQTtJQUNJO1FBQ0ksTUFBTTtRQUNOLFVBQVU7SUFDZDtJQUNBO1FBQ0ksVUFBVTtRQUNWLFVBQVU7SUFDZDtBQUNKOztBQUVBO0lBQ0k7UUFDSSxNQUFNO1FBQ04sVUFBVTtJQUNkO0lBQ0E7UUFDSSxTQUFTO1FBQ1QsVUFBVTtJQUNkO0FBQ0o7O0FBQ0E7Ozs7Ozs7O0tBUUs7O0FBRUw7SUFDSSxXQUFXO0lBQ1gsT0FBTztBQUNYOztBQUVBO0lBQ0ksVUFBVTtJQUNWLFFBQVE7QUFDWjs7QUFFQTtJQUNJLG1DQUEyQjtZQUEzQiwyQkFBMkI7SUFDM0IsU0FBUztBQUNiOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxxQ0FBcUM7SUFDckMsd0JBQXdCO0lBQ3hCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQiwyR0FBMkc7SUFDM0csMEJBQTBCO0lBQzFCLHVCQUF1QjtJQUN2QixzQkFBc0I7SUFDdEIscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZUFBZTtJQUNmLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQiw0QkFBNEI7SUFDNUIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLFNBQVM7QUFDYjs7QUFFQTtJQUNJLG1DQUFtQztJQUNuQyxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQiwyQkFBMkI7SUFDM0IsZUFBZTtJQUNmLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJO1FBQ0ksVUFBVTtRQUNWLFNBQVM7UUFDVCxRQUFRO0lBQ1o7QUFDSjs7QUFFQTtJQUNJO1FBQ0ksY0FBYztJQUNsQjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvX3NoYXJlZC9hbGVydC9hbGVydC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFsZXJ0LW1zZyB7XG4gICAgcGFkZGluZzogMTVweCAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDExMHB4O1xuICAgIHJpZ2h0OiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgei1pbmRleDogOTk5O1xuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlaW4gMC41cywgZmFkZW91dCAwLjVzIDEuNXM7XG4gICAgYW5pbWF0aW9uOiBmYWRlaW4gMC41cywgZmFkZW91dCAwLjVzIDEuNXM7XG59XG5cbi5kYXNoYm9hcmQtYWxlcnQuY2VudGVyLWFsZXJ0IHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5jZW50ZXItYWxlcnQgLmFsZXJ0LW1zZyB7XG4gICAgdG9wOiA0OHB4O1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gICAgLXdlYmtpdC1hbmltYXRpb246IGZhZGVpbjIgMC41cywgZmFkZW91dDIgMC41cyAxLjVzO1xuICAgIGFuaW1hdGlvbjogZmFkZWluMiAwLjVzLCBmYWRlb3V0MiAwLjVzIDEuNXM7XG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlaW4ge1xuICAgIGZyb20ge1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIG9wYWNpdHk6IDA7XG4gICAgfVxuICAgIHRvIHtcbiAgICAgICAgdG9wOiAxMjBweDtcbiAgICAgICAgb3BhY2l0eTogMTtcbiAgICB9XG59XG5cbkBrZXlmcmFtZXMgZmFkZWluIHtcbiAgICBmcm9tIHtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBvcGFjaXR5OiAwO1xuICAgIH1cbiAgICB0byB7XG4gICAgICAgIHRvcDogMTIwcHg7XG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgfVxufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZWluMiB7XG4gICAgZnJvbSB7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgb3BhY2l0eTogMDtcbiAgICB9XG4gICAgdG8ge1xuICAgICAgICB0b3A6IDEyMHB4O1xuICAgICAgICBvcGFjaXR5OiAxO1xuICAgIH1cbn1cblxuQGtleWZyYW1lcyBmYWRlaW4yIHtcbiAgICBmcm9tIHtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBvcGFjaXR5OiAwO1xuICAgIH1cbiAgICB0byB7XG4gICAgICAgIHRvcDogNDVweDtcbiAgICAgICAgb3BhY2l0eTogMTtcbiAgICB9XG59XG4vKiBALXdlYmtpdC1rZXlmcmFtZXMgZmFkZW91dCB7XG4gICAgZnJvbSB7dG9wOiAxMjBweDsgb3BhY2l0eTogMTt9XG4gICAgdG8ge3RvcDogMDsgb3BhY2l0eTogMDt9XG4gIH1cblxuICBAa2V5ZnJhbWVzIGZhZGVvdXQge1xuICAgIGZyb20ge3RvcDogMTIwcHg7IG9wYWNpdHk6IDE7fVxuICAgIHRvIHt0b3A6IDA7IG9wYWNpdHk6IDA7fVxuICB9ICovXG5cbi5hbGVydC1tc2cubGVmdCB7XG4gICAgcmlnaHQ6IGF1dG87XG4gICAgbGVmdDogMDtcbn1cblxuLmFsZXJ0LW1zZy5yaWdodCB7XG4gICAgbGVmdDogYXV0bztcbiAgICByaWdodDogMDtcbn1cblxuLmFsZXJ0LW1zZy5jZW50ZXIge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgICBsZWZ0OiA1MCU7XG59XG5cbi5hbGVydC1tc2c+bGkge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIHBvaW50ZXItZXZlbnRzOiBhbGw7XG59XG5cbi5hbGVydC1tc2cgLmFsZXJ0LWJveC13cmFwcGVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODcpO1xuICAgIGJveC1zaGFkb3c6IDAgMCA1cHggI2RkZDtcbiAgICBwYWRkaW5nOiA2cHggMTJweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZm9udC1zaXplOiAxNS44cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMjVweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIHBhZGRpbmctcmlnaHQ6IDcwcHg7XG4gICAgYm94LXNoYWRvdzogMCAzcHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDNweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDhweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAtby1ib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi5jbG9zZS1hbGVydCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAxNXB4O1xuICAgIHRvcDogNi41cHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjI1cHg7XG59XG5cbi5hbGVydC10eXBlLXR4dCB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNkZGQ7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbn1cblxuLmFsZXJ0LW1zZyBsaSBwLCAuYWxlcnQtbXNnLWVycm9yIGxpIHAge1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5hbGVydC1tc2cgbGkgLmZhIHtcbiAgICBmb250LXNpemU6IDM1cHg7XG59XG5cbi5hbGVydC1oZWFkaW5nIHtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLmFsZXJ0LW1zZy1lcnJvciBsaSBpbWcuaW1nLW9mLWVycm9yIHtcbiAgICBtYXgtd2lkdGg6IDM1cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogLThweDtcbn1cblxuLmFsZXJ0LW1zZy1lcnJvciB7XG4gICAgYmFja2dyb3VuZDogcmdiYSgxNzcsIDQ5LCA0MCwgMC44OCk7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgcGFkZGluZzogMTVweCAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3gtc2hhZG93OiAwIDAgM3B4ICNhN2E3YTc7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMzBweDtcbiAgICB6LWluZGV4OiA5OTtcbn1cblxuLmRhc2hib2FyZC1hbGVydCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbi1sZWZ0OiAxNzVweDtcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cblxuQG1lZGlhKG1heC13aWR0aDo0MDBweCkge1xuICAgIC5hbGVydC1tc2csIC5hbGVydC1tc2ctZXJyb3Ige1xuICAgICAgICB3aWR0aDogOTQlO1xuICAgICAgICByaWdodDogMyU7XG4gICAgICAgIGxlZnQ6IDMlO1xuICAgIH1cbn1cblxuQG1lZGlhKG1heC13aWR0aDo3NjdweCkge1xuICAgIC5kYXNoYm9hcmQtYWxlcnQge1xuICAgICAgICBtYXJnaW4tbGVmdDogMDtcbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/_shared/alert/alert.component.html":
/*!****************************************************!*\
  !*** ./src/app/_shared/alert/alert.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"dashboard-alert\"\n  [ngClass]=\"{ 'left' : message.position == 'left', 'center-alert': message.position == 'center' }\">\n  <ul *ngIf=\"showerror || showsuccess\" class=\"alert-msg list-unstyled list-inline\">\n    <li>\n      <div class=\"d-flex flex-wrap alert-box-wrapper\">\n        <span class=\"close-alert\" (click)=\"closeAlert()\"> CLOSE </span>\n        <div *ngIf=\"false\" class=\"alert-type-txt\">Sucsess</div>\n        <div class=\"alert-body-txt\">{{message && message.text ? message.text : 'Something went wrong'}}</div>\n      </div>\n    </li>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/_shared/alert/alert.component.ts":
/*!**************************************************!*\
  !*** ./src/app/_shared/alert/alert.component.ts ***!
  \**************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./alert.service */ "./src/app/_shared/alert/alert.service.ts");



var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
        this.message = "";
        this.showerror = false;
        this.showsuccess = false;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getMessage().subscribe(function (message) {
            if (message) {
                _this.message = message;
                if (_this.message.type === "success") {
                    _this.showsuccess = true;
                    _this.setHide();
                }
                if (_this.message.type === "error") {
                    _this.setHide();
                    _this.showerror = true;
                }
            }
        });
    };
    AlertComponent.prototype.setHide = function () {
        var _this = this;
        setTimeout(function () {
            _this.showerror = false;
            _this.showsuccess = false;
        }, 10000);
    };
    AlertComponent.prototype.closeModal = function () {
        this.showerror = false;
        this.showsuccess = false;
    };
    AlertComponent.prototype.closeAlert = function () {
        this.showerror = false;
        this.showsuccess = false;
    };
    AlertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-alert",
            template: __webpack_require__(/*! ./alert.component.html */ "./src/app/_shared/alert/alert.component.html"),
            styles: [__webpack_require__(/*! ./alert.component.css */ "./src/app/_shared/alert/alert.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alert_service__WEBPACK_IMPORTED_MODULE_2__["AlertService"]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/_shared/alert/alert.service.ts":
/*!************************************************!*\
  !*** ./src/app/_shared/alert/alert.service.ts ***!
  \************************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    // this.subject.next();
                }
            }
        });
    }
    // OnSuccess Show message
    AlertService.prototype.success = function (message, keepAfterNavigationChange, position) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        if (position === void 0) { position = ""; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message, position: position });
    };
    AlertService.prototype.showPopup = function (message) {
        this.subject.next({ type: 'modal', text: message });
    };
    // Onerror show message
    AlertService.prototype.error = function (message, keepAfterNavigationChange, position) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        if (position === void 0) { position = ""; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message, position: position });
    };
    // getMessage is method to take message from component
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".without-top-border {\n    border-top: 0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvX3NoYXJlZC9jb25maXJtYXRpb24tZGlhbG9ndWUvY29uZmlybWF0aW9uLWRpYWxvZ3VlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSx3QkFBd0I7QUFDNUIiLCJmaWxlIjoic3JjL2FwcC9fc2hhcmVkL2NvbmZpcm1hdGlvbi1kaWFsb2d1ZS9jb25maXJtYXRpb24tZGlhbG9ndWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53aXRob3V0LXRvcC1ib3JkZXIge1xuICAgIGJvcmRlci10b3A6IDAgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div tabindex=\"-1\" class=\"modal confirmation-modal\" id=\"app-confirmation\" data-backdrop=\"static\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n    <div class=\"modal-content\">\n        <input class=\"firstFocus\" type=\"text\">\n      <!-- Modal body -->\n      <input type=\"text\" class=\"firstFocus\">\n      <div class=\"modal-body font-14\">\n        <div class=\"pb-3 text-color font-16 font-weight-500\">{{confirmationTitle}}</div>\n        <div class=\"\">{{confirmationMessage}}</div>\n      </div>\n      <!-- Modal footer -->\n      <div class=\"modal-footer\" [ngClass]=\"{'without-top-border':  !showCancleBtn }\">\n        <button *ngIf=\"showCancleBtn\" id=\"app-confirmation-cancle\" (click)=\"cancle()\" type=\"button\" mat-raised-button\n          color=\"\" class=\"w-50 print-submit-btn text-uppercase white-btn cancel-btn modal-cancel\">CANCEL</button>\n        <button mat-raised-button color=\"primary\" class=\"print-submit-btn text-uppercase w-50 modal-confirm\"\n          (click)=\"confirm()\">{{ btnTitle ? btnTitle : 'CONFIRM' }}</button>\n      </div>\n      <input type=\"text\" class=\"lastFocus\">\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ConfirmationDialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationDialogueComponent", function() { return ConfirmationDialogueComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirmation-dialogue.service */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts");



var ConfirmationDialogueComponent = /** @class */ (function () {
    function ConfirmationDialogueComponent(confirmation) {
        this.confirmation = confirmation;
        this.showCancleBtn = true;
    }
    ConfirmationDialogueComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.confirmation.ConfirmationSubscription().subscribe(function (res) {
            // tslint:disable-next-line:no-
            _this.subject = res.subject;
            _this.showConfirmationBox(res.title, res.message, res.btnOkText, res.showCancleBtn);
        });
    };
    ConfirmationDialogueComponent.prototype.ngAfterViewInit = function () {
        $(document).on("keydown", ".modal-confirm", function (event) {
            if (event.keyCode === 9) {
                event.preventDefault();
                $(".modal-cancel").focus();
            }
        });
    };
    ConfirmationDialogueComponent.prototype.showConfirmationBox = function (title, message, btnOkText, showCancleBtn) {
        if (showCancleBtn === void 0) { showCancleBtn = true; }
        this.confirmationMessage = message;
        this.confirmationTitle = title;
        this.btnTitle = btnOkText;
        this.showCancleBtn = showCancleBtn;
        $("#app-confirmation").modal("show");
        // $("#app-confirmation-cancle").focus();
    };
    ConfirmationDialogueComponent.prototype.confirm = function () {
        this.subject.next(true);
        $("#app-confirmation").modal("hide");
    };
    ConfirmationDialogueComponent.prototype.cancle = function () {
        $("#app-confirmation").modal("hide");
        // this.subject.next(false);
    };
    ConfirmationDialogueComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-confirmation-dialogue",
            template: __webpack_require__(/*! ./confirmation-dialogue.component.html */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.html"),
            styles: [__webpack_require__(/*! ./confirmation-dialogue.component.css */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationDialogueService"]])
    ], ConfirmationDialogueComponent);
    return ConfirmationDialogueComponent;
}());



/***/ }),

/***/ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts":
/*!********************************************************************************!*\
  !*** ./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts ***!
  \********************************************************************************/
/*! exports provided: ConfirmationDialogueService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationDialogueService", function() { return ConfirmationDialogueService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var ConfirmationDialogueService = /** @class */ (function () {
    function ConfirmationDialogueService() {
        this.confirmation = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    ConfirmationDialogueService.prototype.ngAfterViewInit = function () {
    };
    ConfirmationDialogueService.prototype.confirm = function (title, message, btnOkText, showCancleBtn, btnCancelText, dialogSize) {
        if (btnOkText === void 0) { btnOkText = 'OK'; }
        if (showCancleBtn === void 0) { showCancleBtn = true; }
        if (btnCancelText === void 0) { btnCancelText = 'CANCLE'; }
        if (dialogSize === void 0) { dialogSize = 'sm'; }
        var subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.confirmation.next({ subject: subject, title: title, message: message, btnOkText: btnOkText, showCancleBtn: showCancleBtn, btnCancelText: btnCancelText, dialogSize: dialogSize });
        return subject.asObservable();
    };
    ConfirmationDialogueService.prototype.ConfirmationSubscription = function () {
        return this.confirmation.asObservable();
    };
    ConfirmationDialogueService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConfirmationDialogueService);
    return ConfirmationDialogueService;
}());



/***/ }),

/***/ "./src/app/_shared/grid/grid.component.css":
/*!*************************************************!*\
  !*** ./src/app/_shared/grid/grid.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL19zaGFyZWQvZ3JpZC9ncmlkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/_shared/grid/grid.component.html":
/*!**************************************************!*\
  !*** ./src/app/_shared/grid/grid.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"container-fluid own-dashboard-section\">\n  <section class=\"wrapper-section pb-3\">\n    <div class=\"data-border-md\">\n      <div class=\"date-selection-2 filter-date-section\">\n        <p class=\"filter-p\">\n          Filter by Type\n        </p>\n        <div class=\"dropdown dropdown-filter\">\n          <div class=\"font-14 dropdown-txt-black dropdown-selected-text cursor\">\n            <span>Showing {{filters.type.length}} of {{userTypes.length}}</span>\n          </div>\n          <div class=\"dropdown-select-wrapper\">\n            <ul class=\"list-unstyled checkbox-list dropdown-items font-14\">\n              <li (click)=\"addTypesFilter(item.value)\" *ngFor=\"let item of userTypes\">\n                <mat-checkbox [checked]=\"item.selected\">{{item.value}}</mat-checkbox>\n              </li>\n            </ul>\n            <div onclick=\"closeDropdown()\" class=\"text-right padding-left-15 pr-4 pb-1 pt-2 text-color font-14\">\n              DONE\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"data-border-md\">\n      <div class=\"date-selection-2 filter-date-section\">\n        <p class=\"filter-p\">\n          Filter by Status\n        </p>\n        <div class=\"dropdown dropdown-filter\">\n          <div class=\"font-14 dropdown-txt-black dropdown-selected-text cursor\">\n            <span>Showing {{filters.status.length}} of {{usersStatus.length}}</span>\n          </div>\n          <div class=\"dropdown-select-wrapper\">\n            <ul class=\"list-unstyled checkbox-list dropdown-items font-14\">\n              <li (click)=\"addStatusFilter(item.value)\" *ngFor=\"let item of usersStatus\">\n                <mat-checkbox [checked]=\"item.selected\">{{item.value}}</mat-checkbox>\n              </li>\n            </ul>\n            <div onclick=\"closeDropdown()\" class=\"text-right padding-left-15 pr-4 pb-1 pt-2 text-color font-14\">\n              DONE\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"data-border-sm text-center reset-btn\">\n      <div class=\"date-selection-2 d-flex justify-content-center cursor filter-date-section\">\n        <div class=\"filter-p filter-font\">\n          <div (click)=\"reset()\">\n            RESET\n          </div>\n          <div (click)=\"filter()\">\n            FILTERS\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"own-table-dashboard mb-3\" *ngIf=\"usersList && usersList.length > 0\">\n    <div class=\"table-responsive\">\n      <table class=\"table table-hover mb-0\">\n        <thead>\n          <tr>\n            <th scope=\"col\" (click)=\"sort('firstName')\">First<i *ngIf=\"filters.sort == 'firstName'\"\n                class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-down': filters.order == 'asc', 'fa-arrow-up': filters.order == 'desc'}\"></i>\n            </th>\n            <th scope=\"col\" (click)=\"sort('lastName')\">Last<i *ngIf=\"filters.sort == 'lastName'\"\n                class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-down': filters.order == 'asc', 'fa-arrow-up': filters.order == 'desc'}\"></i>\n            </th>\n            <th scope=\"col\" (click)=\"sort('email')\">Email<i *ngIf=\"filters.sort == 'email'\" class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-down': filters.order == 'asc', 'fa-arrow-up': filters.order == 'desc'}\"></i>\n            </th>\n            <th scope=\"col\" (click)=\"sort('type')\">Type<i *ngIf=\"filters.sort == 'type'\" class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-down': filters.order == 'asc', 'fa-arrow-up': filters.order == 'desc'}\"></i>\n            </th>\n            <th scope=\"col\" (click)=\"sort('status')\">Status<i *ngIf=\"filters.sort == 'status'\" class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-down': filters.order == 'asc', 'fa-arrow-up': filters.order == 'desc'}\"></i>\n            </th>\n            <th scope=\"col\" (click)=\"sort('last_login')\">Last Login<i *ngIf=\"filters.sort == 'last_login'\"\n                class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-down': filters.order == 'asc', 'fa-arrow-up': filters.order == 'desc'}\"></i>\n            </th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let user of usersList\" (click)=\"update(user)\">\n            <td>{{user.firstName}}</td>\n            <td>{{user.lastName}}</td>\n            <td>{{user.email}}</td>\n            <td>{{user.type}}</td>\n            <td>{{user.status}}</td>\n            <td>{{user.last_login}}</td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n    <mat-paginator itemsPerPageLabel=\"rows per page\" (page)=\"pageChanged($event)\" [length]=\"totalUsers\"\n      [pageSize]=\"30\" [pageSizeOptions]=\"[5, 10, 20, 30, 50, 70, 100]\">\n    </mat-paginator>\n  </section>\n  <app-welcome-page *ngIf=\"!(usersList && usersList.length > 0)\" (clear)=\"reset()\"></app-welcome-page>\n</div> -->"

/***/ }),

/***/ "./src/app/_shared/grid/grid.component.ts":
/*!************************************************!*\
  !*** ./src/app/_shared/grid/grid.component.ts ***!
  \************************************************/
/*! exports provided: GridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridComponent", function() { return GridComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_pagination_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_models/pagination-model */ "./src/app/_models/pagination-model.ts");



var GridComponent = /** @class */ (function () {
    function GridComponent() {
        this.pageInfoChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.action = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.indexer = 0;
    }
    GridComponent.prototype.ngOnChanges = function (changes) {
        this.setColumns();
        this.setPagination();
    };
    GridComponent.prototype.setPagination = function () {
        if (this.pageInfo && this.pageInfo.TotalRecord > this.pageInfo.PageSize) {
            var totalPages = Math.ceil(this.pageInfo.TotalRecord / this.pageInfo.PageSize);
            this.LastPage = totalPages;
            this.totalPage = this.getVisiblePageIndex(totalPages, this.pageInfo.CurrentPage);
            // this.showPagination = true;
        }
        else {
            // this.showPagination = false;
        }
    };
    GridComponent.prototype.setColumns = function () {
        if (this.displayColumns) {
            this.cols = Object.assign([], this.displayColumns);
        }
        else {
            if (this.gridData && this.gridData.length > 0) {
                var props = Object.keys(this.gridData[0]).filter(function (x) { return !x.toLowerCase().endsWith("id"); });
                this.cols = props.map(function (x) {
                    return {
                        ColumnName: x.toUpperCase(),
                        PropertyName: x,
                        PropertyType: src_app_models_pagination_model__WEBPACK_IMPORTED_MODULE_2__["GridColumnType"].Text
                    };
                });
            }
            else {
                this.cols = [];
            }
        }
    };
    GridComponent.prototype.SetPage = function (index) {
        this.pageInfo.CurrentPage = Number(index);
        this.emitPageInfoChange();
    };
    GridComponent.prototype.changePageSize = function (val) {
        this.pageInfo.PageSize = Number(val);
        this.emitPageInfoChange();
    };
    GridComponent.prototype.emitPageInfoChange = function () {
        this.pageInfoChange.emit(this.pageInfo);
    };
    GridComponent.prototype.getVisiblePageIndex = function (totalPages, CurrentPage) {
        var array = [];
        if (totalPages > 7) {
            var j = 1;
            for (var i = 0; i < 7 || array.length < 7; i++) {
                if (CurrentPage - (3 - i) >= 1 || CurrentPage - (3 - i) <= totalPages) {
                    if (CurrentPage - (3 - i) > 0) {
                        if (CurrentPage - (3 - i) <= totalPages) {
                            array.push(CurrentPage - (3 - i));
                        }
                        else {
                            array.unshift(CurrentPage - (3 + j));
                            j++;
                        }
                    }
                }
            }
            return array;
        }
        else {
            for (var i = 1; i <= totalPages; i++) {
                array.push(i);
            }
            return array;
        }
    };
    GridComponent.prototype.ActionButtoClick = function (data, action, event) {
        if (event) {
            event.preventDefault();
        }
        this.action.emit({ action: action, value: data });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GridComponent.prototype, "gridData", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_models_pagination_model__WEBPACK_IMPORTED_MODULE_2__["PaginationModel"])
    ], GridComponent.prototype, "pageInfo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GridComponent.prototype, "displayColumns", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GridComponent.prototype, "pageInfoChange", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GridComponent.prototype, "action", void 0);
    GridComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-grid',
            template: __webpack_require__(/*! ./grid.component.html */ "./src/app/_shared/grid/grid.component.html"),
            styles: [__webpack_require__(/*! ./grid.component.css */ "./src/app/_shared/grid/grid.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GridComponent);
    return GridComponent;
}());



/***/ }),

/***/ "./src/app/_shared/loader/loader.component.css":
/*!*****************************************************!*\
  !*** ./src/app/_shared/loader/loader.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loader {\n    border: 6px solid #f3f3f3;\n     border-radius: 50%;\n     border-top:6px solid #21b8c6;\n     width: 35px;\n     height: 35px;\n     -webkit-animation: spin 2s linear infinite;\n     animation: spin 2s linear infinite;\n       position: fixed;\n       z-index: 130;\n       top:45%;\n       left:50%;\n        -webkit-transform: translate(-50%, -50%);\n                transform: translate(-50%, -50%);\n   }\n   \n   @-webkit-keyframes spin {\n       0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }\n       100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }\n   }\n   \n   @keyframes spin {\n       0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }\n       100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }\n   }\n   \n   .overlay-bg{\n       height: 100vh;\n       width: 100vw;\n       position: fixed;\n       z-index: 9999;\n       background: rgba(0, 0, 0, 0.7);\n       top:0;\n   }\n   \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvX3NoYXJlZC9sb2FkZXIvbG9hZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSx5QkFBeUI7S0FDeEIsa0JBQWtCO0tBQ2xCLDRCQUE0QjtLQUM1QixXQUFXO0tBQ1gsWUFBWTtLQUNaLDBDQUEwQztLQUMxQyxrQ0FBa0M7T0FDaEMsZUFBZTtPQUNmLFlBQVk7T0FDWixPQUFPO09BQ1AsUUFBUTtRQUNQLHdDQUFnQztnQkFBaEMsZ0NBQWdDO0dBQ3JDOztHQUVBO09BQ0ksS0FBSywrQkFBdUIsRUFBdkIsdUJBQXVCLEVBQUU7T0FDOUIsT0FBTyxpQ0FBeUIsRUFBekIseUJBQXlCLEVBQUU7R0FDdEM7O0dBSEE7T0FDSSxLQUFLLCtCQUF1QixFQUF2Qix1QkFBdUIsRUFBRTtPQUM5QixPQUFPLGlDQUF5QixFQUF6Qix5QkFBeUIsRUFBRTtHQUN0Qzs7R0FFQTtPQUNJLGFBQWE7T0FDYixZQUFZO09BQ1osZUFBZTtPQUNmLGFBQWE7T0FDYiw4QkFBOEI7T0FDOUIsS0FBSztHQUNUIiwiZmlsZSI6InNyYy9hcHAvX3NoYXJlZC9sb2FkZXIvbG9hZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGVyIHtcbiAgICBib3JkZXI6IDZweCBzb2xpZCAjZjNmM2YzO1xuICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgIGJvcmRlci10b3A6NnB4IHNvbGlkICMyMWI4YzY7XG4gICAgIHdpZHRoOiAzNXB4O1xuICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcbiAgICAgYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcbiAgICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICAgei1pbmRleDogMTMwO1xuICAgICAgIHRvcDo0NSU7XG4gICAgICAgbGVmdDo1MCU7XG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAgfVxuICAgXG4gICBAa2V5ZnJhbWVzIHNwaW4ge1xuICAgICAgIDAlIHsgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7IH1cbiAgICAgICAxMDAlIHsgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgfVxuICAgfVxuICAgXG4gICAub3ZlcmxheS1iZ3tcbiAgICAgICBoZWlnaHQ6IDEwMHZoO1xuICAgICAgIHdpZHRoOiAxMDB2dztcbiAgICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICAgei1pbmRleDogOTk5OTtcbiAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNyk7XG4gICAgICAgdG9wOjA7XG4gICB9XG4gICAiXX0= */"

/***/ }),

/***/ "./src/app/_shared/loader/loader.component.html":
/*!******************************************************!*\
  !*** ./src/app/_shared/loader/loader.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay-bg\">\n  <div class=\"loader\"></div>\n</div>"

/***/ }),

/***/ "./src/app/_shared/loader/loader.component.ts":
/*!****************************************************!*\
  !*** ./src/app/_shared/loader/loader.component.ts ***!
  \****************************************************/
/*! exports provided: LoaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderComponent", function() { return LoaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LoaderComponent = /** @class */ (function () {
    function LoaderComponent() {
    }
    LoaderComponent.prototype.ngOnInit = function () {
    };
    LoaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-loader',
            template: __webpack_require__(/*! ./loader.component.html */ "./src/app/_shared/loader/loader.component.html"),
            styles: [__webpack_require__(/*! ./loader.component.css */ "./src/app/_shared/loader/loader.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LoaderComponent);
    return LoaderComponent;
}());



/***/ }),

/***/ "./src/app/_shared/main-loader/loader.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/_shared/main-loader/loader.service.ts ***!
  \*******************************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var LoaderService = /** @class */ (function () {
    function LoaderService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    _this.subject.next(false);
                }
            }
        });
    }
    // OnSuccess Show message
    LoaderService.prototype.show = function (keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next(true);
    };
    LoaderService.prototype.hide = function (keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next(false);
    };
    // getMessage is method to take message from component
    LoaderService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    LoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "./src/app/_shared/main-loader/main-loader.component.css":
/*!***************************************************************!*\
  !*** ./src/app/_shared/main-loader/main-loader.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-spinner{\n    display: block;\n    background-color:rgba(255, 255, 255, 0.35);\n    position: fixed;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    z-index: 99999999;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvX3NoYXJlZC9tYWluLWxvYWRlci9tYWluLWxvYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztJQUNkLDBDQUEwQztJQUMxQyxlQUFlO0lBQ2YsT0FBTztJQUNQLFFBQVE7SUFDUixNQUFNO0lBQ04sU0FBUztJQUNULGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL19zaGFyZWQvbWFpbi1sb2FkZXIvbWFpbi1sb2FkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImFwcC1zcGlubmVye1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJhY2tncm91bmQtY29sb3I6cmdiYSgyNTUsIDI1NSwgMjU1LCAwLjM1KTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHotaW5kZXg6IDk5OTk5OTk5O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/_shared/main-loader/main-loader.component.html":
/*!****************************************************************!*\
  !*** ./src/app/_shared/main-loader/main-loader.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-spinner *ngIf=\"showLoader\"></app-spinner>"

/***/ }),

/***/ "./src/app/_shared/main-loader/main-loader.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/_shared/main-loader/main-loader.component.ts ***!
  \**************************************************************/
/*! exports provided: MainLoaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLoaderComponent", function() { return MainLoaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./loader.service */ "./src/app/_shared/main-loader/loader.service.ts");



var MainLoaderComponent = /** @class */ (function () {
    function MainLoaderComponent(loader) {
        this.loader = loader;
        this.showLoader = true;
    }
    MainLoaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loader.getMessage().subscribe(function (res) {
            _this.showLoader = res;
        });
    };
    MainLoaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-main-loader",
            template: __webpack_require__(/*! ./main-loader.component.html */ "./src/app/_shared/main-loader/main-loader.component.html"),
            styles: [__webpack_require__(/*! ./main-loader.component.css */ "./src/app/_shared/main-loader/main-loader.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]])
    ], MainLoaderComponent);
    return MainLoaderComponent;
}());



/***/ }),

/***/ "./src/app/_shared/page-not-found/page-not-found.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/_shared/page-not-found/page-not-found.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL19zaGFyZWQvcGFnZS1ub3QtZm91bmQvcGFnZS1ub3QtZm91bmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/_shared/page-not-found/page-not-found.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/_shared/page-not-found/page-not-found.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"dashboard-section\">\n        <div class=\"container-fluid\">\n                <div class=\"\">\n                        <div class=\" text-center pt-4\">\n                                <div class=\"pb-3 heading-6 light-black font-weight-500\">\n                                        PAGE NOT FOUND\n                                </div>\n                                <div class=\"pb-3 font-14 medium-black\">\n                                        <span class=\"light-blue cursor\">GO TO DASHBOARD</span>\n                                </div>\n                        </div>\n                </div>\n        </div>\n</section>"

/***/ }),

/***/ "./src/app/_shared/page-not-found/page-not-found.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/_shared/page-not-found/page-not-found.component.ts ***!
  \********************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/_shared/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/_shared/page-not-found/page-not-found.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/_shared/pipe/safeurl.pipe.ts":
/*!**********************************************!*\
  !*** ./src/app/_shared/pipe/safeurl.pipe.ts ***!
  \**********************************************/
/*! exports provided: SafeUrlPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafeUrlPipe", function() { return SafeUrlPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var SafeUrlPipe = /** @class */ (function () {
    function SafeUrlPipe(domSanitizer) {
        this.domSanitizer = domSanitizer;
    }
    SafeUrlPipe.prototype.transform = function (url) {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafeUrlPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'safeUrl'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], SafeUrlPipe);
    return SafeUrlPipe;
}());



/***/ }),

/***/ "./src/app/_shared/pipe/tel.pipe.ts":
/*!******************************************!*\
  !*** ./src/app/_shared/pipe/tel.pipe.ts ***!
  \******************************************/
/*! exports provided: TelPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TelPipe", function() { return TelPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var libphonenumber_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! libphonenumber-js */ "./node_modules/libphonenumber-js/index.es6.js");
/* harmony import */ var src_app_services_shared_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_services/shared-service.service */ "./src/app/_services/shared-service.service.ts");




var TelPipe = /** @class */ (function () {
    function TelPipe(SharedServiceService) {
        this.SharedServiceService = SharedServiceService;
        // alert("SharedServiceService")
    }
    TelPipe.prototype.transform = function (value, countryCode, args) {
        //  if(value.length>=10){
        if (value && args) {
            var completePhoneNumber = args + value;
            var sss = completePhoneNumber.replace(" ", "");
            var phoneNumber = Object(libphonenumber_js__WEBPACK_IMPORTED_MODULE_2__["parsePhoneNumberFromString"])(sss);
            if (phoneNumber) {
                var a = phoneNumber.isValid();
                this.SharedServiceService.update("isPhoneNumberValid", a);
                var result = phoneNumber.formatNational();
                return result;
            }
            else {
                return value;
            }
        }
        else {
            return value;
        }
        // }else{
        //   return value;
        // }
        // console.log(a)
        //  if (!value) { return ''; }
        //  let val = value.toString().trim().replace(/^\+/, '');
        //  if (val.match(/[^0-9]/)) {
        //     return value;
        // }
        //  let country, city, number;
        //  switch (val.length) {
        //     case 10: // +1PPP####### -> C (PPP) ###-####
        //         country = 1;
        //         city = val.slice(0, 3);
        //         number = val.slice(3);
        //         break;
        //     case 11: // +CPPP####### -> CCC (PP) ###-####
        //         country = val[0];
        //         city = val.slice(1, 4);
        //         number = val.slice(4);
        //         break;
        //     case 12: // +CCCPP####### -> CCC (PP) ###-####
        //         country = val.slice(0, 3);
        //         city = val.slice(3, 5);
        //         number = val.slice(5);
        //         break;
        //     default:
        //         return value;
        // }
        //  if (country === 1) {
        //     country = "";
        // }
        //  number = number.slice(0, 3) + '-' + number.slice(3);
        //  return (country + " (" + city + ") " + number).trim();
    };
    TelPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'tel'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_shared_service_service__WEBPACK_IMPORTED_MODULE_3__["SharedServiceService"]])
    ], TelPipe);
    return TelPipe;
}());



/***/ }),

/***/ "./src/app/_shared/shared.module.ts":
/*!******************************************!*\
  !*** ./src/app/_shared/shared.module.ts ***!
  \******************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _spinner_spinner_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../spinner/spinner.component */ "./src/app/spinner/spinner.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../welcome-page/welcome-page.component */ "./src/app/welcome-page/welcome-page.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/_shared/page-not-found/page-not-found.component.ts");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _loader_loader_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./loader/loader.component */ "./src/app/_shared/loader/loader.component.ts");
/* harmony import */ var _alert_alert_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./alert/alert.component */ "./src/app/_shared/alert/alert.component.ts");
/* harmony import */ var _addapters_date_addapter__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./addapters/date.addapter */ "./src/app/_shared/addapters/date.addapter.ts");
/* harmony import */ var _addapters_custom_mat_paginator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./addapters/custom-mat-paginator */ "./src/app/_shared/addapters/custom-mat-paginator.ts");
/* harmony import */ var _main_loader_main_loader_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./main-loader/main-loader.component */ "./src/app/_shared/main-loader/main-loader.component.ts");
/* harmony import */ var _confirmation_dialogue_confirmation_dialogue_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./confirmation-dialogue/confirmation-dialogue.component */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.component.ts");
/* harmony import */ var _utility_httpintercepter_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../_utility/httpintercepter.service */ "./src/app/_utility/httpintercepter.service.ts");
/* harmony import */ var _pipe_safeurl_pipe__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pipe/safeurl.pipe */ "./src/app/_shared/pipe/safeurl.pipe.ts");
/* harmony import */ var _pipe_tel_pipe__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pipe/tel.pipe */ "./src/app/_shared/pipe/tel.pipe.ts");








var MY_DATE_FORMATS = {
    parse: {
        dateInput: { month: "short", year: "numeric", day: "numeric" }
    },
    display: {
        // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
        dateInput: "input",
        monthYearLabel: { year: "numeric", month: "short" },
        dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
        monthYearA11yLabel: { year: "numeric", month: "long" }
    }
};











var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTreeModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_7__["ScrollDispatchModule"],
            ],
            exports: [
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_6__["PageNotFoundComponent"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatAutocompleteModule"],
                _welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_3__["WelcomePageComponent"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTreeModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _loader_loader_component__WEBPACK_IMPORTED_MODULE_10__["LoaderComponent"],
                _alert_alert_component__WEBPACK_IMPORTED_MODULE_11__["AlertComponent"],
                _spinner_spinner_component__WEBPACK_IMPORTED_MODULE_1__["SpinnerComponent"],
                _main_loader_main_loader_component__WEBPACK_IMPORTED_MODULE_14__["MainLoaderComponent"],
                _confirmation_dialogue_confirmation_dialogue_component__WEBPACK_IMPORTED_MODULE_15__["ConfirmationDialogueComponent"],
                _pipe_safeurl_pipe__WEBPACK_IMPORTED_MODULE_17__["SafeUrlPipe"],
                _pipe_tel_pipe__WEBPACK_IMPORTED_MODULE_18__["TelPipe"],
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_7__["ScrollDispatchModule"],
            ],
            declarations: [
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_6__["PageNotFoundComponent"],
                _welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_3__["WelcomePageComponent"],
                _loader_loader_component__WEBPACK_IMPORTED_MODULE_10__["LoaderComponent"],
                _alert_alert_component__WEBPACK_IMPORTED_MODULE_11__["AlertComponent"],
                _spinner_spinner_component__WEBPACK_IMPORTED_MODULE_1__["SpinnerComponent"],
                _main_loader_main_loader_component__WEBPACK_IMPORTED_MODULE_14__["MainLoaderComponent"],
                _confirmation_dialogue_confirmation_dialogue_component__WEBPACK_IMPORTED_MODULE_15__["ConfirmationDialogueComponent"],
                _pipe_safeurl_pipe__WEBPACK_IMPORTED_MODULE_17__["SafeUrlPipe"],
                _pipe_tel_pipe__WEBPACK_IMPORTED_MODULE_18__["TelPipe"]
            ],
            providers: [
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_8__["DateAdapter"], useClass: _addapters_date_addapter__WEBPACK_IMPORTED_MODULE_12__["MyDateAdapter"] },
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_8__["MAT_DATE_FORMATS"], useValue: MY_DATE_FORMATS },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"], useClass: _utility_httpintercepter_service__WEBPACK_IMPORTED_MODULE_16__["AuthInterceptor"], multi: true },
                {
                    provide: _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorIntl"],
                    useClass: _addapters_custom_mat_paginator__WEBPACK_IMPORTED_MODULE_13__["CustomMatPaginatorIntl"]
                }
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/_utility/httpintercepter.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_utility/httpintercepter.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(router) {
        this.router = router;
        // Migrated from AngularJS https://raw.githubusercontent.com/Ins87/angular-date-interceptor/master/src/angular-date-interceptor.js
        this.iso8601 = /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/;
    }
    AuthInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        var token = localStorage.getItem("token");
        if (token) {
            request = request.clone({ headers: request.headers.set("token", token) });
        }
        if (request.body) {
            // this.convertToDate(request.body);
        }
        return next.handle(request).pipe(
        // map((event: HttpEvent<any>) => {
        //   if (event instanceof HttpResponse) {
        //     const body = event.body;
        //    // this.convertToDate(body);
        //     // console.log("recieving request body", event.body);
        //   }
        //   return event;
        // }),
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            if (error.status === 401) {
                localStorage.clear();
                _this.router.navigate(["login"]);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpErrorResponse"]({}));
            }
            else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
            }
        }));
    };
    AuthInterceptor.prototype.convertToDate = function (body) {
        if (body === null || body === undefined) {
            return body;
        }
        if (typeof body !== "object") {
            return body;
        }
        for (var _i = 0, _a = Object.keys(body); _i < _a.length; _i++) {
            var key = _a[_i];
            var value = body[key];
            if (this.isIso8601(value)) {
                body[key] = new Date(value);
                body[key] = new Date(new Date(body[key]).setDate(new Date(body[key]).getDate() + 1));
            }
            else if (typeof value === "object") {
                this.convertToDate(value);
            }
        }
    };
    AuthInterceptor.prototype.isIso8601 = function (value) {
        if (value === null || value === undefined) {
            return false;
        }
        return this.iso8601.test(value);
    };
    AuthInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], AuthInterceptor);
    return AuthInterceptor;
}());

//  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//    return next.handle(req).do((event: HttpEvent<any>) => {
//     , (err: any) => {
//      if (err instanceof HttpErrorResponse) {
//        if (err.status === 401) {
//        }
//      }
//    });
//  }


/***/ }),

/***/ "./src/app/_utility/string.helper.ts":
/*!*******************************************!*\
  !*** ./src/app/_utility/string.helper.ts ***!
  \*******************************************/
/*! exports provided: StringHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StringHelper", function() { return StringHelper; });
var StringHelper = /** @class */ (function () {
    function StringHelper() {
    }
    StringHelper.ValueIsString = function (myVar) {
        if (typeof myVar === "string" || myVar instanceof String) {
            return true;
        }
        else {
            return false;
        }
    };
    StringHelper.RemoveRedundantSpacesFromObject = function (obj) {
        var _this = this;
        var body = Object.assign({}, obj);
        try {
            Object.keys(body).forEach(function (key) {
                if (_this.ValueIsString(body[key])) {
                    body[key] = body[key].trim();
                }
            });
        }
        catch (ex) {
            // tslint:disable-next-line:no-
        }
        return body;
    };
    return StringHelper;
}());



/***/ }),

/***/ "./src/app/_utility/user_auth.helper.ts":
/*!**********************************************!*\
  !*** ./src/app/_utility/user_auth.helper.ts ***!
  \**********************************************/
/*! exports provided: UserAuthHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAuthHelper", function() { return UserAuthHelper; });
var UserAuthHelper = /** @class */ (function () {
    function UserAuthHelper() {
    }
    UserAuthHelper.PrintPage = function () {
        var divContents = document.getElementById("dvContents").innerHTML;
        var printWindow = window.open("", "_blank", "");
        printWindow.document.write("<html><head> <style>\n    *{\n      box-sizing: border-box;\n    }\n    body{\n      font-size: 15px;\n      color: rgba(0, 0, 0, 0.6);\n      font-family: sans-serif;\n    }\n    h1{\n      font-size: 22px;\n      font-weight: 300;\n      color: #202020;\n      margin: 0;\n    }\n    h2{\n      color: #202020;\n      font-weight: 500;\n      font-size: 18px;\n      margin-bottom: 16px;\n      margin-top: 20px;\n    }\n    .float-right{\n      float: right;\n    }\n    .page-footer, .page-footer-space {\n      padding-top: 5px;\n      height: 30px;\n    }\n    .page-header, .page-header-space {\n      height: 30px;\n    }\n    .page-footer {\n      position: fixed;\n      bottom: 0;\n      width: 100%;\n    }\n    .page-header {\n      position: fixed;\n      top: 0mm;\n      width: 100%;\n    }\n    .page {\n      page-break-after: always;\n    }\n    @page {\n      size: auto;\n      margin: 5mm 5mm;\n    }\n    @media print {\n      thead {\n        display: table-header-group;\n      }\n      tfoot {\n        display: table-footer-group;\n      }\n      body {\n        margin: 0;\n      }\n      section {\n        padding: 10px 10px;\n      }\n      .page-header, .page-footer {\n        display: block !important;\n      }\n    }\n    .page-header, .page-footer {\n      display: none;\n    }\n    .text-dark{\n      font-size: 14px;\n      color: #202020;\n      margin-bottom: 14px;\n    }\n    .mb-0{\n      margin-bottom: 0;\n    }\n    .print-wrapper{\n      padding: 0 50px;\n  }\n  </style>");
        printWindow.document.write("</head><body >");
        printWindow.document.write("</body></html>");
        printWindow.document.write(divContents);
        printWindow.print();
        printWindow.onfocus = function () { printWindow.close(); };
    };
    UserAuthHelper.GetProfile = function () {
        return JSON.parse(localStorage.getItem("profile"));
    };
    return UserAuthHelper;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_shared/page-not-found/page-not-found.component */ "./src/app/_shared/page-not-found/page-not-found.component.ts");
/* harmony import */ var _data_grid_data_grid_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./data-grid/data-grid.component */ "./src/app/data-grid/data-grid.component.ts");
/* harmony import */ var _auth_gaurd_auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/gaurd/auth.guard */ "./src/app/auth/gaurd/auth.guard.ts");






var routes = [
    {
        path: "",
        loadChildren: "./dashboard/dashboard.module#DashboardModule",
        canActivate: [_auth_gaurd_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
    },
    { path: "", loadChildren: "./auth/auth.module#AuthModule" },
    {
        path: "html",
        component: _data_grid_data_grid_component__WEBPACK_IMPORTED_MODULE_4__["DataGridComponent"]
    },
    { path: "404", component: _shared_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_3__["PageNotFoundComponent"] },
    { path: "**", redirectTo: "404", pathMatch: "full" }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { scrollPositionRestoration: 'enabled' })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-alert></app-alert>\n<app-main-loader></app-main-loader>\n<app-confirmation-dialogue></app-confirmation-dialogue>\n<div class=\"main-section\">\n    <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_title_title_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_services/title/title.service */ "./src/app/_services/title/title.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");






var AppComponent = /** @class */ (function () {
    function AppComponent(http, titleService, activatedRoute, router, alertService) {
        var _this = this;
        this.http = http;
        this.titleService = titleService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.alertService = alertService;
        this.pageTitle = {
            "/login": ""
        };
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                var currentUrl = event.url;
                var title = _this.getTitle(currentUrl);
                titleService.setTitle(title);
            }
        });
    }
    AppComponent.prototype.onWindowScroll = function () {
        var aa = document.body.offsetHeight;
        //  console.log('aa', aa);
    };
    // collect that title data properties from all child routes
    // there might be a better way but this worked for me
    AppComponent.prototype.getTitle = function (currentUrl) {
        if (currentUrl.includes("/login")) {
            return "Login - Participant Media";
        }
        else if (currentUrl.includes("/forgot")) {
            return "Request Password Reset - Participant Media";
        }
        else if (currentUrl.includes("/reset")) {
            return "Password Reset - Participant Media";
        }
        else if (currentUrl.includes("dashboard")) {
            return "Dashboard - Participant Media";
        }
        else if (currentUrl.includes("users")) {
            return "Users - Participant Media";
        }
        else if (currentUrl.includes("404")) {
            return "404 - Participant Media";
        }
        else if (currentUrl.includes("projects/")) {
            var urlParts = currentUrl.split("/");
            if (urlParts.length > 2) {
                return urlParts[2] + " - Participant Media";
            }
        }
        else if (currentUrl.includes("addendum/")) {
            var urlParts = currentUrl.split("/");
            if (urlParts.length > 2) {
                return urlParts[2] + " - Participant Media";
            }
        }
        return "Participant Media";
    };
    AppComponent.prototype.alert = function () {
        this.alertService.error("Some Message");
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll', []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], AppComponent.prototype, "onWindowScroll", null);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-root",
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"],
            _services_title_title_service__WEBPACK_IMPORTED_MODULE_2__["TitleService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _auth_gaurd_auth_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth/gaurd/auth.guard */ "./src/app/auth/gaurd/auth.guard.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _dashboard_section_dashboard_section_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dashboard-section/dashboard-section.component */ "./src/app/dashboard-section/dashboard-section.component.ts");
/* harmony import */ var _data_grid_data_grid_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./data-grid/data-grid.component */ "./src/app/data-grid/data-grid.component.ts");
/* harmony import */ var _memo_memo_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./memo/memo.component */ "./src/app/memo/memo.component.ts");
/* harmony import */ var _search_bar_search_bar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./search-bar/search-bar.component */ "./src/app/search-bar/search-bar.component.ts");
/* harmony import */ var _project_memo_form_project_memo_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./project-memo-form/project-memo-form.component */ "./src/app/project-memo-form/project-memo-form.component.ts");
/* harmony import */ var _services_title_title_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./_services/title/title.service */ "./src/app/_services/title/title.service.ts");
/* harmony import */ var _user_list_user_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./user-list/user-list.component */ "./src/app/user-list/user-list.component.ts");
/* harmony import */ var _shared_grid_grid_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./_shared/grid/grid.component */ "./src/app/_shared/grid/grid.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./_shared/shared.module */ "./src/app/_shared/shared.module.ts");
/* harmony import */ var src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! src/app/_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var src_assets_scripts_ngx_s3_uploader__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! src/assets/scripts/ngx-s3-uploader */ "./src/assets/scripts/ngx-s3-uploader/index.js");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var _shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./_shared/confirmation-dialogue/confirmation-dialogue.service */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts");
/* harmony import */ var _services_shared_service_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./_services/shared-service.service */ "./src/app/_services/shared-service.service.ts");






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _dashboard_section_dashboard_section_component__WEBPACK_IMPORTED_MODULE_8__["DashboardSectionComponent"],
                _search_bar_search_bar_component__WEBPACK_IMPORTED_MODULE_11__["SearchBarComponent"],
                _project_memo_form_project_memo_form_component__WEBPACK_IMPORTED_MODULE_12__["ProjectMemoFormComponent"],
                _data_grid_data_grid_component__WEBPACK_IMPORTED_MODULE_9__["DataGridComponent"],
                _memo_memo_component__WEBPACK_IMPORTED_MODULE_10__["MemoComponent"],
                _user_list_user_list_component__WEBPACK_IMPORTED_MODULE_14__["UserListComponent"],
                _shared_grid_grid_component__WEBPACK_IMPORTED_MODULE_15__["GridComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_16__["SharedModule"],
                src_assets_scripts_ngx_s3_uploader__WEBPACK_IMPORTED_MODULE_18__["S3UploaderModule"].forRoot({
                    region: "US-West-Oregon",
                    bucket: "participant-media-files",
                    credentials: {
                        accessKeyId: "AKIAJTDLYI34ZN5SNNTQ",
                        secretAccessKey: "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"
                    }
                })
            ],
            providers: [
                _services_title_title_service__WEBPACK_IMPORTED_MODULE_13__["TitleService"],
                _services_shared_service_service__WEBPACK_IMPORTED_MODULE_21__["SharedServiceService"],
                src_app_shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_17__["AlertService"],
                _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_19__["LoaderService"],
                _auth_gaurd_auth_guard__WEBPACK_IMPORTED_MODULE_1__["AuthGuard"],
                _shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_20__["ConfirmationDialogueService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/gaurd/auth.guard.ts":
/*!******************************************!*\
  !*** ./src/app/auth/gaurd/auth.guard.ts ***!
  \******************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var Permissions = /** @class */ (function () {
    function Permissions() {
    }
    Permissions.prototype.canActivate = function () {
        return localStorage.getItem("token") ? true : false;
    };
    return Permissions;
}());
var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
        this.permissions = new Permissions();
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (this.permissions.canActivate()) {
            return true;
        }
        else {
            this.router.navigate(["/login"], { queryParams: { returnUrl: state.url } });
        }
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/dashboard-section/dashboard-section.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/dashboard-section/dashboard-section.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC1zZWN0aW9uL2Rhc2hib2FyZC1zZWN0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dashboard-section/dashboard-section.component.html":
/*!********************************************************************!*\
  !*** ./src/app/dashboard-section/dashboard-section.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n \n</div>"

/***/ }),

/***/ "./src/app/dashboard-section/dashboard-section.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/dashboard-section/dashboard-section.component.ts ***!
  \******************************************************************/
/*! exports provided: DashboardSectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardSectionComponent", function() { return DashboardSectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DashboardSectionComponent = /** @class */ (function () {
    function DashboardSectionComponent() {
    }
    DashboardSectionComponent.prototype.ngOnInit = function () {
    };
    DashboardSectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard-section',
            template: __webpack_require__(/*! ./dashboard-section.component.html */ "./src/app/dashboard-section/dashboard-section.component.html"),
            styles: [__webpack_require__(/*! ./dashboard-section.component.css */ "./src/app/dashboard-section/dashboard-section.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DashboardSectionComponent);
    return DashboardSectionComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/_service/project.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/_service/project.service.ts ***!
  \*******************************************************/
/*! exports provided: ProjectService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectService", function() { return ProjectService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_models_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_models/_config */ "./src/app/_models/_config.ts");
/* harmony import */ var src_app_utility_string_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_utility/string.helper */ "./src/app/_utility/string.helper.ts");





var ProjectService = /** @class */ (function () {
    function ProjectService(http) {
        this.http = http;
    }
    ProjectService.prototype.getProjects = function (filters) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getProjects;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.post(url, filters, httpOptions);
    };
    ProjectService.prototype.generateProjectId = function () {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].generateProjectId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.getProjectsDetails = function (projectId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getProjectDetails + projectId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.SaveProgress = function (projectId, project) {
        project.contractorContactPhone = project.contractorContactPhone.replace(/[&\/\\#,+()$~%.'":*?<>{}" "-]/g, '');
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].updateProject + projectId;
        var body = Object.assign({}, project);
        body = src_app_utility_string_helper__WEBPACK_IMPORTED_MODULE_4__["StringHelper"].RemoveRedundantSpacesFromObject(body);
        if (!body.attachment) {
            delete body.attachment;
        }
        delete body.mailList;
        delete body.from;
        body.cc = project.mailList.map(function (x) { return x.email; }).join(",");
        body.pmURL = location.href;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.put(url, body, httpOptions);
    };
    ProjectService.prototype.loadUsers = function () {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getUsersList;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.getCatalogList = function () {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getCatalogList;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.getPeoplesEmail = function (cc) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].searchEmail + cc;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.GetContractorsList = function () {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getContactList;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.GetRightLineUserList = function () {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getRightLineUserList;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.SubmitProjectStandardUser = function (projectId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].submitProjectSU + projectId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.put(url, {}, httpOptions);
    };
    ProjectService.prototype.deleteProjectMemo = function (projectId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].deleteProjectMemo + projectId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.delete(url, httpOptions);
    };
    ProjectService.prototype.deleteAddendum = function (projectId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].deleteAddendum + projectId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.delete(url, httpOptions);
    };
    ProjectService.prototype.seachProject = function (value) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].searchProject + value;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.getAddendumId = function (pmID, parentProjectMemo) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].createUpdateAddendum;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        var body = { parentProjectMemo: pmID };
        return this.http.put(url, body, httpOptions);
    };
    ProjectService.prototype.loadAddendumDetails = function (addendumId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getAddendumDetails + addendumId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    ProjectService.prototype.updateAddendum = function (addendum) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].createUpdateAddendum;
        var body = Object.assign({}, addendum);
        delete body.mailList;
        body = src_app_utility_string_helper__WEBPACK_IMPORTED_MODULE_4__["StringHelper"].RemoveRedundantSpacesFromObject(body);
        body.cc = addendum.mailList.map(function (x) { return x.email; }).join(",");
        body.adURL = location.href;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.put(url, body, httpOptions);
    };
    ProjectService.prototype.SubmitAddendumSU = function (addendumId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].submitAddendumSU + addendumId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        var body = {};
        return this.http.put(url, body, httpOptions);
    };
    ProjectService.prototype.SubmitAddendumBA = function (addendumId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].submitAddendumBA + addendumId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        var body = {};
        return this.http.put(url, body, httpOptions);
    };
    ProjectService.prototype.SubmitProjectBA = function (projectId) {
        var url = src_app_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].submitProjectBA + projectId;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.put(url, {}, httpOptions);
    };
    ProjectService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ProjectService);
    return ProjectService;
}());



/***/ }),

/***/ "./src/app/data-grid/data-grid.component.css":
/*!***************************************************!*\
  !*** ./src/app/data-grid/data-grid.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RhdGEtZ3JpZC9kYXRhLWdyaWQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/data-grid/data-grid.component.html":
/*!****************************************************!*\
  !*** ./src/app/data-grid/data-grid.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-header></app-header> -->\n<!-- <app-sidebar></app-sidebar> -->\n<!-- <app-print-memo></app-print-memo> -->\n<!-- \n<app-project-memo-form></app-project-memo-form> -->\n<!-- <app-project-memo-form></app-project-memo-form> -->\n<!-- <app-search-bar></app-search-bar> -->\n<!-- <app-spinner></app-spinner> -->\n<!-- <div id=\"mydashboard\" class=\"dashboard-section\">\n  \n  <div class=\"own-dashboard-section\">\n    <section>\n      <table class=\"table table-hover own-table-dashboard table-responsive\">\n        <thead>\n          <tr>\n            <th scope=\"col\">Submitted <i class='fas fa-arrow-down icon-space'></i></th>\n            <th scope=\"col\">ID</th>\n            <th scope=\"col\">Vendor</th>\n            <th scope=\"col\">Project</th>\n            <th scope=\"col\">Type</th>\n            <th scope=\"col\">Assigned To</th>\n            <th scope=\"col\">Status</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>01/22/19 - Tue</td>\n            <td>PM0001234</td>\n            <td>REdLine</td>\n            <td>Avengers</td>\n            <td>Memo</td>\n            <td>Make Batting </td>\n            <td>In review</td>\n          </tr>\n          <tr>\n            <td>01/22/19 - Tue</td>\n            <td>PM0001234</td>\n            <td>REdLine</td>\n            <td>Avengers</td>\n            <td>Memo</td>\n            <td>Make Batting </td>\n            <td>In review</td>\n          </tr>\n          <tr>\n            <td>01/22/19 - Tue</td>\n            <td>PM0001234</td>\n            <td>REdLine</td>\n            <td>Avengers</td>\n            <td>Memo</td>\n            <td>Make Batting </td>\n            <td>In review</td>\n          </tr>\n          <tr>\n            <td>01/22/19 - Tue</td>\n            <td>PM0001234</td>\n            <td>REdLine</td>\n            <td>Avengers</td>\n            <td>Memo</td>\n            <td>Make Batting </td>\n            <td>In review</td>\n          </tr>\n          <td colspan=\"8\">\n            <mat-paginator itemsPerPageLabel=\"rows per page\" [length]=\"100\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\">\n            </mat-paginator>\n          </td>\n        </tbody>\n      </table>\n    </section>\n  </div>\n</div> -->\n\n<!-- \n  \n<div class=\"modal show\" id=\"userModal\">\n  <div class=\"modal-dialog modal-print-memo\">\n    <div class=\"modal-content \">\n      <div class=\"modal-header modal-heading\">\n        <div>\n          <button mat-raised-button color=\"primary\" class=\"complete-form-submit ml-2\">PRINT</button>\n          <button mat-raised-button color=\"\" class=\"print-submit-btn white-btn ml-2\">DOWNLOAD ALL ATTACHMENTS</button>\n\n        </div>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <section class=\"general-form px-3 py-xl-5 py-lg-5 py-md-4 py-sm-3\">\n\n        <div class=\"\">\n          <section class=\"pl-5\">\n                    <div class=\"padding-details-lg\">\n              <div class=\"main-heading font-weight-300\">\n                Project Memo for [vendor name] on [project name] from [user name]\n              </div>\n              <div class=\"light-black font-14\">PM00001222</div>\n            </div>\n\n            <section>\n              <div class=\"pb-1\">\n                <div class=\"heading-5 padding-details-md font-weight-500\">\n                  GENERAL INFORMATION\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n              </div>\n            </section>\n\n            <section>\n              <div class=\"pb-1\">\n                <div class=\"heading-5 padding-details-md font-weight-500\">\n                  GENERAL INFORMATION\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n              </div>\n            </section>\n            <section>\n              <div class=\"pb-1\">\n                <div class=\"heading-5 padding-details-md font-weight-500\">\n                  GENERAL INFORMATION\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n                  </div>\n                </div>\n                <div class=\"padding-details-sm\">\n                  <div class=\"light-black font-15\">\n                    Assigned To\n                  </div>\n                  <div class=\"font-14\">\n                    Matt Randall\n      </div>\n      </div>\n      </div>\n      </section>\n      </section>\n      </div>\n      </section>\n      </div>\n      </div>\n      </div> -->\n      <!-- <div class=\"modal show\" id=\"userModal\">\n        <div class=\"modal-dialog\">\n          <div class=\"modal-content \">\n            <div class=\"modal-header modal-heading\">\n              <h3 class=\"font-20 \">ADDENDUM ASSIGNMENT</h3>\n            </div>\n            <div class=\"pt-3 container\">\n              <form class=\"form-own\">\n                <div class=\"form-group\">      \n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <input matInput placeholder=\"First Name\">\n                  </mat-form-field>\n                  <div class=\"text-color font-14 pt-1 view-project-memo pl-3 cursor\">VIEW PROJECT MEMO</div>\n                </div>\n              </form>\n              <div class=\" white-btn-main mb-3 d-flex justify-content-between row\">\n                <div class=\"col-5 \">\n                  <button mat-raised-button color=\"\" class=\"print-submit-btn  white-btn w-100 close\" data-dismiss=\"modal\">CANCEL</button>\n                </div>\n                <div class=\"col-5 \">\n                  <button mat-raised-button color=\"primary\" class=\"print-submit-btn w-100\">CONTINUE</button>\n      \n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div> -->\n      "

/***/ }),

/***/ "./src/app/data-grid/data-grid.component.ts":
/*!**************************************************!*\
  !*** ./src/app/data-grid/data-grid.component.ts ***!
  \**************************************************/
/*! exports provided: DataGridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataGridComponent", function() { return DataGridComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DataGridComponent = /** @class */ (function () {
    function DataGridComponent() {
    }
    DataGridComponent.prototype.ngOnInit = function () {
    };
    DataGridComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-data-grid',
            template: __webpack_require__(/*! ./data-grid.component.html */ "./src/app/data-grid/data-grid.component.html"),
            styles: [__webpack_require__(/*! ./data-grid.component.css */ "./src/app/data-grid/data-grid.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DataGridComponent);
    return DataGridComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img-size\n{\n    height: auto;\n    width: 85px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLFlBQVk7SUFDWixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1nLXNpemVcbntcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDg1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <nav class=\"d-flex navbar-expand-md bg-nav justify-content-center\">\n  <a class=\"navbar-brand\" href=\"#\"><img src=\"../assets/images/logo.png\" class=\"img-fluid img-size\"></a>\n   <!-- <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button> \n  <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\n   <ul class=\"navbar-nav\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>    \n    </ul> \n  </div>  \n</nav> -->\n\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/memo/memo.component.css":
/*!*****************************************!*\
  !*** ./src/app/memo/memo.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lbW8vbWVtby5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/memo/memo.component.html":
/*!******************************************!*\
  !*** ./src/app/memo/memo.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"general-form\">\n  <div class=\"d-flex justify-content-between align-items-center\">\n    <h5>General Information</h5>\n    <span>PM000012</span>\n  </div>\n\n  <div class=\"pt-5\">\n    <form class=\"form-own\">\n      <div class=\"form-group\">\n        <mat-form-field class=\"sign-input-2\">\n          <mat-label>Select an option</mat-label>\n          <mat-select disableRipple>\n            <mat-option [value]=\"1\">Option 1</mat-option>\n            <mat-option [value]=\"2\">Option 2</mat-option>\n            <mat-option [value]=\"3\">Option 3</mat-option>\n          </mat-select>\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Send Copies To\">\n          <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n          <mat-error>\n            Please enter a valid email address\n          </mat-error>\n          <mat-error>\n            Email is <strong>required</strong>\n          </mat-error>\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Project Name *\">\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Catalog*\">\n        </mat-form-field>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-12 col-sm-12 col-md-5 col-lg-5 filter-date-section\">\n          <div class=\"filter-items\">\n            <p class=\"filter-p\">\n              Filter by Date Submitted\n            </p>\n            <div class=\"date-selection d-inline-block\">\n              <i class='far fa-calendar'></i>\n              <mat-form-field class=\"example-full-width\">\n                <input matInput [matDatepicker]=\"picker\" placeholder=\"\">\n                <mat-datepicker #picker></mat-datepicker>\n              </mat-form-field>\n            </div>\n            <div class=\"d-inline-block own-line\">\n              <mat-form-field class=\"example-full-width\">\n                <input matInput [matDatepicker]=\"picker1\" placeholder=\"\">\n                <mat-datepicker #picker1></mat-datepicker>\n              </mat-form-field>\n            </div>\n          </div>\n        </div>\n\n      </div>\n      <br>\n\n    </form>\n  </div>\n</section>\n<section class=\"general-form mt-3\">\n  <div class=\"d-flex justify-content-between align-items-center\">\n    <h5>CONTRACTOR</h5>\n  </div>\n  <div class=\"pt-5\">\n    <form class=\"form-own\">\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Contractor Name *\">\n          <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n          <mat-error>\n            *Please identify your contractor\n          </mat-error>\n          <mat-error>\n            *Please identify your contractor\n          </mat-error>\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Primary Contact Name *\">\n          <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Primary Contact Email *\">\n          <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <mat-form-field class=\"example-full-width sign-input-2\">\n          <input matInput placeholder=\"Primary Contact Phone *\">\n          <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n        </mat-form-field>\n      </div>\n      <div class=\"form-group\">\n        <div class=\"option-radio\">\n          <p class=\"filter-font\">Will Contractor Work Onsite? *</p>\n          <mat-radio-group aria-label=\"Select an option\">\n            <mat-radio-button [value]=\"1\">Option 1</mat-radio-button>\n            <mat-radio-button [value]=\"2\">Option 2</mat-radio-button>\n          </mat-radio-group>\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <div class=\"option-radio\">\n          <p class=\"filter-font\">Contractor Insurance</p>\n        </div>\n      </div>\n    </form>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/memo/memo.component.ts":
/*!****************************************!*\
  !*** ./src/app/memo/memo.component.ts ***!
  \****************************************/
/*! exports provided: MemoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemoComponent", function() { return MemoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MemoComponent = /** @class */ (function () {
    function MemoComponent() {
    }
    MemoComponent.prototype.ngOnInit = function () {
    };
    MemoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-memo',
            template: __webpack_require__(/*! ./memo.component.html */ "./src/app/memo/memo.component.html"),
            styles: [__webpack_require__(/*! ./memo.component.css */ "./src/app/memo/memo.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MemoComponent);
    return MemoComponent;
}());



/***/ }),

/***/ "./src/app/project-memo-form/project-memo-form.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/project-memo-form/project-memo-form.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QtbWVtby1mb3JtL3Byb2plY3QtbWVtby1mb3JtLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/project-memo-form/project-memo-form.component.html":
/*!********************************************************************!*\
  !*** ./src/app/project-memo-form/project-memo-form.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"mydashboard\" class=\"dashboard-section\">\n    <div class=\"container-own own-dashboard-section\">\n\n        <section class=\"general-form \">\n            <div class=\"d-flex justify-content-between align-items-center\">\n                <h5 class=\"pt-3\">General Information</h5>\n                <h5 class=\"pt-3\">PM000012</h5>\n            </div>\n\n            <div class=\"pt-3\">\n                <form class=\"form-own\">\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <div class=\"date-filter\">\n                                    <div class=\"filter-items \">\n                                        <span class=\"filter-p\">\n                                            Filter by Date Submitted\n                                        </span>\n                                        <div class=\"date-picker-wrapper\">\n\n                                            <div class=\"date-selection right-arrow own-line col-6 d-flex justify-content-center\">\n\n                                                <div>\n                                                    <!-- <i class='far fa-calendar calender-icon'></i> -->\n                                                    <img src=\"assets/images/cal-icon.png\" class='calender-icon'>\n                                                </div>\n                                                <mat-form-field class=\"example-full-width\" floatLabel=\"never\">\n                                                    <input matInput (focus)=\"picker.open()\" [matDatepicker]=\"picker\"\n                                                        placeholder=\"start date\">\n                                                    <mat-datepicker #picker></mat-datepicker>\n                                                </mat-form-field>\n                                                <div class=\"material-icon-section\">\n                                                    <mat-icon aria-hidden=\"false\" aria-label=\"Example home icon\">keyboard_arrow_left</mat-icon>\n                                                    <mat-icon aria-hidden=\"false\" aria-label=\"Example home icon\">keyboard_arrow_right</mat-icon>\n                                                </div>\n                                            </div>\n                                            <div class=\" date-selection col-6 d-flex justify-content-center align-items-end\">\n                                                <mat-form-field class=\"example-full-width\" floatLabel=\"never\">\n                                                    <input matInput (focus)=\"pickera.open()\" [matDatepicker]=\"pickera\"\n                                                        placeholder=\"end date\">\n                                                    <mat-datepicker #pickera></mat-datepicker>\n                                                </mat-form-field>\n                                                <div class=\"material-icon-section\">\n                                                    <mat-icon aria-hidden=\"false\" aria-label=\"Example home icon\">keyboard_arrow_left</mat-icon>\n                                                    <mat-icon aria-hidden=\"false\" aria-label=\"Example home icon\">keyboard_arrow_right</mat-icon>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"sign-input-2\">\n                                    <mat-label>Select an option</mat-label>\n                                    <mat-select disableRipple>\n                                        <mat-option [value]=\"1\">Option 1</mat-option>\n                                        <mat-option [value]=\"2\">Option 2</mat-option>\n                                        <mat-option [value]=\"3\">Option 3</mat-option>\n                                    </mat-select>\n                                </mat-form-field>\n                            </div>\n\n\n                        </div>\n\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row align-items-start\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n\n                                <mat-form-field class=\"example-full-width sign-input-2 sign-input-3 wrapper-padding multiple-email-own\" floatLabel=\"always\">\n                                    <div class=\"attachment-section-1\">\n                                        <ul class=\"d-list-item attachment-items list-unstyled\">\n                                            <li><span>scope.jpg <img src=\"../assets/images/close-icon.png\" class=\"close-icon\"></span></li>\n                                            <li><span>scope.jpg <img src=\"../assets/images/close-icon.png\" class=\"close-icon\"></span></li>\n\n                                            <li class=\"input-wrapper\"> <input matInput placeholder=\"Send Copies To\"></li>\n                                        </ul>\n                                    </div>\n\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                    <mat-error>\n                                        Please enter a valid email address\n                                    </mat-error>\n                                    <mat-error>\n                                        Email is <strong>required</strong>\n                                    </mat-error>\n\n                                </mat-form-field>\n                            </div>\n                            <div class=\"col-6\">\n\n                                <div class=\"tooltip-box py-2 px-3 active\">\n                                    <div>Make sure to CC others who need to be in ‘the know’:</div>\n                                    <ul class=\"tooltip-items list-unstyled\">\n\n                                        <li>Department heads</li>\n                                        <li>Other relevant personnel</li>\n\n                                    </ul>\n                                    <div class=\"pb-3\">Enter their email addresses here.</div>\n                                    <div><span>Tip</span> : <i>For Participant Media personnel you can also start\n                                            typing their name and select from existing matches.</i> </div>\n                                </div>\n\n                            </div>\n\n                        </div>\n\n\n\n                    </div>\n\n\n\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Project Name *\">\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Catalog*\">\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </section>\n        <section class=\"general-form mt-2\">\n            <div class=\"d-flex justify-content-between align-items-center\">\n                <h5 class=\"pt-3\">CONTRACTOR</h5>\n            </div>\n\n            <div class=\"pt-3\">\n                <form class=\"form-own\">\n\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Contractor Name *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                    <mat-error>\n                                        *Please identify your contractor\n                                    </mat-error>\n                                    <mat-error>\n                                        *Please identify your contractor\n                                    </mat-error>\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Primary Contact Name *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                            <app-project-memo-form></app-project-memo-form>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Primary Contact Email *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Primary Contact Phone *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n\n\n                    <div class=\"form-group pb-3\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <div class=\"option-radio filter-items\">\n                                    <div class=\"filter-p\">Will Contractor Work Onsite? *</div>\n                                    <mat-radio-group aria-label=\"Select an option\">\n                                        <mat-radio-button value=\"1\"> Yes </mat-radio-button>\n                                        <mat-radio-button value=\"2\"> No </mat-radio-button>\n                                    </mat-radio-group>\n                                </div>\n                            </div>\n\n                        </div>\n                    </div>\n\n                    <div class=\"form-group pt-4\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <div class=\"option-radio filter-items\">\n                                    <span class=\"filter-p\">Contractor Insurance</span>\n                                    <ul class=\"list-unstyled\">\n                                        <li>\n                                            <mat-checkbox>GENERAL LIABILITY</mat-checkbox>\n                                        </li>\n                                        <li>\n                                            <mat-checkbox>E & O</mat-checkbox>\n                                        </li>\n                                        <li>\n                                            <mat-checkbox>WORKER'S COMP</mat-checkbox>\n                                        </li>\n                                        <li>\n                                            <mat-checkbox>PRODUCATION INSURANCE</mat-checkbox>\n                                        </li>\n\n                                    </ul>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </form>\n            </div>\n        </section>\n        <section class=\"general-form mt-2\">\n            <div class=\"d-flex justify-content-between align-items-center\">\n                <h5 class=\"pt-3\">SERVICES / DELIVERABLES / PAYMENT</h5>\n            </div>\n\n            <div class=\"pt-3\">\n                <form class=\"form-own\">\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Contractor Name *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                    <mat-error>\n                                        *Please identify your contractor\n                                    </mat-error>\n                                    <mat-error>\n                                        *Please identify your contractor\n                                    </mat-error>\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Approximate Hours of Work *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Deliverables *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Delivery Schedule *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Total Fee *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Payment Schedule *\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n\n                </form>\n            </div>\n        </section>\n        <section class=\"general-form mt-2\">\n            <div class=\"d-flex justify-content-between align-items-center\">\n                <h5>Content</h5>\n\n            </div>\n\n            <div class=\"pt-4\">\n                <form class=\"form-own\">\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Ownership of Deliverables\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                    <mat-error>\n                                        *Please identify your contractor\n                                    </mat-error>\n                                    <mat-error>\n                                        *Please identify your contractor\n                                    </mat-error>\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Rights Licensed by Contractor (if any)\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Intended Use by Contractor\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Intended Use by Participant\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Assets Provided by Participant\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                    </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                                    <input matInput placeholder=\"Assets Provided by Contractor\">\n                                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group mb-3\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <div class=\"option-radio filter-items\">\n                                    <p class=\"filter-p\">Third Party Assets to be Included</p>\n                                    <mat-radio-group aria-label=\"Select an option\">\n                                        <mat-radio-button value=\"1\"> Yes </mat-radio-button>\n                                        <mat-radio-button value=\"2\"> No </mat-radio-button>\n                                    </mat-radio-group>\n                                </div>\n                            </div>\n\n                        </div>\n                    </div>\n                    <div class=\"form-group mt-3\">\n                        <div class=\"row\">\n                            <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                                <div class=\"option-radio filter-items download-attachments\">\n                                    <p class=\"filter-p\">Attachments</p>\n                                    <div class=\"attachment-section\">\n                                        <ul class=\"d-list-item attachment-items list-unstyled\">\n                                            <li><span>scope.jpg <img src=\"../assets/images/close-icon.png\" class=\"close-icon\"></span></li>\n                                            <li><span>scopkkkkkkke.jpg <img src=\"../assets/images/close-icon.png\" class=\"close-icon\"></span></li>\n                                            <li><span>scope111.jpg <img src=\"../assets/images/close-icon.png\" class=\"close-icon\"></span></li>\n                                        </ul>\n                                    </div>\n                                    <div class=\" white-btn-main  d-flex justify-content-between\">\n                                        <div>\n                                            <button mat-raised-button color=\"\" class=\"print-submit-btn font-weight-600  white-btn\">ADD\n                                                ATTACHMENTS</button>\n                                        </div>\n                                        <div>\n                                            <button mat-raised-button color=\"\" class=\"print-submit-btn font-weight-600  white-btn\">DOWNLOAD\n                                                ALL</button>\n\n                                        </div>\n                                    </div>\n\n                                </div>\n                            </div>\n                        </div>\n\n\n                    </div>\n                </form>\n            </div>\n        </section>\n\n        <div class=\" white-btn-main pt-4 d-flex justify-content-between\">\n            <div class=\"col-12 col-md-6 col-lg-6 col-xl-6 col-sm-12\">\n                <button mat-raised-button color=\"primary\" class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMIT</button>\n\n\n            </div>\n            <div class=\"col-12 col-md-4 col-lg-4 col-xl-4 col-sm-12\">\n                <button mat-raised-button color=\"\" class=\"w-100 print-submit-btn  white-btn\">DELETE PROJECT MEMO</button>\n\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/project-memo-form/project-memo-form.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/project-memo-form/project-memo-form.component.ts ***!
  \******************************************************************/
/*! exports provided: ProjectMemoFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectMemoFormComponent", function() { return ProjectMemoFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ProjectMemoFormComponent = /** @class */ (function () {
    function ProjectMemoFormComponent() {
    }
    ProjectMemoFormComponent.prototype.ngOnInit = function () {
    };
    ProjectMemoFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-project-memo-form',
            template: __webpack_require__(/*! ./project-memo-form.component.html */ "./src/app/project-memo-form/project-memo-form.component.html"),
            styles: [__webpack_require__(/*! ./project-memo-form.component.css */ "./src/app/project-memo-form/project-memo-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProjectMemoFormComponent);
    return ProjectMemoFormComponent;
}());



/***/ }),

/***/ "./src/app/search-bar/search-bar.component.css":
/*!*****************************************************!*\
  !*** ./src/app/search-bar/search-bar.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaC1iYXIvc2VhcmNoLWJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/search-bar/search-bar.component.html":
/*!******************************************************!*\
  !*** ./src/app/search-bar/search-bar.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"mydashboard\" class=\"dashboard-section\">\n\n  <nav class=\"navbar-bottam bg-dark\">\n    <div class=\"container-fluid\">\n      <button mat-raised-button color=\"primary\" class=\"bg-color space-left\"><img src=\"../assets/images/addendum.png\"\n          class=\"dashboard-img active\">PROJECT MEMO</button>\n      <button mat-raised-button color=\"primary\" class=\"bg-color space-left\"><img src=\"../assets/images/addendum.png\"\n          class=\"dashboard-img active\">ADDENDUM</button>\n      <div class=\"input-group search-btn space-left\" style=\"height:33px;\">\n        <div style=\"position:relative;\">\n          <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n        </div>\n        <div class=\"input-group-btn search-btn-icon\" style=\"height:33px;\">\n          <button class=\"btn btn-default\" type=\"submit\">\n            <i style='font-size:18px' class='fas'>&#xf002;</i>\n          </button>\n        </div>\n      </div>\n    </div>\n  </nav>\n\n</section>\n"

/***/ }),

/***/ "./src/app/search-bar/search-bar.component.ts":
/*!****************************************************!*\
  !*** ./src/app/search-bar/search-bar.component.ts ***!
  \****************************************************/
/*! exports provided: SearchBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchBarComponent", function() { return SearchBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent() {
    }
    SearchBarComponent.prototype.ngOnInit = function () {
    };
    SearchBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-search-bar',
            template: __webpack_require__(/*! ./search-bar.component.html */ "./src/app/search-bar/search-bar.component.html"),
            styles: [__webpack_require__(/*! ./search-bar.component.css */ "./src/app/search-bar/search-bar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SearchBarComponent);
    return SearchBarComponent;
}());



/***/ }),

/***/ "./src/app/spinner/spinner.component.css":
/*!***********************************************!*\
  !*** ./src/app/spinner/spinner.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".spinner {\n    width: 70px;\n    position: absolute;\n    left: 50%;\n    top: 50%;\n}\n\n.spinner > div {\n  width: 18px;\n  height: 18px;\n  background-color: #13b5ea;\n  border-radius: 100%;\n  display: inline-block;\n  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;\n  animation: sk-bouncedelay 1.4s infinite ease-in-out both;\n}\n\n.spinner .bounce1 {\n  -webkit-animation-delay: -0.32s;\n  animation-delay: -0.32s;\n}\n\n.spinner .bounce2 {\n  -webkit-animation-delay: -0.16s;\n  animation-delay: -0.16s;\n}\n\n@-webkit-keyframes sk-bouncedelay {\n  0%, 80%, 100% { -webkit-transform: scale(0) }\n  40% { -webkit-transform: scale(1.0) }\n}\n\n@keyframes sk-bouncedelay {\n  0%, 80%, 100% { \n    -webkit-transform: scale(0);\n    transform: scale(0);\n  } 40% { \n    -webkit-transform: scale(1.0);\n    transform: scale(1.0);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3Bpbm5lci9zcGlubmVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0FBQ1o7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLGdFQUFnRTtFQUNoRSx3REFBd0Q7QUFDMUQ7O0FBRUE7RUFDRSwrQkFBK0I7RUFDL0IsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0UsK0JBQStCO0VBQy9CLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGdCQUFnQiw0QkFBNEI7RUFDNUMsTUFBTSw4QkFBOEI7QUFDdEM7O0FBRUE7RUFDRTtJQUNFLDJCQUEyQjtJQUMzQixtQkFBbUI7RUFDckIsRUFBRTtJQUNBLDZCQUE2QjtJQUM3QixxQkFBcUI7RUFDdkI7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NwaW5uZXIvc3Bpbm5lci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNwaW5uZXIge1xuICAgIHdpZHRoOiA3MHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG59XG5cbi5zcGlubmVyID4gZGl2IHtcbiAgd2lkdGg6IDE4cHg7XG4gIGhlaWdodDogMThweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzEzYjVlYTtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAtd2Via2l0LWFuaW1hdGlvbjogc2stYm91bmNlZGVsYXkgMS40cyBpbmZpbml0ZSBlYXNlLWluLW91dCBib3RoO1xuICBhbmltYXRpb246IHNrLWJvdW5jZWRlbGF5IDEuNHMgaW5maW5pdGUgZWFzZS1pbi1vdXQgYm90aDtcbn1cblxuLnNwaW5uZXIgLmJvdW5jZTEge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuMzJzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjMycztcbn1cblxuLnNwaW5uZXIgLmJvdW5jZTIge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuMTZzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjE2cztcbn1cblxuQC13ZWJraXQta2V5ZnJhbWVzIHNrLWJvdW5jZWRlbGF5IHtcbiAgMCUsIDgwJSwgMTAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKSB9XG4gIDQwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgxLjApIH1cbn1cblxuQGtleWZyYW1lcyBzay1ib3VuY2VkZWxheSB7XG4gIDAlLCA4MCUsIDEwMCUgeyBcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMCk7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcbiAgfSA0MCUgeyBcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMS4wKTtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMCk7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/spinner/spinner.component.html":
/*!************************************************!*\
  !*** ./src/app/spinner/spinner.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"spinner\">\n    <div class=\"bounce1\"></div>\n    <div class=\"bounce2\"></div>\n    <div class=\"bounce3\"></div>\n  </div>"

/***/ }),

/***/ "./src/app/spinner/spinner.component.ts":
/*!**********************************************!*\
  !*** ./src/app/spinner/spinner.component.ts ***!
  \**********************************************/
/*! exports provided: SpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpinnerComponent", function() { return SpinnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SpinnerComponent = /** @class */ (function () {
    function SpinnerComponent() {
    }
    SpinnerComponent.prototype.ngOnInit = function () {
    };
    SpinnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-spinner',
            template: __webpack_require__(/*! ./spinner.component.html */ "./src/app/spinner/spinner.component.html"),
            styles: [__webpack_require__(/*! ./spinner.component.css */ "./src/app/spinner/spinner.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SpinnerComponent);
    return SpinnerComponent;
}());



/***/ }),

/***/ "./src/app/user-list/user-list.component.css":
/*!***************************************************!*\
  !*** ./src/app/user-list/user-list.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItbGlzdC91c2VyLWxpc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/user-list/user-list.component.html":
/*!****************************************************!*\
  !*** ./src/app/user-list/user-list.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  user-list works!\n</p>\n"

/***/ }),

/***/ "./src/app/user-list/user-list.component.ts":
/*!**************************************************!*\
  !*** ./src/app/user-list/user-list.component.ts ***!
  \**************************************************/
/*! exports provided: UserListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListComponent", function() { return UserListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserListComponent = /** @class */ (function () {
    function UserListComponent() {
    }
    UserListComponent.prototype.ngOnInit = function () { };
    UserListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__(/*! ./user-list.component.html */ "./src/app/user-list/user-list.component.html"),
            styles: [__webpack_require__(/*! ./user-list.component.css */ "./src/app/user-list/user-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ }),

/***/ "./src/app/welcome-page/welcome-page.component.css":
/*!*********************************************************!*\
  !*** ./src/app/welcome-page/welcome-page.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUtcGFnZS93ZWxjb21lLXBhZ2UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/welcome-page/welcome-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/welcome-page/welcome-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n  <div>\n    <div class=\"welcome-wrapper\">\n      <div class=\" welcome-section pt-4\">\n        <div class=\"pb-3 heading-6 light-black font-weight-500\">\n          {{heading}}\n        </div>\n        <div class=\"pb-3 font-14 medium-black\">{{message}}</div>\n        <div class=\"welcome-btn\">\n          <button *ngIf=\"!showAddButton\" mat-raised-button color=\"primary\" (click)=\"resetFilters()\"\n            class=\"w-100 text-uppercase\">CLEAR SEARCH & FILTERS\n          </button>\n          <button *ngIf=\"showAddButton\" mat-raised-button color=\"primary\" (click)=\"addProjects()\"\n            class=\"w-100 text-uppercase\">Start a Project Memo Now\n          </button>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "./src/app/welcome-page/welcome-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/welcome-page/welcome-page.component.ts ***!
  \********************************************************/
/*! exports provided: WelcomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomePageComponent", function() { return WelcomePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var _dashboard_service_project_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../dashboard/_service/project.service */ "./src/app/dashboard/_service/project.service.ts");








var userHeading = "No matches found.";
var userMessage = "Your filters and/or search parameters didn\u2019t turn up\n  any matches. Make some adjustments or reset your filters to try again.";
var suDefaultHeading = "Welcome!";
var suDefaultMessage = "Get started by creating a Project Memo";
var suFilterHeading = "No matches found.";
// tslint:disable-next-line:max-line-length
var suFilterMessage = "Your filters and/or search parameters didn\u2019t turn up any matches. Make some adjustments or reset your filters to try again.";
var baDefaultHeading = "Welcome!";
// tslint:disable-next-line:max-line-length
var baDefaultMessage = "There are no unassigned documents needing your attention and none assigned to you pending submission to Rightsline.";
var baFilterHeading = "No matches found.";
var baFilterMessage = "Your filters and/or search parameters\ndidn\u2019t turn up any matches. Make some adjustments or reset your filters to try again.";
var WelcomePageComponent = /** @class */ (function () {
    function WelcomePageComponent(router, loader, alert, service) {
        this.router = router;
        this.loader = loader;
        this.alert = alert;
        this.service = service;
        this.userRole = "";
        this.currentUrl = "";
        this.clear = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.message = "Your filters and/or search parameters didn\u2019t turn up any matches.\n                      Make some adjustments or reset your filters to try again";
        this.heading = "Welcome !";
        this.showAddButton = false;
    }
    WelcomePageComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user = _utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__["UserAuthHelper"].GetProfile();
        this.userRole = user ? user.type : "";
        this.currentUrl = document.location.href;
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.currentUrl = event.url;
                _this.setMessage();
            }
        });
        this.setMessage();
    };
    WelcomePageComponent.prototype.resetFilters = function () {
        this.clear.emit();
    };
    WelcomePageComponent.prototype.setMessage = function () {
        if (this.currentUrl.includes("user")) {
            this.heading = userHeading;
            this.message = userMessage;
        }
        else if (this.currentUrl.includes("dashboard")) {
            if (this.userRole.includes("Standard")) {
                if (this.hasFilters) {
                    this.heading = suFilterHeading;
                    this.message = suFilterMessage;
                }
                else {
                    this.heading = suDefaultHeading;
                    this.message = suDefaultMessage;
                    this.showAddButton = true;
                }
            }
            else {
                if (this.hasFilters) {
                    this.heading = baFilterHeading;
                    this.message = baFilterMessage;
                }
                else {
                    this.heading = baDefaultHeading;
                    this.message = baDefaultMessage;
                }
            }
        }
    };
    WelcomePageComponent.prototype.addProjects = function () {
        var _this = this;
        this.loader.show();
        this.service.generateProjectId().subscribe(function (res) {
            if (res.data) {
                var projectId = res.data;
                var url = "/projects/" + projectId;
                _this.loader.hide();
                _this.router.navigate([url]);
            }
        }, function (error) {
            _this.loader.hide();
            _this.alert.error(error.error.details);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WelcomePageComponent.prototype, "clear", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WelcomePageComponent.prototype, "hasFilters", void 0);
    WelcomePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-welcome-page",
            template: __webpack_require__(/*! ./welcome-page.component.html */ "./src/app/welcome-page/welcome-page.component.html"),
            styles: [__webpack_require__(/*! ./welcome-page.component.css */ "./src/app/welcome-page/welcome-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
            _dashboard_service_project_service__WEBPACK_IMPORTED_MODULE_6__["ProjectService"]])
    ], WelcomePageComponent);
    return WelcomePageComponent;
}());



/***/ }),

/***/ "./src/assets/scripts/ngx-s3-uploader/index.js":
/*!*****************************************************!*\
  !*** ./src/assets/scripts/ngx-s3-uploader/index.js ***!
  \*****************************************************/
/*! exports provided: S3UploaderModule, S3UploaderComponent, CONFIG, S3UploaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S3UploaderModule", function() { return S3UploaderModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S3UploaderComponent", function() { return S3UploaderComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONFIG", function() { return CONFIG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S3UploaderService", function() { return S3UploaderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var aws_sdk__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! aws-sdk */ "./node_modules/aws-sdk/lib/browser.js");
/* harmony import */ var aws_sdk__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(aws_sdk__WEBPACK_IMPORTED_MODULE_3__);





var CONFIG = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('config');
var S3UploaderService = (function () {
    /**
     * @param {?} s3UploaderConfig
     */
    function S3UploaderService(s3UploaderConfig) {
        this.s3UploaderConfig = s3UploaderConfig;
        var credentials = {
            accessKeyId: s3UploaderConfig.credentials.accessKeyId,
            secretAccessKey: s3UploaderConfig.credentials.secretAccessKey,
        };
        // if the identity pool id  is populated I use the Cognito credentials to authenticate
        if (s3UploaderConfig.credentials.identityPoolId) {
            credentials = new aws_sdk__WEBPACK_IMPORTED_MODULE_3__["CognitoIdentityCredentials"]({
                IdentityPoolId: s3UploaderConfig.credentials.identityPoolId,
                Logins: this.s3UploaderConfig.credentials.logins || {},
            });
        }
        // if role is populated I use the Web Federated Identity to authenticate
        if (s3UploaderConfig.credentials.roleArn) {
            credentials = new aws_sdk__WEBPACK_IMPORTED_MODULE_3__["WebIdentityCredentials"]({
                RoleArn: s3UploaderConfig.credentials.roleArn,
                RoleSessionName: s3UploaderConfig.credentials.roleName,
                ProviderId: this.s3UploaderConfig.credentials.providerId,
                WebIdentityToken: this.s3UploaderConfig.credentials.token
            });
        }
        aws_sdk__WEBPACK_IMPORTED_MODULE_3__["config"].region = s3UploaderConfig.region;
        aws_sdk__WEBPACK_IMPORTED_MODULE_3__["config"].credentials = credentials;
        this.client = new aws_sdk__WEBPACK_IMPORTED_MODULE_3__["S3"]();
    }
    /**
     * @param {?} providerName
     * @param {?} token
     * @return {?}
     */
    S3UploaderService.prototype.authenticate = function (providerName, token) {
        if (aws_sdk__WEBPACK_IMPORTED_MODULE_3__["config"].credentials instanceof aws_sdk__WEBPACK_IMPORTED_MODULE_3__["CognitoIdentityCredentials"]) {
            aws_sdk__WEBPACK_IMPORTED_MODULE_3__["config"].credentials['params']['Logins'] = {};
            aws_sdk__WEBPACK_IMPORTED_MODULE_3__["config"].credentials['params']['Logins'][providerName] = token;
            // Expire credentials to refresh them on the next request.
            aws_sdk__WEBPACK_IMPORTED_MODULE_3__["config"].credentials['expired'] = true;
        }
    };
    /**
     * @param {?} file
     * @param {?=} acl
     * @param {?=} bucket
     * @return {?}
     */
    S3UploaderService.prototype.upload = function (file, acl, bucket) {
        var _this = this;
        if (acl === void 0) { acl = 'public-read'; }
        return rxjs_Observable__WEBPACK_IMPORTED_MODULE_2__["Observable"].create(function (observer) {
            _this.client.upload({ Key: acl, Body: file, Bucket: bucket || _this.s3UploaderConfig.bucket }, function (error, data) {
                if (error) {
                    return observer.error(error);
                }
                observer.next(data);
                observer.complete();
            });
        });
    };
    return S3UploaderService;
}());
S3UploaderService.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/**
 * @nocollapse
 */
S3UploaderService.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [CONFIG,] },] },
]; };

var S3UploaderComponent = (function () {
    /**
     * @param {?} s3UploaderService
     */
    function S3UploaderComponent(s3UploaderService) {
        this.s3UploaderService = s3UploaderService;
        this.success = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.error = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    /**
     * @return {?}
     */
    S3UploaderComponent.prototype.onClick = function () {
        var /** @type {?} */ event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    /**
     * @return {?}
     */
    S3UploaderComponent.prototype.onChange = function () {
        var _this = this;
        var /** @type {?} */ file = this.fileInput.nativeElement.files[0];
        this.s3UploaderService.upload(file)
            .subscribe(function (data) {
            _this.success.next(data);
        }, function (error) {
            _this.error.next(error);
        });
    };
    return S3UploaderComponent;
}());
S3UploaderComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 's3-uploader',
                template: "\n        <input #fileInput style='display: none;' type='file' (change)='onChange()'>\n        <ng-content></ng-content>\n    ",
            },] },
];
/**
 * @nocollapse
 */
S3UploaderComponent.ctorParameters = function () { return [
    { type: S3UploaderService, },
]; };
S3UploaderComponent.propDecorators = {
    'success': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'error': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'fileInput': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['fileInput',] },],
    'onClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['click',] },],
};

var S3UploaderModule = (function () {
    function S3UploaderModule() {
    }
    /**
     * @param {?} config
     * @return {?}
     */
    S3UploaderModule.forRoot = function (config$$1) {
        return {
            ngModule: S3UploaderModule,
            providers: [
                { provide: CONFIG, useValue: config$$1 },
                S3UploaderService
            ]
        };
    };
    return S3UploaderModule;
}());
S3UploaderModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
                ],
                declarations: [
                    S3UploaderComponent,
                ],
                exports: [
                    S3UploaderComponent,
                ]
            },] },
];
/**
 * @nocollapse
 */
S3UploaderModule.ctorParameters = function () { return []; };




/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var localBaseUrl = "http://localhost:3333/api/"; // "http://10.0.0.47:4000/api/";
var liveBaseUrl = "https://api-llama.participant.net/api/";
var useLocal = true;
var environment = {
    production: false,
    baseUrl: useLocal ? localBaseUrl : liveBaseUrl
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/bhlap-03/Documents/node projects/PMA/frontend/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map