(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/observable/timer.js":
/*!************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/observable/timer.js ***!
  \************************************************************/
/*! exports provided: timer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "timer", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["timer"]; });


//# sourceMappingURL=timer.js.map

/***/ }),

/***/ "./src/app/_models/projects.model.ts":
/*!*******************************************!*\
  !*** ./src/app/_models/projects.model.ts ***!
  \*******************************************/
/*! exports provided: ProjectFilterModel, ProjectListModel, ProjectFormModel, AssignToModel, CatalogListModel, CCListModel, AttachmentListModel, AttachmentListExtModel, AddendumFormModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectFilterModel", function() { return ProjectFilterModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectListModel", function() { return ProjectListModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectFormModel", function() { return ProjectFormModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssignToModel", function() { return AssignToModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogListModel", function() { return CatalogListModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CCListModel", function() { return CCListModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttachmentListModel", function() { return AttachmentListModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttachmentListExtModel", function() { return AttachmentListExtModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddendumFormModel", function() { return AddendumFormModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _users_filter_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users-filter.model */ "./src/app/_models/users-filter.model.ts");


var ProjectFilterModel = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ProjectFilterModel, _super);
    function ProjectFilterModel() {
        var _this = _super.call(this) || this;
        _this.status = [''];
        _this.type = [''];
        _this.assignment = [''];
        _this.startDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        _this.endDate = new Date();
        return _this;
    }
    return ProjectFilterModel;
}(_users_filter_model__WEBPACK_IMPORTED_MODULE_1__["PageDetailsModel"]));

var ProjectListModel = /** @class */ (function () {
    function ProjectListModel() {
    }
    return ProjectListModel;
}());

var ProjectFormModel = /** @class */ (function () {
    function ProjectFormModel() {
        // tslint:disable-next-line:no-string-literal
        this.assignedTo = "";
        this.cc = "";
        this.projectName = "";
        this.catalog = "";
        this.contractorCompanyName = "";
        this.contractorContactEmail = "";
        this.contractorContactPhone = "";
        this.contractorServices = "";
        this.approxHours = "";
        this.deliverablesDescription = "";
        this.deliverySchedule = "";
        this.totalFees = "";
        this.paymentSchedule = "";
        this.ownershipOfDeliverables = "";
        this.rightsLicensedByContractor = "";
        this.intendedUseByContractor = "";
        this.intendedUseByParticipant = "";
        this.assetsProvidedByParticipant = "";
        this.assetsProvidedByContractor = "";
        this.thirdPartyAssets = "";
        this.pmURL = "";
        this.attachment = [];
        this.pmStatus = "";
        this.contractorContactName = "";
        this.contractorWorkOnsite = null;
        this.contractorInsuranceGeneral = false;
        this.contractorInsuranceEnO = false;
        this.contractorInsuranceWorkersComp = false;
        this.contractorInsuranceProduction = false;
        this.effectiveDate = null; // new Date();
        this.expirationDate = null; // new Date();
        this.mailList = [];
        this.from = "";
    }
    return ProjectFormModel;
}());

var AssignToModel = /** @class */ (function () {
    function AssignToModel() {
    }
    return AssignToModel;
}());

var CatalogListModel = /** @class */ (function () {
    function CatalogListModel() {
    }
    return CatalogListModel;
}());

var CCListModel = /** @class */ (function () {
    function CCListModel() {
    }
    return CCListModel;
}());

var AttachmentListModel = /** @class */ (function () {
    function AttachmentListModel() {
    }
    return AttachmentListModel;
}());

var AttachmentListExtModel = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AttachmentListExtModel, _super);
    function AttachmentListExtModel() {
        var _this = _super.call(this) || this;
        _this.NotLoaded = false;
        _this.id = "";
        return _this;
    }
    return AttachmentListExtModel;
}(AttachmentListModel));

var AddendumFormModel = /** @class */ (function () {
    function AddendumFormModel() {
        this.attachment = [];
        this.mailList = [];
        this.parentProjectMemo = "";
        this.reasons = "";
        this.changes = "";
        this.cc = "";
        this.counterparties = "";
        this.adID = "";
        this.assignedTo = "";
        this.responsibleParty = "";
        this.adStatus = "";
        this.adURL = "";
        this.projectName = "";
        this.catalog = "";
        this.contractorCompanyName = "";
        this.projectCreatedAt = "";
    }
    return AddendumFormModel;
}());



/***/ }),

/***/ "./src/app/_models/user-list.model.ts":
/*!********************************************!*\
  !*** ./src/app/_models/user-list.model.ts ***!
  \********************************************/
/*! exports provided: UserListModel, UserModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListModel", function() { return UserListModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModel", function() { return UserModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var UserListModel = /** @class */ (function () {
    function UserListModel() {
    }
    return UserListModel;
}());

var UserModel = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UserModel, _super);
    function UserModel() {
        var _this = _super.call(this) || this;
        _this.type = "";
        _this.password = "";
        return _this;
    }
    return UserModel;
}(UserListModel));



/***/ }),

/***/ "./src/app/_models/users-filter.model.ts":
/*!***********************************************!*\
  !*** ./src/app/_models/users-filter.model.ts ***!
  \***********************************************/
/*! exports provided: PageDetailsModel, UsersFilterModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageDetailsModel", function() { return PageDetailsModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersFilterModel", function() { return UsersFilterModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var PageDetailsModel = /** @class */ (function () {
    function PageDetailsModel() {
        this.limit = 10;
        this.offset = 0;
        this.sort = "";
        this.order = "desc";
    }
    return PageDetailsModel;
}());

var UsersFilterModel = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UsersFilterModel, _super);
    function UsersFilterModel() {
        var _this = _super.call(this) || this;
        _this.type = [''];
        _this.status = [''];
        _this.searchText = "";
        return _this;
    }
    return UsersFilterModel;
}(PageDetailsModel));



/***/ }),

/***/ "./src/app/_utility/filedownload.helper.ts":
/*!*************************************************!*\
  !*** ./src/app/_utility/filedownload.helper.ts ***!
  \*************************************************/
/*! exports provided: DwonloadHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DwonloadHelper", function() { return DwonloadHelper; });
var DwonloadHelper = /** @class */ (function () {
    function DwonloadHelper() {
    }
    DwonloadHelper.downloadFile = function (sUrl, file) {
        // iOS devices do not support downloading. We have to inform user about this.
        if (/(iP)/g.test(navigator.userAgent)) {
            // alert('Your device does not support files downloading. Please try again in desktop browser.');
            window.open(sUrl, "_blank");
            return false;
        }
        // If in Chrome or Safari - download via virtual link click
        if (DwonloadHelper.isChrome || DwonloadHelper.isSafari) {
            // Creating new link node.
            var link = document.createElement("a");
            link.href = sUrl;
            link.setAttribute("target", "_blank");
            if (link.download !== undefined) {
                // Set HTML5 download attribute. This will prevent file from opening if supported.
                var fileName = sUrl.substring(sUrl.lastIndexOf("/") + 1, sUrl.length);
                link.download = fileName;
            }
            // Dispatching click event.
            if (document.createEvent) {
                var e = document.createEvent("MouseEvents");
                e.initEvent("click", true, true);
                link.dispatchEvent(e);
                return true;
            }
        }
        // Force file download (whether supported by server).
        if (sUrl.indexOf("?") === -1) {
            sUrl += "?download";
        }
        window.open(sUrl, "_blank");
        return true;
    };
    DwonloadHelper.isChrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
    DwonloadHelper.isSafari = navigator.userAgent.toLowerCase().indexOf("safari") > -1;
    return DwonloadHelper;
}());



/***/ }),

/***/ "./src/app/dashboard/_gaurd/confirm.deactivate.gaurd.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/_gaurd/confirm.deactivate.gaurd.ts ***!
  \**************************************************************/
/*! exports provided: ConfirmDeactivateGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmDeactivateGuard", function() { return ConfirmDeactivateGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_shared/confirmation-dialogue/confirmation-dialogue.service */ "./src/app/_shared/confirmation-dialogue/confirmation-dialogue.service.ts");



var ConfirmDeactivateGuard = /** @class */ (function () {
    function ConfirmDeactivateGuard(confiramtion) {
        this.confiramtion = confiramtion;
    }
    ConfirmDeactivateGuard.prototype.canDeactivate = function (target) {
        if (target.isUploadIncomplete) {
            return this.confiramtion.confirm('Unsaved Changes', 
            // tslint:disable-next-line:max-line-length
            'You have an upload in progress, leaving now will cause this upload to be abandoned. Are you sure you want to leave?', 'Leave', true, 'Stay');
        }
        else if (target.isFormDirty) {
            return this.confiramtion.confirm('Unsaved Changes', 
            // tslint:disable-next-line:max-line-length
            'Some of the changes you have made are not yet saved. If you leave now those changes will be lost. Are you sure you want to leave?', 'SAVE & CONTINUE');
        }
        return true;
    };
    ConfirmDeactivateGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_confirmation_dialogue_confirmation_dialogue_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationDialogueService"]])
    ], ConfirmDeactivateGuard);
    return ConfirmDeactivateGuard;
}());



/***/ }),

/***/ "./src/app/dashboard/_service/file-upload.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/dashboard/_service/file-upload.service.ts ***!
  \***********************************************************/
/*! exports provided: FileUploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadService", function() { return FileUploadService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_assets_scripts_ngx_s3_uploader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/assets/scripts/ngx-s3-uploader */ "./src/assets/scripts/ngx-s3-uploader/index.js");



// import S3 from 'aws-s3';
var FileUploadService = /** @class */ (function () {
    function FileUploadService(s3UploaderService) {
        this.s3UploaderService = s3UploaderService;
        this.config = {
            bucketName: 'myBucket',
            dirName: 'photos',
            region: 'eu-west-1',
            accessKeyId: 'ANEIFNENI4324N2NIEXAMPLE',
            secretAccessKey: 'cms21uMxçduyUxYjeg20+DEkgDxe6veFosBT7eUgEXAMPLE',
            s3Url: 'https://my-s3-url.com/',
        };
        //  this.S3Client = new S3(this.config);
    }
    FileUploadService.prototype.uploadFile = function (file, newFileName) {
        return this.s3UploaderService.upload(file, newFileName, 'participant-media-files');
    };
    FileUploadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_assets_scripts_ngx_s3_uploader__WEBPACK_IMPORTED_MODULE_2__["S3UploaderService"]])
    ], FileUploadService);
    return FileUploadService;
}());



/***/ }),

/***/ "./src/app/dashboard/_service/users.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/_service/users.service.ts ***!
  \*****************************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_models/_config */ "./src/app/_models/_config.ts");
/* harmony import */ var src_app_utility_string_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_utility/string.helper */ "./src/app/_utility/string.helper.ts");





var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
    }
    UsersService.prototype.updatePassword = function (password) {
        var url = _models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].updatePassword;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.put(url, { password: password }, httpOptions);
    };
    UsersService.prototype.updateUser = function (user) {
        var body = Object.assign({}, user);
        body = src_app_utility_string_helper__WEBPACK_IMPORTED_MODULE_4__["StringHelper"].RemoveRedundantSpacesFromObject(body);
        var url = _models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].updateUser;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.put(url, body, httpOptions);
    };
    UsersService.prototype.addUser = function (user) {
        var body = Object.assign({}, user);
        body = src_app_utility_string_helper__WEBPACK_IMPORTED_MODULE_4__["StringHelper"].RemoveRedundantSpacesFromObject(body);
        body.host = document.location.origin + "/reset/";
        var url = _models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].addUser;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.post(url, body, httpOptions);
    };
    UsersService.prototype.getUsers = function (requestModel) {
        var url = _models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getUsers;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.post(url, requestModel, httpOptions);
    };
    UsersService.prototype.deleteUser = function (user) {
        var url = _models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].deleteUser + user.id;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.delete(url, httpOptions);
    };
    UsersService.prototype.myProfile = function () {
        var url = _models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getuserprofile;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Content-Type": "application/json",
                auth_token: localStorage.getItem("token")
            })
        };
        return this.http.get(url, httpOptions);
    };
    UsersService.prototype.getCountryCodeList = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.http.get(_models_config__WEBPACK_IMPORTED_MODULE_3__["URLS"].getCountryCodeList).toPromise()];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, this.augmentPhoneCodeList(response)];
                }
            });
        });
    };
    UsersService.prototype.augmentPhoneCodeList = function (countryList) {
        var countryCodeOptions = [];
        var countryCodeObj;
        for (var _i = 0, countryList_1 = countryList; _i < countryList_1.length; _i++) {
            var countryObj = countryList_1[_i];
            countryCodeObj = {
                label: "(+" + countryObj.callingCodes[0] + ") " + countryObj.name,
                value: "+" + countryObj.callingCodes[0] + " ",
                flag: countryObj.flag,
            };
            countryCodeOptions.push(countryCodeObj);
        }
        return countryCodeOptions;
    };
    UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/app/dashboard/addendum-form/addendum-form.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/dashboard/addendum-form/addendum-form.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9hZGRlbmR1bS1mb3JtL2FkZGVuZHVtLWZvcm0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/addendum-form/addendum-form.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/dashboard/addendum-form/addendum-form.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-own\" #f=\"ngForm\" autocomplete=\"off\" novalidate>\n  <fieldset [disabled]=\"formDisabled\">\n    <section class=\"dashboard-section\">\n      <nav class=\"navbar-bottam\">\n        <div class=\"container-own\">\n          <div class=\"row justify-content-between align-items-center\">\n            <div class=\"col-12 col-md\">\n              <p class=\"light-gray\">\n                Status: <span class=\"text-white pl-1 cursor\">\n                  {{addendum.adStatus == 'Submitted' && !(userRole == 'Standard') ? 'Needs Pickup' :  addendum.adStatus }}\n                </span>\n              </p>\n            </div>\n            <div class=\"print-memo-submit col-auto\">\n              <div class=\"light-gray print-memo-change\">\n                <div *ngIf=\"savingProgress\" class=\"px-1\">Saving...</div>\n                <div *ngIf=\"!(isFormDirty || isUploadIncomplete) && !savingProgress\" class=\"px-1\">All Changes Saved</div>\n                <div *ngIf=\"!savingProgress && (isFormDirty || isUploadIncomplete)\" class=\"px-1\">Saving</div>\n                <div (click)=\"print()\" class=\"px-3\">\n                  <img src=\"../assets/images/print-icon.png\" class=\"cursor print-img\">\n                </div>\n                <div *ngIf=\"userRole == 'Standard'\" class=\"pl-1\">\n                  <span (click)=\"(f.submitted = true); submit()\">\n                    <button *ngIf=\"addendum.adStatus == 'Draft'\" type=\"button\" mat-raised-button color=\"primary\"\n                      class=\"bg-color print-submit-btn\">Submit</button>\n                  </span>\n                  <button *ngIf=\"addendum.adStatus == 'Submitted' || addendum.adStatus == 'Assigned'\" type=\"button\"\n                    mat-raised-button color=\"primary\" disabled class=\"bg-color print-submit-btn\">Submitted</button>\n                  <button *ngIf=\"addendum.adStatus == 'Accepted'\" disabled type=\"button\" mat-raised-button\n                    color=\"primary\" class=\"bg-color print-submit-btn\">Accepted</button>\n                </div>\n                <div *ngIf=\"userRole !== 'Standard'\" class=\"pl-1\">\n                  <button disabled *ngIf=\"addendum.adStatus == 'Draft'\" type=\"button\" mat-raised-button color=\"primary\"\n                    class=\"bg-color print-submit-btn\">Submit</button>\n                  <button *ngIf=\"addendum.adStatus == 'Submitted' || addendum.adStatus == 'Assigned'\" type=\"button\"\n                    mat-raised-button color=\"primary\" class=\"bg-color print-submit-btn\"\n                    (click)=\"(f.submitted = true); submit()\">Submit</button>\n                  <button disabled *ngIf=\"addendum.adStatus == 'Accepted'\" type=\"button\" mat-raised-button\n                    color=\"primary\" class=\"bg-color print-submit-btn\">Accepted</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </nav>\n    </section>\n    <div id=\"mydashboard\" class=\"dashboard-section\">\n      <div class=\"container-own own-dashboard-section\">\n        <section class=\"general-form \">\n          <div class=\"d-flex justify-content-between align-items-center\">\n            <h5 class=\"heading-5 font-weight-500\">GENERAL INFORMATION</h5>\n            <h5 class=\"heading-5 font-weight-500\">{{addendum.adID}}</h5>\n          </div>\n          <div class=\"pt-3\">\n            <!-- Assigned To    -->\n            <div class=\"form-group\" [ngClass]=\"{'active': id === 1,\n                          'error':!(formDisabled || userRole === 'Standard') && (f.submitted || assignTo.touched) && !assignTo.valid,\n                          'disabled': formDisabled || userRole === 'Standard' || addendum.adStatus === 'Draft' }\"\n              id=\"1\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"sign-input-2\" floatLabel=\"always\">\n                    <mat-label>Assigned To</mat-label>\n                    <!-- <mat-select [(ngModel)]=\"addendum.assignedTo\"\n                      [disabled]=\"formDisabled || userRole === 'Standard' || addendum.adStatus === 'Draft'\"\n                      name=\"assignedTo\" #assignTo=\"ngModel\" disableRipple placeholder=\"_ _\" (focus)=\"focus(1)\"\n                      (blur)=\"blur()\" (selectionChange)=\"selectionChanged()\" required>\n                      <mat-option *ngFor=\"let user of Users\" [value]=\"user.uniqueid\">{{user.firstName}}\n                        {{user.lastName}}\n                      </mat-option>\n                    </mat-select> -->\n                    <input type=\"text\" placeholder=\"\" (focus)=\"focus(1)\" (blur)=\"blur()\" name=\"contractorCompanyName\"\n                      [disabled]=\"formDisabled || userRole === 'Standard' || addendum.adStatus === 'Draft'\"\n                      (focus)=\"focus(1)\" (blur)=\"blur()\" [(ngModel)]=\"addendum.assignedUser\" name=\"assignedTo\"\n                      #assignTo=\"ngModel\" (keyup)=\"FilterAssignTo()\" (change)=\"ValidateAssignTo()\" matInput\n                      [matAutocomplete]=\"autoassign\" required>\n                    <mat-autocomplete #autoassign=\"matAutocomplete\" [displayWith]=\"displayFn\"\n                      (optionSelected)='selectionChanged($event.option)'>\n                      <mat-option *ngFor=\"let option of Users\" [value]=\"option.uniqueid\">\n                        {{option.firstName + ' ' + option.lastName}}\n                      </mat-option>\n                    </mat-autocomplete>\n                    <mat-error>\n                      *You must assign a Business Affairs member before sending this to Rightsline\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 1 }\" id=\"1\">\n                    <div>Once the project memo has been approved and submitted,\n                      this will signify which Business Affairs team member has been assigned to the project memo.</div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <!-- Send Copies To -->\n            <div class=\"form-group\"\n              [ngClass]=\"{'active': id === 2,'error' : (f.submitted || cc.touched) && (!cc.valid || ccIsInvalid) }\"\n              id=\"2\">\n              <div class=\"row align-items-start\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (click)=\"focus(2, cc);\">\n                  <mat-form-field\n                    class=\"example-full-width sign-input-2 sign-input-3 wrapper-padding multiple-email-own\"\n                    floatLabel=\"always\">\n                    <div class=\"attachment-section-1\">\n                      <ul class=\"d-list-item attachment-items-list list-unstyled\">\n                        <li *ngFor=\"let p of CCList; let ind = index;\">\n                          <div class=\"attachment-item\">\n                            <p>{{p.name ? p.name : p.email}}\n                              <span *ngIf=\"p.name\">{{ '('+ p.email + ')'}}</span>\n                            </p>\n                            <img (click)=\"removeFromCCList(ind)\" src=\"../assets/images/close-icon.png\"\n                              class=\"close-icon\">\n                          </div>\n                        </li>\n                        <li class=\"simple-input\">\n                          <!-- <input matInput > -->\n                          <div>\n                            <mat-label class=\"label-own mat-form-field-label\">Send Copies To</mat-label>\n                            <!-- onkeypress=\"this.style.width = ((this.value.length + 1) * 10) + 'px';\" style=\"width: 10px;\"  -->\n                            <input id=\"ccinput\" type=\"text\" matInput [matAutocomplete]=\"auto\" [(ngModel)]=\"addendum.cc\"\n                              [ngClass]=\"{'d-none': !addendum.cc && id !== 2}\"\n                              (keyup)=\"SaveProgress(); (addendum.cc && addendum.cc.length > 1 && searchPeopleEmail()); (($event.code === 'Enter' || $event.code === 'Comma') && addendum.cc && addEmailToList())\"\n                              name=\"cc\" #cc=\"ngModel\" (focus)=\"focus(2)\" (blur)=\"blur(); sendCopiesBlur();\" email>\n                          </div>\n                          <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)='getPosts($event.option)'>\n                            <mat-option class=\"multi-email-option\" *ngFor=\"let option of CCSuggestions\"\n                              [value]=\"option.email\">\n                              <strong style=\"font-size: 14px;\">\n                                {{option.name}}\n                              </strong>\n                              <div style=\"font-size: 12px;opacity: 0.8;\">\n                                {{option.email}}\n                              </div>\n                            </mat-option>\n                          </mat-autocomplete>\n                        </li>\n                      </ul>\n                    </div>\n                    <mat-error *ngIf=\"!ccIsInvalid && !cc.valid\">\n                      Please enter a valid email address\n\n                    </mat-error>\n                  </mat-form-field>\n                  <span class=\"mat-error outer-error\" *ngIf=\"ccIsInvalid\">\n                    *One or more provided values is not a valid email address\n                  </span>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 2 }\" id=\"2\">\n                    <div>Make sure to CC others who need to be in the know:</div>\n                    <ul class=\"tooltip-items list-unstyled\">\n                      <li>Department heads</li>\n                      <li>Other relevant personnel</li>\n                    </ul>\n                    <!-- <div class=\"pb-3\">Enter their email addresses here.</div> -->\n                    <div><span>Tip</span> : <i>For Participant Media personnel you can\n                        also start typing their name and select from existing matches.</i> </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <!-- Project Name -->\n            <div class=\"form-group\" [ngClass]=\"{'active': id === 3 }\" id=\"3\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 position-relative\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <input matInput name=\"projectName\" #projectName=\"\" disabled\n                      [value]=\"addendum.contractorCompanyName + ' on ' + addendum.projectName + ' from ' +  addendum.catalog + ' ' + (addendum.projectCreatedAt | date : 'MM/dd/yy')\"\n                      placeholder=\"Project Memo *\" (focus)=\"focus(3)\" (blur)=\"blur()\">\n                  </mat-form-field>\n                  <div (click)=\"viewProject()\" class=\"project-memo-bottom text-color font-14 cursor\">VIEW PROJECT MEMO\n                  </div>\n                  <div id=\"myDiv\" class=\"project-memo-bottom text-color font-14 cursor d-none\">\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"4\"\n              [ngClass]=\"{'active': id === 4, 'error': (f.submitted || counterparties.touched) && !counterparties.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      name=\"counterparties\" #counterparties=\"ngModel\" maxlength=\"10000\"\n                      [(ngModel)]=\"addendum.counterparties\" placeholder=\"Counterparties to Original Agreement*\"\n                      (focus)=\"focus(4)\" (blur)=\"blur()\" (keyup)=\"SaveProgress()\" required></textarea>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((addendum && addendum.counterparties) ? addendum.counterparties.length : 0 )| number }} /\n                      {{ 10000 | number}}</div>\n                    <mat-error *ngIf=\"!counterparties.valid\">\n                      *Please provide information about the counterparties to the original agreement\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 4 }\" id=\"4\">\n                    <div>Please list the Contractor that the original project memo was\n                      for (including the contact name, address, and email address).</div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"5\"\n              [ngClass]=\"{'active': id === 5, 'error': (f.submitted || responsibleParty.touched) && !responsibleParty.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      name=\"responsibleParty\" (keyup)=\"SaveProgress()\" [(ngModel)]=\"addendum.responsibleParty\" required\n                      #responsibleParty=\"ngModel\" placeholder=\"Responsible Party*\" (focus)=\"focus(5)\"\n                      (blur)=\"blur()\"></textarea>\n                    <mat-error>\n                      *Please provide information about the responsible party\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 5 }\" id=\"5\">\n                    <div>Please list the name of the contact at Participant.</div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </section>\n        <section class=\"general-form mt-2\">\n          <div class=\"d-flex justify-content-between align-items-center\">\n            <h5 class=\"pt-3 heading-5 font-weight-500\">CHANGES TO EXISTING AGREEMENT AND/OR ADDITIONAL TERMS</h5>\n          </div>\n          <div class=\"pt-3\">\n            <div class=\"form-group\" id=\"6\"\n              [ngClass]=\"{'active': id === 6, 'error': (f.submitted || changes.touched) && !changes.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      #changes=\"ngModel\" (keyup)=\"SaveProgress()\" [(ngModel)]=\"addendum.changes\" required\n                      placeholder=\"Description of Changes and/or Additional Terms *\" name=\"contractorCompanyName\"\n                      (focus)=\"focus(6)\" (blur)=\"blur()\" #contractorCompanyName=\"\"></textarea>\n                    <mat-error>\n                      *Please provide information about the changes\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 6 }\" id=\"6\">\n                    <div>Please list and describe any changes and/or additional terms to the original engagement.</div>\n                    <div><span>e.g.</span> : <div>any additional services, deliverables and/or payments</div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"7\"\n              [ngClass]=\"{'active': id === 7, 'error': (f.submitted || reasons.touched) && !reasons.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      #reasons=\"ngModel\" [(ngModel)]=\"addendum.reasons\" placeholder=\"Reason(s) for Changes *\"\n                      (keyup)=\"SaveProgress()\" name=\"reasons\" (focus)=\"focus(7)\" (blur)=\"blur()\"\n                      #contractorContactName=\"\" required></textarea>\n                    <mat-error>\n                      *Please provide information about the reason the changes are necessary\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 7 }\" id=\"7\">\n                    <div>Please let us know why the changes above need to be made.</div>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"form-group mt-3\"\n              [ngClass]=\"{'active': id === 25, 'error' : showFileSizeError, 'disabled' : formDisabled }\" id=\"25\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseenter)=\"focus(25)\" (click)=\"focus(25)\"\n                  (mouseleave)=\"blur()\">\n                  <div class=\"option-radio download-attachments\">\n                    <p class=\"filter-font\">Attachments</p>\n                    <div class=\"attachment-section\">\n                      <ul class=\"d-list-item attachment-items-list list-unstyled\">\n                        <li *ngFor=\"let f of attachments\">\n                          <div class=\"attachment-item\">\n                            <p>\n                              <span class=\"downloadItem cursor\"\n                                (click)=\"!f.NotLoaded && downloadSingleFile(f)\">{{f.file}}</span>\n                            </p>\n                            <span (click)=\"removeAttachments(f)\">\n                              <img class=\"cursor\" *ngIf=\"!f.NotLoaded\" src=\"../assets/images/close-icon.png\"\n                                class=\"close-icon\">\n                            </span>\n                            <mat-spinner *ngIf=\"f.NotLoaded\" diameter=\"18\" class=\"close-icon\"></mat-spinner>\n                          </div>\n                        </li>\n                      </ul>\n                    </div>\n                    <div class=\" white-btn-main d-flex justify-content-between row\">\n                      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5 mb-2\">\n                        <input id=\"fileInput\" type=\"file\" multiple class=\"d-none\" (change)=\"uploadAttachment($event)\"\n                          #fileLabel>\n                        <button [disabled]=\"formDisabled\" (focus)=\"focus(25)\" (blur)=\"blur()\" type=\"button\"\n                          (click)=\"fileLabel.click(); focus(25)\" mat-raised-button color=\"\"\n                          class=\"print-submit-btn font-weight-600  white-btn\">ADD\n                          ATTACHMENTS </button>\n                      </div>\n                      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5\">\n                        <span (click)=\"downLoadFiles(); focus(25)\">\n                          <button (focus)=\"focus(25)\" (blur)=\"blur()\" type=\"button\"\n                            *ngIf=\"attachments && attachments.length > 0\" mat-raised-button color=\"\"\n                            class=\"print-submit-btn font-weight-600  white-btn\">\n                            DOWNLOAD ALL</button>\n                        </span>\n                      </div>\n                    </div>\n                  </div>\n                  <mat-error *ngIf=\"showFileSizeError\">\n                    Each file must be no more than 25mb.\n                  </mat-error>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 25 }\" id=\"25\">\n                    <div>Wherever possible, please ask for a written proposal from the Contractor that describes the\n                      scope of their services. Our preference is that it\n                      be on their own letterhead and be in their own words, and perhaps\n                      referencing similar work that they’ve done for other clients. <br>\n                      Attach the proposal here.<br>\n                      Each file must be no more than 25mb.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <!-- <div class=\"form-group mt-3\" [ngClass]=\"{'active': id === 25, 'error' : showFileSizeError }\" id=\"25\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseenter)=\"focus(25)\" (mouseleave)=\"blur()\">\n                  <div class=\"option-radio download-attachments\">\n                    <p class=\"filter-font\">Attachments</p>\n                    <div class=\"attachment-section\">\n                      <ul class=\"d-list-item attachment-items-list list-unstyled\">\n                        <li *ngFor=\"let f of attachments\">\n                          <div class=\"attachment-item\">\n                            <p>\n                              <span class=\"cursor\" (click)=\"!f.NotLoaded && downloadSingleFile(f)\">\n                                {{f.file}}\n                              </span>\n                            </p>\n                            <span (click)=\"removeAttachments(f)\">\n                              <img class=\"cursor\" *ngIf=\"!f.NotLoaded\" src=\"../assets/images/close-icon.png\"\n                                class=\"close-icon\">\n                            </span>\n\n                            <mat-spinner *ngIf=\"f.NotLoaded\" diameter=\"18\" class=\"close-icon\"></mat-spinner>\n                          </div>\n                        </li>\n                      </ul>\n                    </div>\n                    <div class=\" white-btn-main d-flex justify-content-between row\">\n                      <div class=\"col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mb-2\">\n                        <input type=\"file\" multiple class=\"d-none\" (change)=\"uploadAttachment($event)\" #fileLabel>\n                        <button type=\"button\" (click)=\"fileLabel.click()\" mat-raised-button color=\"\"\n                          class=\"print-submit-btn font-weight-600  white-btn\">ADD\n                          ATTACHMENTS </button>\n                      </div>\n                      <div class=\"col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5\">\n                        <span (click)=\"downLoadFiles()\">\n                          <button type=\"button\" mat-raised-button class=\"print-submit-btn font-weight-600  white-btn\"\n                          *ngIf=\"attachments && attachments.length > 0\">\n                            DOWNLOAD ALL</button>\n                        </span>\n                      </div>\n                    </div>\n                  </div>\n                  <mat-error *ngIf=\"showFileSizeError\">\n                    Each file must be no more than 25mb.\n                  </mat-error>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 25 }\" id=\"25\">\n                    <div>Wherever possible, please ask for a written proposal from the Contractor that describes the\n                      scope of their services. Our preference is that it\n                      be on their own letterhead and be in their own words, and perhaps\n                      referencing similar work that they’ve done for other clients. <br>\n                      Attach the proposal here.<br>\n                      Each file must be no more than 25mb.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div> -->\n          </div>\n        </section>\n        <div class=\" white-btn-main pt-4 d-flex justify-content-between row\">\n          <div *ngIf=\"userRole == 'Standard'\" class=\"col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6\">\n            <button *ngIf=\"addendum.adStatus == 'Draft'\" type=\"button\" (click)=\"(f.submitted = true); submit()\"\n              mat-raised-button color=\"primary\" class=\"bg-color w-100 sign-btn\">SUBMIT</button>\n            <button *ngIf=\"addendum.adStatus == 'Submitted' || addendum.adStatus == 'Assigned'\" type=\"button\"\n              mat-raised-button color=\"primary\" disabled class=\"bg-color w-100 sign-btn\">SUBMITTED</button>\n            <button *ngIf=\"addendum.adStatus == 'Accepted'\" disabled type=\"button\" mat-raised-button color=\"primary\"\n              class=\"bg-color w-100 sign-btn\">ACCEPTED</button>\n          </div>\n          <div *ngIf=\"userRole !== 'Standard'\" class=\"col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6\">\n            <button *ngIf=\"addendum.adStatus == 'Draft'\" type=\"button\" disabled mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMIT</button>\n            <button *ngIf=\"addendum.adStatus == 'Submitted' || addendum.adStatus == 'Assigned'\" type=\"button\"\n              mat-raised-button color=\"primary\" (click)=\"(f.submitted = true); submit()\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMIT</button>\n            <button *ngIf=\"addendum.adStatus == 'Accepted'\" disabled type=\"button\" mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">ACCEPTED</button>\n          </div>\n          <div *ngIf=\"!(addendum.adStatus == 'Accepted')\" class=\"col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4 mb-4\">\n            <span (click)=\"openDeleteModal()\">\n              <button type=\"button\" mat-raised-button color=\"\" class=\"w-100 white-btn\">\n                DELETE ADDENDUM</button>\n            </span>\n          </div>\n        </div>\n      </div>\n    </div>\n  </fieldset>\n</form>\n<app-print-addendum [addendum]=\"addendum\"></app-print-addendum>\n<div class=\"modal confirmation-modal\" id=\"confirmationModal\" data-backdrop=\"static\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <!-- Modal body -->\n      <input type=\"text\" class=\"firstFocus\">\n      <div class=\"modal-body font-14\">\n        <div class=\"pb-3 text-color font-16 font-weight-500\">{{confirmationTitle}}</div>\n        <div class=\"\">{{confirmationMessage}}</div>\n      </div>\n      <!-- Modal footer -->\n      <div class=\"modal-footer\">\n        <button id=\"confirmation-modal-cancle\" type=\"button\" mat-raised-button color=\"\"\n          class=\"w-100 white-btn cancel-btn text-uppercase\">CANCEL</button>\n        <button mat-raised-button color=\"primary\" class=\"w-100 text-uppercase\" (click)=\"confirm()\">CONFIRM</button>\n      </div>\n      <input type=\"text\" class=\"lastFocus\">\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/addendum-form/addendum-form.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/dashboard/addendum-form/addendum-form.component.ts ***!
  \********************************************************************/
/*! exports provided: AddendumFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddendumFormComponent", function() { return AddendumFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var _models_projects_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../_models/projects.model */ "./src/app/_models/projects.model.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _service_file_upload_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_service/file-upload.service */ "./src/app/dashboard/_service/file-upload.service.ts");
/* harmony import */ var src_app_utility_filedownload_helper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/_utility/filedownload.helper */ "./src/app/_utility/filedownload.helper.ts");
/* harmony import */ var _header_dashboard_header_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../header/dashboard-header.service */ "./src/app/dashboard/header/dashboard-header.service.ts");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");











var AddendumFormComponent = /** @class */ (function () {
    function AddendumFormComponent(service, router, activatedRoute, fileService, alert, headerService, loader) {
        this.service = service;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.fileService = fileService;
        this.alert = alert;
        this.headerService = headerService;
        this.loader = loader;
        this.Users = [];
        this.CCList = [];
        this.CCSuggestions = [];
        this.attachments = [];
        this.formDisabled = true;
        this.isFormDirty = false;
        this.startDetectingChanges = false;
        this.needReload = false;
        this.UsersList = [];
    }
    AddendumFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userRole = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_6__["UserAuthHelper"].GetProfile().type;
        this.addendum = new _models_projects_model__WEBPACK_IMPORTED_MODULE_2__["AddendumFormModel"]();
        this.attachments = [];
        this.loadUsers();
        this.activatedRoute.params.subscribe(function (res) {
            _this.addendumId = res.id;
            _this.loadDetails();
        });
    };
    AddendumFormComponent.prototype.loadUsers = function () {
        var _this = this;
        this.service.loadUsers().subscribe(function (res) {
            _this.UsersList = res.data;
            _this.Users = Object.assign([], _this.UsersList);
        });
    };
    AddendumFormComponent.prototype.loadDetails = function () {
        var _this = this;
        this.service.loadAddendumDetails(this.addendumId).subscribe(function (res) {
            _this.addendum = res.data;
            _this.setHeaderTitle();
            _this.attachments = Object.assign([], _this.addendum.attachment);
            if (_this.addendum.cc) {
                _this.CCList = Object.assign([], _this.addendum.mailList);
            }
            _this.addendum.cc = "";
            _this.formDisabled =
                (_this.userRole === "Standard" &&
                    _this.addendum.adStatus !== "Draft") ||
                    _this.addendum.adStatus === "Accepted";
            setTimeout(function () {
                _this.startDetectingChanges = true;
                _this.createChroneJob();
            }, 500);
        }, function (error) {
            _this.alert.error(error.error.details, true);
            _this.isFormDirty = false;
            _this.router.navigate(["/dashboard"]);
        });
    };
    AddendumFormComponent.prototype.setHeaderTitle = function () {
        // tslint:disable-next-line:no-debugger
        debugger;
        if (this.addendum.contractorCompanyName &&
            this.addendum.catalog &&
            this.addendum.from) {
            var title = "Addendum for " +
                this.addendum.contractorCompanyName +
                " on " +
                this.addendum.catalog +
                " from " +
                this.addendum.from;
            this.headerService.setTitle(title);
        }
        else {
            this.headerService.setTitle("Addendum - " + this.addendumId);
        }
    };
    AddendumFormComponent.prototype.blur = function () {
        this.id = 0;
    };
    AddendumFormComponent.prototype.focus = function (id, ele) {
        this.id = id;
        if (ele) {
            setTimeout(function () {
                var it = document.getElementById("ccinput");
                it.focus();
            }, 550);
        }
        return true;
    };
    AddendumFormComponent.prototype.viewProject = function () {
        var webpage = location.origin + "/projects/" + this.addendum.parentProjectMemo;
        var mydiv = document.getElementById("myDiv");
        var aTag = document.createElement("a");
        aTag.setAttribute("href", webpage);
        aTag.setAttribute("target", "_blank");
        aTag.setAttribute("id", "dumLink");
        aTag.setAttribute("rel", "noopener");
        aTag.innerHTML = "link text";
        mydiv.appendChild(aTag);
        document.getElementById("dumLink").click();
    };
    AddendumFormComponent.prototype.SaveProgress = function (reload) {
        if (reload === void 0) { reload = false; }
        if (this.startDetectingChanges) {
            if (reload) {
                this.needReload = true;
            }
            this.isFormDirty = true;
        }
    };
    AddendumFormComponent.prototype.saveNewChanges = function () {
        var _this = this;
        this.savingProgress = true;
        if (this.needReload) {
            this.loader.show();
        }
        setTimeout(function () {
            _this.addendum.mailList = _this.CCList;
            _this.addendum.attachment = _this.attachments.filter(function (x) { return !x.NotLoaded; });
            _this.service.updateAddendum(_this.addendum).subscribe(function (res) {
                if (_this.needReload) {
                    _this.loadDetails();
                }
                _this.savingProgress = false;
                _this.isFormDirty = false;
                _this.needReload = false;
                _this.loader.hide();
            }, function (error) {
                _this.savingProgress = false;
                _this.isFormDirty = false;
                _this.needReload = false;
                _this.loader.hide();
                _this.alert.error(error.error.details);
            });
        }, 0);
    };
    AddendumFormComponent.prototype.searchPeopleEmail = function () {
        var _this = this;
        this.CheckIfAllEmailsAreValid();
        if (this.addendum.cc && this.addendum.cc !== this.searchedEmail) {
            this.searchedEmail = this.addendum.cc;
            this.service.getPeoplesEmail(this.addendum.cc).subscribe(function (res) {
                _this.CCSuggestions = res.data;
            });
        }
    };
    AddendumFormComponent.prototype.CheckIfAllEmailsAreValid = function () {
        var _this = this;
        this.ccIsInvalid = this.CCList.some(function (x) {
            return !x.name ? !_this.validateEmail(x.email) : false;
        });
    };
    AddendumFormComponent.prototype.validateEmail = function (email) {
        // tslint:disable-next-line:max-line-length
        // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var re = /^\S+@\S+$/;
        return re.test(String(email).toLowerCase());
    };
    AddendumFormComponent.prototype.addEmailToList = function () {
        var _this = this;
        if (this.CCSuggestions.length > 0) {
            this.CCList.push(this.CCSuggestions[0]);
        }
        else {
            this.addendum.cc = this.addendum.cc.replace(",", "");
            this.CCList.push({ name: null, email: this.addendum.cc });
        }
        setTimeout(function () {
            _this.addendum.cc = "";
            _this.CheckIfAllEmailsAreValid();
        }, 0);
        this.SaveProgress();
    };
    AddendumFormComponent.prototype.removeFromCCList = function (p) {
        if (!this.formDisabled) {
            this.CCList.splice(p, 1);
            this.CheckIfAllEmailsAreValid();
        }
        this.SaveProgress();
    };
    AddendumFormComponent.prototype.uploadAttachment = function (event) {
        this.isFormDirty = true;
        this.isUploadIncomplete = true;
        var length = event.target.files.length;
        this.files = event.target.files;
        this.showFileSizeError = false;
        var mxFileSize = 25 * 1024 * 1024;
        for (var i = 0; i < length; i++) {
            if (event.target.files[i].size < mxFileSize) {
                this.attachments.push({
                    file: event.target.files[i].name,
                    url: "",
                    NotLoaded: true,
                    id: this.addendumId
                });
            }
            else {
                this.showFileSizeError = true;
            }
        }
        this.UploadFiles();
    };
    AddendumFormComponent.prototype.removeAttachments = function (f) {
        if (!this.formDisabled) {
            this.attachments = this.attachments.filter(function (x) { return x !== f; });
            this.addendum.attachment = this.addendum.attachment.filter(function (x) { return x !== f; });
            $("#fileInput").replaceWith($("#fileInput").val('').clone(true));
            this.SaveProgress();
        }
    };
    AddendumFormComponent.prototype.UploadFiles = function () {
        var _this = this;
        var length = this.files.length;
        var mxFileSize = 25 * 1024 * 1024;
        for (var i = 0; i < length; i++) {
            if (this.files[i].size < mxFileSize) {
                this.fileService
                    .uploadFile(this.files[i], this.addendumId + "-" + this.files[i].name)
                    .subscribe(function (data) {
                    // data.Bucket, data.ETag, data.Key, data.Location, data.key
                    var item = _this.attachments.find(function (x) { return x.id + "-" + x.file === data.key; });
                    item.NotLoaded = false;
                    item.url = data.Location;
                    _this.addendum.attachment = _this.attachments
                        .filter(function (x) { return !x.NotLoaded; })
                        .map(function (x) { return ({
                        file: item.file,
                        url: data.Location
                    }); });
                    if (_this.attachments.every(function (x) { return !x.NotLoaded; })) {
                        _this.isUploadIncomplete = false;
                        console.log("this.isUploadIncomplete = false;", _this.isUploadIncomplete);
                        _this.SaveProgress();
                    }
                }, function (error) { });
            }
        }
    };
    AddendumFormComponent.prototype.getPosts = function (option) {
        var p = this.CCSuggestions.find(function (x) { return x.email === option.value; });
        this.addendum.cc = "";
        this.CCList.push(p);
        this.CCSuggestions = [];
    };
    AddendumFormComponent.prototype.print = function () {
        this.showPrintModal = true;
        setTimeout(function () {
            $("#printAddendumModal").modal("show");
        }, 500);
    };
    AddendumFormComponent.prototype.downLoadFiles = function (str) {
        this.addendum.attachment.forEach(function (ele) {
            src_app_utility_filedownload_helper__WEBPACK_IMPORTED_MODULE_8__["DwonloadHelper"].downloadFile(ele.url, ele.file);
        });
    };
    AddendumFormComponent.prototype.downloadSingleFile = function (f) {
        src_app_utility_filedownload_helper__WEBPACK_IMPORTED_MODULE_8__["DwonloadHelper"].downloadFile(f.url, f.file);
    };
    AddendumFormComponent.prototype.deleteAddendum = function () {
        var _this = this;
        this.loader.show();
        this.service.deleteAddendum(this.addendumId).subscribe(function (res) {
            _this.isFormDirty = false;
            _this.loader.hide();
            _this.router.navigate(["/dashboard"]);
        }, function (error) {
            _this.loader.hide();
            _this.alert.error(error.error.details);
        });
    };
    AddendumFormComponent.prototype.openDeleteModal = function () {
        this.confirmationTitle = "Delete Addendum";
        this.confirmationMessage =
            "Are you sure you want to delete this Addendum? Once deleted it cannot be recovered.";
        this.confirmationAction = "DELETE";
        $("#confirmationModal").modal("show");
        $("#confirmation-modal-cancle").focus();
    };
    AddendumFormComponent.prototype.confirm = function () {
        if (this.confirmationAction === "DELETE") {
            this.deleteAddendum();
            $("#confirmationModal").modal("hide");
        }
        else if (this.confirmationAction === "SUBMIT") {
            if (this.userRole.includes("Standard")) {
                this.confirmSubmitStandardUser();
            }
            else {
                this.confirmSubmitBA();
            }
        }
        $("#confirmationModal").modal("hide");
    };
    AddendumFormComponent.prototype.confirmSubmitBA = function () {
        var _this = this;
        this.loader.show();
        if (this.isFormDirty) {
            this.service.updateAddendum(this.addendum).subscribe(function (res) {
                _this.savingProgress = false;
                _this.isFormDirty = false;
                _this.needReload = false;
                _this.BAFinalSubmit();
            }, function (error) {
                _this.savingProgress = false;
                _this.isFormDirty = false;
                _this.needReload = false;
                _this.loader.hide();
                _this.alert.error(error.error.details);
            });
        }
        else {
            this.BAFinalSubmit();
        }
    };
    AddendumFormComponent.prototype.BAFinalSubmit = function () {
        var _this = this;
        this.service.SubmitAddendumBA(this.addendumId).subscribe(function (res) {
            _this.loader.hide();
            _this.alert.success("This Addendum has been marked as Accepted.", true);
            _this.loadDetails();
        }, function (error) {
            _this.alert.error(error.error.details);
            _this.loader.hide();
        });
    };
    AddendumFormComponent.prototype.FilterAssignTo = function () {
        var _this = this;
        this.Users = this.UsersList.filter(function (x) {
            return (x.firstName + " " + x.lastName).toLocaleLowerCase().includes(_this.addendum.assignedUser.toLocaleLowerCase());
        });
    };
    AddendumFormComponent.prototype.ValidateAssignTo = function () {
        var _this = this;
        if (this.addendum.assignedTo) {
            if (!this.UsersList.some(function (x) { return x.firstName + " " + x.lastName === _this.addendum.assignedTo.trim(); })) {
                this.addendum.assignedUser = "";
                this.addendum.assignedTo = "";
            }
        }
    };
    AddendumFormComponent.prototype.selectionChanged = function (option) {
        var user = this.UsersList.find(function (x) { return x.uniqueid === option.value; });
        if (user) {
            this.addendum.assignedUser = user.firstName + " " + user.lastName;
            this.addendum.assignedTo = option.value;
        }
        this.needReload = true;
        this.saveNewChanges();
    };
    AddendumFormComponent.prototype.submit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.submitFormValid()) {
                if (_this.userRole === "Standard") {
                    _this.openConfirmSubmitStandardUserModal();
                }
                else {
                    _this.openConfirmSubmitBAUserModal();
                }
            }
        }, 0);
    };
    AddendumFormComponent.prototype.openConfirmSubmitBAUserModal = function () {
        this.confirmationTitle = "Submit Addendum";
        this.confirmationMessage =
            "Are you sure you want to mark this as completed? Keep in mind that you are required to manually update data in Rightsline.";
        this.confirmationAction = "SUBMIT";
        $("#confirmationModal").modal("show");
        $("#confirmation-modal-cancle").focus();
    };
    AddendumFormComponent.prototype.submitFormValid = function () {
        var errorEle = document.getElementsByClassName("form-group error");
        if (errorEle.length === 0) {
            return true;
        }
        else {
            var ele = errorEle[0];
            var errorTop = ele.offsetTop;
            $("html, body").animate({
                scrollTop: errorTop - 128
            }, 500);
        }
    };
    AddendumFormComponent.prototype.openConfirmSubmitStandardUserModal = function () {
        this.confirmationTitle = "Submit Addendum";
        this.confirmationMessage =
            "Are you sure this is ready to be sent to Business Affairs?";
        this.confirmationAction = "SUBMIT";
        $("#confirmationModal").modal("show");
        $("#confirmation-modal-cancle").focus();
    };
    AddendumFormComponent.prototype.createChroneJob = function () {
        var _this = this;
        this.chroneJob = null;
        this.chroneJob = setInterval(function () {
            if (_this.isFormDirty && !_this.isUploadIncomplete) {
                _this.saveNewChanges();
            }
        }, 5000);
    };
    AddendumFormComponent.prototype.confirmSubmitStandardUser = function () {
        var _this = this;
        this.loader.show();
        if (this.isFormDirty) {
            this.service.updateAddendum(this.addendum).subscribe(function (res) {
                _this.savingProgress = false;
                _this.isFormDirty = false;
                _this.needReload = false;
                _this.UserFinalSubmit();
            }, function (error) {
                _this.savingProgress = false;
                _this.isFormDirty = false;
                _this.needReload = false;
                _this.loader.hide();
                _this.alert.error(error.error.details);
            });
        }
        else {
            this.UserFinalSubmit();
        }
    };
    AddendumFormComponent.prototype.UserFinalSubmit = function () {
        var _this = this;
        this.service.SubmitAddendumSU(this.addendumId).subscribe(function (res) {
            _this.loader.hide();
            _this.alert.success("Your Addendum has been submitted.", true);
            _this.isFormDirty = false;
            _this.router.navigate(["dashboard"]);
        }, function (error) {
            _this.alert.error(error.error.details);
            _this.loader.hide();
        });
    };
    AddendumFormComponent.prototype.sendCopiesBlur = function () {
        this.addendum.cc = this.addendum.cc.trim();
    };
    AddendumFormComponent.prototype.ngOnDestroy = function () {
        this.chroneJob = null;
        if (this.isFormDirty) {
            this.saveNewChanges();
        }
    };
    AddendumFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: "app-addendum-form",
            template: __webpack_require__(/*! ./addendum-form.component.html */ "./src/app/dashboard/addendum-form/addendum-form.component.html"),
            styles: [__webpack_require__(/*! ./addendum-form.component.css */ "./src/app/dashboard/addendum-form/addendum-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _service_file_upload_service__WEBPACK_IMPORTED_MODULE_7__["FileUploadService"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_1__["AlertService"],
            _header_dashboard_header_service__WEBPACK_IMPORTED_MODULE_9__["DashboardHeaderService"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_10__["LoaderService"]])
    ], AddendumFormComponent);
    return AddendumFormComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/addendum-modal/addendum-modal.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/dashboard/addendum-modal/addendum-modal.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9hZGRlbmR1bS1tb2RhbC9hZGRlbmR1bS1tb2RhbC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/dashboard/addendum-modal/addendum-modal.component.html":
/*!************************************************************************!*\
  !*** ./src/app/dashboard/addendum-modal/addendum-modal.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"modal show\" data-backdrop=\"static\" id=\"addendumModal\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header modal-heading\">\n          <h3 class=\"font-20 \">ADDENDUM ASSIGNMENT</h3>\n        </div>\n        <div class=\"pt-3 container\">\n          <input type=\"text\" class=\"firstFocus\">\n          <form class=\"form-own\">\n            <div class=\"form-group\" id=\"6\">\n              <mat-form-field class=\"example-full-width sign-input-2 input-icon\" floatLabel=\"always\">\n                <mat-label>Project Memo</mat-label>\n                <input (mouseenter)=\"mouseEntered($event)\" (keyup)=\"FilterProject($event.target.value)\" (change)=\"validateEntries()\" [ngModel]=\"name\" type=\"text\" placeholder=\"\"\n                  name=\"contractorCompanyName\" matInput [matAutocomplete]=\"autoa\" required>\n                <mat-autocomplete [autoActiveFirstOption]=\"true\" (optionSelected)='projectSelected($event.option)' #autoa=\"matAutocomplete\"\n                  [displayWith]=\"displayFn\">\n                  <mat-option *ngFor=\"let p of projects\"\n                    title=\"{{p.contractorCompanyName}} on {{p.projectName}} ({{p.pmID}}) created on {{ p.createdAt | date : 'MM/dd/yy' }}\"\n                    [value]=\"p.pmID\">\n                    {{p.contractorCompanyName}} on {{p.projectName}} ({{p.pmID}}) created on\n                    {{ p.createdAt | date : 'MM/dd/yy' }}\n                  </mat-option>\n                </mat-autocomplete>\n              </mat-form-field>\n              <div [ngClass]=\"{ 'notactive' : !selectedProject }\" (click)=\"selectedProject && viewProject()\"\n                class=\"text-color font-14 view-project-memo pl-3 cursor\" style=\"margin-top: -5px;\">\n                VIEW PROJECT MEMO</div>\n            </div>\n          </form>\n          <div class=\" white-btn-main mb-3 d-flex justify-content-between row\">\n            <div class=\"col-5 \">\n              <button (click)=\"clear()\" mat-raised-button color=\"\"\n                class=\"print-submit-btn  white-btn w-100 text-uppercase\" data-dismiss=\"modal\">CANCEL</button>\n            </div>\n            <div class=\"col-5 \">\n              <button [disabled]=\"!selectedProject\" (click)=\"selectedProject && countinue()\" mat-raised-button\n                color=\"primary\" class=\"print-submit-btn w-100 text-uppercase\">CONTINUE</button>\n            </div>\n          </div>\n          <input type=\"text\" class=\"lastFocus\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"d-none\" id=\"myDiv1\"></div>"

/***/ }),

/***/ "./src/app/dashboard/addendum-modal/addendum-modal.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/dashboard/addendum-modal/addendum-modal.component.ts ***!
  \**********************************************************************/
/*! exports provided: AddendumModalComponent, ProjectListModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddendumModalComponent", function() { return AddendumModalComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectListModel", function() { return ProjectListModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");





var AddendumModalComponent = /** @class */ (function () {
    function AddendumModalComponent(service, router, loader) {
        this.service = service;
        this.router = router;
        this.loader = loader;
        this.projects = [];
        this.selectedProject = null;
        this.projectsList = [];
    }
    AddendumModalComponent.prototype.ngOnInit = function () {
        this.projects = [];
        this.SearchProjects("");
    };
    AddendumModalComponent.prototype.FilterProject = function (value) {
        if (this.lastSearch !== value) {
            this.lastSearch = value;
            // this.SearchProjects(value);
            var lastvalue_1 = value.toLocaleLowerCase();
            this.projects = this.projectsList.filter(function (x) {
                return x.catalog.toLocaleLowerCase().includes(lastvalue_1) ||
                    x.contractorCompanyName.toLocaleLowerCase().includes(lastvalue_1) ||
                    x.contractorContactName.toLocaleLowerCase().includes(lastvalue_1) ||
                    x.createdAt
                        .toString()
                        .toLocaleLowerCase()
                        .includes(lastvalue_1) ||
                    x.pmID.toLocaleLowerCase().includes(lastvalue_1) ||
                    x.projectName.toLocaleLowerCase().includes(lastvalue_1);
            });
        }
    };
    AddendumModalComponent.prototype.SearchProjects = function (value) {
        var _this = this;
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.subscription = this.service
            .seachProject(value)
            .subscribe(function (res) {
            _this.projectsList = res.data;
            _this.FilterProject("");
        });
    };
    AddendumModalComponent.prototype.projectSelected = function (option) {
        this.selectedProject = this.projects.find(function (x) { return x.pmID === option.value; });
        if (this.selectedProject) {
            this.name =
                this.selectedProject.contractorCompanyName +
                    " on " +
                    this.selectedProject.projectName;
        }
    };
    AddendumModalComponent.prototype.countinue = function () {
        var _this = this;
        this.loader.show();
        this.service
            .getAddendumId(this.selectedProject.pmID, this.selectedProject.projectName)
            .subscribe(function (res) {
            _this.clear();
            var url = "/addendum/" + res.data;
            _this.loader.hide();
            _this.router.navigate([url]);
            $("#addendumModal").modal("hide");
        }, function (error) {
            _this.loader.hide();
        });
    };
    AddendumModalComponent.prototype.viewProject = function () {
        var webpage = location.origin + "/projects/" + this.selectedProject.pmID;
        var mydiv = document.getElementById("myDiv1");
        var aTag = document.createElement("a");
        aTag.setAttribute("href", webpage);
        aTag.setAttribute("target", "_blank");
        aTag.setAttribute("id", "dumLink");
        aTag.setAttribute("rel", "noopener");
        aTag.innerHTML = "link text";
        mydiv.appendChild(aTag);
        document.getElementById("dumLink").click();
    };
    AddendumModalComponent.prototype.clear = function () {
        if (this.name) {
            this.selectedProject = null;
            this.name = "";
        }
    };
    AddendumModalComponent.prototype.validateEntries = function () {
        var _this = this;
        if (this.name) {
            this.name = this.name.trim();
            if (this.name &&
                this.projects.filter(function (x) {
                    return x.contractorCompanyName ===
                        _this.selectedProject.contractorCompanyName +
                            " on " +
                            _this.selectedProject.projectName;
                }).length !== 1) {
                this.name = "";
                this.selectedProject = null;
            }
        }
    };
    AddendumModalComponent.prototype.mouseEntered = function (event) {
        if (!this.notfirst) {
            this.notfirst = true;
            event.target.blur();
            setTimeout(function () {
                event.target.focus();
            }, 10);
        }
    };
    AddendumModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-addendum-modal",
            template: __webpack_require__(/*! ./addendum-modal.component.html */ "./src/app/dashboard/addendum-modal/addendum-modal.component.html"),
            styles: [__webpack_require__(/*! ./addendum-modal.component.css */ "./src/app/dashboard/addendum-modal/addendum-modal.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_project_service__WEBPACK_IMPORTED_MODULE_2__["ProjectService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]])
    ], AddendumModalComponent);
    return AddendumModalComponent;
}());

var ProjectListModel = /** @class */ (function () {
    function ProjectListModel() {
    }
    return ProjectListModel;
}());



/***/ }),

/***/ "./src/app/dashboard/component/dashboard.component.css":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/component/dashboard.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9jb21wb25lbnQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dashboard/component/dashboard.component.html":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/component/dashboard.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-sidebar></app-sidebar>\n<app-dashboard-header></app-dashboard-header>\n<router-outlet></router-outlet>\n<app-addendum-modal></app-addendum-modal>"

/***/ }),

/***/ "./src/app/dashboard/component/dashboard.component.ts":
/*!************************************************************!*\
  !*** ./src/app/dashboard/component/dashboard.component.ts ***!
  \************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(router) {
        this.router = router;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        // if (location.href.includes('refresh')) {
        //   this.router.navigate(['/dashboard']);
        // }
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-dashboard",
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/component/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/component/dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home/home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_users_dashboard_users_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users/users-dashboard/users-dashboard.component */ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.ts");
/* harmony import */ var _component_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./component/dashboard.component */ "./src/app/dashboard/component/dashboard.component.ts");
/* harmony import */ var _projects_projects_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./projects/projects.component */ "./src/app/dashboard/projects/projects.component.ts");
/* harmony import */ var _addendum_form_addendum_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./addendum-form/addendum-form.component */ "./src/app/dashboard/addendum-form/addendum-form.component.ts");
/* harmony import */ var _gaurd_confirm_deactivate_gaurd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./_gaurd/confirm.deactivate.gaurd */ "./src/app/dashboard/_gaurd/confirm.deactivate.gaurd.ts");
/* harmony import */ var _project_form_project_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./project-form/project-form.component */ "./src/app/dashboard/project-form/project-form.component.ts");
/* harmony import */ var _project_memo_print_project_memo_print_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./project-memo-print/project-memo-print.component */ "./src/app/dashboard/project-memo-print/project-memo-print.component.ts");
/* harmony import */ var _project_ad_print_project_ad_print_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./project-ad-print/project-ad-print.component */ "./src/app/dashboard/project-ad-print/project-ad-print.component.ts");







// import { PageNotFoundComponent } from "../_shared/page-not-found/page-not-found.component";





var routes = [
    {
        path: "",
        component: _component_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"],
        children: [
            { path: "", redirectTo: "dashboard", pathMatch: "full" },
            { path: "dashboard", component: _projects_projects_component__WEBPACK_IMPORTED_MODULE_6__["ProjectsComponent"] },
            { path: "users", component: _users_users_dashboard_users_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["UsersDashboardComponent"] },
            { path: "users/:token", component: _users_users_dashboard_users_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["UsersDashboardComponent"] },
            { path: "projects/:projectId", component: _project_form_project_form_component__WEBPACK_IMPORTED_MODULE_9__["ProjectFormComponent"], canDeactivate: [_gaurd_confirm_deactivate_gaurd__WEBPACK_IMPORTED_MODULE_8__["ConfirmDeactivateGuard"]] },
            { path: "addendum/:id", component: _addendum_form_addendum_form_component__WEBPACK_IMPORTED_MODULE_7__["AddendumFormComponent"], canDeactivate: [_gaurd_confirm_deactivate_gaurd__WEBPACK_IMPORTED_MODULE_8__["ConfirmDeactivateGuard"]] },
            { path: "home", component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] }
            // { path: "**", component: PageNotFoundComponent }
        ]
    },
    { path: "projects/:projectId/print", component: _project_memo_print_project_memo_print_component__WEBPACK_IMPORTED_MODULE_10__["ProjectMemoPrintComponent"] },
    { path: "addendum/:id/print", component: _project_ad_print_project_ad_print_component__WEBPACK_IMPORTED_MODULE_11__["ProjectAdPrintComponent"] },
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _component_dashboard_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./component/dashboard.component */ "./src/app/dashboard/component/dashboard.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _users_users_dashboard_users_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users/users-dashboard/users-dashboard.component */ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.ts");
/* harmony import */ var _users_user_account_user_account_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./users/user-account/user-account.component */ "./src/app/dashboard/users/user-account/user-account.component.ts");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_service/users.service */ "./src/app/dashboard/_service/users.service.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/dashboard/sidebar/sidebar.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/_shared/shared.module.ts");
/* harmony import */ var _projects_projects_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./projects/projects.component */ "./src/app/dashboard/projects/projects.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home/home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony import */ var _header_dashboard_header_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./header/dashboard-header.component */ "./src/app/dashboard/header/dashboard-header.component.ts");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var _service_file_upload_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./_service/file-upload.service */ "./src/app/dashboard/_service/file-upload.service.ts");
/* harmony import */ var _addendum_form_addendum_form_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./addendum-form/addendum-form.component */ "./src/app/dashboard/addendum-form/addendum-form.component.ts");
/* harmony import */ var _print_memo_print_memo_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./print-memo/print-memo.component */ "./src/app/dashboard/print-memo/print-memo.component.ts");
/* harmony import */ var _addendum_modal_addendum_modal_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./addendum-modal/addendum-modal.component */ "./src/app/dashboard/addendum-modal/addendum-modal.component.ts");
/* harmony import */ var _print_addendum_print_addendum_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./print-addendum/print-addendum.component */ "./src/app/dashboard/print-addendum/print-addendum.component.ts");
/* harmony import */ var _header_dashboard_header_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./header/dashboard-header.service */ "./src/app/dashboard/header/dashboard-header.service.ts");
/* harmony import */ var _gaurd_confirm_deactivate_gaurd__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./_gaurd/confirm.deactivate.gaurd */ "./src/app/dashboard/_gaurd/confirm.deactivate.gaurd.ts");
/* harmony import */ var _project_form_project_form_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./project-form/project-form.component */ "./src/app/dashboard/project-form/project-form.component.ts");
/* harmony import */ var _project_memo_print_project_memo_print_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./project-memo-print/project-memo-print.component */ "./src/app/dashboard/project-memo-print/project-memo-print.component.ts");
/* harmony import */ var _project_ad_print_project_ad_print_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./project-ad-print/project-ad-print.component */ "./src/app/dashboard/project-ad-print/project-ad-print.component.ts");























var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _component_dashboard_component__WEBPACK_IMPORTED_MODULE_1__["DashboardComponent"],
                _users_users_dashboard_users_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["UsersDashboardComponent"],
                _header_dashboard_header_component__WEBPACK_IMPORTED_MODULE_11__["DashboardHeaderComponent"],
                _users_user_account_user_account_component__WEBPACK_IMPORTED_MODULE_5__["UserAccountComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_7__["SidebarComponent"],
                _projects_projects_component__WEBPACK_IMPORTED_MODULE_9__["ProjectsComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_10__["HomeComponent"],
                _project_form_project_form_component__WEBPACK_IMPORTED_MODULE_20__["ProjectFormComponent"],
                _addendum_form_addendum_form_component__WEBPACK_IMPORTED_MODULE_14__["AddendumFormComponent"],
                _print_memo_print_memo_component__WEBPACK_IMPORTED_MODULE_15__["PrintMemoComponent"],
                _addendum_modal_addendum_modal_component__WEBPACK_IMPORTED_MODULE_16__["AddendumModalComponent"],
                _print_addendum_print_addendum_component__WEBPACK_IMPORTED_MODULE_17__["PrintAddendumComponent"],
                _project_memo_print_project_memo_print_component__WEBPACK_IMPORTED_MODULE_21__["ProjectMemoPrintComponent"],
                _project_ad_print_project_ad_print_component__WEBPACK_IMPORTED_MODULE_22__["ProjectAdPrintComponent"]
            ],
            imports: [_shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"], _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__["DashboardRoutingModule"]],
            providers: [
                _service_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"],
                _service_project_service__WEBPACK_IMPORTED_MODULE_12__["ProjectService"],
                _service_file_upload_service__WEBPACK_IMPORTED_MODULE_13__["FileUploadService"],
                _header_dashboard_header_service__WEBPACK_IMPORTED_MODULE_18__["DashboardHeaderService"],
                _gaurd_confirm_deactivate_gaurd__WEBPACK_IMPORTED_MODULE_19__["ConfirmDeactivateGuard"]
            ],
            bootstrap: [_component_dashboard_component__WEBPACK_IMPORTED_MODULE_1__["DashboardComponent"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/header/dashboard-header.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/dashboard/header/dashboard-header.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-section .navbar-nav>li>a {\n    color: #ffffff;\n}\n\n.header-section .mobile-header .navbar-collapse,\n.header-section .navbar-form {\n    border-color: rgba(231, 231, 231, 0);\n}\n\n.header-section .navbar-toggle:focus,\n.header-section .navbar-toggle:hover {\n    background-color: transparent;\n}\n\n.header-section .mobile-header {\n    height: 45px;\n    background-color: black;\n    border-color: transparent;\n}\n\n.mobile-header .navbar-nav>li>a:focus,\n.mobile-header .navbar-nav>li>a:hover {\n    color: #fff;\n}\n\n.mobile-header-sub-menu>ul {\n    margin-top: 0;\n    background: rgba(0, 0, 0, 0.26);\n    margin-bottom: 0;\n    padding-left: 30px;\n}\n\n@media (min-width:768px) {\n\n    .header-section .mobile-header-sub-menu,\n    .header-section .navbar-toggle {\n        display: none !important;\n    }\n}\n\n@media(max-width: 767px) {\n    .main-body {\n        padding-top: 45px;\n    }\n\n    .navbar-fixed-bottom .navbar-collapse,\n    .navbar-fixed-top .navbar-collapse {\n        max-height: 350px !important;\n        overflow-y: auto !important;\n    }\n\n    .header-section .mobile-header-sub-menu {\n        background: rgba(0, 0, 0, 0.73);\n        position: absolute;\n        top: 100%;\n        left: 0;\n        right: 0;\n    }\n\n    .header-section .navbar-brand>img {\n        max-height: 40px;\n    }\n\n    .header-section a.navbar-brand {\n        padding: 5px 15px;\n    }\n\n    .specialization-details-container>li {\n        display: block !important;\n        text-align: center !important;\n    }\n\n    .mobile-header-sub-menu ul {\n        padding-top: 10px;\n        padding-bottom: 10px;\n    }\n\n    .mobile-header-sub-menu ul li {\n        padding: 10px 0;\n        text-align: left;\n        position: relative;\n        padding-left: 55px;\n        padding-right: 15px;\n        font-size: 14px;\n    }\n\n    .mobile-header-sub-menu ul li.active a {\n        border-bottom: 1px solid #fff;\n    }\n\n    .mobile-header-sub-menu ul li:first-child {\n        /*padding-top: 14px;*/\n    }\n\n    .technology-list-container>li img {\n        padding: 8px;\n        height: 40px;\n    }\n\n    .contact-form .form-control::-webkit-input-placeholder {\n        /* Chrome/Opera/Safari */\n        color: rgba(204, 203, 203, 0.84);\n        font-size: 13px;\n    }\n\n    .contact-form .form-control::-moz-placeholder {\n        /* Firefox 19+ */\n        color: rgba(204, 203, 203, 0.84);\n        font-size: 13px;\n    }\n\n    .contact-form .form-control:-ms-input-placeholder {\n        /* IE 10+ */\n        color: rgba(204, 203, 203, 0.84);\n        font-size: 13px;\n    }\n\n    .contact-form .form-control:-moz-placeholder {\n        /* Firefox 18- */\n        color: rgba(204, 203, 203, 0.84);\n        font-size: 13px;\n    }\n\n    .solution-heading {\n        padding: 20px 2% 0;\n    }\n\n    .carousel-control.left span,\n    .carousel-control.right span {\n        font-size: 0 !important;\n    }\n\n    .service-section img {\n        width: 60%;\n    }\n}\n\n.navbar-toggle * {\n    transition: all 0.5s ease;\n}\n\n.navbar-toggle {\n    color: #fff;\n    height: 20px;\n    width: 20px;\n    cursor: pointer;\n    margin-right: 15px;\n    border-radius: 0px;\n    border: none;\n}\n\n.navbar-toggle span {\n    width: 20px;\n    height: 2px;\n    margin-bottom: 3px;\n    background-color: #fff;\n    display: block;\n}\n\n.navbar-toggle span.bar1 {\n    -webkit-transform: rotate(45deg);\n    transform: rotate(45deg);\n    -webkit-transform-origin: 1px 3px;\n    transform-origin: 1px 3px;\n    width: 24px;\n}\n\n.navbar-toggle span.bar2 {\n    -webkit-transform: rotate(-360deg) scale(0);\n    transform: rotate(-360deg) scale(0);\n}\n\n.navbar-toggle span.bar3 {\n    -webkit-transform: rotate(360deg) scale(0);\n    transform: rotate(360deg) scale(0);\n}\n\n.navbar-toggle span.bar4 {\n    -webkit-transform: rotate(-45deg);\n    transform: rotate(-45deg);\n    -webkit-transform-origin: 2px 0px;\n    transform-origin: 2px 0px;\n    width: 24px;\n}\n\n.navbar-toggle.collapsed span.bar1,\n.navbar-toggle.collapsed span.bar2,\n.navbar-toggle.collapsed span.bar3,\n.navbar-toggle.collapsed span.bar4 {\n    -webkit-transform: none;\n    transform: none;\n    width: 20px;\n}\n\n.navbar-toggle:focus,\n.navbar-toggle:hover,\n.navbar-toggle {\n    border-color: rgba(221, 221, 221, 0) !important;\n}\n\n.container-fluid>.navbar-collapse,\n.container-fluid>.navbar-header,\n.container>.navbar-collapse,\n.container>.navbar-header {\n    padding-right: 8px !important;\n}\n\n.navbar-fixed-bottom .navbar-collapse,\n.navbar-fixed-top .navbar-collapse {\n    overflow-x: hidden !important;\n}\n\n.nav>li>a {\n    padding-left: 0 !important;\n}\n\n@-webkit-keyframes fadeInDown {\n    0% {\n        opacity: 0;\n        -webkit-transform: translate3d(0, -100%, 0);\n        transform: translate3d(0, -100%, 0);\n    }\n\n    100% {\n        opacity: 1;\n        -webkit-transform: none;\n        transform: none;\n    }\n}\n\n@keyframes fadeInDown {\n    0% {\n        opacity: 0;\n        -webkit-transform: translate3d(0, -100%, 0);\n        transform: translate3d(0, -100%, 0);\n    }\n\n    100% {\n        opacity: 1;\n        -webkit-transform: none;\n        transform: none;\n    }\n}\n\n.fadeInDown {\n    -webkit-animation-name: fadeInDown;\n    animation-name: fadeInDown;\n}\n\n.delay1 {\n    -webkit-animation-delay: .2s;\n    animation-delay: .2s;\n}\n\n.delay2 {\n    -webkit-animation-delay: .3s;\n    animation-delay: .3s;\n}\n\n.delay3 {\n    -webkit-animation-delay: .4s;\n    animation-delay: .4s;\n}\n\n.animated {\n    -webkit-animation-duration: 1s;\n    animation-duration: 1s;\n    -webkit-animation-fill-mode: both;\n    animation-fill-mode: both;\n}\n\n.new_des_border {\n    border-right: 1px solid #ffffff;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2hlYWRlci9kYXNoYm9hcmQtaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBOztJQUVJLG9DQUFvQztBQUN4Qzs7QUFFQTs7SUFFSSw2QkFBNkI7QUFDakM7O0FBRUE7SUFDSSxZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCLHlCQUF5QjtBQUM3Qjs7QUFFQTs7SUFFSSxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsK0JBQStCO0lBQy9CLGdCQUFnQjtJQUNoQixrQkFBa0I7QUFDdEI7O0FBRUE7O0lBRUk7O1FBRUksd0JBQXdCO0lBQzVCO0FBQ0o7O0FBRUE7SUFDSTtRQUNJLGlCQUFpQjtJQUNyQjs7SUFFQTs7UUFFSSw0QkFBNEI7UUFDNUIsMkJBQTJCO0lBQy9COztJQUVBO1FBQ0ksK0JBQStCO1FBQy9CLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsT0FBTztRQUNQLFFBQVE7SUFDWjs7SUFFQTtRQUNJLGdCQUFnQjtJQUNwQjs7SUFFQTtRQUNJLGlCQUFpQjtJQUNyQjs7SUFFQTtRQUNJLHlCQUF5QjtRQUN6Qiw2QkFBNkI7SUFDakM7O0lBRUE7UUFDSSxpQkFBaUI7UUFDakIsb0JBQW9CO0lBQ3hCOztJQUVBO1FBQ0ksZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsa0JBQWtCO1FBQ2xCLG1CQUFtQjtRQUNuQixlQUFlO0lBQ25COztJQUVBO1FBQ0ksNkJBQTZCO0lBQ2pDOztJQUVBO1FBQ0kscUJBQXFCO0lBQ3pCOztJQUVBO1FBQ0ksWUFBWTtRQUNaLFlBQVk7SUFDaEI7O0lBRUE7UUFDSSx3QkFBd0I7UUFDeEIsZ0NBQWdDO1FBQ2hDLGVBQWU7SUFDbkI7O0lBRUE7UUFDSSxnQkFBZ0I7UUFDaEIsZ0NBQWdDO1FBQ2hDLGVBQWU7SUFDbkI7O0lBRUE7UUFDSSxXQUFXO1FBQ1gsZ0NBQWdDO1FBQ2hDLGVBQWU7SUFDbkI7O0lBRUE7UUFDSSxnQkFBZ0I7UUFDaEIsZ0NBQWdDO1FBQ2hDLGVBQWU7SUFDbkI7O0lBRUE7UUFDSSxrQkFBa0I7SUFDdEI7O0lBRUE7O1FBRUksdUJBQXVCO0lBQzNCOztJQUVBO1FBQ0ksVUFBVTtJQUNkO0FBQ0o7O0FBRUE7SUFLSSx5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksV0FBVztJQUNYLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsc0JBQXNCO0lBQ3RCLGNBQWM7QUFDbEI7O0FBRUE7SUFFSSxnQ0FBZ0M7SUFHaEMsd0JBQXdCO0lBRXhCLGlDQUFpQztJQUdqQyx5QkFBeUI7SUFDekIsV0FBVztBQUNmOztBQUVBO0lBRUksMkNBQTJDO0lBRzNDLG1DQUFtQztBQUN2Qzs7QUFFQTtJQUVJLDBDQUEwQztJQUcxQyxrQ0FBa0M7QUFDdEM7O0FBRUE7SUFFSSxpQ0FBaUM7SUFDakMseUJBQXlCO0lBRXpCLGlDQUFpQztJQUNqQyx5QkFBeUI7SUFDekIsV0FBVztBQUNmOztBQUVBOzs7O0lBS0ksdUJBQXVCO0lBQ3ZCLGVBQWU7SUFDZixXQUFXO0FBQ2Y7O0FBRUE7OztJQUdJLCtDQUErQztBQUNuRDs7QUFFQTs7OztJQUlJLDZCQUE2QjtBQUNqQzs7QUFFQTs7SUFFSSw2QkFBNkI7QUFDakM7O0FBRUE7SUFDSSwwQkFBMEI7QUFDOUI7O0FBRUE7SUFDSTtRQUNJLFVBQVU7UUFDViwyQ0FBMkM7UUFDM0MsbUNBQW1DO0lBQ3ZDOztJQUVBO1FBQ0ksVUFBVTtRQUNWLHVCQUF1QjtRQUN2QixlQUFlO0lBQ25CO0FBQ0o7O0FBRUE7SUFDSTtRQUNJLFVBQVU7UUFDViwyQ0FBMkM7UUFFM0MsbUNBQW1DO0lBQ3ZDOztJQUVBO1FBQ0ksVUFBVTtRQUNWLHVCQUF1QjtRQUV2QixlQUFlO0lBQ25CO0FBQ0o7O0FBRUE7SUFDSSxrQ0FBa0M7SUFDbEMsMEJBQTBCO0FBQzlCOztBQUVBO0lBQ0ksNEJBQTRCO0lBQzVCLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLDRCQUE0QjtJQUM1QixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSw0QkFBNEI7SUFDNUIsb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksOEJBQThCO0lBQzlCLHNCQUFzQjtJQUN0QixpQ0FBaUM7SUFDakMseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0ksK0JBQStCO0FBQ25DIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL2hlYWRlci9kYXNoYm9hcmQtaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLXNlY3Rpb24gLm5hdmJhci1uYXY+bGk+YSB7XG4gICAgY29sb3I6ICNmZmZmZmY7XG59XG5cbi5oZWFkZXItc2VjdGlvbiAubW9iaWxlLWhlYWRlciAubmF2YmFyLWNvbGxhcHNlLFxuLmhlYWRlci1zZWN0aW9uIC5uYXZiYXItZm9ybSB7XG4gICAgYm9yZGVyLWNvbG9yOiByZ2JhKDIzMSwgMjMxLCAyMzEsIDApO1xufVxuXG4uaGVhZGVyLXNlY3Rpb24gLm5hdmJhci10b2dnbGU6Zm9jdXMsXG4uaGVhZGVyLXNlY3Rpb24gLm5hdmJhci10b2dnbGU6aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4uaGVhZGVyLXNlY3Rpb24gLm1vYmlsZS1oZWFkZXIge1xuICAgIGhlaWdodDogNDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4ubW9iaWxlLWhlYWRlciAubmF2YmFyLW5hdj5saT5hOmZvY3VzLFxuLm1vYmlsZS1oZWFkZXIgLm5hdmJhci1uYXY+bGk+YTpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5tb2JpbGUtaGVhZGVyLXN1Yi1tZW51PnVsIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yNik7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOjc2OHB4KSB7XG5cbiAgICAuaGVhZGVyLXNlY3Rpb24gLm1vYmlsZS1oZWFkZXItc3ViLW1lbnUsXG4gICAgLmhlYWRlci1zZWN0aW9uIC5uYXZiYXItdG9nZ2xlIHtcbiAgICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICAgIH1cbn1cblxuQG1lZGlhKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAubWFpbi1ib2R5IHtcbiAgICAgICAgcGFkZGluZy10b3A6IDQ1cHg7XG4gICAgfVxuXG4gICAgLm5hdmJhci1maXhlZC1ib3R0b20gLm5hdmJhci1jb2xsYXBzZSxcbiAgICAubmF2YmFyLWZpeGVkLXRvcCAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICAgICAgbWF4LWhlaWdodDogMzUwcHggIWltcG9ydGFudDtcbiAgICAgICAgb3ZlcmZsb3cteTogYXV0byAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5oZWFkZXItc2VjdGlvbiAubW9iaWxlLWhlYWRlci1zdWItbWVudSB7XG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC43Myk7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAxMDAlO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICByaWdodDogMDtcbiAgICB9XG5cbiAgICAuaGVhZGVyLXNlY3Rpb24gLm5hdmJhci1icmFuZD5pbWcge1xuICAgICAgICBtYXgtaGVpZ2h0OiA0MHB4O1xuICAgIH1cblxuICAgIC5oZWFkZXItc2VjdGlvbiBhLm5hdmJhci1icmFuZCB7XG4gICAgICAgIHBhZGRpbmc6IDVweCAxNXB4O1xuICAgIH1cblxuICAgIC5zcGVjaWFsaXphdGlvbi1kZXRhaWxzLWNvbnRhaW5lcj5saSB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5tb2JpbGUtaGVhZGVyLXN1Yi1tZW51IHVsIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgIH1cblxuICAgIC5tb2JpbGUtaGVhZGVyLXN1Yi1tZW51IHVsIGxpIHtcbiAgICAgICAgcGFkZGluZzogMTBweCAwO1xuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNTVweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cblxuICAgIC5tb2JpbGUtaGVhZGVyLXN1Yi1tZW51IHVsIGxpLmFjdGl2ZSBhIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmZmY7XG4gICAgfVxuXG4gICAgLm1vYmlsZS1oZWFkZXItc3ViLW1lbnUgdWwgbGk6Zmlyc3QtY2hpbGQge1xuICAgICAgICAvKnBhZGRpbmctdG9wOiAxNHB4OyovXG4gICAgfVxuXG4gICAgLnRlY2hub2xvZ3ktbGlzdC1jb250YWluZXI+bGkgaW1nIHtcbiAgICAgICAgcGFkZGluZzogOHB4O1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgfVxuXG4gICAgLmNvbnRhY3QtZm9ybSAuZm9ybS1jb250cm9sOjotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgLyogQ2hyb21lL09wZXJhL1NhZmFyaSAqL1xuICAgICAgICBjb2xvcjogcmdiYSgyMDQsIDIwMywgMjAzLCAwLjg0KTtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgIH1cblxuICAgIC5jb250YWN0LWZvcm0gLmZvcm0tY29udHJvbDo6LW1vei1wbGFjZWhvbGRlciB7XG4gICAgICAgIC8qIEZpcmVmb3ggMTkrICovXG4gICAgICAgIGNvbG9yOiByZ2JhKDIwNCwgMjAzLCAyMDMsIDAuODQpO1xuICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgfVxuXG4gICAgLmNvbnRhY3QtZm9ybSAuZm9ybS1jb250cm9sOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgICAgIC8qIElFIDEwKyAqL1xuICAgICAgICBjb2xvcjogcmdiYSgyMDQsIDIwMywgMjAzLCAwLjg0KTtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgIH1cblxuICAgIC5jb250YWN0LWZvcm0gLmZvcm0tY29udHJvbDotbW96LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgLyogRmlyZWZveCAxOC0gKi9cbiAgICAgICAgY29sb3I6IHJnYmEoMjA0LCAyMDMsIDIwMywgMC44NCk7XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICB9XG5cbiAgICAuc29sdXRpb24taGVhZGluZyB7XG4gICAgICAgIHBhZGRpbmc6IDIwcHggMiUgMDtcbiAgICB9XG5cbiAgICAuY2Fyb3VzZWwtY29udHJvbC5sZWZ0IHNwYW4sXG4gICAgLmNhcm91c2VsLWNvbnRyb2wucmlnaHQgc3BhbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5zZXJ2aWNlLXNlY3Rpb24gaW1nIHtcbiAgICAgICAgd2lkdGg6IDYwJTtcbiAgICB9XG59XG5cbi5uYXZiYXItdG9nZ2xlICoge1xuICAgIC1tcy10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZTtcbiAgICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG4gICAgLW8tdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xufVxuXG4ubmF2YmFyLXRvZ2dsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbn1cblxuLm5hdmJhci10b2dnbGUgc3BhbiB7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgaGVpZ2h0OiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogM3B4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5uYXZiYXItdG9nZ2xlIHNwYW4uYmFyMSB7XG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtby10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAgIC1tcy10cmFuc2Zvcm0tb3JpZ2luOiAxcHggM3B4O1xuICAgIC13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjogMXB4IDNweDtcbiAgICAtbW96LXRyYW5zZm9ybS1vcmlnaW46IDFweCAzcHg7XG4gICAgLW8tdHJhbnNmb3JtLW9yaWdpbjogMXB4IDNweDtcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiAxcHggM3B4O1xuICAgIHdpZHRoOiAyNHB4O1xufVxuXG4ubmF2YmFyLXRvZ2dsZSBzcGFuLmJhcjIge1xuICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgtMzYwZGVnKSBzY2FsZSgwKTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKC0zNjBkZWcpIHNjYWxlKDApO1xuICAgIC1tb3otdHJhbnNmb3JtOiByb3RhdGUoLTM2MGRlZykgc2NhbGUoMCk7XG4gICAgLW8tdHJhbnNmb3JtOiByb3RhdGUoLTM2MGRlZykgc2NhbGUoMCk7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTM2MGRlZykgc2NhbGUoMCk7XG59XG5cbi5uYXZiYXItdG9nZ2xlIHNwYW4uYmFyMyB7XG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZykgc2NhbGUoMCk7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpIHNjYWxlKDApO1xuICAgIC1tb3otdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKSBzY2FsZSgwKTtcbiAgICAtby10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpIHNjYWxlKDApO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZykgc2NhbGUoMCk7XG59XG5cbi5uYXZiYXItdG9nZ2xlIHNwYW4uYmFyNCB7XG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG4gICAgLW1zLXRyYW5zZm9ybS1vcmlnaW46IDJweCAwcHg7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOiAycHggMHB4O1xuICAgIHRyYW5zZm9ybS1vcmlnaW46IDJweCAwcHg7XG4gICAgd2lkdGg6IDI0cHg7XG59XG5cbi5uYXZiYXItdG9nZ2xlLmNvbGxhcHNlZCBzcGFuLmJhcjEsXG4ubmF2YmFyLXRvZ2dsZS5jb2xsYXBzZWQgc3Bhbi5iYXIyLFxuLm5hdmJhci10b2dnbGUuY29sbGFwc2VkIHNwYW4uYmFyMyxcbi5uYXZiYXItdG9nZ2xlLmNvbGxhcHNlZCBzcGFuLmJhcjQge1xuICAgIC1tcy10cmFuc2Zvcm06IG5vbmU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IG5vbmU7XG4gICAgdHJhbnNmb3JtOiBub25lO1xuICAgIHdpZHRoOiAyMHB4O1xufVxuXG4ubmF2YmFyLXRvZ2dsZTpmb2N1cyxcbi5uYXZiYXItdG9nZ2xlOmhvdmVyLFxuLm5hdmJhci10b2dnbGUge1xuICAgIGJvcmRlci1jb2xvcjogcmdiYSgyMjEsIDIyMSwgMjIxLCAwKSAhaW1wb3J0YW50O1xufVxuXG4uY29udGFpbmVyLWZsdWlkPi5uYXZiYXItY29sbGFwc2UsXG4uY29udGFpbmVyLWZsdWlkPi5uYXZiYXItaGVhZGVyLFxuLmNvbnRhaW5lcj4ubmF2YmFyLWNvbGxhcHNlLFxuLmNvbnRhaW5lcj4ubmF2YmFyLWhlYWRlciB7XG4gICAgcGFkZGluZy1yaWdodDogOHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5uYXZiYXItZml4ZWQtYm90dG9tIC5uYXZiYXItY29sbGFwc2UsXG4ubmF2YmFyLWZpeGVkLXRvcCAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICBvdmVyZmxvdy14OiBoaWRkZW4gIWltcG9ydGFudDtcbn1cblxuLm5hdj5saT5hIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVJbkRvd24ge1xuICAgIDAlIHtcbiAgICAgICAgb3BhY2l0eTogMDtcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC0xMDAlLCAwKTtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAtMTAwJSwgMCk7XG4gICAgfVxuXG4gICAgMTAwJSB7XG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiBub25lO1xuICAgICAgICB0cmFuc2Zvcm06IG5vbmU7XG4gICAgfVxufVxuXG5Aa2V5ZnJhbWVzIGZhZGVJbkRvd24ge1xuICAgIDAlIHtcbiAgICAgICAgb3BhY2l0eTogMDtcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC0xMDAlLCAwKTtcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgLTEwMCUsIDApO1xuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC0xMDAlLCAwKTtcbiAgICB9XG5cbiAgICAxMDAlIHtcbiAgICAgICAgb3BhY2l0eTogMTtcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IG5vbmU7XG4gICAgICAgIC1tcy10cmFuc2Zvcm06IG5vbmU7XG4gICAgICAgIHRyYW5zZm9ybTogbm9uZTtcbiAgICB9XG59XG5cbi5mYWRlSW5Eb3duIHtcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1uYW1lOiBmYWRlSW5Eb3duO1xuICAgIGFuaW1hdGlvbi1uYW1lOiBmYWRlSW5Eb3duO1xufVxuXG4uZGVsYXkxIHtcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLjJzO1xuICAgIGFuaW1hdGlvbi1kZWxheTogLjJzO1xufVxuXG4uZGVsYXkyIHtcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLjNzO1xuICAgIGFuaW1hdGlvbi1kZWxheTogLjNzO1xufVxuXG4uZGVsYXkzIHtcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLjRzO1xuICAgIGFuaW1hdGlvbi1kZWxheTogLjRzO1xufVxuXG4uYW5pbWF0ZWQge1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDFzO1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLWZpbGwtbW9kZTogYm90aDtcbiAgICBhbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xufVxuXG4ubmV3X2Rlc19ib3JkZXIge1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNmZmZmZmY7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/dashboard/header/dashboard-header.component.html":
/*!******************************************************************!*\
  !*** ./src/app/dashboard/header/dashboard-header.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"loader-wrapper\"><div class=\"loader\"></div></div> -->\n<nav class=\"d-flex navbar-expand-md bg-nav my-nav header-section flex-wrap\">\n  <div class=\"d-flex w-100 align-items-center justify-content-between\">\n    <div class=\"d-flex align-items-center logo-wrapper\">\n      <a class=\"logo\" (click)=\"gotoDashboard()\">\n          <img src=\"../assets/images/logo.png\" class=\"img-fluid img-size\">\n      </a>\n    </div>\n    <div class=\"navbar-toggle collapsed\">\n      <span class=\"bar1\"></span>\n      <span class=\"bar2\"></span>\n      <span class=\"bar3\"></span>\n      <span class=\"bar4\"></span>\n    </div>\n    <div id=\"headerTitle\" class=\"dashboard-heading\" [title]=\"header\">{{header}}</div>\n    <div class=\"padding-right-15 dropdown account-dropdown active-account\">\n      <div class=\"text-white font-14 dropdown-account-name cursor \">\n        {{username}}\n      </div>\n      <div class=\"dropdown-select-wrapper\">\n        <ul class=\"list-unstyled dropdown-items font-14\">\n          <li (click)=\"myProfile()\">\n            <button type=\"button\" mat-raised-button class=\"w-100 print-submit-btn attachment-btn white-btn\">\n              <img src=\"../assets/images/profile.png\" class=\"profile-img\"><span class=\"pl-3\">MY\n                ACCOUNT</span></button>\n          </li>\n          <li (click)=\"logout()\">\n            <button type=\"button\" mat-raised-button class=\"w-100 print-submit-btn attachment-btn white-btn\">\n              <img src=\"../assets/images/logout.png\" class=\"profile-img\"><span class=\"pl-3\">LOGOUT</span></button>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n  <div class=\"collapse navbar-collapse mobile-header-sub-menu new\" id=\"bs-example-navbar-collapse-1\"\n    aria-expanded=\"false\">\n    <div class=\"animated fadeInDown delay1 new_des_border\">\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li class=\"animated fadeInDown delay1 menu_mobile active\"><a id=\"homelink\" class=\"new_a active\" href=\"javascript:;\"><img\n              _ngcontent-c4=\"\" class=\"menu-icon-menu img-white\" src=\"../assets/images/dashboard-w.png\"><span>DASHBOARD</span></a></li>\n        <li class=\"animated fadeInDown delay1 menu_mobile\"><a href=\"javascript:;\" class=\"new_a\"><img\n          _ngcontent-c4=\"\" class=\"menu-icon-menu img-white\" src=\"../assets/images/project-memo-w.png\"><span>PROJECT</span></a></li>\n          <li class=\"animated fadeInDown delay1 menu_mobile\"><a href=\"javascript:;\" class=\"new_a\"><img\n            _ngcontent-c4=\"\" class=\"menu-icon-menu img-white\" src=\"../assets/images/addendum-w.png\"><span>ADDENDUM</span></a></li>\n        <li class=\"animated fadeInDown delay1 menu_mobile\"><a href=\"javascript:;\"class=\"new_a\"><img\n          _ngcontent-c4=\"\" class=\"menu-icon-menu img-white\" src=\"../assets/images/user-w.png\"><span>USER</span></a></li>\n          <li (click)=\"myProfile()\" class=\"animated fadeInDown delay1 menu_mobile\">\n            <a href=\"javascript:;\"class=\"new-a\">\n              <img src=\"../assets/images/profile-w.png\" class=\"menu-icon-menu\"><span class=\"pl-0 pl-md-3 pl-lg-3 pl-xl-3\">MY\n                ACCOUNT</span></a>\n          </li>\n          <li (click)=\"logout()\" class=\"animated fadeInDown delay1 menu_mobile\">\n\n            <a href=\"javascript:;\"class=\"new-a\"><img src=\"../assets/images/logout-w.png\" class=\"menu-icon-menu\"><span class=\"pl-0 pl-md-3 pl-lg-3 pl-xl-3\">LOGOUT</span></a>\n          </li>\n      </ul>\n    </div>\n  </div>\n  <!-- <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\n     <ul class=\"navbar-nav\">\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" href=\"/\">Link</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" href=\"/\">Link</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" href=\"/\">Link</a>\n        </li>\n      </ul>\n    </div>    -->\n</nav>\n<app-user-account [user]=\"user\" *ngIf=\"displayForm\" [isProfile]=\"true\" (closes)=\"ModelClosed($event)\">\n</app-user-account>\n"

/***/ }),

/***/ "./src/app/dashboard/header/dashboard-header.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/dashboard/header/dashboard-header.component.ts ***!
  \****************************************************************/
/*! exports provided: DashboardHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardHeaderComponent", function() { return DashboardHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var src_app_dashboard_service_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/dashboard/_service/users.service */ "./src/app/dashboard/_service/users.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_models/user-list.model */ "./src/app/_models/user-list.model.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _dashboard_header_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard-header.service */ "./src/app/dashboard/header/dashboard-header.service.ts");
/* harmony import */ var _utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../_utility/hardreload.helper */ "./src/app/_utility/hardreload.helper.ts");








var DashboardHeaderComponent = /** @class */ (function () {
    function DashboardHeaderComponent(userService, router, headerService) {
        this.userService = userService;
        this.router = router;
        this.headerService = headerService;
        this.username = "";
        this.user = new src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_3__["UserModel"]();
    }
    DashboardHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.header = this.getTitle(location.href);
        var profile = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_5__["UserAuthHelper"].GetProfile();
        this.username = profile.fullName;
        this.userRole = profile.type;
        this.header = this.getTitle(location.href);
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
                var currentUrl = event.url;
                _this.header = _this.getTitle(currentUrl);
            }
        });
        this.headerService.getTitle().subscribe(function (res) {
            if (res) {
                _this.header = res;
            }
        });
    };
    DashboardHeaderComponent.prototype.getTitle = function (currentUrl) {
        if (currentUrl.includes("user")) {
            return "Users";
        }
        else if (currentUrl.includes("projects/")) {
            var urlParts = currentUrl.split("/");
            if (urlParts.length > 4) {
                return "Project Memo " + urlParts[4];
            }
        }
        else if (currentUrl.includes("addendum/")) {
            var urlParts = currentUrl.split("/");
            if (urlParts.length > 4) {
                return "Addendum " + urlParts[4];
            }
        }
        else {
            return "Dashboard";
        }
    };
    DashboardHeaderComponent.prototype.myProfile = function () {
        var _this = this;
        this.userService.myProfile().subscribe(function (res) {
            _this.user = res.data;
            _this.user.password = "";
            _this.displayForm = true;
            _this.openModel();
        }, function (error) { });
    };
    DashboardHeaderComponent.prototype.openModel = function () {
        setTimeout(function () {
            $("#userProfile").modal("show");
        }, 100);
    };
    DashboardHeaderComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(["login"]);
    };
    DashboardHeaderComponent.prototype.ModelClosed = function (val) {
        var _this = this;
        $("#userProfile").modal("hide");
        setTimeout(function () {
            _this.displayForm = false;
        }, 200);
    };
    DashboardHeaderComponent.prototype.gotoDashboard = function () {
        _utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_7__["HardReload"].redirectTo("/", this.router);
    };
    DashboardHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-dashboard-header",
            template: __webpack_require__(/*! ./dashboard-header.component.html */ "./src/app/dashboard/header/dashboard-header.component.html"),
            styles: [__webpack_require__(/*! ./dashboard-header.component.css */ "./src/app/dashboard/header/dashboard-header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_dashboard_service_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _dashboard_header_service__WEBPACK_IMPORTED_MODULE_6__["DashboardHeaderService"]])
    ], DashboardHeaderComponent);
    return DashboardHeaderComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/header/dashboard-header.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/header/dashboard-header.service.ts ***!
  \**************************************************************/
/*! exports provided: DashboardHeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardHeaderService", function() { return DashboardHeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var DashboardHeaderService = /** @class */ (function () {
    function DashboardHeaderService(router) {
        this.router = router;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    DashboardHeaderService.prototype.setTitle = function (title) {
        this.subject.next(title);
    };
    DashboardHeaderService.prototype.getTitle = function () {
        return this.subject.asObservable();
    };
    DashboardHeaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], DashboardHeaderService);
    return DashboardHeaderService;
}());



/***/ }),

/***/ "./src/app/dashboard/home/home.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/home/home.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/home/home.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  home works!\n</p>\n"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/dashboard/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/dashboard/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/print-addendum/print-addendum.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/dashboard/print-addendum/print-addendum.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#dvContents {\n    font-size: 15px;\n    color: rgba(0, 0, 0, 0.6);\n    font-family: sans-serif;\n}\n\nh1 {\n    font-size: 22px;\n    font-weight: 300;\n    color: #202020;\n    margin: 0;\n}\n\nh2 {\n    color: #202020;\n    font-weight: 500;\n    font-size: 18px;\n    margin-bottom: 16px;\n    margin-top: 20px;\n}\n\n.float-right {\n    float: right;\n}\n\n.page-footer, .page-footer-space {\n    padding-top: 5px;\n    height: 30px;\n}\n\n.page-header, .page-header-space {\n    height: 30px;\n}\n\n.page-footer {\n    position: fixed;\n    bottom: 0;\n    width: 100%;\n}\n\n.page-header {\n    position: fixed;\n    top: 0mm;\n    width: 100%;\n}\n\n.page {\n    page-break-after: always;\n}\n\n@page {\n    size: auto;\n    margin: 5mm 5mm;\n}\n\n@media print {\n    thead {\n        display: table-header-group;\n    }\n    tfoot {\n        display: table-footer-group;\n    }\n    body {\n        margin: 0;\n    }\n    section {\n        padding: 10px 10px;\n    }\n    .page-header, .page-footer {\n        display: block !important;\n    }\n}\n\n.page-header, .page-footer {\n    display: none;\n}\n\n.text-dark {\n    font-size: 14px;\n    color: #202020 !important;\n    margin-bottom: 14px;\n}\n\n.mb-0 {\n    margin-bottom: 0;\n}\n\n.print-wrapper {\n    padding: 0 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3ByaW50LWFkZGVuZHVtL3ByaW50LWFkZGVuZHVtLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLFNBQVM7QUFDYjs7QUFFQTtJQUNJLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsUUFBUTtJQUNSLFdBQVc7QUFDZjs7QUFFQTtJQUNJLHdCQUF3QjtBQUM1Qjs7QUFFQTtJQUNJLFVBQVU7SUFDVixlQUFlO0FBQ25COztBQUVBO0lBQ0k7UUFDSSwyQkFBMkI7SUFDL0I7SUFDQTtRQUNJLDJCQUEyQjtJQUMvQjtJQUNBO1FBQ0ksU0FBUztJQUNiO0lBQ0E7UUFDSSxrQkFBa0I7SUFDdEI7SUFDQTtRQUNJLHlCQUF5QjtJQUM3QjtBQUNKOztBQUVBO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGVBQWU7SUFDZix5QkFBeUI7SUFDekIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9wcmludC1hZGRlbmR1bS9wcmludC1hZGRlbmR1bS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2R2Q29udGVudHMge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjYpO1xuICAgIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xufVxuXG5oMSB7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgY29sb3I6ICMyMDIwMjA7XG4gICAgbWFyZ2luOiAwO1xufVxuXG5oMiB7XG4gICAgY29sb3I6ICMyMDIwMjA7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4uZmxvYXQtcmlnaHQge1xuICAgIGZsb2F0OiByaWdodDtcbn1cblxuLnBhZ2UtZm9vdGVyLCAucGFnZS1mb290ZXItc3BhY2Uge1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgaGVpZ2h0OiAzMHB4O1xufVxuXG4ucGFnZS1oZWFkZXIsIC5wYWdlLWhlYWRlci1zcGFjZSB7XG4gICAgaGVpZ2h0OiAzMHB4O1xufVxuXG4ucGFnZS1mb290ZXIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBib3R0b206IDA7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5wYWdlLWhlYWRlciB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMG1tO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ucGFnZSB7XG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xufVxuXG5AcGFnZSB7XG4gICAgc2l6ZTogYXV0bztcbiAgICBtYXJnaW46IDVtbSA1bW07XG59XG5cbkBtZWRpYSBwcmludCB7XG4gICAgdGhlYWQge1xuICAgICAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7XG4gICAgfVxuICAgIHRmb290IHtcbiAgICAgICAgZGlzcGxheTogdGFibGUtZm9vdGVyLWdyb3VwO1xuICAgIH1cbiAgICBib2R5IHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgIH1cbiAgICBzZWN0aW9uIHtcbiAgICAgICAgcGFkZGluZzogMTBweCAxMHB4O1xuICAgIH1cbiAgICAucGFnZS1oZWFkZXIsIC5wYWdlLWZvb3RlciB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gICAgfVxufVxuXG4ucGFnZS1oZWFkZXIsIC5wYWdlLWZvb3RlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLnRleHQtZGFyayB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjMjAyMDIwICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luLWJvdHRvbTogMTRweDtcbn1cblxuLm1iLTAge1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5wcmludC13cmFwcGVyIHtcbiAgICBwYWRkaW5nOiAwIDUwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/print-addendum/print-addendum.component.html":
/*!************************************************************************!*\
  !*** ./src/app/dashboard/print-addendum/print-addendum.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal show\" id=\"printAddendumModal\">\n  <div class=\"modal-dialog modal-print-memo\">\n    <div class=\"modal-content \">\n      <div class=\"modal-header modal-heading\">\n        <div>\n          <button type=\"button\" mat-raised-button color=\"primary\" (click)=\"print()\"\n            class=\"complete-form-submit ml-2 mb-2\">PRINT</button>\n          <button *ngIf=\"addendum.attachment.length > 0\" mat-raised-button color=\"\" type=\"button\" (click)=\"download()\"\n            class=\"print-submit-btn white-btn ml-2\">DOWNLOAD ALL ATTACHMENTS</button>\n        </div>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <section id=\"dvContents\" class=\"general-form px-3 pb-4\">\n        <div class=\"page-header\">\n          Addendum {{addendumId}}\n          <div class=\"float-right\">Status:\n            {{ addendum.adStatus == 'Submitted' && !(userRole == 'Standard') ? 'Needs Pickup' :  addendum.adStatus }}\n          </div>\n        </div>\n        <div class=\"page-footer\">\n          Exported on: {{ today | date : 'MM/dd/yyyy' }}\n          <div class=\"float-right\">\n            <span id=\"pageFooter\" class=\"page-counter\">\n            </span>\n          </div>\n        </div>\n        <table #table id=\"tableContent\" style=\"width: 100%;\">\n          <thead>\n            <tr>\n              <td>\n                <!--place holder for the fixed-position header-->\n                <h6 class=\"page-header-space\"></h6>\n              </td>\n            </tr>\n          </thead>\n          <tbody>\n            <tr>\n              <td>\n                <div class=\"print-wrapper\">\n                  <div class=\"print-header\">\n                    <h1>Addendum for {{addendum.contractorCompanyName}} on {{addendum.catalog}} from {{addendum.from}}\n                    </h1>\n                    <div>Addendum {{addendum.adID}}</div>\n                  </div>\n                  <div class=\"information-group\">\n                    <h2 class=\"text-uppercase\">GENERAL INFORMATION</h2>\n                    <div>Assigned To </div>\n                    <div class=\"text-dark\">{{addendum.assignedUser ? addendum.assignedUser :  '_ _'  }}</div>\n                    <div>In CC</div>\n                    <div class=\"text-dark\">\n                      <div *ngIf=\"!(addendum.mailList && addendum.mailList.length > 0)\" class=\"font-14\">\n                        _ _\n                      </div>\n                      <div *ngIf=\"addendum.mailList && addendum.mailList.length > 0\" class=\"font-14\">\n                        <span *ngFor=\"let p of addendum.mailList;let i = index\"> {{p.email}}<span\n                            *ngIf=\"i < (addendum.mailList.length - 1)\">,</span></span>\n                      </div>\n                    </div>\n                    <div>Project Memo</div>\n                    <div class=\"text-dark\">\n                      {{ addendum.contractorCompanyName }}\n                      on {{ addendum.catalog }}\n                      from {{ addendum.from }}\n                      (#{{ addendum.parentProjectMemo }})\n                      {{ addendum.projectCreatedAt | date : 'MM/dd/yy' }}\n                    </div>\n                    <div>Counterparties to Original Agreement</div>\n                    <div class=\"text-dark\">{{addendum.counterparties ? addendum.counterparties : '_ _'}}</div>\n                    <div>Responsible Party</div>\n                    <div class=\"text-dark\">{{addendum.responsibleParty ? addendum.responsibleParty : '_ _'}}</div>\n                    <h2>Changes to Existing Agreement and/or Additional Terms</h2>\n                    <div>Description of Changes and/or Additional Terms</div>\n                    <div class=\"text-dark\">\n                      {{addendum.changes ? addendum.changes : '_ _'}}\n                    </div>\n                    <div>Reason(s) for Changes</div>\n                    <div class=\"text-dark\">{{ addendum.reasons ? addendum.reasons : '_ _' }}</div>\n                    <div>Attachments</div>\n                    <div *ngFor=\"let p of addendum.attachment\" class=\"text-dark mb-0\">\n                      {{p.file}}\n                    </div>\n                  </div>\n                </div>\n              </td>\n            </tr>\n          </tbody>\n          <tfoot>\n            <tr>\n              <td>\n                <!--place holder for the fixed-position footer-->\n                <div class=\"page-footer-space\"></div>\n              </td>\n            </tr>\n          </tfoot>\n        </table>\n      </section>\n    </div>\n  </div>\n  <div id=\"myprintDiv\" class=\"d-none\"></div>\n</div>\n\n\n<!-- \n<div *ngIf=\"false\" class=\"\">\n  <section class=\"pl-3 pl-sm-3 pl-3 pl-md-5 pl-lg-5\">\n    <div class=\"padding-details-lg\">\n      <div *ngIf=\"addendum.contractorCompanyName && addendum.catalog && addendum.from\"\n        class=\"main-heading font-weight-300\">\n        Addendum for {{addendum.contractorCompanyName}} on {{addendum.catalog}} from {{addendum.from}}\n      </div>\n      <div *ngIf=\"!(addendum.contractorCompanyName && addendum.catalog && addendum.from)\"\n        class=\"main-heading font-weight-300\">\n        Addendum {{addendum.adID}}\n      </div>\n      <div class=\"light-black font-14\">{{addendum.adID}}</div>\n    </div>\n    <section>\n      <div class=\"pb-1\">\n        <div class=\"heading-5 padding-details-md font-weight-500\">\n          GENERAL INFORMATION\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Assigned To\n          </div>\n          <div class=\"font-14\">\n            {{addendum.assignedUser ? addendum.assignedUser :  '_ _'  }}\n          </div>\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            In CC\n          </div>\n          <div *ngIf=\"!(addendum.mailList && addendum.mailList.length > 0)\" class=\"font-14\">\n            _ _\n          </div>\n          <div *ngIf=\"addendum.mailList && addendum.mailList.length > 0\" class=\"font-14\">\n            <span *ngFor=\"let p of addendum.mailList;let i = index\"> {{p.email}}<span\n                *ngIf=\"i < (addendum.mailList.length - 1)\">,</span></span>\n          </div>\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Project Memo\n          </div>\n          <div class=\"font-14\">\n            {{ addendum.contractorCompanyName }}\n            on {{ addendum.catalog }}\n            from {{ addendum.from }}\n            (#{{ addendum.parentProjectMemo }})\n            {{ addendum.projectCreatedAt | date : 'MM/dd/yy'}}\n          </div>\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Counterparties to Original Agreement\n          </div>\n          <div class=\"font-14\">\n            {{addendum.counterparties ? addendum.counterparties : '_ _'}}\n          </div>\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Responsible Party\n          </div>\n          <div class=\"font-14\">\n            {{addendum.responsibleParty ? addendum.responsibleParty : '_ _'}}\n          </div>\n        </div>\n      </div>\n    </section>\n    <section>\n      <div class=\"pb-1\">\n        <div class=\"heading-5 padding-details-md font-weight-500\">\n          Changes to Existing Agreement and/or Additional Terms\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Description of Changes and/or Additional Terms\n          </div>\n          <div class=\"font-14\">\n            {{addendum.changes ? addendum.changes : '_ _'}}\n          </div>\n        </div>\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Reason(s) for Changes\n          </div>\n          <div class=\"font-14\">\n            {{ addendum.reasons ? addendum.reasons : '_ _' }}\n          </div>\n        </div>\n      </div>\n    </section>\n    <section>\n      <div class=\"pb-1\">\n        <div class=\"padding-details-sm\">\n          <div class=\"light-black font-15\">\n            Attachments\n          </div>\n          <div *ngFor=\"let p of addendum.attachment\" class=\"font-14\">\n            {{p.file}}\n          </div>\n        </div>\n      </div>\n    </section>\n  </section>\n</div> -->"

/***/ }),

/***/ "./src/app/dashboard/print-addendum/print-addendum.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/dashboard/print-addendum/print-addendum.component.ts ***!
  \**********************************************************************/
/*! exports provided: PrintAddendumComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintAddendumComponent", function() { return PrintAddendumComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_projects_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../_models/projects.model */ "./src/app/_models/projects.model.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");




var PrintAddendumComponent = /** @class */ (function () {
    function PrintAddendumComponent() {
        this.downLoadFiles = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.today = new Date();
    }
    PrintAddendumComponent.prototype.ngOnInit = function () {
        // this.downLoadFiles = new EventEmitter<string>();
    };
    PrintAddendumComponent.prototype.print = function () {
        src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__["UserAuthHelper"].PrintPage();
    };
    PrintAddendumComponent.prototype.download = function () {
        this.downLoadFiles.emit("nae");
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_projects_model__WEBPACK_IMPORTED_MODULE_1__["AddendumFormModel"])
    ], PrintAddendumComponent.prototype, "addendum", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PrintAddendumComponent.prototype, "downLoadFiles", void 0);
    PrintAddendumComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-print-addendum",
            template: __webpack_require__(/*! ./print-addendum.component.html */ "./src/app/dashboard/print-addendum/print-addendum.component.html"),
            styles: [__webpack_require__(/*! ./print-addendum.component.css */ "./src/app/dashboard/print-addendum/print-addendum.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PrintAddendumComponent);
    return PrintAddendumComponent;
}());

// const webpage =
// location.origin + "/addendum/" + this.addendum.adID + "/print";
// const mydiv = document.getElementById("myprintDiv");
// const aTag = document.createElement("a");
// aTag.setAttribute("href", webpage);
// aTag.setAttribute("target", "_blank");
// aTag.setAttribute("id", "dumprintLink");
// aTag.setAttribute("rel", "noopener");
// aTag.innerHTML = "link text";
// mydiv.appendChild(aTag);
// document.getElementById("dumprintLink").click();
// UserAuthHelper.Addendum = this.addendum;


/***/ }),

/***/ "./src/app/dashboard/print-memo/print-memo.component.css":
/*!***************************************************************!*\
  !*** ./src/app/dashboard/print-memo/print-memo.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#dvContents {\n    font-size: 15px;\n    color: rgba(0, 0, 0, 0.6);\n    font-family: sans-serif;\n}\n\nh1 {\n    font-size: 22px;\n    font-weight: 300;\n    color: #202020;\n    margin: 0;\n}\n\nh2 {\n    color: #202020;\n    font-weight: 500;\n    font-size: 18px;\n    margin-bottom: 16px;\n    margin-top: 20px;\n}\n\n.float-right {\n    float: right;\n}\n\n.page-footer, .page-footer-space {\n    padding-top: 5px;\n    height: 30px;\n}\n\n.page-header, .page-header-space {\n    height: 30px;\n}\n\n.page-footer {\n    position: fixed;\n    bottom: 0;\n    width: 100%;\n}\n\n.page-header {\n    position: fixed;\n    top: 0mm;\n    width: 100%;\n}\n\n.page {\n    page-break-after: always;\n}\n\n@page {\n    size: auto;\n    margin: 5mm 5mm;\n}\n\n@media print {\n    thead {\n        display: table-header-group;\n    }\n    tfoot {\n        display: table-footer-group;\n    }\n    body {\n        margin: 0;\n    }\n    section {\n        padding: 10px 10px;\n    }\n    .page-header, .page-footer {\n        display: block !important;\n    }\n}\n\n.page-header, .page-footer {\n    display: none;\n}\n\n.text-dark {\n    font-size: 14px;\n    color: #202020 !important;\n    margin-bottom: 14px;\n}\n\n.mb-0 {\n    margin-bottom: 0;\n}\n\n.print-wrapper {\n    padding: 0 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3ByaW50LW1lbW8vcHJpbnQtbWVtby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZUFBZTtJQUNmLHlCQUF5QjtJQUN6Qix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixTQUFTO0lBQ1QsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtJQUNmLFFBQVE7SUFDUixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSx3QkFBd0I7QUFDNUI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsZUFBZTtBQUNuQjs7QUFFQTtJQUNJO1FBQ0ksMkJBQTJCO0lBQy9CO0lBQ0E7UUFDSSwyQkFBMkI7SUFDL0I7SUFDQTtRQUNJLFNBQVM7SUFDYjtJQUNBO1FBQ0ksa0JBQWtCO0lBQ3RCO0lBQ0E7UUFDSSx5QkFBeUI7SUFDN0I7QUFDSjs7QUFFQTtJQUNJLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGVBQWU7QUFDbkIiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvcHJpbnQtbWVtby9wcmludC1tZW1vLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjZHZDb250ZW50cyB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG59XG5cbmgxIHtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICBjb2xvcjogIzIwMjAyMDtcbiAgICBtYXJnaW46IDA7XG59XG5cbmgyIHtcbiAgICBjb2xvcjogIzIwMjAyMDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5mbG9hdC1yaWdodCB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4ucGFnZS1mb290ZXIsIC5wYWdlLWZvb3Rlci1zcGFjZSB7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgICBoZWlnaHQ6IDMwcHg7XG59XG5cbi5wYWdlLWhlYWRlciwgLnBhZ2UtaGVhZGVyLXNwYWNlIHtcbiAgICBoZWlnaHQ6IDMwcHg7XG59XG5cbi5wYWdlLWZvb3RlciB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogMDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnBhZ2UtaGVhZGVyIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwbW07XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5wYWdlIHtcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XG59XG5cbkBwYWdlIHtcbiAgICBzaXplOiBhdXRvO1xuICAgIG1hcmdpbjogNW1tIDVtbTtcbn1cblxuQG1lZGlhIHByaW50IHtcbiAgICB0aGVhZCB7XG4gICAgICAgIGRpc3BsYXk6IHRhYmxlLWhlYWRlci1ncm91cDtcbiAgICB9XG4gICAgdGZvb3Qge1xuICAgICAgICBkaXNwbGF5OiB0YWJsZS1mb290ZXItZ3JvdXA7XG4gICAgfVxuICAgIGJvZHkge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgfVxuICAgIHNlY3Rpb24ge1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDEwcHg7XG4gICAgfVxuICAgIC5wYWdlLWhlYWRlciwgLnBhZ2UtZm9vdGVyIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcbiAgICB9XG59XG5cbi5wYWdlLWhlYWRlciwgLnBhZ2UtZm9vdGVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4udGV4dC1kYXJrIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICMyMDIwMjAgIWltcG9ydGFudDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNHB4O1xufVxuXG4ubWItMCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbn1cblxuLnByaW50LXdyYXBwZXIge1xuICAgIHBhZGRpbmc6IDAgNTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/dashboard/print-memo/print-memo.component.html":
/*!****************************************************************!*\
  !*** ./src/app/dashboard/print-memo/print-memo.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal show\" id=\"printProjectModal\">\n  <div class=\"modal-dialog modal-print-memo\">\n    <div class=\"modal-content \">\n      <div class=\"modal-header modal-heading\">\n        <div>\n          <button type=\"button\" mat-raised-button color=\"primary\" (click)=\"print()\"\n            class=\"complete-form-submit ml-2 mb-2\">PRINT</button>\n          <button *ngIf=\"project.attachment.length > 0\" mat-raised-button color=\"\" type=\"button\" (click)=\"download()\"\n            class=\"print-submit-btn white-btn ml-2\">DOWNLOAD ALL ATTACHMENTS</button>\n        </div>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <section id=\"dvContents\" class=\"general-form px-3 py-xl-5 py-lg-5 py-md-4 py-sm-3\">\n        <div class=\"page-header\">\n          Project Memo {{projectId}}\n          <div class=\"float-right\">Status:\n            {{ project.Status == 'Submitted' && !(userRole == 'Standard') ? 'Needs Pickup' :  project.Status }}\n          </div>\n        </div>\n        <div class=\"page-footer\">\n          Exported on: {{ today | date : 'MM/dd/yyyy' }}\n          <div class=\"float-right\">\n            <span id=\"pageFooter\" class=\"page-counter\">\n            </span>\n          </div>\n        </div>\n        <table #table id=\"tableContent\" style=\"width: 100%;\">\n          <thead>\n            <tr>\n              <td>\n                <!--place holder for the fixed-position header-->\n                <h6 class=\"page-header-space\"></h6>\n              </td>\n            </tr>\n          </thead>\n          <tbody>\n            <tr>\n              <td>\n                <div class=\"print-wrapper\">\n                  <div class=\"print-header\">\n                    <h1 *ngIf=\"project.contractorCompanyName && project.catalog && project.from\">\n                      Project Memo for\n                      {{project.contractorCompanyName? project.contractorCompanyName : '[vender name]'}}\n                      on {{project.catalog ? project.catalog : '[project catalog]'}}\n                      from {{project.from ? project.from : '[user name]'}}\n                    </h1>\n                    <h1 *ngIf=\"!(project.contractorCompanyName && project.catalog && project.from)\">\n                      Project Memo {{projectId}}\n                    </h1>\n                    <div>{{projectId}}</div>\n                  </div>\n                  <div class=\"information-group\">\n                    <h2 class=\"text-uppercase\">GENERAL INFORMATION</h2>\n                    <div>Assigned To </div>\n                    <div class=\"text-dark\">\n                      {{project.assignedUser ? project.assignedUser : '- -'}}\n                    </div>\n                    <div>In CC</div>\n                    <div class=\"text-dark\">\n                      <div *ngIf=\"project.mailList.length == 0\" class=\"font-14 text-justify\">\n                        - -\n                      </div>\n                      <div *ngIf=\"project.mailList.length > 0\" class=\"font-14 text-justify\">\n                        <span *ngFor=\"let p of project.mailList;let i = index\"> {{p.email}}<span\n                            *ngIf=\"i < (project.mailList.length - 1)\">,</span></span>\n                      </div>\n                    </div>\n                    <div>Project Name</div>\n                    <div class=\"text-dark\">\n                      {{project.projectName? project.projectName :'- -'}}\n                    </div>\n                    <div>Catalog</div>\n                    <div class=\"text-dark\">\n                      {{project.catalog ? project.catalog : '- -'}}\n                    </div>\n                    <div>Effective Dates\n                    </div>\n                    <div class=\"text-dark\">\n                      From {{ project.effectiveDate ? (project.effectiveDate | date : 'MM/dd/yy') : ' - - ' }}\n                      to {{ project.expirationDate ? (project.expirationDate | date : 'MM/dd/yy') : ' - - ' }}\n                    </div>\n                    <h2>CONTRACTOR</h2>\n                    <div>\n                      Contractor Name\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.contractorCompanyName ? project.contractorCompanyName : '- -'}}\n                    </div>\n                    <div>\n                      Primary Contact Name\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.contractorContactName ? project.contractorContactName : '- -'}}\n                    </div>\n                    <div>\n                      Primary Contact Email\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.contractorContactEmail ? project.contractorContactEmail : '- -'}}\n                    </div>\n                    <div>\n                      Primary Contact Phone\n                    </div>\n                    <div class=\"text-dark\">\n                     \n                   <span *ngIf=\"codeSelected\">{{codeSelected.code}}</span><span *ngIf=\"project.contractorContactPhone && codeSelected\"> {{project.contractorContactPhone|tel:'hh': codeSelected.code}}</span> <span *ngIf=\"!project.contractorContactPhone\">- -</span>\n                    </div>\n                    <div>\n                      Will Contractor Work Onsite?\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.contractorWorkOnsite == true ? 'Yes' : project.contractorWorkOnsite == false ? 'No' : '- -'}}\n                    </div>\n                    <div>\n                      Contractor Insurance\n                    </div>\n                    <div class=\"text-dark\">\n                      <span *ngIf=\"project.contractorInsuranceGeneral\">\n                        General Liability,\n                      </span>\n                      <span *ngIf=\"project.contractorInsuranceEnO\">\n                        E & O,\n                      </span>\n                      <span *ngIf=\"project.contractorInsuranceProduction\">\n                        Worker's Comp,\n                      </span>\n                      <span *ngIf=\"project.contractorInsuranceWorkersComp\">\n                        Production Insurance,\n                      </span>\n                    </div>\n                    <h2>SERVICES / DELIVERABLES / PAYMENT</h2>\n                    <div>\n                      Contractor's Service\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.contractorServices ? project.contractorServices : '- -'}}\n                    </div>\n                    <div>\n                      Approximate Hours of Work\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.approxHours ? project.approxHours : '- -'}}\n                    </div>\n                    <div>\n                      Deliverables\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.deliverablesDescription ? project.deliverablesDescription : '- -'}}\n                    </div>\n                    <div>\n                      Delivery Schedule\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.deliverySchedule ? project.deliverySchedule : '- -'}}\n                    </div>\n                    <div>\n                      Total Fee\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.totalFees ? project.totalFees : '- -'}}\n                    </div>\n                    <div>\n                      Payment Schedule\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.paymentSchedule ? project.paymentSchedule : '- -'}}\n                    </div>\n                    <h2>CONTENT</h2>\n                    <div>\n                      Ownership of Deliverables\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.ownershipOfDeliverables ? project.ownershipOfDeliverables : '- -'}}\n                    </div>\n                    <div>\n                      Rights Licensed by Contractor (if any)\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.rightsLicensedByContractor ? project.rightsLicensedByContractor : '- -'}}\n                    </div>\n                    <div>\n                        Intentended Use by Participant\n                      </div>\n                      <div class=\"text-dark\">\n                        {{project.intendedUseByParticipant ? project.intendedUseByParticipant : '- -'}}\n                      </div>\n                    <div>\n                      Intentended Use by Contractor\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.intendedUseByContractor ? project.intendedUseByContractor : '- -'}}\n                    </div>\n                    <div>\n                      Third Party Assets to be Included\n                    </div>\n                    <div class=\"text-dark\">\n                      {{project.thirdPartyAssets == true ? 'Yes' : project.thirdPartyAssets == false ? 'No' : '- -'}}\n                    </div>\n                    <div>Attachments</div>\n                    <div *ngFor=\"let p of project.attachment\" class=\"text-dark mb-0\">\n                      {{p.file}}\n                    </div>\n                  </div>\n                </div>\n              </td>\n            </tr>\n          </tbody>\n          <tfoot>\n            <tr>\n              <td>\n                <!--place holder for the fixed-position footer-->\n                <div class=\"page-footer-space\"></div>\n              </td>\n            </tr>\n          </tfoot>\n        </table>\n      </section>\n    </div>\n  </div>\n</div>\n\n<!-- \n\n<div *ngIf=\"false\" class=\"\">\n    <section class=\"pl-3 pl-sm-3 pl-3 pl-md-5 pl-lg-5\">\n      <div class=\"padding-details-lg\">\n        <div *ngIf=\"project.contractorCompanyName && project.catalog && project.from\"\n          class=\"main-heading font-weight-300\">\n          Project Memo for {{project.contractorCompanyName? project.contractorCompanyName : '[vender name]'}}\n          on {{project.catalog ? project.catalog : '[project catalog]'}}\n          from {{project.from ? project.from : '[user name]'}}\n        </div>\n        <div *ngIf=\"!(project.contractorCompanyName && project.catalog && project.from)\"\n          class=\"main-heading font-weight-300\">\n          Project Memo {{projectId}}\n        </div>\n        <div class=\"light-black font-14 text-justify\">{{projectId}}</div>\n      </div>\n      <section>\n        <div class=\"pb-1\">\n          <div class=\"heading-5 padding-details-md font-weight-500\">\n            GENERAL INFORMATION\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Assigned To\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.assignedUser ? project.assignedUser : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              In CC\n            </div>\n            <div *ngIf=\"project.mailList.length == 0\" class=\"font-14 text-justify\">\n              - -\n            </div>\n            <div *ngIf=\"project.mailList.length > 0\" class=\"font-14 text-justify\">\n              <span *ngFor=\"let p of project.mailList;let i = index\"> {{p.email}}<span\n                  *ngIf=\"i < (project.mailList.length - 1)\">,</span></span>\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Project Name\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.projectName? project.projectName :'- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Catalog\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.catalog ? project.catalog : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Effective Dates\n            </div>\n            <div class=\"font-14 text-justify\">\n              From {{ project.effectiveDate ? (project.effectiveDate | date : 'MM/dd/yy') : ' - - ' }}\n              to {{ project.expirationDate ? (project.expirationDate | date : 'MM/dd/yy') : ' - - ' }}\n            </div>\n          </div>\n        </div>\n      </section>\n      <section>\n        <div class=\"pb-1\">\n          <div class=\"heading-5 padding-details-md font-weight-500\">\n            CONTRACTOR\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Contractor Name\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.contractorCompanyName ? project.contractorCompanyName : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Primary Contact Name\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.contractorContactName ? project.contractorContactName : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Primary Contact Email\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.contractorContactEmail ? project.contractorContactEmail : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Primary Contact Phone\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.contractorContactPhone ? project.contractorContactPhone : '- -' }}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Will Contractor Work Onsite?\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.contractorWorkOnsite == true ? 'Yes' : project.contractorWorkOnsite == false ? 'No' : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Contractor Insurance\n            </div>\n            <div class=\"font-14 text-justify\">\n              <span *ngIf=\"project.contractorInsuranceGeneral\">\n                General Liability,\n              </span>\n              <span *ngIf=\"project.contractorInsuranceEnO\">\n                E & O,\n              </span>\n              <span *ngIf=\"project.contractorInsuranceProduction\">\n                Worker's Comp,\n              </span>\n              <span *ngIf=\"project.contractorInsuranceWorkersComp\">\n                Production Insurance,\n              </span>\n            </div>\n          </div>\n        </div>\n      </section>\n      <section>\n        <div class=\"pb-1\">\n          <div class=\"heading-5 padding-details-md font-weight-500\">\n            SERVICES / DELIVERABLES / PAYMENT\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Contractor's Service\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.contractorServices ? project.contractorServices : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Approximate Hours of Work\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.approxHours ? project.approxHours : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Deliverables\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.deliverablesDescription ? project.deliverablesDescription : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Delivery Schedule\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.deliverySchedule ? project.deliverySchedule : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Total Fee\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.totalFees ? project.totalFees : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Payment Schedule\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.paymentSchedule ? project.paymentSchedule : '- -'}}\n            </div>\n          </div>\n        </div>\n      </section>\n      <section>\n        <div class=\"pb-1\">\n          <div class=\"heading-5 padding-details-md font-weight-500\">\n            CONTENT\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Ownership of Deliverables\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.ownershipOfDeliverables ? project.ownershipOfDeliverables : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Rights Licensed by Contractor (if any)\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.rightsLicensedByContractor ? project.rightsLicensedByContractor : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Intentended Use by Contractor\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.intendedUseByContractor ? project.intendedUseByContractor : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Intentended Use by Participant\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.intendedUseByParticipant ? project.intendedUseByParticipant : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Third Party Assets to be Included\n            </div>\n            <div class=\"font-14 text-justify\">\n              {{project.thirdPartyAssets == true ? 'Yes' : project.thirdPartyAssets == false ? 'No' : '- -'}}\n            </div>\n          </div>\n          <div class=\"padding-details-sm\">\n            <div class=\"light-black font-15\">\n              Attachments\n            </div>\n            <div *ngFor=\"let p of project.attachment\" class=\"font-14 text-justify\">\n              {{p.file}}\n            </div>\n          </div>\n        </div>\n      </section>\n    </section>\n  </div> -->"

/***/ }),

/***/ "./src/app/dashboard/print-memo/print-memo.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/print-memo/print-memo.component.ts ***!
  \**************************************************************/
/*! exports provided: PrintMemoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintMemoComponent", function() { return PrintMemoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_models/projects.model */ "./src/app/_models/projects.model.ts");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");




var PrintMemoComponent = /** @class */ (function () {
    function PrintMemoComponent() {
        this.downLoadFiles = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        //codeSelected: {};
        this.today = new Date();
    }
    PrintMemoComponent.prototype.ngOnInit = function () {
        // this.downLoadFiles = new EventEmitter<string>();
    };
    PrintMemoComponent.prototype.ngAfterViewInit = function () {
        // debugger
        // setTimeout(()=>{
        //   var data = this.countryCodeOptions.find(ele=>ele.label === this.project.contractorCountryCode);
        // debugger
        //   if(data){
        //     this.codeSelected={
        //       flag: data.flag,
        //       code: data.value
        //     }
        //   }else{
        //     this.codeSelected={
        //       flag: this.countryCodeOptions[241].flag,
        //       code: this.countryCodeOptions[241].value
        //     }
        //   }
        // },1000)
    };
    PrintMemoComponent.prototype.print = function () {
        src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__["UserAuthHelper"].PrintPage();
    };
    PrintMemoComponent.prototype.download = function () {
        this.downLoadFiles.emit("nae");
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_2__["ProjectFormModel"])
    ], PrintMemoComponent.prototype, "project", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], PrintMemoComponent.prototype, "projectId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PrintMemoComponent.prototype, "codeSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PrintMemoComponent.prototype, "downLoadFiles", void 0);
    PrintMemoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-print-memo",
            template: __webpack_require__(/*! ./print-memo.component.html */ "./src/app/dashboard/print-memo/print-memo.component.html"),
            styles: [__webpack_require__(/*! ./print-memo.component.css */ "./src/app/dashboard/print-memo/print-memo.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PrintMemoComponent);
    return PrintMemoComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/project-ad-print/project-ad-print.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/dashboard/project-ad-print/project-ad-print.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9wcm9qZWN0LWFkLXByaW50L3Byb2plY3QtYWQtcHJpbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/project-ad-print/project-ad-print.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/dashboard/project-ad-print/project-ad-print.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-header\">\n    Addendum {{addendumId}}\n    <div class=\"float-right\">Status: {{ addendum.adStatus == 'Submitted' && !(userRole == 'Standard') ? 'Needs Pickup' :  addendum.adStatus }}</div>\n  </div>\n  <div class=\"page-footer\">\n    Exported on: {{ today | date : 'MM/dd/yyyy' }}\n    <div class=\"float-right\">\n      <span id=\"pageFooter\" class=\"page-counter\">\n      </span></div>\n  </div>\n  <table #table id=\"tableContent\" (domChange)=\"alert('asas')\">\n    <thead>\n      <tr>\n        <td>\n          <!--place holder for the fixed-position header-->\n          <h6 class=\"page-header-space\"></h6>\n        </td>\n      </tr>\n    </thead>\n    <tbody>\n      <tr>\n        <td>\n          <div class=\"page\" style=\"line-height: 1.5;\">\n            <section *ngIf=\"compLoaded\" id=\"dvContents\" class=\"general-form px-4\">\n                <div class=\"\">\n                    <section class=\"pl-3 pl-sm-3 pl-3 pl-md-5 pl-lg-5\">\n                      <div class=\"padding-details-lg\">\n                        <div *ngIf=\"addendum.contractorCompanyName && addendum.catalog && addendum.from\"\n                          class=\"main-heading font-weight-300\">\n                          Addendum for {{addendum.contractorCompanyName}} on {{addendum.catalog}} from {{addendum.from}}\n                        </div>\n                        <div *ngIf=\"!(addendum.contractorCompanyName && addendum.catalog && addendum.from)\"\n                          class=\"main-heading font-weight-300\">\n                          Addendum {{addendum.adID}}\n                        </div>\n                        <div class=\"light-black font-14\">{{addendum.adID}}</div>\n                      </div>\n                      <section>\n                        <div class=\"pb-1\">\n                          <div class=\"heading-5 padding-details-md font-weight-500\">\n                            GENERAL INFORMATION\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Assigned To\n                            </div>\n                            <div class=\"font-14\">\n                              {{addendum.assignedUser ? addendum.assignedUser :  '_ _'  }}\n                            </div>\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              In CC\n                            </div>\n                            <div *ngIf=\"!(addendum.mailList && addendum.mailList.length > 0)\" class=\"font-14\">\n                              _ _\n                            </div>\n                            <div *ngIf=\"addendum.mailList && addendum.mailList.length > 0\" class=\"font-14\">\n                              <span *ngFor=\"let p of addendum.mailList;let i = index\"> {{p.email}}<span\n                                  *ngIf=\"i < (addendum.mailList.length - 1)\">,</span></span>\n                            </div>\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Project Memo\n                            </div>\n                            <div class=\"font-14\">\n                              {{ addendum.contractorCompanyName }}\n                              on {{ addendum.catalog }}\n                              from {{ addendum.from }}\n                              (#{{ addendum.parentProjectMemo }})\n                              {{ addendum.projectCreatedAt | date : 'MM/dd/yy'}}\n                            </div>\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Counterparties to Original Agreement\n                            </div>\n                            <div class=\"font-14\">\n                              {{addendum.counterparties ? addendum.counterparties : '_ _'}}\n                            </div>\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Responsible Party\n                            </div>\n                            <div class=\"font-14\">\n                              {{addendum.responsibleParty ? addendum.responsibleParty : '_ _'}}\n                            </div>\n                          </div>\n                        </div>\n                      </section>\n                      <section>\n                        <div class=\"pb-1\">\n                          <div class=\"heading-5 padding-details-md font-weight-500\">\n                            Changes to Existing Agreement and/or Additional Terms\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Description of Changes and/or Additional Terms\n                            </div>\n                            <div class=\"font-14\">\n                              {{addendum.changes ? addendum.changes : '_ _'}}\n                            </div>\n                          </div>\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Reason(s) for Changes\n                            </div>\n                            <div class=\"font-14\">\n                              {{ addendum.reasons ? addendum.reasons : '_ _' }}\n                            </div>\n                          </div>\n                        </div>\n                      </section>\n                      <section>\n                        <div class=\"pb-1\">\n                          <div class=\"padding-details-sm\">\n                            <div class=\"light-black font-15\">\n                              Attachments\n                            </div>\n                            <div *ngFor=\"let p of addendum.attachment\" class=\"font-14\">\n                              {{p.file}}\n                            </div>\n                          </div>\n                        </div>\n                      </section>\n                    </section>\n                  </div>\n            </section>\n          </div>\n        </td>\n      </tr>\n    </tbody>\n  \n    <tfoot>\n      <tr>\n        <td>\n          <!--place holder for the fixed-position footer-->\n          <div class=\"page-footer-space\"></div>\n        </td>\n      </tr>\n    </tfoot>\n  </table>"

/***/ }),

/***/ "./src/app/dashboard/project-ad-print/project-ad-print.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/dashboard/project-ad-print/project-ad-print.component.ts ***!
  \**************************************************************************/
/*! exports provided: ProjectAdPrintComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectAdPrintComponent", function() { return ProjectAdPrintComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_projects_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../_models/projects.model */ "./src/app/_models/projects.model.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var rxjs_observable_timer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/observable/timer */ "./node_modules/rxjs-compat/_esm5/observable/timer.js");








var ProjectAdPrintComponent = /** @class */ (function () {
    function ProjectAdPrintComponent(activatedRoute, service, loader) {
        this.activatedRoute = activatedRoute;
        this.service = service;
        this.loader = loader;
        this.addendum = new _models_projects_model__WEBPACK_IMPORTED_MODULE_1__["AddendumFormModel"]();
        this.timer = Object(rxjs_observable_timer__WEBPACK_IMPORTED_MODULE_7__["timer"])(1000); // wait one second before calling the method
    }
    ProjectAdPrintComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loader.show();
        this.userRole = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_5__["UserAuthHelper"].GetProfile().type;
        this.activatedRoute.params.subscribe(function (res) {
            _this.addendumId = res.id;
            _this.loadDetails();
        });
    };
    ProjectAdPrintComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    ProjectAdPrintComponent.prototype.loadDetails = function () {
        var _this = this;
        this.compLoaded = true;
        this.service.loadAddendumDetails(this.addendumId).subscribe(function (res) {
            _this.addendum = res.data;
            setTimeout(function () {
                window.print();
                _this.loader.hide();
            }, 1000);
            // this.loader.hide();
            // this.subscription = this.timer.subscribe(() => {
            //   this.triggerTableWatcher();
            // });
        }, function (error) {
            _this.loader.hide();
            window.close();
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("table"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProjectAdPrintComponent.prototype, "table", void 0);
    ProjectAdPrintComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-project-ad-print",
            template: __webpack_require__(/*! ./project-ad-print.component.html */ "./src/app/dashboard/project-ad-print/project-ad-print.component.html"),
            styles: [__webpack_require__(/*! ./project-ad-print.component.css */ "./src/app/dashboard/project-ad-print/project-ad-print.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _service_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]])
    ], ProjectAdPrintComponent);
    return ProjectAdPrintComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/project-form/project-form.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/dashboard/project-form/project-form.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-select-arrow-wrapper{\n    position: absolute !important;\n    top: -29px !important;\n    left: 70px !important;\n}\n.mat-form-field-infix span {\n    display: none!important;\n}\n.item\n{\n    display:flex;\n    justify-content: flex-start;\n    align-items: center;\n  \n}\n.item-section\n{\n    display:flex;\n    flex-direction: column;\n}\n.mat-select-panel .mat-optgroup-label, .mat-select-panel .mat-option span {\n        margin-left: 31px;\n    width: 200px!important\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3Byb2plY3QtZm9ybS9wcm9qZWN0LWZvcm0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDZCQUE2QjtJQUM3QixxQkFBcUI7SUFDckIscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSx1QkFBdUI7QUFDM0I7QUFDQTs7SUFFSSxZQUFZO0lBQ1osMkJBQTJCO0lBQzNCLG1CQUFtQjs7QUFFdkI7QUFDQTs7SUFFSSxZQUFZO0lBQ1osc0JBQXNCO0FBQzFCO0FBQ0E7UUFDUSxpQkFBaUI7SUFDckI7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9wcm9qZWN0LWZvcm0vcHJvamVjdC1mb3JtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LXNlbGVjdC1hcnJvdy13cmFwcGVye1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICAgIHRvcDogLTI5cHggIWltcG9ydGFudDtcbiAgICBsZWZ0OiA3MHB4ICFpbXBvcnRhbnQ7XG59XG4ubWF0LWZvcm0tZmllbGQtaW5maXggc3BhbiB7XG4gICAgZGlzcGxheTogbm9uZSFpbXBvcnRhbnQ7XG59XG4uaXRlbVxue1xuICAgIGRpc3BsYXk6ZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgXG59XG4uaXRlbS1zZWN0aW9uXG57XG4gICAgZGlzcGxheTpmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG4ubWF0LXNlbGVjdC1wYW5lbCAubWF0LW9wdGdyb3VwLWxhYmVsLCAubWF0LXNlbGVjdC1wYW5lbCAubWF0LW9wdGlvbiBzcGFuIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDMxcHg7XG4gICAgd2lkdGg6IDIwMHB4IWltcG9ydGFudFxufVxuIl19 */"

/***/ }),

/***/ "./src/app/dashboard/project-form/project-form.component.html":
/*!********************************************************************!*\
  !*** ./src/app/dashboard/project-form/project-form.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-own\" #f=\"ngForm\"  novalidate>\n  <fieldset [disabled]=\"formDisabled\">\n    <section class=\"dashboard-section\">\n      <nav class=\"navbar-bottam\">\n        <div class=\"container-own\">\n          <div class=\"row justify-content-between align-items-center\">\n            <div class=\"col-12 col-md\">\n              <p class=\"light-gray\">Status: <span  [class.light-blue]=\"userRole=='Standard'\">\n                  {{project.pmStatus == 'Submitted' && !(userRole == 'Standard') ? 'Needs Pickup' :  project.pmStatus }}</span>\n              </p>\n            </div>\n            <div class=\"print-memo-submit col-auto\">\n              <div class=\"light-gray print-memo-change\">\n                <div *ngIf=\"savingProgress\" class=\"px-1\">Saving...</div>\n                <div *ngIf=\"!(isFormDirty || isUploadIncomplete) && !savingProgress\" class=\"px-1\">All Changes Saved\n                </div>\n                <div *ngIf=\"!savingProgress && (isFormDirty || isUploadIncomplete)\" class=\"px-1\">Saving</div>\n                <div (click)=\"print()\" class=\"px-3\">\n                  <img src=\"../assets/images/print-icon.png\" class=\"cursor print-img\">\n                </div>\n                <div *ngIf=\"userRole == 'Standard'\" class=\"pl-1\">\n                  <button *ngIf=\"project.pmStatus == 'Draft'\" type=\"button\" (click)=\"(f.submitted = true); submit()\"\n                    mat-raised-button color=\"primary\" class=\"bg-color print-submit-btn\">Submit</button>\n                  <button disabled *ngIf=\"project.pmStatus == 'Submitted' || project.pmStatus == 'Assigned'\"\n                    type=\"button\" mat-raised-button color=\"primary\" disabled\n                    class=\"bg-color print-submit-btn\">Submitted</button>\n                  <span (click)=\"addAddendum()\">\n                    <button *ngIf=\"project.pmStatus == 'Accepted'\" type=\"button\" mat-raised-button color=\"primary\"\n                      class=\"bg-color print-submit-btn\">Addendum</button>\n                  </span>\n                </div>\n                <div *ngIf=\"userRole !== 'Standard'\" class=\"pl-1\">\n                  <button *ngIf=\"project.pmStatus == 'Draft'\" type=\"button\" mat-raised-button color=\"primary\"\n                    class=\"bg-color print-submit-btn\" disabled>Submit</button>\n                  <button *ngIf=\"project.pmStatus == 'Submitted' || project.pmStatus == 'Assigned'\" type=\"button\"\n                    mat-raised-button color=\"primary\" class=\"bg-color print-submit-btn\"\n                    (click)=\"(f.submitted = true); submit()\">Submit</button>\n                  <button *ngIf=\"project.pmStatus == 'Accepted'\" type=\"button\" mat-raised-button color=\"primary\"\n                    class=\"bg-color print-submit-btn\" disabled>Accepted</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </nav>\n    </section>\n    <!-- Form -->\n    <div id=\"mydashboard\" class=\"dashboard-section\">\n      <div class=\"container-own own-dashboard-section\">\n        <section class=\"general-form \">\n          <div class=\"d-flex justify-content-between align-items-center\">\n            <h5 class=\"heading-5 font-weight-500\">GENERAL INFORMATION</h5>\n            <h5 class=\"heading-5 font-weight-500\">{{projectId}}</h5>\n        \n          </div>\n          <div class=\"pt-3\">\n            <!-- Assigned To -->\n            <div class=\"form-group\" [ngClass]=\"{\n                          'active': id === 1,\n                          'error' : userRole !== 'Standard' && (f.submitted || assignTo.touched) && !assignTo.valid,\n                          'disabled' : formDisabled || userRole === 'Standard' || (userRole !== 'Standard' && project.pmStatus == 'Draft')\n                         }\" id=\"1\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"sign-input-2\" floatLabel=\"always\">\n                    <mat-label>Assigned To</mat-label>\n                    <input type=\"text\" placeholder=\"\" autocomplete=\"on\" (focus)=\"focus(1)\" (blur)=\"blur()\"\n                      name=\"contractorCompanyName\"\n                      [disabled]=\"formDisabled || userRole === 'Standard' || (userRole !== 'Standard' && project.pmStatus == 'Draft')\"\n                      (focus)=\"focus(1)\" (blur)=\"blur()\" [(ngModel)]=\"project.assignedUser\" name=\"assignedTo\"\n                      #assignTo=\"ngModel\" (keyup)=\"FilterAssignTo()\" (change)=\"ValidateAssignTo()\" matInput\n                      [matAutocomplete]=\"autoassign\" required>\n                    <mat-autocomplete #autoassign=\"matAutocomplete\" [displayWith]=\"displayFn\"\n                      (optionSelected)='selectionChanged($event.option)'>\n                      <mat-option *ngFor=\"let option of Users\" [value]=\"option.uniqueid\">\n                        {{option.firstName + ' ' + option.lastName}}\n                      </mat-option>\n                    </mat-autocomplete>\n                    <mat-error>\n                      *You must assign a Business Affairs member before sending this to Rightsline\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 1 }\" id=\"1\">\n                    <div>Once the project memo has been approved and submitted,\n                      this will signify which Business Affairs team member has been assigned to the project memo.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <!-- Send Copies To -->\n            <div class=\"form-group\"\n              [ngClass]=\"{'active': id === 2,'error' : (f.submitted || cc.touched) && (!cc.valid || ccIsInvalid) }\"\n              id=\"2\">\n              <div class=\"row align-items-start\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (click)=\"focus(2, cc);\">\n                  <mat-form-field\n                    class=\"example-full-width sign-input-2 sign-input-3 wrapper-padding multiple-email-own\"\n                    floatLabel=\"always\">\n                    <div class=\"attachment-section-1\">\n                      <ul class=\"d-list-item attachment-items-list list-unstyled\">\n                        <li *ngFor=\"let p of CCList; let ind = index\">\n                          <div class=\"attachment-item\">\n                            <p>{{p.name ? p.name : p.email}}\n                              <span *ngIf=\"p.name\">{{ '('+ p.email + ')'}}</span>\n                            </p>\n                            <img (click)=\"removeFromCCList(ind)\" src=\"../assets/images/close-icon.png\"\n                              class=\"close-icon\">\n                          </div>\n                        </li>\n                        <li class=\"simple-input\">\n                          <!-- <input matInput > -->\n                          <div>\n                            <mat-label class=\"label-own mat-form-field-label\">Send Copies To</mat-label>\n                            <!-- onkeypress=\"this.style.width = ((this.value.length + 1) * 10) + 'px';\" style=\"width: 10px;\"  -->\n                            <input id=\"ccinput\" type=\"text\" matInput [matAutocomplete]=\"auto\" [(ngModel)]=\"project.cc\"\n                              [ngClass]=\"{'d-none': !project.cc && id !== 2}\" autocomplete=\"on\"\n                              (keyup)=\"(project.cc && project.cc.length > 1 && searchPeopleEmail()); (($event.code === 'Enter' || $event.code === 'Comma') && project.cc && addEmailToList())\"\n                              name=\"cc\" #cc=\"ngModel\" (focus)=\"focus(2) && SaveProgress()\"\n                              (blur)=\"blur(); sendCopiesBlur();\" email>\n                          </div>\n                          <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)='getPosts($event.option)'>\n                            <mat-option class=\"multi-email-option\" *ngFor=\"let option of CCSuggestions\"\n                              [value]=\"option.email\">\n                              <strong style=\"font-size: 14px;\">\n                                {{option.name}}\n                              </strong>\n                              <div style=\"font-size: 12px;opacity: 0.8;\">\n                                {{option.email}}\n                              </div>\n                            </mat-option>\n                          </mat-autocomplete>\n                        </li>\n                      </ul>\n                    </div>\n                    <mat-error *ngIf=\"!ccIsInvalid && !cc.valid\">\n                      Please enter a valid email address\n                    </mat-error>\n                  </mat-form-field>\n                  <span class=\"mat-error outer-error\" *ngIf=\"ccIsInvalid\">\n                    *One or more provided values is not a valid email address\n                  </span>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 2 }\" id=\"2\">\n                    <div>Make sure to CC others who need to be in the know:</div>\n                    <ul class=\"tooltip-items list-unstyled\">\n                      <li>Department heads</li>\n                      <li>Other relevant personnel</li>\n                    </ul>\n                    <!-- <div class=\"pb-3\">Enter their email addresses here.</div> -->\n                    <div><span>Tip</span> : <i>For Participant Media personnel you can\n                        also start typing their name and select from existing matches.</i> </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <!-- Project Name -->\n            <div class=\"form-group\"\n              [ngClass]=\"{'active': id === 3, 'error' : (f.submitted || projectName.touched) && !projectName.valid }\"\n              id=\"3\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      [(ngModel)]=\"project.projectName\" name=\"projectName\" #projectName=\"ngModel\" required\n                      placeholder=\"Project Name *\" maxlength=\"10000\" (focus)=\"focus(3)\" (blur)=\"blur()\"\n                      (keyup)=\"SaveProgress()\"></textarea>\n                    <mat-error>\n                      *Please tell us the project's name\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.projectName) ? project.projectName.length : 0) | number }} /\n                      {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 3 }\" id=\"3\">\n                    <div>Enter a description of what initiative / campaign /\n                      budget this project memo connects to and will be funded by. </div>\n                    <div><span>e.g.</span> : <div>\"Social Impact – The Price of Free\" or \"Awards Budget\"</div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\"\n              [ngClass]=\"{'active': id === 4,'error' : (f.submitted || catalog.touched) && !catalog.valid}\" id=\"4\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"sign-input-2\" floatLabel=\"always\">\n                    <mat-label>Catalog *</mat-label>\n                    <input type=\"text\" placeholder=\"\" (change)=\"ValidateCatalog($event.target.value)\"\n                      (keyup)=\"FilterCatalogList($event.target.value)\" name=\"catalog\" (focus)=\"focus(4)\" (blur)=\"blur()\"\n                      [(ngModel)]=\"project.catalog\" #catalog=\"ngModel\" matInput [matAutocomplete]=\"autoa2\" required>\n                    <mat-autocomplete #autoa2=\"matAutocomplete\" [displayWith]=\"displayFn\"\n                      (optionSelected)='setCatalog($event) && SaveProgress()'>\n                      <mat-option *ngFor=\"let cat of CatalogList\" [value]=\"cat.catalogName\">\n                        {{cat.catalogName}}\n                      </mat-option>\n                    </mat-autocomplete>\n                    <mat-error>\n                      *Please select a Catalog assignment, if you don’t see what you are looking for contact Business\n                      Affairs.\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 4 }\" id=\"4\">\n                    <div>This field relates to intellectual property. <br>\n                      If this project is related to a Participant film or a specific piece of content, enter the name\n                      here. <br>\n                      If not related to content, enter \"NON-SPECIFIC PROJECT\".\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" [ngClass]=\"{\n                          'active': id === 5,\n                          'error' : (f.submitted || pickerEnd.touched || pickerStart.touched) && (!project.effectiveDate || !project.expirationDate),\n                          'disabled' : formDisabled}\" id=\"5\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseleave)=\"blur()\">\n                  <div class=\"date-filter\">\n                    <div class=\"filter-items \">\n                      <span class=\"filter-p\">\n                        Effective Dates *\n                      </span>\n                      <div class=\"date-picker-wrapper\">\n                        <div class=\"date-selection right-arrow col-6 d-flex justify-content-center\">\n                          <div class=\"cursor\" (click)=\"pickerStart.open()\">\n                            <img src=\"assets/images/cal-icon.png\" class='calender-icon'>\n                          </div>\n                          <mat-form-field class=\"example-full-width cursor\" floatLabel=\"never\">\n                            <input class=\"cursor\" matInput (dateChange)=\"SaveProgress()\" readonly\n                              [max]=\"project.expirationDate\" [ngClass]=\"{ 'input-border' : formDisabled }\"\n                              [value]=\"project.effectiveDate\" [(ngModel)]=\"project.effectiveDate\"\n                              (click)=\"pickerStart.open()\" name=\"effectiveDate\" #effectiveDate=\"ngModel\"\n                              (focus)=\"pickerStart.open(); focus(5)\" [matDatepicker]=\"pickerStart\" placeholder=\"\"\n                              required>\n                            <mat-datepicker #pickerStart></mat-datepicker>\n                          </mat-form-field>\n                          <div class=\"material-icon-section\">\n                            <mat-icon class=\"cursor\" aria-hidden=\"false\" aria-label=\"Example home icon\"\n                              (click)=\"addDate('effectiveDate', -1)\">keyboard_arrow_left</mat-icon>\n                            <mat-icon class=\"cursor\" aria-hidden=\"false\" aria-label=\"Example home icon\"\n                              (click)=\"addDate('effectiveDate', 1)\">keyboard_arrow_right</mat-icon>\n                          </div>\n                        </div>\n                        <div class=\"cursor date-selection col-6 d-flex justify-content-center align-items-end\">\n                          <mat-form-field class=\"example-full-width\" floatLabel=\"never\">\n                            <input class=\"cursor\" matInput (dateChange)=\"SaveProgress()\" readonly\n                              [min]=\"project.effectiveDate\" [ngClass]=\"{ 'input-border' : formDisabled }\"\n                              [(ngModel)]=\"project.expirationDate\" (click)=\"pickerEnd.open()\"\n                              [value]=\"project.expirationDate\" name=\"expirationDate\" #expirationDate=\"ngModel\"\n                              (focus)=\"focus(5) && pickerEnd.open()\" [matDatepicker]=\"pickerEnd\" placeholder=\"\"\n                              required>\n                            <mat-datepicker #pickerEnd></mat-datepicker>\n                          </mat-form-field>\n                          <div class=\"material-icon-section\">\n                            <mat-icon class=\"cursor\" aria-hidden=\"false\" aria-label=\"Example home icon\"\n                              (click)=\"addDate('expirationDate', -1)\">keyboard_arrow_left</mat-icon>\n                            <mat-icon class=\"cursor\" aria-hidden=\"false\" aria-label=\"Example home icon\"\n                              (click)=\"addDate('expirationDate', 1)\">keyboard_arrow_right</mat-icon>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <mat-error\n                      *ngIf=\"(f.submitted || pickerEnd.touched || pickerStart.touched)  && (!project.effectiveDate || !project.expirationDate)\">\n                      *Please provide expected effective and expiration dates\n                    </mat-error>\n                  </div>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 5 }\" id=\"5\">\n                    <div>Be sure to enter both the anticipated start date and the end dates here.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </section>\n        <section class=\"general-form mt-2\">\n          <div class=\"d-flex justify-content-between align-items-center\">\n            <h5 class=\"pt-3 heading-5\">CONTRACTOR</h5>\n          </div>\n          <div class=\"pt-3\">\n          \n            <div class=\"form-group\"\n              [ngClass]=\"{'active': id === 6,'error' : (f.submitted || contractorCompanyName.touched) && !contractorCompanyName.valid}\" id=\"6\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2 input-icon\" floatLabel=\"always\">\n                      <!-- <mat-label>Catalog *</mat-label> -->\n                    <input autocomplete=\"off\" type=\"text\" (change)=\"validateCompanyName()\" placeholder=\"Contractor Name*\"\n                      name=\"contractorCompanyName\" (focus)=\"focus(6)\" (blur)=\"blur()\"\n                      [(ngModel)]=\"project.contractorCompanyName\" #contractorCompanyName=\"ngModel\"\n                      (keyup)=\"FilterCompany($event.target.value)\" matInput [matAutocomplete]=\"autoa\" required>\n                    <mat-autocomplete #autoa=\"matAutocomplete\" [displayWith]=\"displayFn\"\n                      (optionSelected)='compantSelected($event.option)'>\n                      <mat-option *ngFor=\"let option of ContractorsListOptions\" [value]=\"option.contactName\">\n                        {{option.contactName}}\n                      </mat-option>\n                    </mat-autocomplete>\n                    <mat-error>\n                      *Please identify your contractor\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 6 }\" id=\"6\">\n                    <div>Contractors listed here are those available in the Rightsline system. <br>\n                      If you don’t see the name you are looking for then, please contact Business Affairs.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"7\"\n              [ngClass]=\"{'active': id === 7, 'error' : (f.submitted || contractorContactName.touched) && !contractorContactName.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <input matInput autocomplete=\"on\" (keyup)=\"SaveProgress()\" placeholder=\"Primary Contact Name *\"\n                      [(ngModel)]=\"project.contractorContactName\" (focus)=\"focus(7)\" (blur)=\"blur()\" name=\"name\"\n                      #contractorContactName=\"ngModel\" maxlength=\"1000\" required>\n                    <mat-error>\n                      *Please identify your primary contact with this contractor for this Project Memo\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.contractorContactName) ? project.contractorContactName.length : 0) | number}}\n                      / {{ 1000 | number}}\n                    </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 7 }\" id=\"7\">\n                    <div>Enter the name of the contact person for the contractor.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"8\"\n              [ngClass]=\"{'active': id === 8, 'error' : (f.submitted || contractorContactEmail.touched) && !contractorContactEmail.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <input autocomplete=\"on\" matInput (keyup)=\"SaveProgress()\" placeholder=\"Primary Contact Email *\"\n                      [(ngModel)]=\"project.contractorContactEmail\" (focus)=\"focus(8)\" (blur)=\"blur()\"\n                      name=\"contractorContactEmail\" #contractorContactEmail=\"ngModel\" required email>\n                    <mat-error>\n                      *Please provide the email address for this contractor\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 8 }\" id=\"8\">\n                    <div>List complete e-mail address here.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group phone-valid\"\n              [ngClass]=\"{'disabled' : formDisabled,'active': id === 9, 'error' : (f.submitted || contractorContactPhone.touched) && !contractorContactPhone.valid }\"\n              id=\"9\">\n              <div class=\"row\">\n                <div  style=\"position:relative\" class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                    <div class=\"col-sm-3\">\n                     \n                        \n                      </div>\n                    <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                      <div class=\"item-section\">\n                         \n                            <div class=\"col-sm-9 item\" style=\"position:relative\">\n                                <input style=\"position:absolute; left:70px; top:1px; width:150px;\" matInput autocomplete=\"on\" (keyup)=\"SaveProgress()\" placeholder=\"Primary Contact Phone *\"\n                                onkeypress=\"return isNumberKey(event)\" [ngModel]=\"project.contractorContactPhone|tel:'hh': codeSelected.code\" (ngModelChange)=\"project.contractorContactPhone=$event\"\n                                (focus)=\"focus(9)\" (blur)=\"blur()\" name=\"contractorContactPhone\" #contractorContactPhone=\"ngModel\"\n                                minlength=\"8\" maxlength=\"15\" required >\n                              \n                            </div>\n                            <div class=\"item\">\n                                <div class=\"firstitem\">\n                                    <img [src]=\"this.codeSelected.flag\" style=\"height: 17px;\n                                    width: 27px;\"/>\n                                    <!-- {{codeSelected.code}} -->\n                                </div>\n                                 <div class=\"seconditem\">\n                              \n                                    <mat-select [(ngModel)]=\"codeSelected.code\" class=\"own-flag\" (selectionChange)=\"onCountryChange($event.value)\" [ngModelOptions]=\"{standalone: true}\">\n                                        <div style=\"width:100%;top:95px;border:1px solid #ccc;position:relative;\"></div> \n                                      <mat-option [value]=\"list\" *ngFor=\"let list of countryCodeOptions; let i=index\" name=\"sel\" class=\"own-style\">\n                                          <img [src]=\"list.flag\" style=\"width: 28px; margin-right:8px; height: 20px;\"/>{{list.label}}</mat-option>\n                                      \n                                        </mat-select> \n                                 </div>\n                                 \n                              </div>\n                      </div>\n                      <mat-error>\n                          *Please provide the phone number for this contractor\n                        </mat-error>\n                        </mat-form-field>\n                        <p *ngIf=\"!contractorContactPhone.hasError('required') && !contractorContactPhone.hasError('minlength') && ((userRole == 'Standard' &&  project.pmStatus == 'Draft')||(userRole != 'Standard' &&  project.pmStatus != 'Accepted')) && isPhoneValid == 'false'\" style=\"color: #F47B20;\n                        font-size: 11px; margin-top: -16px;\">\n                          <sup>*</sup>Please enter correct phone number\n                        </p>\n                     \n                    \n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 9 }\" id=\"9\">\n                    <div>List complete phone number here.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n\n            <div class=\"form-group pb-5\" id=\"10\"\n              [ngClass]=\"{'disabled' : formDisabled,\n                          'active': id === 10, 'error' : (f.submitted || contractorWorkOnsite.touched) && !contractorWorkOnsite.valid}\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseenter)=\"focus(10)\" (mouseleave)=\"blur()\">\n                  <div class=\"option-radio\">\n                    <p class=\"filter-font\">Will Contractor Work Onsite? *</p>\n                    <mat-radio-group (focus)=\"focus(10)\" (change)=\"SaveProgress()\" aria-label=\"Select an option\"\n                      [(ngModel)]=\"project.contractorWorkOnsite\" name=\"contractorWorkOnsite\"\n                      #contractorWorkOnsite=\"ngModel\" required>\n                      <mat-radio-button (focus)=\"focus(10)\" [value]=\"true\"> Yes </mat-radio-button>\n                      <mat-radio-button (focus)=\"focus(10)\" [value]=\"false\"> No </mat-radio-button>\n                    </mat-radio-group>\n                  </div>\n                  <mat-error *ngIf=\"(f.submitted || contractorWorkOnsite.touched) && !contractorWorkOnsite.valid\">\n                    *Please identify if the contractor will be doing work onsite or not\n                  </mat-error>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 10 }\" id=\"10\">\n                    <div>This is a field that we need to ascertain proper employment classification.\n                      If the Contractor is needed to work onsite at the PM office, please indicate here.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"11\"\n              [ngClass]=\"{'disabled' : formDisabled,\n                          'active': id === 11,'error' : (f.submitted || contractorInsuranceGeneral.touched) && !contractorInsuranceGeneral.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseenter)=\"focus(11)\" (mouseleave)=\"blur()\">\n                  <div class=\"option-radio\">\n                    <p class=\"filter-font\">Contractor Insurance</p>\n                    <ul class=\"list-unstyled\">\n                      <li>\n                        <mat-checkbox (focus)=\"focus(11)\" (click)=\"SaveProgress(); focus(11)\"\n                          [(ngModel)]=\"project.contractorInsuranceGeneral\" name=\"contractorInsuranceGeneral\"\n                          #contractorInsuranceGeneral=\"ngModel\">\n                          GENERAL LIABILITY</mat-checkbox>\n                      </li>\n                      <li>\n                        <mat-checkbox (focus)=\"focus(11)\" (click)=\"SaveProgress(); focus(11)\"\n                          [(ngModel)]=\"project.contractorInsuranceEnO\" name=\"contractorInsuranceEnO\"\n                          #contractorInsuranceEnO=\"ngModel\">\n                          E & O</mat-checkbox>\n                      </li>\n                      <li>\n                        <mat-checkbox (focus)=\"focus(11)\" (click)=\"SaveProgress(); focus(11)\"\n                          [(ngModel)]=\"project.contractorInsuranceWorkersComp\" name=\"contractorInsuranceWorkersComp \"\n                          #contractorInsuranceWorkersComp=\"ngModel\">\n                          WORKER'S COMP</mat-checkbox>\n                      </li>\n                      <li>\n                        <mat-checkbox (focus)=\"focus(11)\" (click)=\"SaveProgress(); focus(11)\"\n                          [(ngModel)]=\"project.contractorInsuranceProduction\" name=\"contractorInsuranceProduction\"\n                          #contractorInsuranceProduction=\"ngModel\">\n                          PRODUCATION INSURANCE</mat-checkbox>\n                      </li>\n                    </ul>\n                  </div>\n\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 11 }\" id=\"11\">\n                    <div>Participant requires that all Contractors carry their own business insurance.\n                      Please indicate that Contractor is fully- insured here.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </section>\n        <section class=\"general-form mt-2\">\n          <div class=\"d-flex justify-content-between align-items-center\">\n            <h5 class=\"pt-3 heading-5\">SERVICES / DELIVERABLES / PAYMENT</h5>\n          </div>\n          <div class=\"pt-3\">\n            <div class=\"form-group\" id=\"12\"\n              [ngClass]=\"{'active': id === 12,'error' : (f.submitted || contractorServices.touched) && !contractorServices.valid}\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" placeholder=\"Contractor's Services *\"\n                      [(ngModel)]=\"project.contractorServices\" (focus)=\"focus(12)\" (blur)=\"blur()\"\n                      name=\"contractorServices\" #contractorServices=\"ngModel\" maxlength=\"10000\" required></textarea>\n                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                    <mat-error>\n                      *Please detail what services the contractor will be providing\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.contractorServices) ? project.contractorServices.length : 0) | number}} /\n                      {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 12 }\" id=\"12\">\n                    <div>Please be as specific as possible—the services should be filling a need/competency that PM\n                      cannot otherwise fulfill internally.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"13\"\n              [ngClass]=\"{'active': id === 13, 'error' : (f.submitted || approxHours.touched) && !approxHours.valid}\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" placeholder=\"Approximate Hours of Work *\"\n                      [(ngModel)]=\"project.approxHours\" (focus)=\"focus(13)\" (blur)=\"blur()\" name=\"approxHours\"\n                      #approxHours=\"ngModel\" required></textarea>\n                    <mat-error>\n                      *Please provide an approximate number of hours the contractor will work\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.approxHours) ? project.approxHours.length : 0)| number}} /\n                      {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 13 }\" id=\"13\">\n                    <div>Provide an estimate of the number of hours that the contractor will work.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"14\"\n              [ngClass]=\"{'active': id === 14, 'error' : (f.submitted || deliverablesDescription.touched) && !deliverablesDescription.valid}\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" placeholder=\"Deliverables *\"\n                      [(ngModel)]=\"project.deliverablesDescription\" (focus)=\"focus(14)\" (blur)=\"blur()\"\n                      name=\"deliverablesDescription\" #deliverablesDescription=\"ngModel\" required></textarea>\n                    <mat-error>\n                      *Please provide a detailed description of the deliverables / content to be provided\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.deliverablesDescription) ? project.deliverablesDescription.length : 0)| number}}\n                      /\n                      {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 14 }\" id=\"14\">\n                    <div>Please be as specific as possible about the deliverables / content that\n                      Contractor is expected to create and deliver as a part of this agreement.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"15\"\n              [ngClass]=\"{'active': id === 15,'error' : (f.submitted || deliverySchedule.touched) && !deliverySchedule.valid}\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" placeholder=\"Delivery Schedule *\" [(ngModel)]=\"project.deliverySchedule\"\n                      (focus)=\"focus(15)\" (blur)=\"blur()\" name=\"deliverySchedule\" #deliverySchedule=\"ngModel\"\n                      required></textarea>\n                    <mat-error>\n                      *Please provide expected delivery dates with as much detail as possible\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.deliverySchedule) ? project.deliverySchedule.length : 0)| number}} /\n                      {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 15 }\" id=\"15\">\n                    <div>\n                      Please be as specific as possible about the expected delivery\n                      dates (including dates for PM feedback and # of rounds).\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\"\n              [ngClass]=\"{'active': id === 16, 'error' : (f.submitted || totalFees.touched) && !totalFees.valid }\"\n              id=\"16\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(16)\" (blur)=\"blur()\" placeholder=\"Total Fee *\"\n                      [(ngModel)]=\"project.totalFees\" name=\"totalFees\" #totalFees=\"ngModel\" required></textarea>\n                    <mat-error>\n                      *Please provide the overall fee that will be paid\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.totalFees) ? project.totalFees.length : 0) | number}} /\n                      {{ 10000 | number}}\n                    </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 16 }\" id=\"16\">\n                    <div>Please enter the overall fee that will be paid in connection with this agreement.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"17\"\n              [ngClass]=\"{'active': id === 17, 'error' : (f.submitted || paymentSchedule.touched) && !paymentSchedule.valid}\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" placeholder=\"Payment Schedule *\" [(ngModel)]=\"project.paymentSchedule\"\n                      (focus)=\"focus(17)\" (blur)=\"blur()\" name=\"paymentSchedule\" #paymentSchedule=\"ngModel\"\n                      required></textarea>\n                    <mat-error>\n                      *Please provide details of the preferred invoice schedule.\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.paymentSchedule) ? project.paymentSchedule.length : 0) | number}} /\n                      {{ 10000 | number}} </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 17 }\" id=\"17\">\n                    <div>Please list the preferred invoice schedule here (and note that Participant’s policy\n                      is to have payments tied to receipt of deliverables whenever possible).\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </section>\n        <section class=\"general-form mt-2\">\n          <div class=\"d-flex justify-content-between align-items-center\">\n            <h5 class=\"heading-5 pt-3\">Content</h5>\n          </div>\n          <div class=\"pt-4\">\n            <div class=\"form-group\" id=\"18\"\n              [ngClass]=\"{'active': id === 18, 'error' : (f.submitted || ownershipOfDeliverables.touched) && !ownershipOfDeliverables.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(18)\" (blur)=\"blur()\" maxlength=\"10000\"\n                      placeholder=\"Ownership of Deliverables\" [(ngModel)]=\"project.ownershipOfDeliverables\"\n                      name=\"ownershipOfDeliverables\" #ownershipOfDeliverables=\"ngModel\"></textarea>\n                    <!-- <mat-hint>Errors appear instantly!</mat-hint> -->\n                    <mat-error>\n                      *Please identify your contractor\n                    </mat-error>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.ownershipOfDeliverables) ? project.ownershipOfDeliverables.length : 0)| number}}\n                      /\n                      {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 18 }\" id=\"18\">\n                    <div>Please indicate if this agreement is intended to be a work made\n                      for hire or if the final product will not be owned by Participant Media.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"19\"\n              [ngClass]=\"{'active': id === 19,'error' : (f.submitted || rightsLicensedByContractor.touched) && !rightsLicensedByContractor.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(19)\" (blur)=\"blur()\"\n                      placeholder=\"Rights Licensed by Contractor (if any)\"\n                      [(ngModel)]=\"project.rightsLicensedByContractor\" name=\"rightsLicensedByContractor\"\n                      #rightsLicensedByContractor=\"ngModel\"></textarea>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.rightsLicensedByContractor) ? project.rightsLicensedByContractor.length : 0) | number}}\n                      / {{ 10000 | number}} </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 19 }\" id=\"19\">\n                    <div>If Contractor is incorporating third-party materials into the deliverables,\n                      please indicate what rights the Contractor is licensing as part of the work product.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" id=\"20\"\n              [ngClass]=\"{'active': id === 20, 'error' : (f.submitted || intendedUseByContractor.touched) && !intendedUseByContractor.valid }\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(20)\" (blur)=\"blur()\"\n                      placeholder=\"Intended Use by Contractor\" [(ngModel)]=\"project.intendedUseByContractor\"\n                      name=\"intendedUseByContractor\" #intendedUseByContractor=\"ngModel\"></textarea>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.intendedUseByContractor) ? project.intendedUseByContractor.length : 0) | number}}\n                      /\n                      {{ 10000 | number}} </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 20 }\" id=\"20\">\n                    <div>Are we giving Contractor permission to\n                      use the deliverables? If so, please indicate how they intend to use it.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" [ngClass]=\"{'active': id === 21 }\" id=\"21\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(21)\" (blur)=\"blur()\"\n                      placeholder=\"Participant’s Intended Use of Deliverables\"\n                      [(ngModel)]=\"project.intendedUseByParticipant\" name=\"intendedUseByParticipant\"\n                      #intendedUseByParticipant=\"ngModel\"></textarea>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.intendedUseByParticipant) ? project.intendedUseByParticipant.length : 0)| number}}\n                      /\n                      {{ 10000 | number}} </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 21 }\" id=\"21\">\n                    <div>Please indicate what the intended use is for the deliverables. <br>\n                    </div>\n                    <div><span>e.g.</span> : <div> is it going on the social impact campaign site?\n                        Is it for internal use only? Will it be displayed publicly at an event? Etc.</div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" [ngClass]=\"{'active': id === 22 }\" id=\"22\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(22)\" (blur)=\"blur()\"\n                      placeholder=\"Assets Provided by Participant\" [(ngModel)]=\"project.assetsProvidedByParticipant\"\n                      name=\"assetsProvidedByParticipant\" #assetsProvidedByParticipant=\"ngModel\"></textarea>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ ((project && project.assetsProvidedByParticipant) ? project.assetsProvidedByParticipant.length : 0) | number}}\n                      / {{ 10000 | number}}</div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 22 }\" id=\"22\">\n                    <div>Please list what the primary assets are that Participant will provide.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" [ngClass]=\"{'active': id === 23 }\" id=\"23\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <textarea cdkTextareaAutosize cdkAutosizeMinRows=\"1\" cdkAutosizeMaxRows=\"100\" matInput\n                      (keyup)=\"SaveProgress()\" (focus)=\"focus(23)\" (blur)=\"blur()\"\n                      placeholder=\"Assets Provided by Contractor\" [(ngModel)]=\"project.assetsProvidedByContractor\"\n                      name=\"assetsProvidedByContractor\" #assetsProvidedByContractor=\"ngModel\"></textarea>\n                    <div class=\"project-memo-count text-color font-12 cursor\">\n                      {{ (project && project.assetsProvidedByContractor) ? project.assetsProvidedByContractor.length : 0 | number }}\n                      / {{ 10000 | number}} </div>\n                  </mat-form-field>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 23 }\" id=\"23\">\n                    <div>Please list any assets that Contractor provides as a part of this deal.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group mb-3\" [ngClass]=\"{'active': id === 24, 'disabled' : formDisabled }\" id=\"24\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseenter)=\"focus(24)\" (mouseleave)=\"blur()\">\n                  <div class=\"option-radio\">\n                    <p class=\"filter-font\">Third Party Assets to be Included</p>\n                    <mat-radio-group (focus)=\"focus(24)\" aria-label=\"Select an option\"\n                      [(ngModel)]=\"project.thirdPartyAssets\" name=\"thirdPartyAssets\" #thirdPartyAssets=\"ngModel\">\n                      <mat-radio-button (focus)=\"focus(24)\" (click)=\"SaveProgress()\" [value]=\"true\"> Yes\n                      </mat-radio-button>\n                      <mat-radio-button (focus)=\"focus(24)\" (click)=\"SaveProgress()\" [value]=\"false\"> No\n                      </mat-radio-button>\n                    </mat-radio-group>\n                  </div>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 24 }\" id=\"24\">\n                    <div>If Contractor will be using third-party materials as a part of their work, please indicate\n                      here. <br>\n                    </div>\n                    <div><span>e.g.</span> : <div>stock footage, clip licenses, etc.</div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group mt-3\"\n              [ngClass]=\"{'active': id === 25, 'error' : showFileSizeError, 'disabled' : formDisabled }\" id=\"25\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\" (mouseenter)=\"focus(25)\" (click)=\"focus(25)\"\n                  (mouseleave)=\"blur()\">\n                  <div class=\"option-radio download-attachments\">\n                    <p class=\"filter-font\">Attachments</p>\n                    <div class=\"attachment-section\">\n                      <ul class=\"d-list-item attachment-items-list list-unstyled\">\n                        <li *ngFor=\"let f of attachments\">\n                          <div class=\"attachment-item\">\n                            <p>\n                              <span class=\"downloadItem cursor\" (click)=\"!f.NotLoaded && downloadSingleFile(f)\">\n                                {{f.file}}\n                              </span>\n                            </p>\n                            <span (click)=\"removeAttachments(f)\">\n                              <img class=\"cursor\" *ngIf=\"!f.NotLoaded\" src=\"../assets/images/close-icon.png\"\n                                class=\"close-icon\">\n                            </span>\n                            <mat-spinner *ngIf=\"f.NotLoaded\" diameter=\"18\" class=\"close-icon\"></mat-spinner>\n                          </div>\n                        </li>\n                      </ul>\n                    </div>\n                    <div class=\" white-btn-main d-flex justify-content-between row\">\n                      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5 mb-2\">\n                        <input type=\"file\" multiple class=\"d-none\" id=\"fileInput\" (change)=\"uploadAttachment($event)\"\n                          #fileLabel>\n                        <button [disabled]=\"formDisabled\" (focus)=\"focus(25)\" (blur)=\"blur()\" type=\"button\"\n                          (click)=\"fileLabel.click(); focus(25)\" mat-raised-button color=\"\"\n                          class=\"print-submit-btn font-weight-600  white-btn\">\n                          ADD ATTACHMENTS</button>\n                      </div>\n                      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5\">\n                        <span (click)=\"downLoadFiles(); focus(25)\">\n                          <button (focus)=\"focus(25)\" (blur)=\"blur()\" type=\"button\"\n                            *ngIf=\"attachments && attachments.length > 0\" mat-raised-button color=\"\"\n                            class=\"print-submit-btn font-weight-600  white-btn\">\n                            DOWNLOAD ALL</button>\n                        </span>\n                      </div>\n                    </div>\n                  </div>\n                  <mat-error *ngIf=\"showFileSizeError\">\n                    Each file must be no more than 25mb.\n                  </mat-error>\n                </div>\n                <div class=\"col-6\">\n                  <div class=\"tooltip-box py-2 px-3\" [ngClass]=\"{'active': id === 25 }\" id=\"25\">\n                    <div>Wherever possible, please ask for a written proposal from the Contractor that describes the\n                      scope of their services. Our preference is that it\n                      be on their own letterhead and be in their own words, and perhaps\n                      referencing similar work that they’ve done for other clients. <br>\n                      Attach the proposal here.<br>\n                      Each file must be no more than 25mb.\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </section>\n        <div class=\" white-btn-main pt-4 d-flex justify-content-between row\">\n          <div *ngIf=\"userRole == 'Standard'\" class=\"col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6\">\n            <button *ngIf=\"project.pmStatus == 'Draft'\" type=\"button\" (click)=\"(f.submitted = true); submit()\"\n              mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMIT</button>\n            <button disabled *ngIf=\"project.pmStatus == 'Submitted' || project.pmStatus == 'Assigned'\" type=\"button\"\n              mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMITTED</button>\n            <span *ngIf=\"project.pmStatus == 'Accepted'\" (click)=\"addAddendum()\">\n              <button type=\"button\" mat-raised-button color=\"primary\"\n                class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">ADDENDUM</button>\n            </span>\n          </div>\n          <div *ngIf=\"userRole !== 'Standard'\" class=\"col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6\">\n            <button *ngIf=\"project.pmStatus == 'Draft'\" disabled type=\"button\" mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMIT</button>\n            <button *ngIf=\"project.pmStatus == 'Submitted' || project.pmStatus == 'Assigned'\" type=\"button\"\n              mat-raised-button color=\"primary\" (click)=\"(f.submitted = true); submit()\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">SUBMIT</button>\n            <button *ngIf=\"project.pmStatus == 'Accepted'\" disabled type=\"button\" mat-raised-button color=\"primary\"\n              class=\"font-weight-500 bg-color  w-100 sign-btn-size-12 sign-btn\">ACCEPTED</button>\n          </div>\n          <div *ngIf=\"(project.pmStatus !== 'Accepted')\" class=\"col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-4\"\n            (click)=\"openDeleteModal()\">\n            <button type=\"button\" mat-raised-button color=\"\" class=\"w-100 white-btn\">\n              DELETE PROJECT MEMO</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </fieldset>\n</form>\n<div class=\"modal confirmation-modal\" id=\"confirmationModal\" data-backdrop=\"static\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <input class=\"firstFocus\" type=\"text\">\n      <!-- Modal body -->\n      <div class=\"modal-body font-14\">\n        <div class=\"pb-3 text-color font-16 font-weight-500\">{{confirmationTitle}}</div>\n        <div class=\"\">{{confirmationMessage}}</div>\n      </div>\n      <!-- Modal footer -->\n      <div class=\"modal-footer\">\n        <button id=\"confirmation-modal-cancle\" type=\"button\" mat-raised-button color=\"\"\n          class=\"w-50 white-btn cancel-btn modal-cancel\">CANCEL</button>\n        <button mat-raised-button color=\"primary\" class=\"w-50 modal-confirm\"\n          (click)=\"confirm()\">{{ btnTitle ? btnTitle : 'CONFIRM' }}</button>\n      </div>\n    </div>\n  </div>\n</div>\n<app-print-memo *ngIf=\"showPrintModal\" [project]=\"project\" [projectId]=\"projectId\" [codeSelected]=\"codeSelected\"\n  (downLoadFiles)=\"downLoadFiles($event)\"></app-print-memo>"

/***/ }),

/***/ "./src/app/dashboard/project-form/project-form.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/dashboard/project-form/project-form.component.ts ***!
  \******************************************************************/
/*! exports provided: ProjectFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectFormComponent", function() { return ProjectFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _utility_filedownload_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../_utility/filedownload.helper */ "./src/app/_utility/filedownload.helper.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/_models/projects.model */ "./src/app/_models/projects.model.ts");
/* harmony import */ var _service_file_upload_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_service/file-upload.service */ "./src/app/dashboard/_service/file-upload.service.ts");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _header_dashboard_header_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../header/dashboard-header.service */ "./src/app/dashboard/header/dashboard-header.service.ts");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../_service/users.service */ "./src/app/dashboard/_service/users.service.ts");
/* harmony import */ var src_app_services_shared_service_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/_services/shared-service.service */ "./src/app/_services/shared-service.service.ts");














var ProjectFormComponent = /** @class */ (function () {
    function ProjectFormComponent(service, activatedRoute, router, alert, fileService, location, headerService, loader, usersService, sharedServiceService) {
        var _this = this;
        this.service = service;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.alert = alert;
        this.fileService = fileService;
        this.location = location;
        this.headerService = headerService;
        this.loader = loader;
        this.usersService = usersService;
        this.sharedServiceService = sharedServiceService;
        this.code = "+91";
        this.selectedFiles = [];
        this.Users = [];
        this.CatalogList = [];
        this.CCList = [];
        this.CCSuggestions = [];
        this.attachments = [];
        this.ContractorsList = [];
        this.formDisabled = true;
        this.userRole = "";
        this.ccIsInvalid = false;
        this.showFileSizeError = false;
        this.CatalogListItems = [];
        this.UsersList = [];
        this.codeSelected = {
            flag: '',
            code: ''
        };
        this.sharedServiceService.onUpdate().subscribe(function (response) {
            setTimeout(function () {
                _this.isPhoneValid = localStorage.getItem('isPhoneNumberValid');
            }, 500);
        });
    }
    ProjectFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.project = new src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_7__["ProjectFormModel"]();
        this.ContractorsList = [];
        this.userRole = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_9__["UserAuthHelper"].GetProfile().type;
        this.activatedRoute.params.subscribe(function (res) {
            if (res && res.projectId) {
                _this.projectId = res.projectId;
                _this.loadDetails();
            }
            else {
                _this.isFormDirty = false;
                _this.router.navigate(["/dashboard"]);
            }
        });
    };
    ProjectFormComponent.prototype.getCountryCodeList = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.usersService.getCountryCodeList()];
                    case 1:
                        data = _a.sent();
                        a = [{
                                label: "(+1) United States of America",
                                value: "+1 ",
                                flag: "https://restcountries.eu/data/usa.svg"
                            },
                            {
                                label: "(+44) United Kingdom of Great Britain and Northern Ireland",
                                value: "+44 ",
                                flag: "https://restcountries.eu/data/gbr.svg"
                            }];
                        this.countryCodeOptions = a.concat(data);
                        if (this.countryCodeOptions.length > 0) {
                            this.loadProjectDetails();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ProjectFormComponent.prototype.onCountryChange = function (ev) {
        var data = this.countryCodeOptions.find(function (ele) { return ele.label === ev.label; });
        this.codeSelected = {
            flag: data.flag,
            code: data.value
        };
        this.project.contractorCountryCode = data.label;
        this.SaveProgress();
    };
    ProjectFormComponent.prototype.ngAfterViewInit = function () {
        $(document).on("keydown", ".modal-confirm", function (event) {
            if (event.keyCode === 9) {
                event.preventDefault();
                $(".modal-cancel").focus();
            }
        });
    };
    ProjectFormComponent.prototype.setCatalog = function (event) {
        var id = null;
        if (event.option.value) {
            id = this.CatalogList.find(function (x) { return x.catalogName === event.option.value; }).id;
        }
        this.project.catalogId = id;
        return true;
    };
    ProjectFormComponent.prototype.generateNewProject = function () {
        var _this = this;
        this.service.generateProjectId().subscribe(function (res) {
            if (res.data) {
                _this.projectId = res.data;
                var url = "/projects/" + _this.projectId;
                _this.location.go(url);
                _this.loadDetails();
            }
        }, function (error) {
            _this.alert.error(error.error.details);
        });
    };
    ProjectFormComponent.prototype.FilterAssignTo = function () {
        var _this = this;
        this.Users = this.UsersList.filter(function (x) { return x.firstName.toLocaleLowerCase().includes(_this.project.assignedUser.toLocaleLowerCase()); });
    };
    ProjectFormComponent.prototype.ValidateAssignTo = function () {
        var _this = this;
        if (this.project.assignedTo) {
            if (!this.UsersList.some(function (x) { return x.firstName + " " + x.lastName === _this.project.assignedTo.trim(); })) {
                this.project.assignedUser = "";
                this.project.assignedTo = "";
            }
        }
    };
    ProjectFormComponent.prototype.loadDetails = function () {
        this.getCountryCodeList();
        this.loadUsers();
        this.loadCatalogList();
        this.loadContacts();
    };
    ProjectFormComponent.prototype.loadContacts = function () {
        var _this = this;
        this.service.GetContractorsList().subscribe(function (res) {
            _this.ContractorsList = res.data.contact;
            _this.ContractorsListOptions = _this.ContractorsList.filter(function (x) { return true; });
            // tslint:disable-next-line:max-line-length
            var result = _this.ContractorsListOptions.find(function (x) { return x.contractorCompanyName === _this.project.contractorCompanyName; });
            if (!result) {
                _this.project.contractorCompanyName = '';
            }
        });
    };
    ProjectFormComponent.prototype.FilterCompany = function (value) {
        value = value.trim();
        this.ContractorsListOptions = this.ContractorsList.filter(function (x) {
            return x.contactName.toLocaleLowerCase().includes(value.toLocaleLowerCase());
        });
    };
    ProjectFormComponent.prototype.loadCatalogList = function () {
        var _this = this;
        this.service.getCatalogList().subscribe(function (res) {
            _this.CatalogListItems = res.data.catalog;
            _this.CatalogList = _this.CatalogListItems.filter(function (x) { return true; });
            var result = _this.CatalogListItems.find(function (x) { return x.catalogName === _this.project.catalog; });
            if (!result) {
                _this.project.catalog = '';
            }
        });
    };
    ProjectFormComponent.prototype.loadUsers = function () {
        var _this = this;
        if (this.projectId) {
            this.service.loadUsers().subscribe(function (res) {
                _this.UsersList = res.data;
                _this.Users = Object.assign([], _this.UsersList);
            });
        }
    };
    ProjectFormComponent.prototype.loadProjectDetails = function () {
        var _this = this;
        this.loader.show();
        if (this.projectId) {
            this.service.getProjectsDetails(this.projectId).subscribe(function (res) {
                _this.project = res.data;
                if (_this.project.contractorCountryCode && _this.countryCodeOptions) {
                    var data = _this.countryCodeOptions.find(function (ele) { return ele.label === _this.project.contractorCountryCode; });
                    if (data) {
                        _this.codeSelected = {
                            flag: data.flag,
                            code: data.value
                        };
                        _this.project.contractorCountryCode = data.label;
                    }
                    else {
                        _this.codeSelected = {
                            flag: _this.countryCodeOptions[241].flag,
                            code: _this.countryCodeOptions[241].value
                        };
                    }
                }
                else {
                    _this.codeSelected = {
                        flag: _this.countryCodeOptions[241].flag,
                        code: _this.countryCodeOptions[241].value
                    };
                    _this.project.contractorCountryCode = _this.countryCodeOptions[241].label;
                }
                _this.setHeaderTitle();
                _this.attachments = Object.assign([], _this.project.attachment);
                if (_this.project && _this.project.cc) {
                    _this.CCList = Object.assign([], _this.project.mailList);
                    _this.CheckIfAllEmailsAreValid();
                }
                _this.project.cc = "";
                _this.formDisabled =
                    _this.project.pmStatus === "Accepted" ||
                        ((_this.project.pmStatus === "Assigned" ||
                            _this.project.pmStatus === "Submitted") &&
                            _this.userRole === "Standard");
                _this.loader.hide();
                setTimeout(function () {
                    _this.startDetectingChanges = true;
                    _this.createChroneJob();
                }, 500);
            }, function (error) {
                _this.loader.hide();
                _this.alert.error(error.error.details, true);
                _this.isFormDirty = false;
                _this.router.navigate(["/dashboard"]);
            });
        }
    };
    ProjectFormComponent.prototype.validateCompanyName = function () {
        var _this = this;
        this.project.contractorCompanyName = this.project.contractorCompanyName.trim();
        if (this.project.contractorCompanyName &&
            this.ContractorsListOptions.filter(function (x) { return x.contactName === _this.project.contractorCompanyName; }).length !== 1) {
            this.project.contractorCompanyName = "";
        }
    };
    ProjectFormComponent.prototype.setHeaderTitle = function () {
        if (this.project.contractorCompanyName &&
            this.project.catalog &&
            this.project.from) {
            var title = "Project Memo for " +
                this.project.contractorCompanyName +
                " on " +
                this.project.catalog +
                " from " +
                this.project.from;
            this.headerService.setTitle(title);
        }
        else {
            this.headerService.setTitle("Project Memo - " + this.projectId);
        }
    };
    ProjectFormComponent.prototype.blur = function () {
        this.id = 0;
    };
    ProjectFormComponent.prototype.focus = function (id, ele) {
        this.id = id;
        if (ele) {
            setTimeout(function () {
                var it = document.getElementById("ccinput");
                it.focus();
            }, 550);
        }
        return true;
    };
    ProjectFormComponent.prototype.SaveProgress = function (reload) {
        if (reload === void 0) { reload = false; }
        if (this.startDetectingChanges) {
            if (reload) {
                this.needReload = true;
            }
            this.isFormDirty = true;
        }
    };
    ProjectFormComponent.prototype.createChroneJob = function () {
        var _this = this;
        this.chroneJob = null;
        this.chroneJob = setInterval(function () {
            if (_this.isFormDirty && !_this.isUploadIncomplete) {
                _this.saveNewChanges();
            }
        }, 5000);
    };
    ProjectFormComponent.prototype.saveNewChanges = function () {
        var _this = this;
        if (this.needReload) {
            this.loader.show();
        }
        this.savingProgress = true;
        setTimeout(function () {
            _this.project.mailList = Object.assign([], _this.CCList);
            _this.project.attachment = _this.attachments
                .filter(function (x) { return !x.NotLoaded; })
                .map(function (x) { return ({
                file: x.file,
                url: x.url
            }); });
            _this.service.SaveProgress(_this.projectId, _this.project).subscribe(function (res) {
                _this.loader.hide();
                if (_this.needReload) {
                    _this.loadProjectDetails();
                }
                _this.needReload = false;
                _this.isFormDirty = false;
                _this.savingProgress = false;
            }, function (error) {
                _this.needReload = false;
                _this.isFormDirty = false;
                _this.savingProgress = false;
                _this.loader.hide();
                _this.alert.error("Hmmm… changes aren’t saving contact support.");
            });
        }, 0);
    };
    ProjectFormComponent.prototype.searchPeopleEmail = function () {
        var _this = this;
        this.CheckIfAllEmailsAreValid();
        if (this.project.cc && this.project.cc !== this.searchedEmail) {
            this.searchedEmail = this.project.cc;
            this.service.getPeoplesEmail(this.project.cc).subscribe(function (res) {
                _this.CCSuggestions = res.data;
            });
        }
    };
    ProjectFormComponent.prototype.CheckIfAllEmailsAreValid = function () {
        var _this = this;
        this.ccIsInvalid = this.CCList.some(function (x) {
            return !x.name ? !_this.validateEmail(x.email) : false;
        });
    };
    ProjectFormComponent.prototype.removeFromCCList = function (p) {
        if (!this.formDisabled) {
            this.CCList.splice(p, 1);
            this.CheckIfAllEmailsAreValid();
        }
        this.SaveProgress();
    };
    ProjectFormComponent.prototype.uploadAttachment = function (event) {
        this.isFormDirty = true;
        this.isUploadIncomplete = true;
        var length = event.target.files.length;
        this.files = event.target.files;
        this.showFileSizeError = false;
        var mxFileSize = 25 * 1024 * 1024;
        for (var i = 0; i < length; i++) {
            if (event.target.files[i].size < mxFileSize) {
                var atch = {
                    file: event.target.files[i].name,
                    url: "",
                    NotLoaded: true,
                    id: this.projectId
                };
                this.attachments.push(atch);
            }
            else {
                this.showFileSizeError = true;
            }
        }
        this.UploadFiles();
    };
    ProjectFormComponent.prototype.removeAttachments = function (f) {
        if (!this.formDisabled) {
            this.attachments = this.attachments.filter(function (x) { return x !== f; });
            this.project.attachment = this.project.attachment.filter(function (x) { return x !== f; });
            $("#fileInput").val(null);
        }
        this.SaveProgress();
    };
    ProjectFormComponent.prototype.UploadFiles = function () {
        var _this = this;
        var length = this.files.length;
        var mxFileSize = 25 * 1024 * 1024;
        for (var i = 0; i < length; i++) {
            if (this.files[i].size < mxFileSize) {
                this.fileService
                    .uploadFile(this.files[i], this.projectId + "-" + this.files[i].name)
                    .subscribe(function (data) {
                    var item = _this.attachments.find(function (x) { return x.id + "-" + x.file === data.key; });
                    item.NotLoaded = false;
                    item.url = data.Location;
                    if (_this.attachments.every(function (x) { return !x.NotLoaded; })) {
                        _this.isUploadIncomplete = false;
                        console.log("this.isUploadIncomplete = false;", _this.isUploadIncomplete);
                        _this.SaveProgress();
                    }
                }, function (error) { });
            }
        }
    };
    ProjectFormComponent.prototype.getPosts = function (option) {
        var _this = this;
        var p = this.CCSuggestions.find(function (x) { return x.email === _this.project.cc; });
        this.CCList.push(p);
        setTimeout(function () {
            _this.project.cc = "";
        }, 0);
        this.CheckIfAllEmailsAreValid();
    };
    ProjectFormComponent.prototype.print = function () {
        this.showPrintModal = true;
        setTimeout(function () {
            $("#printProjectModal").modal("show");
        }, 500);
    };
    ProjectFormComponent.prototype.addAddendum = function () {
        var _this = this;
        this.loader.show();
        this.service
            .getAddendumId(this.projectId, this.project.projectName)
            .subscribe(function (res) {
            var url = "/addendum/" + res.data;
            _this.loader.hide();
            _this.isFormDirty = false;
            _this.router.navigate([url]);
        }, function (error) {
            _this.loader.hide();
        });
    };
    ProjectFormComponent.prototype.compantSelected = function (option) {
        this.project.contractorCompanyName = option.value;
        var id = null;
        if (option.value) {
            id = this.ContractorsListOptions.find(function (x) { return x.contactName === option.value; }).id;
        }
        this.project.contactId = id;
        this.SaveProgress();
    };
    ProjectFormComponent.prototype.addEmailToList = function () {
        var _this = this;
        if (this.CCSuggestions.length > 0) {
            this.CCList.push(this.CCSuggestions[0]);
        }
        else {
            this.project.cc = this.project.cc.replace(",", "");
            this.CCList.push({ name: null, email: this.project.cc });
        }
        setTimeout(function () {
            _this.project.cc = "";
            _this.CheckIfAllEmailsAreValid();
        }, 0);
        this.SaveProgress();
    };
    ProjectFormComponent.prototype.addDate = function (onDate, num) {
        if (typeof this.project.expirationDate === "string" ||
            typeof this.project.effectiveDate === "string") {
            if (this.project.effectiveDate) {
                this.project.effectiveDate = new Date(this.project.effectiveDate);
            }
            if (this.project.expirationDate) {
                this.project.expirationDate = new Date(this.project.expirationDate);
            }
        }
        if (onDate === "effectiveDate") {
            if (!this.project.effectiveDate) {
                if (!this.project.expirationDate) {
                    this.project.effectiveDate = new Date();
                }
                else {
                    this.project.effectiveDate = this.project.expirationDate;
                }
            }
            if (num < 0 ||
                !this.project.expirationDate ||
                (num > 0 && this.project.effectiveDate < this.project.expirationDate)) {
                this.project.effectiveDate = this.addDays(new Date(this.project.effectiveDate), num);
            }
            this.project.effectiveDate.setHours(0, 0, 1, 0);
        }
        else if (onDate === "expirationDate") {
            if (!this.project.expirationDate) {
                if (!this.project.effectiveDate) {
                    this.project.expirationDate = new Date();
                }
                else {
                    this.project.expirationDate = this.project.effectiveDate;
                }
            }
            if (num > 0 ||
                !this.project.effectiveDate ||
                (num < 0 && this.project.effectiveDate < this.project.expirationDate)) {
                this.project.expirationDate = this.addDays(new Date(this.project.expirationDate), num);
            }
            this.project.expirationDate.setHours(0, 0, 0, 0);
        }
        return true;
    };
    ProjectFormComponent.prototype.addDays = function (date, days) {
        if (!date || isNaN(date.getTime())) {
            date = new Date();
            date.setHours(0, 0, 0, 0);
        }
        date.setDate(date.getDate() + days);
        return date;
    };
    ProjectFormComponent.prototype.validateEmail = function (email) {
        // tslint:disable-next-line:max-line-length
        // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var re = /^\S+@\S+$/;
        return re.test(String(email).toLowerCase());
    };
    ProjectFormComponent.prototype.downLoadFiles = function (str) {
        this.project.attachment.forEach(function (ele) {
            _utility_filedownload_helper__WEBPACK_IMPORTED_MODULE_1__["DwonloadHelper"].downloadFile(ele.url, ele.file);
        });
    };
    ProjectFormComponent.prototype.downloadSingleFile = function (f) {
        _utility_filedownload_helper__WEBPACK_IMPORTED_MODULE_1__["DwonloadHelper"].downloadFile(f.url, f.file);
    };
    ProjectFormComponent.prototype.deleteProjectMemo = function () {
        var _this = this;
        this.service.deleteProjectMemo(this.projectId).subscribe(function (res) {
            _this.isFormDirty = false;
            _this.router.navigate(["/dashboard"]);
        }, function (error) {
            _this.alert.error(error.details);
        });
    };
    ProjectFormComponent.prototype.openDeleteModal = function () {
        this.confirmationTitle = "Delete Project Memo";
        this.confirmationMessage =
            "Are you sure you want to delete this Project Memo? Once deleted it cannot be recovered.";
        this.confirmationAction = "DELETE";
        $("#confirmationModal").modal("show");
    };
    ProjectFormComponent.prototype.confirm = function () {
        if (this.confirmationAction === "DELETE") {
            this.delete();
            $("#confirmationModal").modal("hide");
        }
        else if (this.confirmationAction === "SUBMIT") {
            if (this.userRole.includes("Standard")) {
                this.confirmSubmitStandardUser();
            }
            else {
                this.confirmSubmitBA();
            }
        }
        $("#confirmationModal").modal("hide");
    };
    ProjectFormComponent.prototype.confirmSubmitBA = function () {
        var _this = this;
        this.loader.show();
        if (this.isFormDirty) {
            this.service.SaveProgress(this.projectId, this.project).subscribe(function (res) {
                _this.needReload = false;
                _this.isFormDirty = false;
                _this.savingProgress = false;
                _this.BAFinalSubmit();
            }, function (error) {
                _this.needReload = false;
                _this.isFormDirty = false;
                _this.savingProgress = false;
                _this.loader.hide();
                _this.alert.error("Hmmm… changes aren’t saving contact support.");
            });
        }
        else {
            this.BAFinalSubmit();
        }
    };
    ProjectFormComponent.prototype.BAFinalSubmit = function () {
        var _this = this;
        this.service.SubmitProjectBA(this.projectId).subscribe(function (res) {
            _this.alert.success("Your Project Memo has been submitted.", true);
            _this.loader.hide();
            _this.isFormDirty = false;
            _this.router.navigate(["dashboard"]);
        }, function (error) {
            _this.alert.error(error.error.details);
            _this.loader.hide();
        });
    };
    ProjectFormComponent.prototype.selectionChanged = function (option) {
        var user = this.UsersList.find(function (x) { return x.uniqueid === option.value; });
        if (user) {
            this.project.assignedUser = user.firstName + " " + user.lastName;
            this.project.assignedTo = option.value;
        }
        this.needReload = true;
        this.saveNewChanges();
    };
    ProjectFormComponent.prototype.delete = function () {
        var _this = this;
        this.service.deleteProjectMemo(this.projectId).subscribe(function (res) {
            _this.alert.error("That Project Memo has been deleted.", true);
            _this.isFormDirty = false;
            _this.router.navigate(["/dashboard"]);
        }, function (error) {
            _this.alert.error(error.details ? error.details : "Something went wrong on server!");
        });
    };
    ProjectFormComponent.prototype.submit = function () {
        var _this = this;
        this.btnTitle = null;
        setTimeout(function () {
            if (_this.submitFormValid()) {
                if (_this.userRole === "Standard") {
                    _this.openConfirmSubmitStandardUserModal();
                }
                else {
                    if (_this.checkConatctExist()) {
                        _this.openConfirmSubmitBAUserModal();
                    }
                    else {
                        _this.alert.success("Owner Of This Project Is deleted. Please Reassigned to new user.", true);
                    }
                }
            }
        }, 0);
    };
    ProjectFormComponent.prototype.checkConatctExist = function () {
        var _this = this;
        var contact = this.ContractorsList.find(function (x) { return x.contactName === _this.project.from; });
        if (contact) {
            return true;
        }
        else {
            return false;
        }
    };
    ProjectFormComponent.prototype.openConfirmSubmitBAUserModal = function () {
        this.confirmationTitle = "Submit Project Memo";
        this.confirmationMessage =
            "Are you sure this is ready to be sent to Rightsline?";
        this.confirmationAction = "SUBMIT";
        this.btnTitle = "Submit";
        $("#confirmationModal").modal("show");
        $("#confirmation-modal-cancle").focus();
    };
    ProjectFormComponent.prototype.submitFormValid = function () {
        var errorPhone = document.getElementsByClassName("form-group phone-valid");
        var errorEle = document.getElementsByClassName("form-group error");
        if (errorEle.length === 0) {
            if (this.isPhoneValid === 'false') {
                var a = errorPhone[0];
                var erTop = a.offsetTop;
                $("html, body").animate({
                    scrollTop: erTop - 128
                }, 500);
            }
            else {
                return true;
            }
        }
        else {
            var ele = errorEle[0];
            var errorTop = ele.offsetTop;
            $("html, body").animate({
                scrollTop: errorTop - 128
            }, 500);
        }
        return false;
    };
    ProjectFormComponent.prototype.openConfirmSubmitStandardUserModal = function () {
        this.confirmationTitle = "Submit Project Memo";
        this.confirmationMessage =
            "Are you sure this is ready to be sent to Business Affairs?";
        this.confirmationAction = "SUBMIT";
        $("#confirmationModal").modal("show");
        $("#confirmation-modal-cancle").focus();
    };
    ProjectFormComponent.prototype.confirmSubmitStandardUser = function () {
        var _this = this;
        this.loader.show();
        if (this.isFormDirty) {
            this.service.SaveProgress(this.projectId, this.project).subscribe(function (res) {
                _this.needReload = false;
                _this.isFormDirty = false;
                _this.savingProgress = false;
                _this.UserFinalSubmit();
            }, function (error) {
                _this.needReload = false;
                _this.isFormDirty = false;
                _this.savingProgress = false;
                _this.loader.hide();
                _this.alert.error("Hmmm… changes aren’t saving contact support.");
            });
        }
        else {
            this.UserFinalSubmit();
        }
    };
    ProjectFormComponent.prototype.UserFinalSubmit = function () {
        var _this = this;
        this.service.SubmitProjectStandardUser(this.projectId).subscribe(function (res) {
            _this.alert.success("Your Project Memo has been submitted.", true);
            _this.loader.hide();
            _this.isFormDirty = false;
            _this.router.navigate(["dashboard"]);
        }, function (error) {
            _this.loader.hide();
            _this.alert.error(error.error.details);
        });
    };
    ProjectFormComponent.prototype.ngOnDestroy = function () {
        this.chroneJob = null;
        if (this.isFormDirty) {
            this.saveNewChanges();
        }
    };
    ProjectFormComponent.prototype.sendCopiesBlur = function () {
        this.project.cc = this.project.cc.trim();
    };
    ProjectFormComponent.prototype.FilterCatalogList = function (value) {
        this.CatalogList = this.CatalogListItems.filter(function (x) {
            return x.catalogName.toLocaleLowerCase().includes(value.toLocaleLowerCase());
        });
    };
    ProjectFormComponent.prototype.ValidateCatalog = function (value) {
        if (!this.CatalogListItems.find(function (x) { return x.catalogName.trim() === value.trim(); })) {
            this.project.catalog = "";
        }
    };
    ProjectFormComponent.prototype.addhyphen = function (event) {
        if (event && event.target && event.target.value) {
            event.target.value = event.target.value.split('-').join(''); // Remove dash (-) if mistakenly entered.
            var finalVal = event.target.value.match(/\d{3}(?=\d{2,3})|\d+/g).join('-');
            event.target.value = finalVal;
        }
    };
    ProjectFormComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode === 43) {
            return true;
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"])("cc"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"])
    ], ProjectFormComponent.prototype, "ccEle", void 0);
    ProjectFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: "app-project-form",
            template: __webpack_require__(/*! ./project-form.component.html */ "./src/app/dashboard/project-form/project-form.component.html"),
            styles: [__webpack_require__(/*! ./project-form.component.css */ "./src/app/dashboard/project-form/project-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"],
            _service_file_upload_service__WEBPACK_IMPORTED_MODULE_8__["FileUploadService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"],
            _header_dashboard_header_service__WEBPACK_IMPORTED_MODULE_10__["DashboardHeaderService"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_11__["LoaderService"],
            _service_users_service__WEBPACK_IMPORTED_MODULE_12__["UsersService"],
            src_app_services_shared_service_service__WEBPACK_IMPORTED_MODULE_13__["SharedServiceService"]])
    ], ProjectFormComponent);
    return ProjectFormComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/project-memo-print/project-memo-print.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/dashboard/project-memo-print/project-memo-print.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* @media screen {\n    div.divFooter, div.divHeader {\n        display: none;\n    }\n}\n */\n\n/* @page {\n    margin: 50cm;\n\n    @top-center {\n        content: element(pageHeader);\n    }\n\n    @bottom-center {\n        content: element(pageFooter);\n    }\n} */\n\n/* \nbody {\n    margin: 25mm 25mm 25mm 25mm;\n}\n*/\n\n/* \n@media print {\n    div.divFooter, div.divHeader {\n        position: fixed; \n        left: 0;\n        right: 0;\n    }\n    div.divFooter {\n        bottom: 0;\n    }\n    div.divHeader {\n        top: 0;\n    }\n} \n\n@page {\n    size: auto;\n    margin: 2cm;\n    @top-left {\n        content: \"first: \" string(heading, first);\n    }\n    @top-center {\n        content: \"start: \" string(heading, start);\n    }\n    @top-right {\n        content: \"last: \" string(heading, last);\n    }\n}\n\nh2 {\n    string-set: heading content()\n} */\n\nbody {\n    margin: 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3Byb2plY3QtbWVtby1wcmludC9wcm9qZWN0LW1lbW8tcHJpbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7RUFLRTs7QUFFRjs7Ozs7Ozs7OztHQVVHOztBQUVIOzs7O0NBSUM7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0ErQkc7O0FBRUg7SUFDSSxTQUFTO0FBQ2IiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvcHJvamVjdC1tZW1vLXByaW50L3Byb2plY3QtbWVtby1wcmludC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogQG1lZGlhIHNjcmVlbiB7XG4gICAgZGl2LmRpdkZvb3RlciwgZGl2LmRpdkhlYWRlciB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxufVxuICovXG5cbi8qIEBwYWdlIHtcbiAgICBtYXJnaW46IDUwY207XG5cbiAgICBAdG9wLWNlbnRlciB7XG4gICAgICAgIGNvbnRlbnQ6IGVsZW1lbnQocGFnZUhlYWRlcik7XG4gICAgfVxuXG4gICAgQGJvdHRvbS1jZW50ZXIge1xuICAgICAgICBjb250ZW50OiBlbGVtZW50KHBhZ2VGb290ZXIpO1xuICAgIH1cbn0gKi9cblxuLyogXG5ib2R5IHtcbiAgICBtYXJnaW46IDI1bW0gMjVtbSAyNW1tIDI1bW07XG59XG4qL1xuXG4vKiBcbkBtZWRpYSBwcmludCB7XG4gICAgZGl2LmRpdkZvb3RlciwgZGl2LmRpdkhlYWRlciB7XG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDsgXG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgIH1cbiAgICBkaXYuZGl2Rm9vdGVyIHtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgIH1cbiAgICBkaXYuZGl2SGVhZGVyIHtcbiAgICAgICAgdG9wOiAwO1xuICAgIH1cbn0gXG5cbkBwYWdlIHtcbiAgICBzaXplOiBhdXRvO1xuICAgIG1hcmdpbjogMmNtO1xuICAgIEB0b3AtbGVmdCB7XG4gICAgICAgIGNvbnRlbnQ6IFwiZmlyc3Q6IFwiIHN0cmluZyhoZWFkaW5nLCBmaXJzdCk7XG4gICAgfVxuICAgIEB0b3AtY2VudGVyIHtcbiAgICAgICAgY29udGVudDogXCJzdGFydDogXCIgc3RyaW5nKGhlYWRpbmcsIHN0YXJ0KTtcbiAgICB9XG4gICAgQHRvcC1yaWdodCB7XG4gICAgICAgIGNvbnRlbnQ6IFwibGFzdDogXCIgc3RyaW5nKGhlYWRpbmcsIGxhc3QpO1xuICAgIH1cbn1cblxuaDIge1xuICAgIHN0cmluZy1zZXQ6IGhlYWRpbmcgY29udGVudCgpXG59ICovXG5cbmJvZHkge1xuICAgIG1hcmdpbjogMDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/dashboard/project-memo-print/project-memo-print.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/dashboard/project-memo-print/project-memo-print.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-header\">\n  Project Memo {{projectId}}\n  <div class=\"float-right\">Status: {{project.pmStatus}}</div>\n</div>\n<div class=\"page-footer\">\n  Exported on: {{ today | date : 'MM/dd/yyyy' }}\n  <div class=\"float-right\">\n    <span id=\"pageFooter\" class=\"page-counter\">\n    </span></div>\n</div>\n<table>\n  <thead>\n    <tr>\n      <td>\n        <!--place holder for the fixed-position header-->\n        <h6 class=\"page-header-space\"></h6>\n      </td>\n    </tr>\n  </thead>\n  <tbody>\n    <tr>\n      <td>\n        <div class=\"page\" style=\"line-height: 1.5;\">\n          <section *ngIf=\"compLoaded\" id=\"dvContents\" class=\"general-form px-4\">\n            <div class=\"\">\n              <section class=\"pl-3 pl-sm-3 pl-3 pl-md-5 pl-lg-5\">\n                <div class=\"padding-details-lg\">\n                  <div *ngIf=\"showHeading\"\n                    class=\"main-heading font-weight-300\">\n                    Project Memo for {{project.contractorCompanyName? project.contractorCompanyName : '[vender name]'}}\n                    on {{project.catalog ? project.catalog : '[project catalog]'}}\n                    from {{project.from ? project.from : '[user name]'}}\n                  </div>\n                  <div *ngIf=\"!showHeading\"\n                    class=\"main-heading font-weight-300\">\n                    Project Memo {{projectId}}\n                  </div>\n                  <div class=\"light-black font-14 text-justify\">{{projectId}}</div>\n                </div>\n                <section>\n                  <div class=\"pb-1\">\n                    <div class=\"heading-5 padding-details-md font-weight-500\">\n                      GENERAL INFORMATION\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Assigned To\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.assignedUser ? project.assignedUser : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        In CC\n                      </div>\n                      <div *ngIf=\"project.mailList.length == 0\" class=\"font-14 text-justify\">\n                        - -\n                      </div>\n                      <div *ngIf=\"project.mailList.length > 0\" class=\"font-14 text-justify\">\n                        <span *ngFor=\"let p of project.mailList;let i = index\"> {{p.email}}<span\n                            *ngIf=\"i < (project.mailList.length - 1)\">,</span></span>\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Project Name\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.projectName? project.projectName :'- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Catalog\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.catalog ? project.catalog : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Effective Dates\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        From {{ project.effectiveDate ? (project.effectiveDate | date : 'MM/dd/yy') : ' - - ' }}\n                        to {{ project.expirationDate ? (project.expirationDate | date : 'MM/dd/yy') : ' - - ' }}\n                      </div>\n                    </div>\n                  </div>\n                </section>\n                <section>\n                  <div class=\"pb-1\">\n                    <div class=\"heading-5 padding-details-md font-weight-500\">\n                      CONTRACTOR\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Contractor Name\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.contractorCompanyName ? project.contractorCompanyName : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Primary Contact Name\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.contractorContactName ? project.contractorContactName : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Primary Contact Email\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.contractorContactEmail ? project.contractorContactEmail : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Primary Contact Phone\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.contractorContactPhone ? project.contractorContactPhone : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Will Contractor Work Onsite?\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.contractorWorkOnsite == true ? 'Yes' : project.contractorWorkOnsite == false ? 'No' : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Contractor Insurance\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        <span *ngIf=\"project.contractorInsuranceGeneral\">\n                          General Liability,\n                        </span>\n                        <span *ngIf=\"project.contractorInsuranceEnO\">\n                          E & O,\n                        </span>\n                        <span *ngIf=\"project.contractorInsuranceProduction\">\n                          Worker's Comp,\n                        </span>\n                        <span *ngIf=\"project.contractorInsuranceWorkersComp\">\n                          Production Insurance,\n                        </span>\n                      </div>\n                    </div>\n                  </div>\n                </section>\n                <section>\n                  <div class=\"pb-1\">\n                    <div class=\"heading-5 padding-details-md font-weight-500\">\n                      SERVICES / DELIVERABLES / PAYMENT\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Contractor's Service\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.contractorServices ? project.contractorServices : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Approximate Hours of Work\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.approxHours ? project.approxHours : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Deliverables\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.deliverablesDescription ? project.deliverablesDescription : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Delivery Schedule\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.deliverySchedule ? project.deliverySchedule : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Total Fee\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.totalFees ? project.totalFees : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Payment Schedule\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.paymentSchedule ? project.paymentSchedule : '- -'}}\n                      </div>\n                    </div>\n                  </div>\n                </section>\n                <section>\n                  <div class=\"pb-1\">\n                    <div class=\"heading-5 padding-details-md font-weight-500\">\n                      CONTENT\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Ownership of Deliverables\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.ownershipOfDeliverables ? project.ownershipOfDeliverables : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Rights Licensed by Contractor (if any)\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.rightsLicensedByContractor ? project.rightsLicensedByContractor : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Intentended Use by Contractor\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.intendedUseByContractor ? project.intendedUseByContractor : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Intentended Use by Participant\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.intendedUseByParticipant ? project.intendedUseByParticipant : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Third Party Assets to be Included\n                      </div>\n                      <div class=\"font-14 text-justify\">\n                        {{project.thirdPartyAssets == true ? 'Yes' : project.thirdPartyAssets == false ? 'No' : '- -'}}\n                      </div>\n                    </div>\n                    <div class=\"padding-details-sm\">\n                      <div class=\"light-black font-15\">\n                        Attachments\n                      </div>\n                      <div *ngFor=\"let p of project.attachment\" class=\"font-14 text-justify\">\n                        {{p.file}}\n                      </div>\n                    </div>\n                  </div>\n                </section>\n              </section>\n            </div>\n          </section>\n        </div>\n      </td>\n    </tr>\n  </tbody>\n\n  <tfoot>\n    <tr>\n      <td>\n        <!--place holder for the fixed-position footer-->\n        <div class=\"page-footer-space\"></div>\n      </td>\n    </tr>\n  </tfoot>\n</table>"

/***/ }),

/***/ "./src/app/dashboard/project-memo-print/project-memo-print.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/dashboard/project-memo-print/project-memo-print.component.ts ***!
  \******************************************************************************/
/*! exports provided: ProjectMemoPrintComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectMemoPrintComponent", function() { return ProjectMemoPrintComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/_models/projects.model */ "./src/app/_models/projects.model.ts");







var ProjectMemoPrintComponent = /** @class */ (function () {
    function ProjectMemoPrintComponent(activatedRoute, loader, service, alert) {
        this.activatedRoute = activatedRoute;
        this.loader = loader;
        this.service = service;
        this.alert = alert;
        this.project = new src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_6__["ProjectFormModel"]();
        this.today = new Date();
        this.i = 0;
        this.showHeading = false;
    }
    ProjectMemoPrintComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (res) {
            if (res && res.projectId) {
                _this.projectId = res.projectId;
                _this.loadDetails();
            }
            else {
                window.close();
            }
        });
    };
    ProjectMemoPrintComponent.prototype.ngAfterViewInit = function () { };
    ProjectMemoPrintComponent.prototype.loadDetails = function () {
        var _this = this;
        this.loader.show();
        if (this.projectId) {
            this.service.getProjectsDetails(this.projectId).subscribe(function (res) {
                _this.project = res.data;
                _this.compLoaded = true;
                _this.loader.hide();
                if (_this.project.contractorCompanyName &&
                    _this.project.catalog &&
                    _this.project.from) {
                    _this.showHeading = true;
                }
                setTimeout(function () {
                    window.print();
                }, 500);
            }, function (error) {
                _this.loader.hide();
                _this.compLoaded = true;
                _this.alert.error(error.error.details, true);
                setTimeout(function () {
                    window.close();
                }, 500);
            });
        }
    };
    ProjectMemoPrintComponent.prototype.value = function () {
        this.i++;
        return this.i;
    };
    ProjectMemoPrintComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-project-memo-print",
            template: __webpack_require__(/*! ./project-memo-print.component.html */ "./src/app/dashboard/project-memo-print/project-memo-print.component.html"),
            styles: [__webpack_require__(/*! ./project-memo-print.component.css */ "./src/app/dashboard/project-memo-print/project-memo-print.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"],
            _service_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"]])
    ], ProjectMemoPrintComponent);
    return ProjectMemoPrintComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/projects/projects.component.css":
/*!***********************************************************!*\
  !*** ./src/app/dashboard/projects/projects.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9wcm9qZWN0cy9wcm9qZWN0cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/dashboard/projects/projects.component.html":
/*!************************************************************!*\
  !*** ./src/app/dashboard/projects/projects.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"mydashboard\" class=\"dashboard-section\">\n  <nav class=\"navbar-bottam\">\n    <div class=\"container-fluid d-flex justify-content-between\">\n      <div class=\"position-relative\">\n        <div *ngIf=\"userRole === 'Standard'\" (click)=\"newproject()\" class=\"d-inline-block cursor\">\n          <button mat-raised-button color=\"primary\" class=\"btn-icon ml-0 mr-3\">\n            <img src=\"../assets/images/project-memo-w.png\" class=\"icon\">\n            PROJECT MEMO\n          </button>\n        </div>\n        <div *ngIf=\"userRole == 'Standard'\" (click)=\"addAddendum()\" class=\"d-inline-block cursor\">\n          <button mat-raised-button color=\"primary\" class=\"btn-icon ml-0 mr-3\">\n            <img src=\"../assets/images/addendum-w.png\" class=\"icon\">\n            ADDENDUM\n          </button>\n        </div>\n      </div>\n      <div class=\"input-group search-btn space-left \">\n        <div style=\"position:relative;\">\n          <input [(ngModel)]=\"filters.searchText\" maxlength=\"250\"\n            (keyup)=\"$event.code === 'Enter' && filters.searchText && search()\" type=\"text\"\n            class=\"form-control own-input-outline input-search light-black font-15\" placeholder=\"Search\">\n        </div>\n        <div class=\"input-group-btn search-icon\" [ngClass]=\"{ 'notactive' : !filters.searchText  }\">\n          <button *ngIf=\"!isTestSearchActive\" class=\"btn btn-default \" [disabled]=\"!filters.searchText\" (click)=\"search()\" type=\"button\">\n            <img src=\"../assets/images/search.png\" class=\"search-icon-img\">\n          </button>\n          <button *ngIf=\"isTestSearchActive\"  class=\"btn btn-default\"\n                  (click)=\"(filters.searchText = ''); search()\" type=\"button\">\n            <img src=\"../assets/images/search-close-icon.png\" class=\"search-icon-img\">\n          </button>\n        </div>\n      </div>\n    </div>\n  </nav>\n  <div class=\"container-fluid own-dashboard-section form-own\">\n    <section class=\"wrapper-section\">\n      <div class=\"date-filter\">\n        <div class=\"dashboard-filter-items \">\n          <p *ngIf=\"userRole == 'Standard'\" class=\"filter-p\">\n            Filter by Date Created\n          </p>\n          <p *ngIf=\"userRole !== 'Standard'\" class=\"filter-p\">\n              Filter by Date Submitted\n            </p>\n          <div class=\"date-picker-wrapper\">\n            <div class=\"date-selection own-line col-6 cursor\" (click)=\"picker.open()\">\n              <img src=\"assets/images/cal-icon.png\" class='calender-icon'>\n              <mat-form-field class=\" cursor example-full-width\" floatLabel=\"never\">\n                <input class=\"cursor\" readonly matInput [max]=\"filters.endDate\" (focus)=\"picker.open()\" (dateChange)=\"search()\"\n                  [matDatepicker]=\"picker\" [(ngModel)]=\"filters.startDate\" placeholder=\"\">\n                <mat-datepicker #picker></mat-datepicker>\n              </mat-form-field>\n            </div>\n            <div class=\"cursor date-selection col-6 cursor\" (click)=\"pickera.open()\">\n              <mat-form-field class=\"cursor example-full-width\" floatLabel=\"never\">\n                <input class=\"cursor\" readonly [min]=\"filters.startDate\" (dateChange)=\"search()\" matInput (focus)=\"pickera.open()\"\n                  [matDatepicker]=\"pickera\" [(ngModel)]=\"filters.endDate\" placeholder=\"\">\n                <mat-datepicker #pickera></mat-datepicker>\n              </mat-form-field>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"data-border-md\">\n        <div class=\"date-selection-2 filter-date-section\" [ngClass]=\"{ 'error': filters.type.length == 0 }\">\n          <div class=\"filter-p\">\n            <span> Filter by Type</span>\n          </div>\n          <mat-form-field floatLabel=\"always\" class=\"custom-txt-select-wrapper\">\n            <div class=\"custom-txt-select\">Showing {{filters.type.length}} of {{userTypes.length}}</div>\n            <mat-select #firstSelect multiple class=\"own-select\">\n              <mat-option class=\"own-select-option remove-default-checkbox\" *ngFor=\"let item of userTypes\"\n                [value]=\"item.value\">\n                <mat-checkbox (click)=\"setFilter()\" [(ngModel)]=\"item.selected\" class=\"own-select-checkbox\">\n                  {{ item.display }}\n                </mat-checkbox>\n              </mat-option>\n              <mat-option class=\"done-btn own-select-option\">\n                <div (click)=\"closeDD()\" class=\"text-color\">\n                  DONE\n                </div>\n              </mat-option>\n            </mat-select>\n            <span class=\"mat-error\" *ngIf=\"filters.type.length == 0\">\n              *Select at least one option\n            </span>\n          </mat-form-field>\n        </div>\n      </div>\n      <div class=\"data-border-md\">\n        <div class=\"date-selection-2 filter-date-section\" [ngClass]=\"{ 'error': filters.status.length == 0 }\">\n          <div class=\"filter-p\">\n            <span> Filter by Status</span>\n          </div>\n          <mat-form-field floatLabel=\"always\" class=\"custom-txt-select-wrapper\">\n            <div class=\"custom-txt-select\">Showing {{filters.status.length}} of {{usersStatus.length}}</div>\n            <mat-select #secondSelect multiple class=\"own-select\">\n              <mat-option class=\"own-select-option remove-default-checkbox\" *ngFor=\"let item of usersStatus\"\n                selected=\"true\" [value]=\"item.value\">\n                <mat-checkbox (click)=\"setFilter()\" [(ngModel)]=\"item.selected\" class=\"own-select-checkbox\">\n                  {{ item.display }}\n                </mat-checkbox>\n              </mat-option>\n              <mat-option class=\"done-btn own-select-option\">\n                <div (click)=\"closeDD()\" class=\"text-color\">\n                  DONE\n                </div>\n              </mat-option>\n            </mat-select>\n            <span class=\"mat-error\" *ngIf=\"filters.status.length == 0\">\n              *Select at least one option\n            </span>\n          </mat-form-field>\n        </div>\n      </div>\n      <div class=\"data-border-md\" *ngIf=\"userRole !== 'Standard'\">\n        <div class=\"date-selection-2 filter-date-section\" [ngClass]=\"{ 'error': filters.assignment.length == 0 }\">\n          <div class=\"filter-p\">\n            <span> Filter by Assignment </span>\n          </div>\n          <mat-form-field floatLabel=\"always\" class=\"custom-txt-select-wrapper\">\n            <div class=\"custom-txt-select\">Showing {{filters.assignment.length}} of {{assignmentTypes.length}}</div>\n            <mat-select #thirdSelect multiple class=\"own-select\">\n              <mat-option class=\"own-select-option remove-default-checkbox\" *ngFor=\"let item of assignmentTypes\"\n                selected=\"true\" [value]=\"item.value\">\n                <mat-checkbox (click)=\"setFilter()\" [(ngModel)]=\"item.selected\" class=\"own-select-checkbox\">\n                  {{ item.display }} </mat-checkbox>\n                <!-- stop-click -->\n              </mat-option>\n              <mat-option class=\"done-btn own-select-option\">\n                <div (click)=\"closeDD()\" class=\"text-color\">\n                  DONE\n                </div>\n              </mat-option>\n            </mat-select>\n            <span class=\"mat-error\" *ngIf=\"filters.assignment.length == 0\">\n              *Select at least one option\n            </span>\n          </mat-form-field>\n        </div>\n      </div>\n      <div\n        *ngIf=\"!loadingFilters &&\n        !(filters.status.length === statusLength && filters.type.length === 2 && filters.assignment.length == 3  && !filters.searchText)\"\n        class=\"data-border-sm text-center mb-3\">\n        <div class=\"date-selection-2 d-flex justify-content-center cursor reset-btn\">\n          <button id=\"dashboardResetBTN\" type=\"button\" mat-raised-button color=\"\" (click)=\"reset()\"\n            class=\"w-100 print-submit-btn attachment-btn white-btn\">\n            <div class=\"filter-reset-btn\">\n              <div class=\"font-weight-500\">\n                RESET\n              </div>\n              <div class=\"font-weight-500\">\n                FILTERS\n              </div>\n            </div>\n          </button>\n        </div>\n      </div>\n    </section>\n    <section class=\"own-table-dashboard mb-3\" *ngIf=\"List && List.length > 0\">\n      <div class=\"table-responsive\">\n        <table class=\"table table-hover mb-0\">\n          <thead>\n            <tr>\n              <th *ngIf=\"userRole =='Standard'\" scope=\"col\" [ngClass]=\"{'active': filters.sort == 'createdAt' }\"\n                (click)=\"sort('createdAt')\">Created<i *ngIf=\"filters.sort == 'createdAt'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th *ngIf=\"userRole !=='Standard'\" [ngClass]=\"{'active': filters.sort == 'submittedAt' }\" scope=\"col\"\n                (click)=\"sort('submittedAt')\">\n                Submitted<i *ngIf=\"filters.sort == 'submittedAt'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th *ngIf=\"userRole !== 'Standard'\" [ngClass]=\"{'active': filters.sort == 'startAt' }\" scope=\"col\" (click)=\"sort('startAt')\">Started<i\n                *ngIf=\"filters.sort == 'startAt'\" class='fas icon-space'\n                [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n            </th>\n              <th [ngClass]=\"{'active': filters.sort == 'projectId' }\" scope=\"col\" (click)=\"sort('projectId')\">ID<i\n                  *ngIf=\"filters.sort == 'projectId'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'vendor' }\" scope=\"col\" (click)=\"sort('vendor')\">Contractor<i\n                  *ngIf=\"filters.sort == 'vendor'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'project' }\" scope=\"col\" (click)=\"sort('project')\">Project<i\n                  *ngIf=\"filters.sort == 'project'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'type' }\" scope=\"col\" (click)=\"sort('type')\">Type\n                <i *ngIf=\"filters.sort == 'type'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'assignedTo' }\" scope=\"col\" (click)=\"sort('assignedTo')\">\n                Assigned To<i *ngIf=\"filters.sort == 'assignedTo'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'status' }\" scope=\"col\" (click)=\"sort('status')\">Status<i\n                  *ngIf=\"filters.sort == 'status'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <!-- <th *ngIf=\"userRole =='Standard'\" [ngClass]=\"{'active': filters.sort == 'submittedAt' }\" scope=\"col\"\n                (click)=\"sort('submittedAt')\">\n                Submitted<i *ngIf=\"filters.sort == 'submittedAt'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th> -->\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let user of List\" (click)=\"goto(user)\">\n              <td *ngIf=\"userRole =='Standard'\">\n                <span *ngIf=\"user.createdAt\">\n                  {{user.createdAt | date : 'MM/dd/yy'}} - {{user.createdAt | date : 'EEE'}}\n                </span>\n              </td>\n              <td *ngIf=\"userRole !=='Standard'\">\n                <span *ngIf=\"user.submittedAt\">\n                  {{user.submittedAt | date : 'MM/dd/yy'}} - {{user.submittedAt | date : 'EEE'}}\n                </span>\n              </td>\n              <td *ngIf=\"userRole !== 'Standard'\">\n                {{user.startAt | date : 'MM/dd/yy'}} - {{user.startAt | date : 'EEE'}}\n              </td>\n              <td>{{user.projectId | slice:0:20 }}</td>\n              <td>{{user.vendor | slice:0:20 }}</td>\n              <td>{{user.project | slice:0:20 }}</td>\n              <td>{{user.type | slice:0:20 }}</td>\n              <td>{{user.assignedTo | slice:0:20 }}</td>\n              <td>{{user.status | slice:0:20 }}</td>\n              <!-- <td *ngIf=\"userRole =='Standard'\">\n                <span *ngIf=\"user.submittedAt\">\n                  {{user.submittedAt | date : 'MM/dd/yy'}} - {{user.submittedAt | date : 'EEE'}}\n                </span>\n              </td> -->\n            </tr>\n          </tbody>\n        </table>\n      </div>\n      <div *ngIf=\"totalUsers >= 10\" class=\"d-flex flex-wrap align-items-center justify-content-end\">\n        <div class=\"mat-page-label-own font-12 light-black\">Rows per page : </div>\n        <mat-paginator class=\"own-pagination remove-pagination-label hide-underline text-center\"\n          (page)=\"pageChanged($event)\" [length]=\"totalUsers\" [pageSize]=\"filters.limit\" [hidePageSize]=\"false\"\n          [pageIndex]=\"pageIndex\" [pageSizeOptions]=\"[5, 10, 20, 30, 50, 70, 100]\">\n        </mat-paginator>\n      </div>\n    </section>\n    <app-welcome-page\n      [hasFilters]=\"!(filters.status.length === statusLength && filters.type.length === 2 && filters.assignment.length == 3 && !filters.searchText)\"\n      *ngIf=\"!(List && List.length > 0) && !loading\" (clear)=\"reset()\"></app-welcome-page>\n    <app-spinner *ngIf=\"loading\"></app-spinner>\n  </div>\n  <app-user-account [user]=\"user\" *ngIf=\"displayForm\" [isProfile]=\"isProfile\" (closes)=\"ModelClosed($event)\">\n  </app-user-account>\n</section>\n"

/***/ }),

/***/ "./src/app/dashboard/projects/projects.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/dashboard/projects/projects.component.ts ***!
  \**********************************************************/
/*! exports provided: ProjectsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsComponent", function() { return ProjectsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_models/projects.model */ "./src/app/_models/projects.model.ts");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");








var ProjectsComponent = /** @class */ (function () {
    function ProjectsComponent(service, router, loader, alert) {
        this.service = service;
        this.router = router;
        this.loader = loader;
        this.alert = alert;
        // @ViewChildren(MatCheckbox) matCheckbox: QueryList<MatCheckbox>;
        this.filters = new src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_2__["ProjectFilterModel"]();
        this.List = [];
        this.displayForm = false;
        this.isProfile = false;
        this.userTypes = [];
        this.usersStatus = [];
        this.assignmentTypes = [];
        // user: UserModel = new UserModel();
        this.pageIndex = 0;
        this.userRole = "";
        this.loading = true;
        this.statusLength = 4;
        this.isTestSearchActive = false;
    }
    ProjectsComponent.prototype.ngOnInit = function () {
        // alert('This screen is still pending please ignore');
        this.userRole = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_4__["UserAuthHelper"].GetProfile().type;
        this.first = true;
        this.setFilters();
    };
    ProjectsComponent.prototype.setFilters = function () {
        this.userTypes = [
            { selected: true, value: "PM", display: "PM" },
            { selected: true, value: "AD", display: "AD" }
        ];
        this.assignmentTypes = [
            { selected: true, value: "Assigned to Me", display: "Assigned to Me" },
            {
                selected: true,
                value: "Assigned to Others",
                display: "Assigned to Others"
            },
            { selected: true, value: "Unassigned", display: "Unassigned" }
        ];
        this.filters = new src_app_models_projects_model__WEBPACK_IMPORTED_MODULE_2__["ProjectFilterModel"]();
        if (this.userRole === "Standard") {
            this.usersStatus = [
                { selected: true, value: "Draft", display: "Draft" },
                { selected: true, value: "Submitted", display: "Submitted" },
                { selected: true, value: "Assigned", display: "In Review" },
                { selected: false, value: "Accepted", display: "Accepted" }
            ];
            this.statusLength = 3;
            this.filters.sort = "createdAt";
        }
        else {
            this.usersStatus = [
                { selected: false, value: "Draft", display: "Draft" },
                { selected: true, value: "Submitted", display: "Needs Pickup" },
                { selected: true, value: "Assigned", display: "Assigned" },
                { selected: false, value: "Accepted", display: "Accepted" }
            ];
            this.filters.sort = "submittedAt";
            this.statusLength = 2;
        }
        this.setFilter();
    };
    ProjectsComponent.prototype.loadProjects = function () {
        var _this = this;
        if (!(this.filters.type.length === 0 ||
            this.filters.status.length === 0 ||
            this.filters.assignment.length === 0)) {
            this.loading = true;
            this.List = [];
            this.totalUsers = 0;
            if (this.subscription) {
                this.subscription.unsubscribe();
            }
            var url = "/dashboard";
            localStorage.setItem('queryParam', JSON.stringify(this.filters));
            this.subscription = this.service.getProjects(this.filters).subscribe(function (res) {
                _this.totalUsers = res.data.totalCount;
                _this.List = res.data.result;
                _this.loading = false;
            }, function (error) {
                _this.loading = false;
            });
        }
    };
    ProjectsComponent.prototype.sort = function (key) {
        if (this.filters.sort === key) {
            this.filters.order = this.filters.order === "desc" ? "asc" : "desc";
        }
        else {
            this.filters.order = "desc";
        }
        this.filters.sort = key;
        this.loadProjects();
    };
    ProjectsComponent.prototype.pageChanged = function (event) {
        this.filters.limit = event.pageSize;
        this.filters.offset = event.pageIndex * event.pageSize;
        this.pageIndex = event.pageIndex;
        this.loadProjects();
    };
    ProjectsComponent.prototype.search = function () {
        // this.filters.limit = 10;
        this.filters.offset = 0;
        this.pageIndex = 0;
        if (this.filters.searchText) {
            this.isTestSearchActive = true;
        }
        else {
            this.isTestSearchActive = false;
        }
        this.loadProjects();
    };
    ProjectsComponent.prototype.setFilter = function () {
        var _this = this;
        // this.queryParam = Object.assign(
        //   this.activatedRouter.snapshot.queryParams
        // );
        // this.queryParam = JSON.parse(JSON.stringify(this.queryParam));
        this.loadingFilters = true;
        if (this.first && localStorage.getItem('queryParam')) {
            this.queryParam = localStorage.getItem('queryParam');
            this.filters = JSON.parse(this.queryParam);
            this.assignmentTypes.forEach(function (ele) {
                if (_this.filters.assignment.includes(ele.value)) {
                    ele.selected = true;
                }
                else {
                    ele.selected = false;
                }
            });
            this.usersStatus.forEach(function (ele) {
                if (_this.filters.status.includes(ele.value)) {
                    ele.selected = true;
                }
                else {
                    ele.selected = false;
                }
            });
            this.userTypes.forEach(function (ele) {
                if (_this.filters.type.includes(ele.value)) {
                    ele.selected = true;
                }
                else {
                    ele.selected = false;
                }
            });
            if (this.filters.searchText) {
                this.isTestSearchActive = true;
            }
            var pageIndex = (this.filters.offset / this.filters.limit);
            this.pageIndex = pageIndex;
            this.loadingFilters = false;
            this.loadProjects();
            this.first = false;
        }
        else {
            this.loadingFilters = true;
            this.first = false;
            setTimeout(function () {
                _this.filters.type = _this.userTypes
                    .filter(function (x) { return x.selected; })
                    .map(function (x) { return x.value; });
                _this.filters.assignment = _this.assignmentTypes
                    .filter(function (x) { return x.selected; })
                    .map(function (x) { return x.value; });
                _this.filters.status = _this.usersStatus
                    .filter(function (x) { return x.selected; })
                    .map(function (x) { return x.value; });
                _this.loadingFilters = false;
                _this.search();
            }, 200);
        }
    };
    ProjectsComponent.prototype.reset = function () {
        this.setFilters();
        this.loadProjects();
    };
    ProjectsComponent.prototype.goto = function (user) {
        var url = "";
        if (user.type === "Project Memo") {
            url = "/projects/" + user.projectId;
        }
        else if (user.type === "Addendum") {
            url = "addendum/" + user.projectId;
        }
        this.router.navigate([url]);
    };
    ProjectsComponent.prototype.openModel = function () {
        setTimeout(function () {
            $("#userModal").modal("show");
        }, 300);
    };
    ProjectsComponent.prototype.ModelClosed = function (val) {
        var _this = this;
        $("#userModal").modal("hide");
        setTimeout(function () {
            _this.displayForm = false;
        }, 200);
        if (val) {
            this.search();
        }
    };
    ProjectsComponent.prototype.addAddendum = function () {
        $("#addendumModal").modal("show");
    };
    ProjectsComponent.prototype.generateNewProject = function () {
        var _this = this;
        this.loader.show();
        this.service.generateProjectId().subscribe(function (res) {
            if (res.data) {
                var projectId = res.data;
                var url = "/projects/" + projectId;
                _this.loader.hide();
                _this.router.navigate([url]);
            }
        }, function (error) {
            _this.loader.hide();
            _this.alert.error(error.error.details);
        });
    };
    ProjectsComponent.prototype.newproject = function () {
        this.generateNewProject();
    };
    ProjectsComponent.prototype.closeDD = function () {
        this.firstSelect.close();
        this.secondSelect.close();
        if (this.thirdSelect) {
            this.thirdSelect.close();
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("firstSelect"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProjectsComponent.prototype, "firstSelect", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("secondSelect"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProjectsComponent.prototype, "secondSelect", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("thirdSelect"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProjectsComponent.prototype, "thirdSelect", void 0);
    ProjectsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-projects",
            template: __webpack_require__(/*! ./projects.component.html */ "./src/app/dashboard/projects/projects.component.html"),
            styles: [__webpack_require__(/*! ./projects.component.css */ "./src/app/dashboard/projects/projects.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_project_service__WEBPACK_IMPORTED_MODULE_3__["ProjectService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_7__["AlertService"]])
    ], ProjectsComponent);
    return ProjectsComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/sidebar/sidebar.component.css":
/*!*********************************************************!*\
  !*** ./src/app/dashboard/sidebar/sidebar.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9zaWRlYmFyL3NpZGViYXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/sidebar/sidebar.component.html":
/*!**********************************************************!*\
  !*** ./src/app/dashboard/sidebar/sidebar.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"sidebar-menu\">\n  <ul class=\"list-unstyled sidebar-items font-14\">\n    <li title=\"Dashboard\" (click)=\"dashboard()\" class=\"cursor\" [class.active]=\"isActive(['dashboard', ''])\">\n      <img src=\"../assets/images/dashboard-w.png\" class=\"dashboard-img img-white\">\n      <img src=\"../assets/images/dashboard-b.png\" class=\"dashboard-img img-blue \">\n      <span class=\"items-name text-uppercase\">Dashboard</span>\n    </li>\n    <!-- <li routerLink=\"/dashboard/addendum\" [routerLinkActive]=\"['active']\">\n      <img src=\"../assets/images/project-memo-w.png\" class=\"dashboard-img img-white\">\n      <img src=\"../assets/images/project-memo-b.png\" class=\"dashboard-img img-blue \">\n      <span class=\"items-name text-uppercase\">Addendum</span>\n    </li> -->\n    <li *ngIf=\"!showUsers\" title=\"Project Memo\" class=\"cursor\" (click)=\"newproject()\" [class.active]=\"isActive(['project', ''])\">\n      <img src=\"../assets/images/project-memo-w.png\" class=\"dashboard-img img-white\">\n      <img src=\"../assets/images/project-memo-b.png\" class=\"dashboard-img img-blue \">\n      <span class=\"items-name text-uppercase\">Project Memo</span>\n    </li>\n    <li *ngIf=\"!showUsers\" title=\"Addendum\" class=\"cursor\" (click)=\"addendum()\" [class.active]=\"isActive(['addendum', ''])\">\n      <div style=\"pointer-events: none;\">\n        <img src=\"../assets/images/addendum-w.png\" class=\"dashboard-img img-white\">\n        <img src=\"../assets/images/addendum-b.png\" class=\"dashboard-img img-blue \">\n        <span class=\"items-name text-uppercase\">Addendum</span>\n      </div>\n    </li>\n    <li *ngIf=\"showUsers\" title=\"Users\" class=\"cursor\" (click)=\"usersList()\" routerLink=\"/users\" [routerLinkActive]=\"['active']\">\n      <img src=\"../assets/images/user-w.png\" class=\"dashboard-img img-white\">\n      <img src=\"../assets/images/user-b.png\" class=\"dashboard-img img-blue \">\n      <span class=\"items-name text-uppercase\">Users</span>\n    </li>\n  </ul>\n  <span class=\"right-sidebar-icon \"><img src=\"../assets/images/36-Filled.png\" class=\"sidebar-menu-icon\"></span>\n</aside>"

/***/ }),

/***/ "./src/app/dashboard/sidebar/sidebar.component.ts":
/*!********************************************************!*\
  !*** ./src/app/dashboard/sidebar/sidebar.component.ts ***!
  \********************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_shared/main-loader/loader.service */ "./src/app/_shared/main-loader/loader.service.ts");
/* harmony import */ var src_app_utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/_utility/hardreload.helper */ "./src/app/_utility/hardreload.helper.ts");








var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(router, service, alert, loader) {
        this.router = router;
        this.service = service;
        this.alert = alert;
        this.loader = loader;
        this.open = true;
        this.showUsers = false;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var role = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__["UserAuthHelper"].GetProfile().type;
        this.showUsers = role === "Business Affairs" || role === "Super Admin";
        // $(window).on("scroll", () => {
        //   const scrollVal = $(window).scrollTop();
        //   if (scrollVal <= 50) {
        //     $(".sidebar-menu").css("padding-top", 50 - scrollVal + "px");
        //   } else {
        //     $(".sidebar-menu").css("padding-top", "0");
        //   }
        // });
    };
    SidebarComponent.prototype.addendum = function () {
        $("#addendumModal").modal("show");
    };
    SidebarComponent.prototype.generateNewProject = function () {
        var _this = this;
        this.loader.show();
        this.service.generateProjectId().subscribe(function (res) {
            if (res.data) {
                var projectId = res.data;
                var url = "/projects/" + projectId;
                _this.loader.hide();
                src_app_utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_7__["HardReload"].redirectTo(url, _this.router);
            }
        }, function (error) {
            _this.loader.hide();
            _this.alert.error(error.error.details);
        });
    };
    SidebarComponent.prototype.newproject = function () {
        this.generateNewProject();
    };
    SidebarComponent.prototype.toggel = function () {
        // this.open = !this.open;
        // document.getElementById('mydashboard').classList.toggle("dashboard-close");
    };
    SidebarComponent.prototype.isActive = function (instruction) {
        // Set the second parameter to true if you want to require an exact match.
        return location.href.includes(instruction[0]);
    };
    SidebarComponent.prototype.dashboard = function () {
        if (location.href.includes("/dashboard")) {
            src_app_utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_7__["HardReload"].redirectTo("/", this.router);
        }
        else {
            this.router.navigate(["/dashboard"]);
        }
    };
    SidebarComponent.prototype.usersList = function () {
        if (location.href.includes("/users")) {
            src_app_utility_hardreload_helper__WEBPACK_IMPORTED_MODULE_7__["HardReload"].redirectTo("/users", this.router);
        }
        else {
            this.router.navigate(["/users"]);
        }
    };
    SidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-sidebar",
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/dashboard/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.css */ "./src/app/dashboard/sidebar/sidebar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _service_project_service__WEBPACK_IMPORTED_MODULE_4__["ProjectService"],
            _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
            _shared_main_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/users/user-account/user-account.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/dashboard/users/user-account/user-account.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/* Important part */\n.modal-dialog{\n    overflow-y: initial !important\n}\n.modal-dialog{\n    margin: 2.75rem auto;\n}\n.missing {\n    color: #f88100;\n    font-weight: 400;\n    font-size: inherit;\n}\n.shadow-back {\n    text-shadow:rgba(0,0,0,0.20) 1px 0 10px !important;;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3VzZXJzL3VzZXItYWNjb3VudC91c2VyLWFjY291bnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsbUJBQW1CO0FBQ25CO0lBQ0k7QUFDSjtBQUVBO0lBQ0ksb0JBQW9CO0FBQ3hCO0FBRUE7SUFDSSxjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0ksa0RBQWtEO0FBQ3REIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL3VzZXJzL3VzZXItYWNjb3VudC91c2VyLWFjY291bnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLyogSW1wb3J0YW50IHBhcnQgKi9cbi5tb2RhbC1kaWFsb2d7XG4gICAgb3ZlcmZsb3cteTogaW5pdGlhbCAhaW1wb3J0YW50XG59XG5cbi5tb2RhbC1kaWFsb2d7XG4gICAgbWFyZ2luOiAyLjc1cmVtIGF1dG87XG59XG5cbi5taXNzaW5nIHtcbiAgICBjb2xvcjogI2Y4ODEwMDtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGZvbnQtc2l6ZTogaW5oZXJpdDtcbn1cblxuLnNoYWRvdy1iYWNrIHtcbiAgICB0ZXh0LXNoYWRvdzpyZ2JhKDAsMCwwLDAuMjApIDFweCAwIDEwcHggIWltcG9ydGFudDs7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/users/user-account/user-account.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/dashboard/users/user-account/user-account.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal show uniqueModal\" cdkScrollable  data-backdrop=\"static\" [id]=\"isProfile ? 'userProfile' : 'userModal'\"\n  style=\"padding-right:0\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <section class=\"general-form px-3\">\n        <h5 class=\"pt-3\"> {{ isProfile? 'MY ACCOUNT' : 'USER ACCOUNT'}}</h5>\n        <div class=\"pt-3\">\n          <input type=\"text\" class=\"firstFocus\">\n          <form class=\"form-own form-own-lg\" #f=\"ngForm\" (submit)=\"submitFormValid(); (f.valid && save())\" autocomplete='off' novalidate>\n         <!-- Email If userRole Not Equal Standard -->\n            <div class=\"form-group\" *ngIf=\"userRole!='Standard'\">\n                <div class=\"row align-items-start\">\n                  <div class=\"col-12 \">\n                    <mat-form-field class=\"example-full-width sign-input-2 input-icon\" floatLabel=\"always\">\n                      <mat-label>Email *</mat-label>\n                      <input type=\"text\" placeholder=\"\" (change)=\"validateContact()\"\n                        (keyup)=\"FilterContact($event.target.value)\" name=\"email\" (focus)=\"focus(4)\" (blur)=\"blur()\"\n                        [(ngModel)]=\"user.email\" #email=\"ngModel\" matInput [matAutocomplete]=\"autoa2\" autocomplete=\"off\" required>\n                      <mat-autocomplete #autoa2=\"matAutocomplete\" [displayWith]=\"displayFn\"\n                        (optionSelected)='contactSelected($event.option)'>\n                        <mat-option *ngFor=\"let option of rightlineListOptions\" [value]=\"option.contactemail\">\n                          {{option.contactemail}} - {{option.contact_name}}\n                        </mat-option>\n                      </mat-autocomplete>\n                      <mat-error>\n                        *Please select a Contact, if you don’t see what you are looking for contact Business\n                        Affairs.\n                      </mat-error>\n                    </mat-form-field>\n                  </div>\n                </div>\n              </div>\n                <!-- Name  -->\n              <div class=\"form-group\">\n              <div class=\"row\">\n                <div class=\"col-12 shadow-back\">\n                  <p> Name: {{ user.firstName}}</p>\n                </div>\n              </div>\n              </div>\n            <!-- Email If userRole Standard -->\n              <div class=\"form-group shadow-back\"*ngIf=\"userRole=='Standard'\">\n                <div class=\"row\">\n                  <div class=\"col-12 \">\n                    <p> Email: {{ user.email}}</p>\n                  </div>\n                </div>\n                </div>\n            <!-- Right Line Conatct Id -->\n            <div class=\"form-group\">\n            <div class=\"row\">\n                <div class=\"col-12 shadow-back\" [ngClass]=\"{'missing': user.is_deleted || !user.rightline_id}\">\n                   RightsLine Contact Id: {{user.rightline_id}}  <span *ngIf=\"user.is_deleted  || !user.rightline_id\">MISSING</span>\n                </div>\n              </div>\n            </div>\n            <!-- User Type -->\n            <div class=\"form-group\" *ngIf=\"!isProfile\"\n              [ngClass]=\"{'active': id === 1, 'error' : (f.submitted || type.touched) && (type.hasError('required'))}\"\n              id=\"1\">\n              <div class=\"row\">\n                <div class=\"col-12\">\n                  <mat-form-field class=\"sign-input-2\" floatLabel=\"always\">\n                    <mat-label>User Type</mat-label>\n                    <mat-select [(ngModel)]=\"user.type\" name=\"type\" #type=\"ngModel\" disableRipple placeholder=\"_ _\"\n                      (focus)=\"focus(1)\" (blur)=\"blur()\" required>\n                      <mat-option (click)=\"dirty()\" value=\"Standard\">\n                        Standard\n                      </mat-option>\n                      <mat-option (click)=\"dirty()\" value=\"Business Affairs\">\n                        Business Affairs\n                      </mat-option>\n                    </mat-select>\n                    <mat-error>\n                      *Please select a user type for this user\n                    </mat-error>\n                  </mat-form-field>\n                </div>\n              </div>\n            </div>\n            <!-- Account Status -->\n            <div class=\"form-group padding-bottom-35\" *ngIf=\"!isProfile\" id=\"2\"\n              [ngClass]=\"{'error' : f.submitted && status.hasError('required'), 'active': id === 2 }\">\n              <div class=\"row\">\n                <div class=\"col-12\" (mouseenter)=\"focus(2)\" (mouseleave)=\"blur()\">\n                  <div class=\"option-radio margin-bottom-20\">\n                    <p class=\"filter-font\">Account Status</p>\n                    <mat-radio-group aria-label=\"Select an option\" [(ngModel)]=\"user.status\" name=\"status\"\n                      #status=\"ngModel\" required>\n                      <mat-radio-button (click)=\"dirty()\" value=\"Active\"> Active </mat-radio-button>\n                      <mat-radio-button (click)=\"dirty()\" value=\"Inactive\"> Inactive </mat-radio-button>\n                    </mat-radio-group>\n                  </div>\n                  <mat-error *ngIf=\"f.submitted && status.hasError('required')\">\n                    Please select status\n                  </mat-error>\n                </div>\n              </div>\n            </div>\n            <!-- Password -->\n            <div class=\"form-group\"\n              [ngClass]=\"{'error' : (f.submitted || passs.touched) && (passs.hasError('required')) }\">\n              <div class=\"row\">\n                <div class=\"col-12 \">\n                  <mat-form-field class=\"example-full-width sign-input-2\" floatLabel=\"always\">\n                    <mat-label>New Password\n                      <span *ngIf=\"!isProfile && (!isProfile && user.id)\">(optional)</span>\n                    </mat-label>\n                    <input autocomplete=\"new-password\" type=\"password\" matInput placeholder=\"\" name=\"pwd\" #passs=\"ngModel\"\n                      [required]=\"!user.id\" [(ngModel)]=\"user.password\" (keyup)=\"dirty(); passwordChanged()\">\n                  </mat-form-field>\n                  <div class=\"light-black font-14\">Please make sure your password has:</div>\n                  <div>\n                    <ul class=\"list-unstyled password-text-list font-12 pt-2\">\n                      <li\n                        [ngClass]=\"{'error' : f.submitted && ( user.password  || !user.id) && user.password.length < 8, 'active' :user. password.length > 7 }\">\n                        At least 8 characters\n                      </li>\n                      <li\n                        [ngClass]=\"{'error' : f.submitted && ( user.password  || !user.id) && symbolMissing, 'active' : !symbolMissing  }\">\n                        With a Symbol\n                      </li>\n                      <li\n                        [ngClass]=\"{'error' : f.submitted && (user.password  || !user.id) && numberMissing, 'active' : !numberMissing  }\">\n                        A Number\n                      </li>\n                      <li\n                        [ngClass]=\"{'error' : f.submitted &&( user.password  || !user.id) && lowerCaseMissing, 'active' : !lowerCaseMissing  }\">\n                        And a lowercase letter .</li>\n                      <li\n                        [ngClass]=\"{'error' : f.submitted &&( user.password  || !user.id) && upperCaseMissing, 'active' : !upperCaseMissing }\">\n                        And a capital letter</li>\n                    </ul>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <!-- Cancel And Close Button -->\n            <div class=\"row px-0\">\n              <div class=\"col-sm-6 col-12 mb-3 mb-md-0\">\n                <button type=\"button\" mat-raised-button color=\"\" (click)=\"closeModel()\"\n                  class=\"w-100 white-btn\">CANCEL</button>\n              </div>\n              <div class=\"col-sm-6 col-12\">\n                <button mat-raised-button color=\"primary\" class=\"w-100\">SAVE</button>\n              </div>\n              <div *ngIf=\"!isProfile && (!isProfile && user.id) && (currentUserRole == 'Super Admin')\"\n                class=\"pt-3 px-3 font-14\">\n                <div (click)=\"openDeleteModal()\" class=\"d-inline-block px-2 cursor font-weight-500\">DELETE</div>\n              </div>\n            </div>\n          </form>\n          <input type=\"text\" class=\"lastFocus\">\n        </div>\n      </section>\n    </div>\n  </div>\n</div>\n<div class=\"modal confirmation-modal\" id=\"confirmationModal\" data-backdrop=\"static\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <!-- Modal body -->\n      <input type=\"text\" class=\"firstFocus\">\n      <div class=\"modal-body font-14\">\n        <div class=\"pb-3 text-color font-16 font-weight-500\">{{confirmationHeader}}</div>\n        <div> {{confirmationMessage}}</div>\n      </div>\n      <!-- Modal footer -->\n      <div class=\"modal-footer\">\n        <button id=\"confirmation-modal-cancle\" type=\"button\" mat-raised-button color=\"\"\n          class=\"w-100  white-btn cancel-btn\">CANCEL</button>\n        <button mat-raised-button color=\"primary\" class=\" w-100\" (click)=\"delete()\">{{confirmationButtonText}}</button>\n      </div>\n      <input type=\"text\" class=\"lastFocus\">\n    </div>\n  </div>\n</div>\n<div class=\"modal confirmation-modal\" id=\"confirmationUnsaved\" data-backdrop=\"static\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <!-- Modal body -->\n      <input type=\"text\" class=\"firstFocus\">\n      <div class=\"modal-body font-14\">\n        <div class=\"pb-3 text-color font-16 font-weight-500\">Unsaved Changes</div>\n        <div> You have unsaved changes. Are you sure you want to discard these changes? </div>\n      </div>\n      <!-- Modal footer -->\n      <div class=\"modal-footer\">\n        <button type=\"button\" mat-raised-button (click)=\"closeConfirmationUnsaved()\"\n          class=\"w-100 white-btn cancel-btn\">Discard Changes</button>\n        <button mat-raised-button color=\"primary\" class=\"w-100\" (click)=\"cancleConfirmationUnsaved()\">Cancel</button>\n      </div>\n      <input type=\"text\" class=\"lastFocus\">\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/dashboard/users/user-account/user-account.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/dashboard/users/user-account/user-account.component.ts ***!
  \************************************************************************/
/*! exports provided: UserAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAccountComponent", function() { return UserAccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/_models/user-list.model */ "./src/app/_models/user-list.model.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_utility/user_auth.helper */ "./src/app/_utility/user_auth.helper.ts");
/* harmony import */ var src_app_dashboard_service_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/dashboard/_service/users.service */ "./src/app/dashboard/_service/users.service.ts");
/* harmony import */ var _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../_shared/alert/alert.service */ "./src/app/_shared/alert/alert.service.ts");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");







var UserAccountComponent = /** @class */ (function () {
    function UserAccountComponent(userService, alert, service) {
        this.userService = userService;
        this.alert = alert;
        this.service = service;
        this.user = new src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_1__["UserModel"]();
        this.closes = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.addNewUser = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.isProfile = false;
        this.currentUserRole = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__["UserAuthHelper"].GetProfile().type;
        this.numberMissing = true;
        this.lowerCaseMissing = true;
        this.symbolMissing = true;
        this.upperCaseMissing = true;
        this.userRole = "";
    }
    UserAccountComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log("after", JSON.stringify(this.user));
        var that = this;
        // this.rightlineUserList = [];
        $(document).on("click", ".uniqueModal", function (e) {
            if ($(e.target).hasClass("uniqueModal")) {
                if (that.isDirty && !that.isProfile) {
                    $(".uniqueModal").modal("show");
                    $("#confirmationUnsaved").modal("show");
                }
                else {
                    $(".uniqueModal").modal("hide");
                    _this.isDirty = false;
                    setTimeout(function () {
                        that.closes.emit(false);
                    }, 100);
                }
            }
        });
        setTimeout(function () {
            _this.isDirty = false;
        }, 400);
    };
    // tslint:disable-next-line:use-life-cycle-interface
    UserAccountComponent.prototype.ngOnInit = function () {
        this.userRole = src_app_utility_user_auth_helper__WEBPACK_IMPORTED_MODULE_3__["UserAuthHelper"].GetProfile().type;
    };
    UserAccountComponent.prototype.closeConfirmationUnsaved = function () {
        var _this = this;
        var that = this;
        $("#confirmationUnsaved").modal("hide");
        setTimeout(function () {
            $(".uniqueModal").modal("hide");
            _this.isDirty = false;
            setTimeout(function () {
                that.closes.emit(false);
            }, 100);
        }, 200);
    };
    UserAccountComponent.prototype.dirty = function (e) {
        this.isDirty = true;
    };
    UserAccountComponent.prototype.cancleConfirmationUnsaved = function () {
        $("#confirmationUnsaved").modal("hide");
    };
    UserAccountComponent.prototype.passwordChanged = function () {
        var password = this.user.password;
        if (/\d/.test(password)) {
            this.numberMissing = false;
        }
        else {
            this.numberMissing = true;
        }
        if (/[a-z]/.test(password)) {
            this.lowerCaseMissing = false;
        }
        else {
            this.lowerCaseMissing = true;
        }
        if (/[A-Z]/.test(password)) {
            this.upperCaseMissing = false;
        }
        else {
            this.upperCaseMissing = true;
        }
        if (/[^\w\s]/gi.test(password)) {
            this.symbolMissing = false;
        }
        else {
            this.symbolMissing = true;
        }
    };
    UserAccountComponent.prototype.closeModel = function () {
        var _this = this;
        $(".uniqueModal").modal("hide");
        this.isDirty = false;
        setTimeout(function () {
            _this.closes.emit(false);
        }, 100);
    };
    UserAccountComponent.prototype.save = function () {
        if (this.isPasswordValid()) {
            if (this.isProfile && this.user.password) {
                this.updatePassword();
            }
            else if (this.formValid()) {
                if (this.user.id) {
                    this.updateUser();
                }
                else {
                    this.addUser();
                }
            }
        }
    };
    UserAccountComponent.prototype.updatePassword = function () {
        var _this = this;
        this.userService.updatePassword(this.user.password).subscribe(function (res) {
            _this.alert.success(res.message, true);
            $(".uniqueModal").modal("hide");
            setTimeout(function () {
                _this.closes.emit(true);
            }, 500);
        }, function (error) {
            _this.alert.error(error.error.details);
        });
    };
    UserAccountComponent.prototype.updateUser = function () {
        var _this = this;
        this.userService.updateUser(this.user).subscribe(function (res) {
            _this.addNewUser.emit();
            _this.alert.success(res.message, true);
            $(".uniqueModal").modal("hide");
            setTimeout(function () {
                _this.closes.emit(true);
            }, 500);
        }, function (error) {
            _this.alert.success(error.error.details);
        });
    };
    UserAccountComponent.prototype.addUser = function () {
        var _this = this;
        this.userService.addUser(this.user).subscribe(function (res) {
            _this.alert.success(res.message, true);
            _this.addNewUser.emit();
            $(".uniqueModal").modal("hide");
            setTimeout(function () {
                _this.closes.emit(true);
            }, 500);
        }, function (error) {
            _this.alert.success(error.error.details, true);
        });
    };
    UserAccountComponent.prototype.setType = function (type) {
        this.user.type = type;
    };
    UserAccountComponent.prototype.isPasswordValid = function () {
        return (!this.user.password ||
            (this.user.password &&
                !this.symbolMissing &&
                !this.upperCaseMissing &&
                !this.lowerCaseMissing &&
                !this.numberMissing));
    };
    UserAccountComponent.prototype.formValid = function () {
        return (this.submitFormValid() &&
            this.user.type &&
            this.user.status &&
            this.user.firstName &&
            this.user.email);
    };
    UserAccountComponent.prototype.submitFormValid = function () {
        var errorEle = $("#userModal").find(".form-group.error");
        if (errorEle.length === 0) {
            return true;
        }
        else {
            var ele = errorEle[0];
            var errorTop = ele.offsetTop;
            $("#userModal").animate({
                scrollTop: errorTop
            }, 500);
        }
        return false;
    };
    UserAccountComponent.prototype.openDeleteModal = function () {
        this.confirmationHeader = "Delete User";
        this.confirmationMessage = "Are you sure you want to delete this user?\n                                It is important to note that the users project memos and\n                                addendums will remain in the system but, the user will no longer be visible.";
        this.confirmationButtonText = "DELETE";
        $("#confirmationModal").modal("show");
        $("#confirmation-modal-cancle").focus();
    };
    UserAccountComponent.prototype.unsavedConfirm = function () { };
    UserAccountComponent.prototype.delete = function () {
        var _this = this;
        $(".uniqueModal").modal("hide");
        $("#confirmationModal").modal("hide");
        this.userService.deleteUser(this.user).subscribe(function (res) {
            _this.alert.error("The user has been deleted.", true);
            _this.isDirty = false;
            setTimeout(function () {
                _this.closes.emit(true);
            }, 100);
        }, function (error) {
            _this.alert.error(error.error.details, true);
        });
    };
    UserAccountComponent.prototype.blur = function () {
        this.id = 0;
    };
    UserAccountComponent.prototype.focus = function (id) {
        this.id = id;
        return true;
    };
    UserAccountComponent.prototype.validateContact = function () {
        var _this = this;
        this.user.email = this.user.email.trim();
        if (this.user.email && this.rightlineListOptions &&
            this.rightlineListOptions.filter(function (x) { return x.contactemail === _this.user.email; }).length !== 1) {
            this.user.email = "";
        }
    };
    UserAccountComponent.prototype.contactSelected = function (option) {
        this.user.email = option.value;
        var user = null;
        if (option.value) {
            user = this.rightlineListOptions.find(function (x) { return x.contactemail === option.value; });
            this.user.firstName = option.value;
            this.user.is_deleted = user.is_deleted;
            this.user.rightline_id = user.rightline_id;
            this.user.firstName = user.contact_name;
        }
    };
    UserAccountComponent.prototype.FilterContact = function (value) {
        value = value.trim();
        this.rightlineListOptions = this.rightlineUserList.filter(function (x) {
            return x.contact_name.toLocaleLowerCase().includes(value.toLocaleLowerCase()) ||
                x.contactemail.toLocaleLowerCase().includes(value.toLocaleLowerCase());
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_1__["UserModel"])
    ], UserAccountComponent.prototype, "user", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserAccountComponent.prototype, "closes", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserAccountComponent.prototype, "addNewUser", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserAccountComponent.prototype, "isProfile", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserAccountComponent.prototype, "rightlineUserList", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserAccountComponent.prototype, "rightlineListOptions", void 0);
    UserAccountComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-user-account",
            template: __webpack_require__(/*! ./user-account.component.html */ "./src/app/dashboard/users/user-account/user-account.component.html"),
            styles: [__webpack_require__(/*! ./user-account.component.css */ "./src/app/dashboard/users/user-account/user-account.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_dashboard_service_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"], _shared_alert_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"], _service_project_service__WEBPACK_IMPORTED_MODULE_6__["ProjectService"]])
    ], UserAccountComponent);
    return UserAccountComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/dashboard/users/users-dashboard/users-dashboard.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC91c2Vycy91c2Vycy1kYXNoYm9hcmQvdXNlcnMtZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/dashboard/users/users-dashboard/users-dashboard.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"mydashboard\" class=\"dashboard-section\">\n\n  <nav class=\"navbar-bottam\">\n    <div class=\"container-fluid d-flex justify-content-between\">\n      <div class=\"position-relative\">\n        <div (click)=\"addNewUser()\" class=\"d-inline-block cursor\">\n          <button mat-raised-button color=\"primary\" class=\"btn-icon ml-0 mr-3\">\n            <img src=\"../assets/images/add-user-w.png\" class=\"icon\"> <span class=\"text\">USER</span></button>\n        </div>\n      </div>\n      <div class=\"input-group search-btn space-left \">\n        <div style=\"position:relative;\">\n          <input [(ngModel)]=\"filters.searchText\" maxlength=\"250\"\n            (keyup)=\"$event.code === 'Enter' && filters.searchText && search()\" type=\"text\"\n            class=\"form-control own-input-outline input-search light-black font-15\" placeholder=\"Search\">\n        </div>\n        <div class=\"input-group-btn search-icon\" [ngClass]=\"{ 'notactive' : !filters.searchText  }\">\n          <button *ngIf=\"!isTestSearchActive\" class=\"btn btn-default \" [disabled]=\"!filters.searchText\"\n            (click)=\"search()\" type=\"button\">\n            <img src=\"../assets/images/search.png\" class=\"search-icon-img\">\n          </button>\n          <button *ngIf=\"isTestSearchActive\" class=\"btn btn-default\" (click)=\"(filters.searchText = ''); search()\"\n            type=\"button\">\n            <img src=\"../assets/images/search-close-icon.png\" class=\"search-icon-img\">\n          </button>\n        </div>\n      </div>\n    </div>\n  </nav>\n\n  <div class=\"container-fluid own-dashboard-section form-own\">\n    <section class=\"wrapper-section\">\n      <div class=\"data-border-md\">\n        <div class=\"date-selection-2 filter-date-section \"\n          [ngClass]=\"{ 'error': filters.type && filters.type.length == 0 }\">\n          <div class=\"filter-p\">\n            <span> Filter by Type</span>\n          </div>\n          <mat-form-field floatLabel=\"always\" class=\"custom-txt-select-wrapper\">\n            <div class=\"custom-txt-select\">Showing {{filters.type.length}} of {{userTypes.length}}</div>\n            <mat-select #firstSelect multiple class=\"own-select\">\n              <mat-option class=\"own-select-option remove-default-checkbox\" *ngFor=\"let item of userTypes\"\n                [value]=\"item.value\">\n                <mat-checkbox (click)=\"setFiltersValue()\" [(ngModel)]=\"item.selected\" class=\"own-select-checkbox\">\n                  {{ item.display }}\n                </mat-checkbox>\n              </mat-option>\n              <mat-option class=\"done-btn own-select-option\">\n                <div (click)=\"closeDD()\" class=\"text-color\">\n                  DONE\n                </div>\n              </mat-option>\n            </mat-select>\n            <span class=\"mat-error\" *ngIf=\"filters.type.length == 0\">\n              *Select at least one option\n            </span>\n          </mat-form-field>\n        </div>\n      </div>\n      <div class=\"data-border-md\">\n        <div class=\"date-selection-2 filter-date-section\"\n          [ngClass]=\"{ 'error': filters.status && filters.status.length == 0 }\">\n          <div class=\"filter-p\">\n            <span> Filter by Status</span>\n          </div>\n          <mat-form-field floatLabel=\"always\" class=\"custom-txt-select-wrapper\">\n            <div class=\"custom-txt-select\">Showing {{filters.status.length}} of {{usersStatus.length}}</div>\n            <mat-select #secondSelect multiple class=\"own-select\">\n              <mat-option class=\"own-select-option remove-default-checkbox\" *ngFor=\"let item of usersStatus\"\n                selected=\"true\" [value]=\"item.value\">\n                <mat-checkbox (click)=\"setFiltersValue()\" [(ngModel)]=\"item.selected\" class=\"own-select-checkbox\">\n                  {{ item.display }}\n                </mat-checkbox>\n              </mat-option>\n              <mat-option class=\"done-btn own-select-option\">\n                <div (click)=\"closeDD()\" class=\"text-color\">\n                  DONE\n                </div>\n              </mat-option>\n            </mat-select>\n            <span class=\"mat-error\" *ngIf=\"filters.status.length == 0\">\n              *Select at least one option\n            </span>\n          </mat-form-field>\n        </div>\n      </div>\n      <div *ngIf=\"!loadingFilters && !(filters.status.length === 2 && filters.type.length === 2 && !filters.searchText)\"\n        class=\"data-border-sm text-center mb-3\">\n        <div class=\"date-selection-2 d-flex justify-content-center cursor reset-btn\">\n          <button id=\"usersResetBTN\" type=\"button\" mat-raised-button color=\"\" (click)=\"reset()\"\n            class=\"w-100 print-submit-btn attachment-btn white-btn\">\n            <div class=\"filter-p filter-reset-btn\">\n              <div class=\"font-weight-500\">\n                RESET\n              </div>\n              <div class=\"font-weight-500\">\n                FILTERS\n              </div>\n            </div>\n          </button>\n        </div>\n      </div>\n    </section>\n    <section class=\"own-table-dashboard mb-3\" *ngIf=\"usersList && usersList.length > 0\">\n      <div class=\"table-responsive\">\n        <table class=\"table table-hover mb-0\">\n          <thead>\n            <tr>\n              <th [ngClass]=\"{'active': filters.sort == 'firstName' }\" scope=\"col\" (click)=\"sort('firstName')\">First\n                Name<i *ngIf=\"filters.sort == 'firstName'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'lastName' }\" scope=\"col\" (click)=\"sort('lastName')\">Last Name<i\n                  *ngIf=\"filters.sort == 'lastName'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'email' }\" scope=\"col\" (click)=\"sort('email')\">Email<i\n                  *ngIf=\"filters.sort == 'email'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'type' }\" scope=\"col\" (click)=\"sort('type')\">Type<i\n                  *ngIf=\"filters.sort == 'type'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'status' }\" scope=\"col\" (click)=\"sort('status')\">Status<i\n                  *ngIf=\"filters.sort == 'status'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n              <th [ngClass]=\"{'active': filters.sort == 'last_login' }\" scope=\"col\" (click)=\"sort('last_login')\">Last\n                Login<i *ngIf=\"filters.sort == 'last_login'\" class='fas icon-space'\n                  [ngClass]=\"{'fa-arrow-up': filters.order == 'asc', 'fa-arrow-down': filters.order == 'desc'}\"></i>\n              </th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let user of usersList\" (click)=\"update(user)\">\n              <td>{{user.firstName}}</td>\n              <td><span *ngIf=\"user.lastName\">{{user.lastName}}</span></td>\n              <td>{{user.email}}</td>\n              <td>{{user.type}}</td>\n              <td>{{user.status}}</td>\n              <td><span *ngIf=\"user.last_login\">\n                  {{user.last_login | date : 'MM/dd/yy'}} at {{user.last_login | date : 'shortTime'}}\n                </span>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n      <div *ngIf=\"totalUsers >= 10\" class=\"d-flex flex-wrap align-items-center justify-content-end\">\n        <div class=\"mat-page-label-own font-12 light-black\">Rows per page : </div>\n        <mat-paginator class=\"own-pagination remove-pagination-label hide-underline text-center\"\n          (page)=\"pageChanged($event)\" [length]=\"totalUsers\" [pageSize]=\"filters.limit\" [pageIndex]=\"pageIndex\"\n          [pageSizeOptions]=\"[5, 10, 20, 30, 50, 70, 100]\">\n        </mat-paginator>\n      </div>\n    </section>\n    <app-welcome-page [hasFilters]=\"!(filters.status.length === 2 && filters.type.length === 2)\"\n      *ngIf=\"!(usersList && usersList.length > 0) && !loading\" (clear)=\"reset()\"></app-welcome-page>\n    <app-spinner *ngIf=\"loading\"></app-spinner>\n  </div>\n  <app-user-account [user]=\"user\" *ngIf=\"displayForm\" [isProfile]=\"isProfile\" [rightlineUserList] =\"rightlineUserList\" [rightlineListOptions]=\"rightlineListOptions\" (closes)=\"ModelClosed($event)\" (addNewUser)=\"addNewUserupdate($event)\">\n  </app-user-account>\n</section>"

/***/ }),

/***/ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/dashboard/users/users-dashboard/users-dashboard.component.ts ***!
  \******************************************************************************/
/*! exports provided: UsersDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersDashboardComponent", function() { return UsersDashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_users_filter_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_models/users-filter.model */ "./src/app/_models/users-filter.model.ts");
/* harmony import */ var src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_models/user-list.model */ "./src/app/_models/user-list.model.ts");
/* harmony import */ var src_app_dashboard_service_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/dashboard/_service/users.service */ "./src/app/dashboard/_service/users.service.ts");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_project_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../_service/project.service */ "./src/app/dashboard/_service/project.service.ts");








var UsersDashboardComponent = /** @class */ (function () {
    function UsersDashboardComponent(service, activeRoute, prodservice) {
        this.service = service;
        this.activeRoute = activeRoute;
        this.prodservice = prodservice;
        this.usersList = [];
        this.displayForm = false;
        this.isProfile = false;
        this.user = new src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_3__["UserModel"]();
        this.pageIndex = 0;
        this.loading = true;
        this.isTestSearchActive = false;
        this.rightlineUserList = [];
        this.loadContacts();
    }
    UsersDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setFilters();
        this.activeRoute.params.subscribe(function (params) {
            if (params) {
                _this.user = jwt_decode__WEBPACK_IMPORTED_MODULE_5___default()(params.token);
                console.log(_this.user);
                if (_this.user.email) {
                    _this.user.password = "";
                    _this.displayForm = true;
                    _this.openModel();
                }
            }
        });
    };
    UsersDashboardComponent.prototype.loadContacts = function () {
        var _this = this;
        this.prodservice.GetRightLineUserList().subscribe(function (res) {
            _this.rightlineUserList = res.data;
            _this.rightlineListOptions = res.data.filter(function (x) { return true; });
            debugger;
        });
    };
    UsersDashboardComponent.prototype.setFilters = function () {
        this.filters = new src_app_models_users_filter_model__WEBPACK_IMPORTED_MODULE_2__["UsersFilterModel"]();
        this.userTypes = [
            { selected: true, value: "Standard", display: "Standard" },
            { selected: true, value: "Business Affairs", display: "Business Affairs" }
        ];
        this.usersStatus = [
            { selected: true, value: "Active", display: "Active" },
            { selected: true, value: "Inactive", display: "Inactive" }
        ];
        this.filters.sort = "firstName";
        this.setFiltersValue();
    };
    UsersDashboardComponent.prototype.loadUsers = function () {
        var _this = this;
        if (this.filters.status &&
            this.filters.type &&
            this.filters.status.length !== 0 &&
            this.filters.type.length !== 0) {
            this.usersList = [];
            this.totalUsers = 0;
            this.loading = true;
            if (this.subscription) {
                this.subscription.unsubscribe();
            }
            this.subscription = this.service.getUsers(this.filters).subscribe(function (res) {
                _this.totalUsers = res.data.totalUsers;
                _this.usersList = res.data.user;
                _this.loading = false;
            }, function (error) {
                _this.loading = false;
            });
        }
    };
    UsersDashboardComponent.prototype.sort = function (key) {
        if (this.filters.sort === key) {
            this.filters.order = this.filters.order === "desc" ? "asc" : "desc";
        }
        else {
            this.filters.order = "desc";
        }
        this.filters.sort = key;
        this.loadUsers();
    };
    UsersDashboardComponent.prototype.pageChanged = function (event) {
        this.filters.limit = event.pageSize;
        this.filters.offset = event.pageIndex * event.pageSize;
        this.pageIndex = event.pageIndex;
        this.loadUsers();
    };
    UsersDashboardComponent.prototype.search = function () {
        // this.filters.limit = 10;
        this.filters.offset = 0;
        this.pageIndex = 0;
        if (this.filters.searchText) {
            this.isTestSearchActive = true;
        }
        else {
            this.isTestSearchActive = false;
        }
        this.loadUsers();
    };
    UsersDashboardComponent.prototype.setFiltersValue = function () {
        var _this = this;
        this.loadingFilters = true;
        setTimeout(function () {
            _this.filters.status = _this.usersStatus
                .filter(function (x) { return x.selected; })
                .map(function (x) { return x.value; });
            _this.filters.type = _this.userTypes
                .filter(function (x) { return x.selected; })
                .map(function (x) { return x.value; });
            _this.loadingFilters = false;
            _this.filter();
        }, 300);
    };
    UsersDashboardComponent.prototype.reset = function () {
        this.isTestSearchActive = false;
        this.setFilters();
    };
    UsersDashboardComponent.prototype.filter = function () {
        this.search();
    };
    UsersDashboardComponent.prototype.update = function (user) {
        this.user = Object.assign({}, user);
        this.user.password = "";
        this.displayForm = true;
        this.openModel();
    };
    UsersDashboardComponent.prototype.openModel = function () {
        setTimeout(function () {
            $("#userModal").modal("show");
        }, 500);
    };
    UsersDashboardComponent.prototype.ModelClosed = function (val) {
        // $("#userModal").modal("hide");
        this.displayForm = false;
        if (val) {
            this.search();
        }
    };
    UsersDashboardComponent.prototype.addNewUser = function () {
        this.user = new src_app_models_user_list_model__WEBPACK_IMPORTED_MODULE_3__["UserModel"]();
        this.user.password = "";
        this.displayForm = true;
        this.openModel();
    };
    UsersDashboardComponent.prototype.closeDD = function () {
        this.firstSelect.close();
        this.secondSelect.close();
    };
    UsersDashboardComponent.prototype.addNewUserupdate = function (event) {
        debugger;
        this.loadContacts();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("firstSelect"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UsersDashboardComponent.prototype, "firstSelect", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("secondSelect"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UsersDashboardComponent.prototype, "secondSelect", void 0);
    UsersDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-users-dashboard",
            template: __webpack_require__(/*! ./users-dashboard.component.html */ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./users-dashboard.component.css */ "./src/app/dashboard/users/users-dashboard/users-dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_dashboard_service_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _service_project_service__WEBPACK_IMPORTED_MODULE_7__["ProjectService"]])
    ], UsersDashboardComponent);
    return UsersDashboardComponent;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map