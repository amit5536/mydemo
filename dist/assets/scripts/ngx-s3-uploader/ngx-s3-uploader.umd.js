(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('rxjs/Observable'), require('aws-sdk/index')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common', 'rxjs/Observable', 'aws-sdk/index'], factory) :
	(factory((global['ngx-s3-uploader'] = {}),global._angular_core,global._angular_common,global.rxjs_Observable,global.awsSdk_index));
}(this, (function (exports,_angular_core,_angular_common,rxjs_Observable,awsSdk_index) { 'use strict';

var CONFIG = new _angular_core.InjectionToken('config');
var S3UploaderService = (function () {
    /**
     * @param {?} s3UploaderConfig
     */
    function S3UploaderService(s3UploaderConfig) {
        this.s3UploaderConfig = s3UploaderConfig;
        var credentials = {
            accessKeyId: s3UploaderConfig.credentials.accessKeyId,
            secretAccessKey: s3UploaderConfig.credentials.secretAccessKey,
        };
        // if the identity pool id  is populated I use the Cognito credentials to authenticate
        if (s3UploaderConfig.credentials.identityPoolId) {
            credentials = new awsSdk_index.CognitoIdentityCredentials({
                IdentityPoolId: s3UploaderConfig.credentials.identityPoolId,
                Logins: this.s3UploaderConfig.credentials.logins || {},
            });
        }
        // if role is populated I use the Web Federated Identity to authenticate
        if (s3UploaderConfig.credentials.roleArn) {
            credentials = new awsSdk_index.WebIdentityCredentials({
                RoleArn: s3UploaderConfig.credentials.roleArn,
                RoleSessionName: s3UploaderConfig.credentials.roleName,
                ProviderId: this.s3UploaderConfig.credentials.providerId,
                WebIdentityToken: this.s3UploaderConfig.credentials.token
            });
        }
        awsSdk_index.config.region = s3UploaderConfig.region;
        awsSdk_index.config.credentials = credentials;
        this.client = new awsSdk_index.S3();
    }
    /**
     * @param {?} providerName
     * @param {?} token
     * @return {?}
     */
    S3UploaderService.prototype.authenticate = function (providerName, token) {
        if (awsSdk_index.config.credentials instanceof awsSdk_index.CognitoIdentityCredentials) {
            awsSdk_index.config.credentials['params']['Logins'] = {};
            awsSdk_index.config.credentials['params']['Logins'][providerName] = token;
            // Expire credentials to refresh them on the next request.
            awsSdk_index.config.credentials['expired'] = true;
        }
    };
    /**
     * @param {?} file
     * @param {?=} acl
     * @param {?=} bucket
     * @return {?}
     */
    S3UploaderService.prototype.upload = function (file, acl, bucket, filename) {
        var _this = this;
        if (acl === void 0) { acl = 'public-read'; }
        return rxjs_Observable.Observable.create(function (observer) {
            _this.client.upload({ Key: acl, Body: file, ACL: null, ContentType: file.type, Bucket: bucket || _this.s3UploaderConfig.bucket }, function (error, data) {
                if (error) {
                    return observer.error(error);
                }
                observer.next(data);
                observer.complete();
            });
        });
    };
    return S3UploaderService;
}());
S3UploaderService.decorators = [
    { type: _angular_core.Injectable },
];
/**
 * @nocollapse
 */
S3UploaderService.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: _angular_core.Inject, args: [CONFIG,] },] },
]; };

var S3UploaderComponent = (function () {
    /**
     * @param {?} s3UploaderService
     */
    function S3UploaderComponent(s3UploaderService) {
        this.s3UploaderService = s3UploaderService;
        this.success = new _angular_core.EventEmitter();
        this.error = new _angular_core.EventEmitter();
    }
    /**
     * @return {?}
     */
    S3UploaderComponent.prototype.onClick = function () {
        var /** @type {?} */ event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    /**
     * @return {?}
     */
    S3UploaderComponent.prototype.onChange = function () {
        var _this = this;
        var /** @type {?} */ file = this.fileInput.nativeElement.files[0];
        this.s3UploaderService.upload(file)
            .subscribe(function (data) {
            _this.success.next(data);
        }, function (error) {
            _this.error.next(error);
        });
    };
    return S3UploaderComponent;
}());
S3UploaderComponent.decorators = [
    { type: _angular_core.Component, args: [{
                selector: 's3-uploader',
                template: "\n        <input #fileInput style='display: none;' type='file' (change)='onChange()'>\n        <ng-content></ng-content>\n    ",
            },] },
];
/**
 * @nocollapse
 */
S3UploaderComponent.ctorParameters = function () { return [
    { type: S3UploaderService, },
]; };
S3UploaderComponent.propDecorators = {
    'success': [{ type: _angular_core.Output },],
    'error': [{ type: _angular_core.Output },],
    'fileInput': [{ type: _angular_core.ViewChild, args: ['fileInput',] },],
    'onClick': [{ type: _angular_core.HostListener, args: ['click',] },],
};

var S3UploaderModule = (function () {
    function S3UploaderModule() {
    }
    /**
     * @param {?} config
     * @return {?}
     */
    S3UploaderModule.forRoot = function (config$$1) {
        return {
            ngModule: S3UploaderModule,
            providers: [
                { provide: CONFIG, useValue: config$$1 },
                S3UploaderService
            ]
        };
    };
    return S3UploaderModule;
}());
S3UploaderModule.decorators = [
    { type: _angular_core.NgModule, args: [{
                imports: [
                    _angular_common.CommonModule
                ],
                declarations: [
                    S3UploaderComponent,
                ],
                exports: [
                    S3UploaderComponent,
                ]
            },] },
];
/**
 * @nocollapse
 */
S3UploaderModule.ctorParameters = function () { return []; };

exports.S3UploaderModule = S3UploaderModule;
exports.S3UploaderComponent = S3UploaderComponent;
exports.CONFIG = CONFIG;
exports.S3UploaderService = S3UploaderService;

Object.defineProperty(exports, '__esModule', { value: true });

})));
