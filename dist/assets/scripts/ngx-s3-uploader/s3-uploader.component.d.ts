import { ElementRef, EventEmitter } from '@angular/core';
import { S3UploaderService } from './s3-uploader.service';
export declare class S3UploaderComponent {
    private s3UploaderService;
    success: EventEmitter<any>;
    error: EventEmitter<any>;
    fileInput: ElementRef;
    constructor(s3UploaderService: S3UploaderService);
    onClick(): void;
    onChange(): void;
}
