$("body").on("click", ".dropdown", function() {
  event.stopPropagation();
  if (!$(this).hasClass("active")) {
    $(".dropdown").each(function() {
      $(this).removeClass("active");
    });
    $(this).addClass("active");
  } else {
    $(".dropdown").each(function() {
      $(this).removeClass("active");
    });
  }
});
$(document).on("click", ".navbar-toggle", function() {
  event.preventDefault();
  if ($(this).hasClass("collapsed")) {
    $(this).removeClass("collapsed");
    $(".mobile-header-sub-menu").addClass("show");
  } else {
    $(this).addClass("collapsed");
    $(".mobile-header-sub-menu").removeClass("show");
  }
});

$(document).on("click", function() {
  let a = event.target;
  var ownLength = $(a).parents(".dropdown").length;
  let length = $(a).hasClass(".dropdown");
  if (!length || ownLength < 1) {
    setTimeout(() => {
      $(".dropdown").each(function() {
        $(this).removeClass("active");
      });
    }, 200);
  }
});

$("body").on("click", ".dropdown-items li", function() {
  event.stopPropagation();
  $(".dropdown").each(function() {
    $(this).removeClass("active");
  });
  $(this).addClass("active");
});

$("body").on("focus", ".form-own .mat-form-field-infix input", function() {
  $(this)
    .parents(".form-group")
    .addClass("active");
});

$("body").on("blur", ".form-own .mat-form-field-infix input", function() {
  $(this)
    .parents(".form-group")
    .removeClass("active");
});

$("body").on("click", ".dropdown-js .dropdown-items li", function() {
  event.stopPropagation();
  setTimeout(() => {
    $(".dropdown").each(function() {
      $(this).removeClass("active");
    });
  }, 200);
});
$("body").on("click", ".account-dropdown .dropdown-items li", function() {
  event.stopPropagation();
  setTimeout(() => {
    $(".dropdown").each(function() {
      $(this).removeClass("active");
    });
  }, 600);
});

function closeDropdown() {
  event.stopPropagation();
  $(".dropdown").each(function() {
    $(this).removeClass("active");
  });
}

$("body").on("click", ".right-sidebar-icon", function() {
  $(".sidebar-menu").toggleClass("sidebar-close");
  $(".dashboard-section").toggleClass("dashboard-close");
  $(".main-section").toggleClass("active");
});

$("body").on("click", ".own-table-dashboard", function() {
  $("#userModal").modal("show");
});

$("body").on("click", ".confirmation-modal .cancel-btn", function() {
  $("#confirmationModal").modal("hide");
});

function isNumberKey(evt) {
  var charCode = evt.which ? evt.which : event.keyCode;
  // if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))) for decimal
  if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
  return true;
}
